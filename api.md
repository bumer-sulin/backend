## Api routes

`api/settings` - настройки \
`api/categories` - категории/рубрики \
`api/articles` - статьи \
`api/articles?type=main` - на слайдер только главные новости \
`api/articles?type=popular` - популярные \
`api/articles?type=slider` - на слайдер только изображения \
`api/socials` - социальные сети \
`api/tags` - тэги \
`api/sections` - секции, выбираются категории и 4 новости/статьи для неё \
`api/subscribe` - подписаться
