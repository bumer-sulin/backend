<?php

// if (!function_exists('name_of_function')) {
//     function name_of_function()
//     {
//         return 'something';
//     }
// }

if (! function_exists('routeMeta')) {
    /**
     * Generate the URL to a named route.
     *
     *
     * routeMeta('index.item', $oArticle)
     *
     * @param  string  $name
     * @param  object  $object
     * @param  string  $url
     * @param  array   $parameters
     * @param  bool    $absolute
     * @return string
     */
    function routeMeta($name, $object = null, $url = 'name', $parameters = [], $absolute = true)
    {
        if (!is_null($object)) {
            $parameters['id'] = $object->id;
            if (!is_null($url)) {
                $parameters['url'] = $object->{$url};
            }
        }
        return app('url')->route($name, $parameters, $absolute);
    }
}

if (! function_exists('routeMetaCategory')) {
    /**
     * Generate the URL to a named route.
     *
     *
     * routeMeta('index.item', $oArticle)
     *
     * @param  string  $name
     * @param  object  $object
     * @param  array   $parameters
     * @param  bool    $absolute
     * @return string
     */
    function routeMetaCategory($name, $object = null, $parameters = [], $absolute = true)
    {
        if (!is_null($object)) {
            $model = $object->getTable();
            /**
             * @see \App\Http\Composers\AllComposer::__construct 220
             */
            $cache = \Illuminate\Support\Facades\Cache::get('tables_with_single_names');
            if (!is_null($cache) && isset($cache[$model])) {
                $parameters[$cache[$model]] = $object->id;
            }
        }
        return app('url')->route($name, $parameters, $absolute);
    }
}


if (! function_exists('imagePath')) {

    function imagePath()
    {
        return (new \App\Services\Image\ImagePath());
    }
}

if (! function_exists('cmfData')) {

    function cmfData()
    {
        return (new \App\Services\CmfDataService());
    }
}

if (! function_exists('filePath')) {

    function filePath()
    {
        return (new \App\Services\File\FilePath());
    }
}

if (! function_exists('systemUsage')) {

    function systemUsage()
    {
        return (new \App\Services\System());
    }
}

if (! function_exists('layoutService')) {

    function layoutService()
    {
        return (new \App\Services\LayoutService());
    }
}

if (!function_exists('responseCommon')) {

    function responseCommon()
    {
        return (new \App\Services\ResponseCommon\ResponseCommonHelpers());
    }
}

if (!function_exists('frontendCommon')) {

    function frontendCommon()
    {
        return (new \App\Services\FrontendCommon());
    }
}
