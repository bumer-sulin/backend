<?php

return [

    /*
    | Хранить пользователя в кэше
    |
    */
    'cache' => [
        'member' => true,
        'script' => 'd36b3ec69850ee3ade2d',
    ],

    /*
    | Отображать сообщения ошибок в 500 view
    |
    */
    'error' => [
        'show_message' => false,
    ],


    /*
    |--------------------------------------------------------------------------
    | Roles for project
    |--------------------------------------------------------------------------
    |
    | Usage for seeding column roles in /database/seeds/...
    |
    */
    'roles' => [
        [
            'slug' => 'admin',
            'name' => 'Администратор',
        ],
        [
            'slug' => 'user',
            'name' => 'Пользователь',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Site Layout
    |--------------------------------------------------------------------------
    | app admin
    |
    */

    'layout' => 'admin',
    'theme' => 'admin',
    'admin_prefix' => env('ADMIN_PREFIX', ''),
    'production_url' => env('APP_PRODUCTION_URL', env('APP_URL', '')),

    /*
    |--------------------------------------------------------------------------
    | Меню для сайдбара у парнера и у админа
    | Меню кэшируется
    |--------------------------------------------------------------------------
    */
    'menu' => [
        'partner' => [
            'dashboard' => [
                'title' => 'Главная',
                'iconCls' => 'icon-puzzle',
            ],
        ],
        'admin' => [
            '' => [
                'title' => 'Главная',
                'iconCls' => 'icon-puzzle',
            ],
            'article' => [
                'title' => 'Статьи',
                'iconCls' => 'icon-note',
            ],
            'user' => [
                'title' => 'Пользователи',
                'iconCls' => 'icon-people',
            ],
            'promotion' => [
                'title' => 'Реклама',
                'iconCls' => 'icon-speech',
            ],
            'subscription' => [
                'title' => 'Подписки',
                'iconCls' => 'icon-envelope',
//                'badge' => [
//                    'title' => 'DEV',
//                    'type' => 'danger',
//                ],
            ],
            'other' => [
                'title' => 'Остальное',
                'iconCls' => 'icon-layers',
                'sub' => [
                    'category' => [
                        'title' => 'Категории',
                        'iconCls' => 'icon-list',
                    ],
                    'option' => [
                        'title' => 'Опции',
                        'iconCls' => 'icon-book-open',
                    ],
//                    'type' => [
//                        'title' => 'Типы',
//                        'iconCls' => 'icon-grid',
//                    ],
//                    'admin/brand' => [
//                        'title' => 'Бренды',
//                        'iconCls' => 'icon-tag',
//                    ],
                    'tag' => [
                        'title' => 'Тэги',
                        'iconCls' => 'icon-tag',
                    ],
                    'social' => [
                        'title' => 'Соц.сети',
                        'iconCls' => 'icon-bubble',
                    ],
                    'comment' => [
                        'title' => 'Комментарии',
                        'iconCls' => 'icon-bubbles',
                    ],
                ],
            ],
            'setting' => [
                'title' => 'Настройки',
                'iconCls' => 'icon-settings',
                'divider' => 'Дополнительное',
            ],
//            'setting' => [
//                'title' => 'Настройки',
//                'iconCls' => 'icon-settings',
//                'sub' => [
//                    'setting' => [
//                        'title' => 'Данные',
//                        'iconCls' => 'icon-screen-desktop',
//                    ],
////                    'admin/setting?key=seo.'            => [ 'title' => 'Seo',          'iconCls' => 'icon-globe'     ],
////                    'admin/setting?key=email.'            => [ 'title' => 'Email',          'iconCls' => 'icon-envelope'     ],
////                    'admin/setting?key=social.'            => [ 'title' => 'Social',          'iconCls' => 'icon-speech'     ],
////                    'admin/setting?key=location.'            => [ 'title' => 'Location',          'iconCls' => 'icon-location-pin'     ],
////                    'admin/setting?key=contacts.'            => [ 'title' => 'Contacts',          'iconCls' => 'icon-notebook'     ],
//                ],
//                'divider' => 'Дополнительное',
//            ],
//            'info' => [
//                'title' => 'Информация',
//                'iconCls' => 'icon-screen-desktop',
//            ],
            //'admin/invoice'            => [ 'title' => 'Счета',          'iconCls' => 'icon-share-alt'     ],
            //'admin/product'            => [ 'title' => 'Продукты',           'iconCls' => 'icon-ghost'     ],
        ],
    ],

    'options' => [
        'app' => [
            'title' => null,
            'title_short' => null,
            'description' => null,
            'keywords' => null,
            'favicon' => [
                'main' => '/img/logo.png',
            ],
            'powered' => null,
        ],
//        'contacts' => [
//            'phone' => null,
//            'address' => null,
//            'email' => null,
//            'city' => null,
//        ],
    ],

];
