<?php

use Illuminate\Database\Seeder;
use App\Models\Setting;
use Illuminate\Support\Facades\Config;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create([
            'key' => 'app.title',
            'value' => Config::get('app.name'),
            'title' => 'Заголовок сайта',
            'type' => 1,
            'status' => 1,
        ]);
        Setting::create([
            'key' => 'app.title_short',
            'value' => Config::get('app.name_short'),
            'title' => 'Краткий заголовок сайта',
            'type' => 1,
            'status' => 1,
        ]);
        Setting::create([
            'key' => 'app.favicon',
            'title' => 'Иконка сайта',
            'type' => 4,
            'status' => 1,
        ]);
        Setting::create([
            'key' => 'app.keywords',
            'value' => 'keywords',
            'title' => 'Ключевые слова главной страницы',
            'type' => 2,
            'status' => 1,
        ]);
        Setting::create([
            'key' => 'app.description',
            'value' => 'Description',
            'title' => 'Описание сайта',
            'type' => 2,
            'status' => 1,
        ]);
        Setting::create([
            'key' => 'app.copyright',
            'value' => '© ООО МедиаХолдинг "Красный Бумер"',
            'title' => 'Права',
            'type' => 1,
            'status' => 1,
        ]);
        Setting::create([
            'key' => 'contacts.email',
            'value' => 'email@email.com',
            'title' => 'Email',
            'type' => 1,
            'status' => 1,
        ]);
        Setting::create([
            'key' => 'app.copyright_footer',
            'value' => 'Все права защищены и охраняются законом. © 2015-2019 Права на опубликованные материалы принадлежат учредителю – ООО "МедиаХолдинг "Красный Бумер". При использовании материалов сайта в электронных источниках информации активная гиперссылка на "Красный Бумер" обязательна. При использовании материалов сайта в бумажных источниках информации ссылка на газету "Красный Бумер" обязательна. За содержание рекламных материалов редакция ответственности не несёт. По всем вопросам обращаться в администрацию портала.',
            'title' => 'Права на основном сайте',
            'type' => 1,
            'status' => 1,
        ]);
        Setting::create([
            'key' => 'app.subscription',
            'value' => 'Будь посоном, подпишись',
            'title' => 'Подписаться на нашу рассылку',
            'type' => 1,
            'status' => 1,
        ]);
        Setting::create([
            'key' => 'app.banner',
            'value' => null,
            'title' => 'Баннер',
            'type' => 4,
            'status' => 1,
        ]);
    }
}
