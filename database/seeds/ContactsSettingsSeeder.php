<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Config;
use Cartalyst\Sentinel\Laravel\Facades\Activation;

class ContactsSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Setting::create([
            'key' => 'app.contacts',
            'value' => '<!--StartFragment--><span>Адрес: 346350, Ростовская область, Красносулинский район, г. Красный Сулин, улица Фурманова, д. 9.<span>&nbsp;</span></span><br><span>Телефоны:<span>&nbsp;</span></span><br><span>8-951-500-05-59,<span>&nbsp;</span></span><br><span>8 (86367) 5-27-57,</span><br><span>8-951-514-58-38.<span>&nbsp;</span></span><br><span>Электронный адрес:<span>&nbsp;</span></span><a href="mailto:rosss33@mail.ru">rosss33@mail.ru</a><!--EndFragment-->',
            'title' => 'Контакты в футере',
            'type' => 7,
            'status' => 1,
        ]);
    }
}
