<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Config;
use Cartalyst\Sentinel\Laravel\Facades\Activation;

class RoleDeveloperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('roles')->insert([
            'id' => 3,
            'slug' => 'developer',
            'name' => 'Разработчик',
            'permissions' => '',
        ]);
        $data = [
            'email' => 'dposkachei@gmail.com',
            'password' => 'U_z85cqt', // 1234567890
            'first_name' => 'Developer',
            'last_name' => 'Developer',
        ];
        $oUser = Sentinel::register($data);
        $slugRole = 'developer';
        $role = Sentinel::findRoleBySlug($slugRole);
        $role->users()->attach($oUser);

        $oActivation = Activation::create($oUser);
        $oActivation->code = 1234;
        $oActivation->save();
        Activation::complete($oUser, $oActivation->code);
    }
}
