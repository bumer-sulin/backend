<?php

use Illuminate\Database\Seeder;

use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Config;
use Cartalyst\Sentinel\Laravel\Facades\Activation;

class PhotoAuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oOption = \App\Models\Option::create([
            'title' => 'Автор фотографии',
            'description' => null,
            'key' => \App\Models\Option::KEY_PHOTO_AUTHOR,
            'type' => 1,
            'status' => 1,
        ]);
    }
}
