<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    use \App\Database\Seeds\CommonDatabaseSeeder;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->clear();
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        $this->call(OptionsParametersTableSeeder::class);
        $this->call(BumerSeeder::class);

        $this->call(SettingsTableSeeder::class);
        //$this->call(FakerSeeder::class);

    }
}
