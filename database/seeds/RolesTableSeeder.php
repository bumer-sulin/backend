<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('roles')->delete();

        $roles = Config::get('site.roles');
        $aRoles = [];
        $id = 1;
        foreach($roles as $key => $role) {
            if (isset($role['slug']) && !empty($role['slug']) && isset($role['name']) && !empty($role['name'])) {
                $aRoles[$key]['id']             = $id;
                $aRoles[$key]['slug']           = $role['slug'];
                $aRoles[$key]['name']           = $role['name'];
                $aRoles[$key]['permissions']    = '';
                $aRoles[$key]['created_at']     = Carbon::now()->format('Y-m-d H:i:s');
                $aRoles[$key]['updated_at']     = Carbon::now()->format('Y-m-d H:i:s');
                $id++;
            }
        }
        if (!empty($aRoles)) {
            \DB::table('roles')->insert($aRoles);
        }
    }
}
