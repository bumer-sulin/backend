<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use App\Models\Category;
use App\Models\Promotion;
use App\Models\Tag;
use App\Models\Type;

class BumerSeeder extends Seeder
{
    use \App\Database\Seeds\CommonDatabaseSeeder;

    /**
     * @var array
     */
    private $categories = [
        [
            'title' => 'Новости Красного Сулина',
        ], [
            'title' => 'Новости Дона',
        ], [
            'title' => 'Новости России и мира',
        ], [
            'title' => 'Происшествия',
        ], [
            'title' => 'Интервью',
        ], [
            'title' => 'Срочно',
        ], [
            'title' => 'Неофициально',
        ], [
            'title' => 'Эксклюзив',
        ],
    ];

    /**
     * @var array
     */
    private $promotional = [
        [
            'title' => 'Бегущая строка',
            'type' => 1,
            'options' => '{"text":["\u00abUber \u0422\u0430\u043a\u0441\u0438\u00bb \u0432 \u041a\u0440\u0430\u0441\u043d\u043e\u043c \u0421\u0443\u043b\u0438\u043d\u0435! \u0417\u0430\u043a\u0430\u0437 \u00ab\u042f\u043d\u0434\u0435\u043a\u0441 \u0422\u0430\u043a\u0441\u0438\u00bb \u043f\u043e \u0431\u0435\u0441\u043f\u043b\u0430\u0442\u043d\u043e\u043c\u0443 \u043d\u043e\u043c\u0435\u0440\u0443: 8-800-301-333-0","\u00ab\u042f\u043d\u0434\u0435\u043a\u0441 \u0422\u0430\u043a\u0441\u0438\u00bb \u0432 \u041a\u0440\u0430\u0441\u043d\u043e\u043c \u0421\u0443\u043b\u0438\u043d\u0435! \u0417\u0430\u043a\u0430\u0437 \u00ab\u042f\u043d\u0434\u0435\u043a\u0441 \u0422\u0430\u043a\u0441\u0438\u00bb \u043f\u043e \u0431\u0435\u0441\u043f\u043b\u0430\u0442\u043d\u043e\u043c\u0443 \u043d\u043e\u043c\u0435\u0440\u0443: 8-800-301-333-0","\u00abUber \u0422\u0430\u043a\u0441\u0438\u00bb \u0432 \u041a\u0440\u0430\u0441\u043d\u043e\u043c \u0421\u0443\u043b\u0438\u043d\u0435! \u0417\u0430\u043a\u0430\u0437 \u00ab\u042f\u043d\u0434\u0435\u043a\u0441 \u0422\u0430\u043a\u0441\u0438\u00bb \u043f\u043e \u0431\u0435\u0441\u043f\u043b\u0430\u0442\u043d\u043e\u043c\u0443 \u043d\u043e\u043c\u0435\u0440\u0443: 8-800-301-333-0","\u00ab\u042f\u043d\u0434\u0435\u043a\u0441 \u0422\u0430\u043a\u0441\u0438\u00bb \u0432 \u041a\u0440\u0430\u0441\u043d\u043e\u043c \u0421\u0443\u043b\u0438\u043d\u0435! \u0417\u0430\u043a\u0430\u0437 \u00ab\u042f\u043d\u0434\u0435\u043a\u0441 \u0422\u0430\u043a\u0441\u0438\u00bb \u043f\u043e \u0431\u0435\u0441\u043f\u043b\u0430\u0442\u043d\u043e\u043c\u0443 \u043d\u043e\u043c\u0435\u0440\u0443: 8-800-301-333-0"]}',
        ], [
            'title' => 'Баннер',
            'type' => 2,
            'link' => '/',
        ], [
            'title' => 'В сайдбаре',
            'type' => 3,
            'link' => '/',
        ], [
            'title' => 'Детальный просмотр',
            'type' => 4,
            'link' => '/',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->before(Category::class);
        $total = count($this->categories);
        foreach ($this->categories as $key => $category) {
            $this->createCategory($category, $key, $total);
        }

        $this->before(Promotion::class);
        foreach ($this->promotional as $key => $promotional) {
            $this->createPromotional($promotional);
        }
    }

    /**
     * @param $key
     * @param $total
     * @param $category
     */
    private function createCategory($category, $key, $total)
    {
        $oCategory = Category::create([
            'title' => $category['title'],
            'parent_id' => $category['parent_id'] ?? null,
            'priority' => $this->priority($key, $total),
            'color' => '#F13D37',
            'status' => 1,
        ]);
        if (isset($category['children'])) {
            $total = count($category['children']);
            foreach ($category['children'] as $key => $item) {
                $item['parent_id'] = $oCategory->id;
                $this->createCategory($item, $key, $total);
            }
        }
    }

    /**
     * @param $item
     */
    private function createPromotional($item)
    {
        $oItem = Promotion::create([
            'title' => $item['title'],
            'type' => $item['type'],
            'link' => isset($item['link']) ? url($item['link']) : null,
            'options' => isset($item['options']) ? json_decode($item['options'], true) : null,
        ]);
    }
}
