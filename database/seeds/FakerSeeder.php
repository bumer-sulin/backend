<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Type;
use Illuminate\Support\Arr;

class FakerSeeder extends Seeder
{
    use \App\Database\Seeds\CommonDatabaseSeeder;

    const ARTICLES = 100;

    private $user;
    private $oUsers;
    private $oTypes;
    private $oCategories;
    private $oTags;
    private $lastComment;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->command->getOutput()->text('delete images');
        \Illuminate\Support\Facades\File::deleteDirectory(public_path('images/article'));

        $this->command->getOutput()->text('user');
        $this->user = factory(\Cartalyst\Sentinel\Users\EloquentUser::class)->create();
        $this->user->roles()->attach(2);

        $this->command->getOutput()->text('users');
        $this->oUsers = factory(\Cartalyst\Sentinel\Users\EloquentUser::class, 50)->create()->each(function ($user) {
            $user->roles()->attach(2);
        });

        $this->command->getOutput()->text('types');
        $this->oTypes = factory(App\Models\Type::class, 5)->create();

        $this->command->getOutput()->text('categories');
        //$this->oCategories = factory(App\Models\Category::class, 30)->create();
        $this->oCategories = Category::all();

        $this->command->getOutput()->text('tags');
        $this->oTags = factory(App\Models\Tag::class, 25)->create();

        $this->command->getOutput()->text('socials');
        $oSocials = factory(App\Models\Social::class, 5)->create();

//        $this->command->getOutput()->text('options');
//        $oOptions = factory(App\Models\Option::class, 20)->create()->each(function ($option) {
//            if ($option->type === 2) {
//                for ($i = 0; $i < Arr::random([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 1)[0]; $i++) {
//                    factory(\App\Models\OptionParameter::class)->create([
//                        'option_id' => $option->id,
//                    ]);
//                }
//            }
//        });

        $this->command->getOutput()->text('articles');
        $this->command->getOutput()->progressStart(FakerSeeder::ARTICLES);

        factory(App\Models\Article::class, FakerSeeder::ARTICLES)->create([
            'creator_id' => $this->user->id,
        ])->each(function ($article) {
            $public = mt_rand(0, 1);
            $article->update([
                //'type_id' => Arr::random(array_keys((new \App\Models\Article())->getTypes()), 1)[0],
                'type_id' => 1,
                'category_id' => Arr::random($this->oCategories->pluck('id')->toArray(), 1)[0],
                'published_at' => $public ? now()->subDay() : null,
                'publication_at' => $public ? now()->subDay() : null,
                'status' => Arr::random([0, 1, 2], 1)[0],
            ]);
            if (mt_rand(0, 1)) {
                $count = mt_rand(1, 100);
                for ($i = 0; $i < $count; $i++) {
                    $byUser = mt_rand(0, 1);
                    $parent_id = null;
                    $reply_id = null;
                    if ($count > 25 && !is_null($this->lastComment) && mt_rand(0, 1)) {
                        $reply_id = $this->lastComment->id;
                        $parent_id = !is_null($this->lastComment->parent_id) ? $this->lastComment->parent_id : $this->lastComment->id;
                    }
                    $this->lastComment = factory(App\Models\Comment::class)->create([
                        'commentable_id' => $article->id,
                        'commentable_type' => \App\Models\Article::class,
                        'commented_id' => $byUser ? Arr::random($this->oUsers->pluck('id')->toArray(), 1)[0] : null,
                        'commented_type' => $byUser ? \App\Models\User::class : null,
                        'parent_id' => $parent_id,
                        'reply_id' => $reply_id,
                    ]);
                    if (mt_rand(0, 1)) {
                        $countLove = mt_rand(1, 10);
                        for ($j = 0; $j < $countLove; $j++) {
                            $oUser = \App\Models\User::find(Arr::random($this->oUsers->pluck('id')->toArray(), 1)[0]);

                            if (!$oUser->hasLoveComment($this->lastComment->id)) {
                                mt_rand(0, 1) ? $oUser->setLoveCommentLike($this->lastComment->id) : $oUser->setLoveCommentDislike($this->lastComment->id);
                            }
                        }
                    }
                }
                $article->statistic()->update([
                    'comments' => $article->comments()->count(),
                    'views' => mt_rand(1, 1000),
                ]);
            }
            foreach ($this->oTags as $oTag) {
                if (mt_rand(0, 1)) {
                    $article->tags()->attach($oTag->id);
                }
            }
            $this->command->getOutput()->progressAdvance();
        });
        $this->command->getOutput()->progressFinish();

        $this->command->getOutput()->text('subscription');
        $oSubscriptions = factory(App\Models\Subscription::class, 50)->create()->each(function ($subscription) {
            if ($subscription->type === 2) {
                foreach ($this->oCategories as $oCategory) {
                    if (mt_rand(0, 1)) {
                        $subscription->categories()->attach($oCategory->id);
                    }
                }
            }
        });

        $this->command->getOutput()->text('images');
        $this->command->getOutput()->progressStart(FakerSeeder::ARTICLES);
        foreach (\App\Models\Article::all() as $oArticle) {
            $this->image($oArticle, mt_rand(0, 9));
            $this->command->getOutput()->progressAdvance();
        }
        $this->command->getOutput()->progressFinish();

        $this->command->getOutput()->text('categories');
        $this->oCategories->each(function ($category) {
            if ($category->activeArticles()->count() > 4) {
                $category->update([
                    'status' => 2,
                ]);
            }
        });
    }

    /**
     * @param $oArticle
     * @param $randomKey
     */
    private function image($oArticle, $randomKey)
    {
        $oService = new \App\Services\Image\ImageService();

        $imageName = $this->images($randomKey, true);
        $imageFile = $this->images($randomKey);

        $imageName = $oService->upload($imageFile, 'article', $oArticle->id, (new \App\Cmf\Project\Article\ArticleBaseController())->getFilters(), $imageFile, $imageName);

        factory(App\Models\Image::class)->create([
            'type' => 'article',
            'imageable_id' => $oArticle->id,
            'imageable_type' => \App\Models\Article::class,
            'filename' => $imageName,
            'is_main' => 1,
        ]);
    }

    /**
     * @param $key
     * @param bool $name
     * @return mixed|string
     */
    private function images($key, $name = false)
    {
        $path = database_path('factories/images/');
        $files = [
            '1.jpg',
            '2.jpg',
            '3.jpg',
            '4.jpg',
            '5.jpg',
            '6.jpg',
            '7.jpg',
            '8.jpg',
            '9.jpg',
            '10.jpg',
        ];
        return !$name ? $path . $files[$key] : $files[$key];
    }
}
