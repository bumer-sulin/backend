<?php

use Illuminate\Database\Seeder;
use App\Models\Tag;
use App\Models\Option;
use App\Models\OptionParameter;

class OptionsParametersTableSeeder extends Seeder
{
    use \App\Database\Seeds\CommonDatabaseSeeder;


    private $default = [
        [
            'title' => 'Подпись автора',
            'description' => 'Автор статьи/Новости',
            'key' => 'avtor',
            'type' => 1,
            'status' => 1,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->before(Option::class);
        $this->before(OptionParameter::class);


        foreach($this->default as $key => $value) {
            $oOption = Option::create([
                'title' => $value['title'],
                'description' => isset($value['description']) ? $value['description'] : null,
                'key' => $value['key'],
                'type' => $value['type'],
                'status' => $value['status'],
            ]);
            if (isset($value['parameters'])) {
                $total = count($value['parameters']);
                foreach ($value['parameters'] as $sKey => $parameter) {
                    OptionParameter::create([
                        'option_id' => $oOption->id,
                        'quantity' => $parameter['quantity'],
                        'priority' => $this->priority($sKey, $total)
                    ]);
                }
            }
        }


        $this->after();
    }
}
