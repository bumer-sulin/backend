<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('type')->default(0);
            $table->unsignedInteger('user_id')->nullable()->default(null);
            $table->string('email');
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });

        Schema::create('subscription_category', function (Blueprint $table) {
            $table->unsignedInteger('subscription_id');
            $table->unsignedInteger('category_id');
        });
        Schema::table('subscription_category', function($table) {
            $table->foreign('subscription_id')->references('id')->on('subscriptions')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('subscriptions');
        Schema::dropIfExists('subscription_category');
        Schema::enableForeignKeyConstraints();
    }
}
