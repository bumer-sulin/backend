<?php
/** actuallymab | 12.06.2016 - 02:00 */

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('commentable_id')->nullable();
            $table->string('commentable_type')->nullable();
            $table->integer('commented_id')->nullable();
            $table->string('commented_type')->nullable();
            $table->longText('comment');
            $table->boolean('approved')->default(true);
            $table->integer('rate')->nullable();
            $table->integer('reply_id')->unsigned()->nullable()->default(null);
            $table->integer('parent_id')->unsigned()->nullable()->default(null);
            $table->timestamps();
        });
        Schema::table('comments', function($table) {
            $table->foreign('reply_id')->references('id')->on('comments')->onDelete('cascade');
        });
        Schema::table('comments', function($table) {
            $table->foreign('parent_id')->references('id')->on('comments')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('comments', function($table) {
            $table->dropForeign('comments_parent_id_foreign');
        });
        Schema::drop('comments');
    }
}
