<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('creator_id')->unsigned()->nullable()->default(null);
            $table->integer('remover_id')->unsigned()->nullable()->default(null);
            $table->string('name')->nullable()->default(null);
            $table->string('title');
            $table->text('description')->nullable()->default(null);
            $table->text('keywords')->nullable()->default(null);
            $table->integer('category_id')->unsigned()->nullable()->default(null);
            $table->integer('type_id')->unsigned()->nullable()->default(null);
            $table->longText('text')->nullable()->default(null);
            $table->timestamp('publication_at')->nullable()->default(null);
            $table->timestamp('published_at')->nullable()->default(null);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();

            $table->softDeletes();
        });
        Schema::table('articles', function($table) {
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('set null');
        });
        Schema::table('articles', function($table) {
            $table->foreign('remover_id')->references('id')->on('users')->onDelete('set null');
        });
        Schema::table('articles', function($table) {
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');
        });
        Schema::table('articles', function($table) {
            //$table->foreign('type_id')->references('id')->on('types')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop('articles');
        Schema::enableForeignKeyConstraints();
    }
}
