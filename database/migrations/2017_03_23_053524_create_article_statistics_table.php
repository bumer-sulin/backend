<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_statistics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('article_id')->unsigned();
            $table->integer('views')->unsigned()->default(0);
            $table->integer('downloads')->unsigned()->default(0);
            $table->integer('comments')->unsigned()->default(0);
            $table->timestamps();
        });
        Schema::table('article_statistics', function($table) {
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('article_statistics', function($table) {
            $table->dropForeign('article_statistics_article_id_foreign');
        });
        Schema::drop('article_statistics');
    }
}
