<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTypesAddIconColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('types', function (Blueprint $table) {
            $table->string('icon')->nullable()->default(null)->after('text');
            $table->string('color')->nullable()->default(null)->after('icon');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->string('icon')->nullable()->default(null)->after('text');
            $table->string('color')->nullable()->default(null)->after('icon');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('types', function (Blueprint $table) {
            $table->dropColumn('icon');
            $table->dropColumn('color');
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->dropColumn('icon');
            $table->dropColumn('color');
        });
    }
}
