<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleOptionValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article_option_values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('article_id')->unsigned();
            $table->integer('option_id')->unsigned();
            $table->integer('parameter_id')->unsigned()->nullable()->default(null);
            $table->string('value')->nullable()->default(null);
            $table->integer('priority')->default(0);
            $table->timestamps();
        });
        Schema::table('article_option_values', function($table) {
            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
        });
        Schema::table('article_option_values', function($table) {
            $table->foreign('option_id')->references('id')->on('options')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop('article_option_values');
        Schema::enableForeignKeyConstraints();
    }
}
