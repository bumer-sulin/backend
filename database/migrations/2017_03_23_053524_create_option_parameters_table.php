<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOptionParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('option_parameters', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('option_id')->unsigned();
            $table->string('quantity');
            $table->integer('priority')->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
        Schema::table('option_parameters', function($table) {
            $table->foreign('option_id')->references('id')->on('options')->onDelete('cascade');
        });
        Schema::table('article_option_values', function($table) {
            $table->foreign('parameter_id')->references('id')->on('option_parameters')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop('option_parameters');
        Schema::enableForeignKeyConstraints();
    }
}
