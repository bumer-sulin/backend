<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Image::class, function (Faker $faker) {
    return [
        'is_main' => $faker->randomElement([0]),
        'priority' => $faker->numberBetween(0, 100),
        'status' => $faker->randomElement([1]),
    ];
});
