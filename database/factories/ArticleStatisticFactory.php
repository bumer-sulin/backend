<?php

use Faker\Generator as Faker;

$factory->define(App\Models\ArticleStatistics::class, function (Faker $faker) {
    return [
        'views' => $faker->numberBetween(0, 1000),
        'downloads' => $faker->numberBetween(0, 1000),
        'comments' => $faker->numberBetween(0, 1000),
    ];
});
