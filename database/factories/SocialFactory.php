<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Social::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement(array_keys((new App\Models\Social())->getTypes())),
        'title' => mb_strtoupper($faker->word),
        'link' => $faker->url,
        'priority' => $faker->numberBetween(0, 1000),
        'status' => $faker->randomElement([0, 1]),
    ];
});
