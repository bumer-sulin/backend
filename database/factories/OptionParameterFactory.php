<?php

use Faker\Generator as Faker;

$factory->define(App\Models\OptionParameter::class, function (Faker $faker) {
    return [
        'quantity' => $faker->word,
        'priority' => $faker->numberBetween(0, 1000),
        'status' => $faker->randomElement([0, 1]),
    ];
});
