<?php

use Faker\Generator as Faker;

$factory->define(Cartalyst\Sentinel\Activations\EloquentActivation::class, function (Faker $faker) {
    return [
        'code' => $faker->unique()->numberBetween(0, 1000000),
        'completed' => $faker->randomElement([0, 1]),
    ];
});
