<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Option::class, function (Faker $faker) {
    $title = mb_strtoupper($faker->word);
    $type = $faker->randomElement(array_keys((new App\Models\Option())->getTypes()));
    return [
        'title' => $title,
        'description' => $faker->word,
        'key' => str_slug($title),
        'unit' => $faker->randomElement([null, 'шт.', 'кг.']),
        'type' => $type,
        'status' => $faker->randomElement([0, 1]),
    ];
});
