<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Subscription::class, function (Faker $faker) {
    return [
        'type' => $faker->randomElement(array_keys((new App\Models\Subscription())->getTypes())),
        'email' => $faker->email,
        'status' => $faker->randomElement([0, 1]),
    ];
});
