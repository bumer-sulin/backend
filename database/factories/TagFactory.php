<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Tag::class, function (Faker $faker) {
    $title = mb_strtoupper($faker->word);
    return [
        'title' => $title,
        'name' => str_slug($title),
        'color' => $faker->randomElement(['#F13D37', '#7FBFFB', '#7FB6B7', '#E66FC7', '#D24861', '#1DA25D', '#E8CA20']),
        'status' => $faker->randomElement([0, 1]),
    ];
});
