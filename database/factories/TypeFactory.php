<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Type::class, function (Faker $faker) {
    $title = mb_strtoupper($faker->word);
    return [
        'title' => $title,
        'name' => str_slug($title),
        'priority' => $faker->numberBetween(0, 1000),
        'status' => $faker->randomElement([0, 1]),
    ];
});
