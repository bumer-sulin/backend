<?php

use Faker\Generator as Faker;

$factory->define(\Cartalyst\Sentinel\Users\EloquentUser::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'first_name' => $faker->firstName,
        'status' => $faker->randomElement([0, 1]),
    ];
});
