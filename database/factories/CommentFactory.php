<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Comment::class, function (Faker $faker) {
    return [
        'comment' => $faker->realText(),
        'approved' => $faker->randomElement([0, 1]),
        'rate' => $faker->numberBetween(-100, 100),
    ];
});
