<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Article::class, function (Faker $faker) {
    $title = $faker->sentence;
    $public = $faker->randomElement([0, 1]);
    return [
        'name' => str_slug($title),
        'title' => $title,
        'description' => $faker->text(200),
        'text' => $faker->text(10000),
        'published_at' => $public ? $faker->randomElement([$faker->dateTimeBetween($startDate = '-1 year', $endDate = '+7 days')]) : null,
        'publication_at' => $public ? $faker->randomElement([$faker->dateTimeBetween($startDate = '-1 year', $endDate = '+7 days')]) : null,
        'status' => $public ? 1 : $faker->randomElement([1, 2]),
    ];
});
