@if(isset($metaController) && $metaController)
    <title>{{ MetaTag::get('title') }}</title>
    <meta name="description" content="{{ MetaTag::get('description') }}">
    <meta name="keywords" content="{{ MetaTag::get('keywords') }}">
    {!! MetaTag::tag('image') !!}
    {!! MetaTag::twitterCard() !!}
    {!! MetaTag::openGraph() !!}
@else
    <title>{{ $oComposerSite->app->title }}</title>
    <meta name="description" content="{{ $oComposerSite->app->description }}">
    <meta name="keywords" content="{{ $oComposerSite->app->keywords }}">
@endif
@if(config('app.env') === 'production')
    <link rel="icon" href="{{ asset('img/favicon.png') }}" type="image/png">
@else
    <link rel="icon" href="{{ asset('img/logo-development.png') }}" type="image/png">
@endif

