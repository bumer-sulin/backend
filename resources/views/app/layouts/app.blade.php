<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <meta name="theme-color" content="#000000">
    <meta name="api" content="{{ config('api.url') }}">
    <meta name="yandex-verification" content="8cc01de09c3a394c">
    {{--<link rel="manifest" href="{{ asset('manifest.json') }}">--}}
    <link rel="stylesheet" href="{{ asset('css/swiper.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    @include('app.components.meta')
    <link href="{{ frontendCommon()->asset('css/vendors~main.chunk.css') }}" rel="stylesheet">
    <link href="{{ frontendCommon()->asset('css/main.chunk.css') }}" rel="stylesheet">
    <script type="text/javascript" src="https://vk.com/js/api/openapi.js?160"></script>
    <link rel="stylesheet" type="text/css" href="https://ost1.gismeteo.ru/assets/flat-ui/legacy/css/informer.min.css">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-137261851-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-137261851-1');
    </script>
</head>
<body>
    {{--<div class="text-center">--}}
        {{--<a href="{{ url('/auth/google/redirect') }}" class="btn btn-primary">Login with Google</a>--}}
        {{--<a href="{{ url('/auth/vkontakte/redirect') }}" class="btn btn-primary">Login with Vkontakte</a>--}}
        {{--<a href="{{ url('/auth/yandex/redirect') }}" class="btn btn-primary">Login with Yandex</a>--}}
        {{--<a href="{{ url('/auth/facebook/redirect') }}" class="btn btn-primary">Login with Facebook</a>--}}
    {{--</div>--}}
    @yield('content')

    <div id="scripts">
        @include('app.components.scripts')
        <script src="{{ frontendCommon()->asset('js/bundle.js') }}"></script>
        <script src="{{ frontendCommon()->asset('js/vendors~main.chunk.js') }}"></script>
        <script src="{{ frontendCommon()->asset('js/main.chunk.js') }}"></script>
        <script type="text/javascript" >
            (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
                m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
            (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

            ym(53006326, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true
            });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/53006326" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    </div>
</body>
</html>
