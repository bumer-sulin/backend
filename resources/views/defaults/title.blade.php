{{-- ------------------------------------------- --}}
{{-- TITLE --}}
{{-- ------------------------------------------- --}}
@hasSection('title')
    @yield('title')
@else
    @if(isset($aComposerSettings['site']['title']))
        @if(isset($oPage) && isset($oPage->title))
            <title>{{ $oPage->title }}</title>
        @else
            <title>{{ $aComposerSettings['site']['title'] }}</title>
        @endif
    @else
        <title>{{ MetaTag::get('title') }}</title>
    @endif
@endif

{{-- ------------------------------------------- --}}
{{-- DESCRIPTION  --}}
{{-- ------------------------------------------- --}}
@if(isset($aComposerSettings['site']['description']))
    @if(isset($oPage) && isset($oPage->seo['description']))
        <meta name="description" content="{{ $oPage->seo['description'] }}">
    @else
        <meta name="description" content="{{ $aComposerSettings['site']['description'] }}">
    @endif
@else
    {!! MetaTag::tag('description') !!}
@endif

{{-- ------------------------------------------- --}}
{{-- KEYWORDS --}}
{{-- ------------------------------------------- --}}
@if(isset($aComposerSettings['site']['keywords']))
    @if(isset($oPage) && isset($oPage->seo['keywords']))
        <meta name="keywords" content="{{ $oPage->seo['keywords'] }}">
    @else
        <meta name="keywords" content="{{ $aComposerSettings['site']['keywords'] }}">
    @endif

@else
    {!! MetaTag::tag('keywords') !!}
@endif

{{-- ------------------------------------------- --}}
{{-- FAVICON --}}
{{-- ------------------------------------------- --}}
@if(isset($aComposerSettings['site']['favicon']))
    <link rel="shortcut icon" href="{{ $aComposerSettings['site']['favicon']['main'] }}">
@else
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}">
@endif


{{--
{!! MetaTag::tag('image') !!}
{!! MetaTag::openGraph() !!}
{!! MetaTag::twitterCard() !!}
--}}