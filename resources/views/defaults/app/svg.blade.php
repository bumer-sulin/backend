<svg class="hidden" style="display: none;">
    <defs>
        <symbol id="icon-triangle" viewBox="0 0 24 24">
            <title>triangle</title>
            <path d="M4.5,19.8C4.5,19.8,4.5,19.8,4.5,19.8V4.2c0-0.3,0.2-0.5,0.4-0.7c0.2-0.1,0.5-0.1,0.8,0l13.5,7.8c0.2,0.1,0.4,0.4,0.4,0.7c0,0.3-0.2,0.5-0.4,0.7L5.7,20.4c-0.1,0.1-0.3,0.1-0.5,0.1C4.8,20.6,4.5,20.2,4.5,19.8z M6,5.6v12.8L17.2,12L6,5.6z"/>
        </symbol>
        <symbol id="icon-basketball" viewBox="0 0 24 24">
            <svg data-name="Layer 1" id="Layer_1" viewBox="0 0 60 60" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                <defs>
                    <style>
                        .cls-1,.cls-4{
                            fill:none;
                        }
                        .cls-2{
                            clip-path:url(#clip-path);
                        }
                        .cls-3{
                            /*fill:#ff6b97;*/
                        }
                        .cls-4{
                            stroke:#fff;
                            /*stroke: #1e1b2c;*/
                            stroke-miterlimit:10;
                            stroke-width:3px;
                        }
                    </style>
                    <clipPath id="clip-path">
                        <circle class="cls-1" cx="29.5" cy="30.52" r="21.5"/>
                    </clipPath>
                </defs>
                <title>basketball</title>
                <g class="cls-2">
                    <rect class="cls-3" height="53" width="53" x="3" y="4.02"/>
                </g>
                <path class="cls-4" d="M32,26.26A58.48,58.48,0,0,1,34.32,32,29.08,29.08,0,0,1,52,32.41c0-.56.09-1.13,0.09-1.71a22.29,22.29,0,0,0-7-16.23A29.21,29.21,0,0,1,32,26.26Z"/>
                <path class="cls-4" d="M15.84,48.21a22.35,22.35,0,0,1-8-22.08"/>
                <path class="cls-4" d="M52,32.42A21.83,21.83,0,0,1,37.67,51.6v0.06"/>
                <path class="cls-4" d="M15.84,48.21A22.23,22.23,0,0,0,37.7,51.6,58.77,58.77,0,0,0,34.32,32,29.15,29.15,0,0,0,15.84,48.21Z"/>
                <path class="cls-4" d="M20.72,10.25a22.28,22.28,0,0,1,24.41,4.23"/>
                <path class="cls-4" d="M32,26.26a59,59,0,0,0-11.25-16A22.4,22.4,0,0,0,7.86,26.12a29,29,0,0,0,12.21,2.69A29,29,0,0,0,32,26.26Z"/>
            </svg>
        </symbol>
        <symbol id="icon-ruble" viewBox="0 0 73 100">
            <svg xmlns="http://www.w3.org/2000/svg" width="72.5" height="100">
                <path style="fill-rule: evenodd" d="M5 0h40a27.5 27.5 0 0 1 0 55h-30v6h38v10h-38v29h-10v-29h-5v-10h5v-6h-5v-10h5zM15 10h30a17.5 17.5 0 0 1 0 35h-30z"/>
            </svg>
        </symbol>
    </defs>
</svg>