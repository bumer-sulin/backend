<div id="pages-dialogs-ajax" class="dialog">
    <div class="dialog__overlay" style="z-index: initial;"></div>
    <form class="modal-content dialog__content card ajax-form" action="{{ url('/admin/pages/action/append-page-post') }}" method="POST"
      data-list=".page-table-block, .page-posts"
      data-list-action="{{ url('/admin/pages/action/get-page-table') }}, {{ url('/admin/pages/action/get-page-posts') }}"
      data-callback="closeDialogAfterSubmit, refreshAfterSubmit"
      data-form-data="#page-form"
      data-ajax-init="tooltip"
>
    <div class="modal-header">
        <h4 class="modal-title">
            Добавить пост
        </h4>
        <button type="button" class="close dialog__close" data-dismiss="modal" aria-label="Close" data-dialog="#pages-dialogs-ajax">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>Пост</label>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary dialog__close" data-dismiss="modal" data-dialog="#pages-dialogs-ajax">{{ trans('admin.submit.close') }}</button>
        <button type="submit" class="btn btn-primary inner-form-submit" data-style="slide-down">{{ trans('admin.submit.add') }}</button>
    </div>
</form>
</div>

<div id="pages-dialogs-confirm" class="dialog">
    <div class="dialog__overlay"></div>
    <div class="dialog__content" style="max-width: 400px;margin-left: 10px; margin-right: 10px; background-color: #fff;">
        <form class="ajax-form" role="form" method="POST" action="/"
              data-list=".ajax-main-table-container"
              data-list-action=""
              data-form-data=".pagination-form"
              data-callback="refreshAfterSubmit, closeDialogAfterSubmit"
        >
            <input type="hidden" name="id" value="">
            <div class="modal-header">
                <h4 class="modal-title">Подтвердите</h4>
                <button type="button" class="close dialog__close" data-dismiss="modal" aria-label="Close" data-dialog="#pages-dialogs-confirm">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <h5 class="subtitle">Действительно?</h5>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-primary inner-form-submit with-loading" type="submit">
                    Да
                </button>
                <button class="btn btn-sm dialog__close" data-dialog="#pages-dialogs-confirm" type="button">
                    Нет
                </button>
            </div>
        </form>
    </div>
</div>


<div class="modal fade is-right-bar edit-form" id="custom-edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="background-color: #fff">
        {{-- <div class="dialog__loading is-black " style="width: 100%;height: 100%;"></div>--}}
        <div class="modal-content">

        </div>
    </div>
</div>

<div class="modal fade is-left-bar edit-form" id="custom-edit-left-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="background-color: #fff">
        {{-- <div class="dialog__loading is-black " style="width: 100%;height: 100%;"></div>--}}
        <div class="modal-content">

        </div>
    </div>
</div>
