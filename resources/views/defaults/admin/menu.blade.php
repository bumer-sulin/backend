@foreach(config('project.menu.admin') as $key => $menu)
    @if(isset($menu['sub']) && !isset($menu['sub-hidden']))
        <li class="nav-item nav-dropdown">
            <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="{{ $menu['icon'] }}"></i>
                {{ $menu['title'] }}
                @if(isset($menu['badge']))
                    <span class="badge badge-info">{{ $menu['badge'] }}</span>
                @endif
            </a>
            <ul class="nav-dropdown-items">
                @foreach($menu['sub'] as $subKey => $submenu)
                    @if(!isset($submenu['hidden']))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ url('/admin/'.$key.'/'.$subKey) }}">
                                <i class="{{ $menu['icon'] }}"></i>
                                {{ $submenu['title'] }}
                                @if(isset($menu['badge']) && $aBadgeMenu = explode('|', $menu['badge']))
                                    <span class="badge badge-{{ $aBadgeMenu[0] or 'info'}}">{{ $aBadgeMenu[1] or ''}}</span>
                                @endif
                            </a>
                        </li>
                    @endif
                @endforeach
            </ul>
        </li>
    @else
        <li class="nav-item">
            <a class="nav-link" href="{{ url('/admin/'.$key) }}">
                <i class="{{ $menu['icon'] }}"></i>
                {{ $menu['title'] }}
                @if(isset($menu['badge']) && $aBadgeMenu = explode('|', $menu['badge']))
                    <span class="badge badge-{{ $aBadgeMenu[0] or 'info'}}">{{ $aBadgeMenu[1] or ''}}</span>
                @endif
            </a>
        </li>
    @endif
@endforeach