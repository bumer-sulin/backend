@extends('email.layouts.article')

@section('content')
    <tr>
        <td align="center" valign="top">
            <!-- BEGIN BODY // -->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateContainerMiddle"
                   class="brdBottomPadd">
                <tbody>
                <tr>
                    <td valign="top" class="bodyContent" mc:edit="body_content">
                        <p style="text-align: center;">
                            Чтобы сбросить пароль перейдите по этой <a href="{{ $url }}" target="_blank">ссылке</a>.
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- // END BODY -->
        </td>
    </tr>
@endsection
