@extends('email.layouts.invoice')

@section('content')
    <p>Ваш заказ успешно совершен.</p>
    <p>Счет №{{ $oInvoice->id }}</p>
    <p>Общая цена: {{ number_format($oInvoice->amount, 2, '.', '') }}</p>
    <div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th class="is-id">#</th>
                <th class="is-image"></th>
                <th>Заголовок</th>
                <th>Тип</th>
                <th>Категория</th>
                <th style="text-align: center">Количество</th>
            </tr>
            </thead>
            <tbody>
            @foreach($oProducts as $oProduct)
                <tr>
                    <td>{{ $oProduct->id }}</td>
                    <td>
                        @include('admin.content.components.table.image', [
                            'oItem' => $oProduct,
                            'path' => 'square',
                            'model' => 'product'
                        ])
                    </td>
                    <td>
                        <b>
                            <a href="{{ route('index.item', ['id' => $oProduct->id]) }}" target="_blank">
                                {{ $oProduct->title }}
                            </a>
                        </b>
                    </td>
                    <td style="font-size: 12px">
                        @if(!is_null($oProduct->type))
                            @foreach($oProduct->type->tree() as $cKey =>  $oType)
                                {{ $oType->title }} @if(is_null($oType->parent_id) && $cKey !== 0) @else | @endif
                            @endforeach
                            {{ $oProduct->type->title }}
                        @endif
                    </td>
                    <td style="font-size: 12px">
                        @if(!is_null($oProduct->category))
                            @foreach($oProduct->category->tree() as $cKey =>  $oCategory)
                                {{ $oCategory->title }} @if(is_null($oCategory->parent_id) && $cKey !== 0) @else | @endif
                            @endforeach
                            {{ $oProduct->category->title }}
                        @else
                            Нет категории
                        @endif
                    </td>
                    <td style="text-align: center">
                        {{ $oProduct->amount }}
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="font-size: 12px">
                        @foreach(json_decode($oProduct->product_options) as $option)
                            {{ $option }} <br>
                        @endforeach
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
