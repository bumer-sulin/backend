@extends('email.layouts.invoice')

@section('content')
    <p>Был оплачен счет №{{ $oInvoice->id }}</p>
    <p>Общая цена: {{ number_format($oInvoice->amount, 2, '.', '') }}р.</p>
    <p>Клиент</p>
    <p>Email: {{ $oInvoice->client->email or '' }}</p>
    <p>Телефон: {{ $oInvoice->client->phone or '' }}</p>
@endsection