@extends('email.layouts.article')

@section('content')
    <tr>
        <td align="center" valign="top">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateContainer">
                <tbody>
                <tr>
                    <td valign="top" class="bodyContent" mc:edit="body_content_01">
                        {{--<p>Hi --First Name=there--,</p>--}}
                        <h1>
                            <strong>Советуем посмотреть Вам новую статью.</strong>
                        </h1>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateContainerImageFull"
                   style="min-height:15px;">
                <tbody>
                <tr>
                    <td width="1">
                        <img src="http://c0185784a2b233b0db9b-d0e5e4adc266f8aacd2ff78abb166d77.r51.cf2.rackcdn.com/blank.png"
                             style="width: 1px; display:block; margin:0; padding:0; border:0;">
                    </td>
                    <td valign="top" class="bodyContentImageFull" mc:edit="body_content_01">
                        <p style="text-align:center;margin:0;padding:0;height: 200px;overflow: hidden;">
                            <img src="{{ imagePath()->main('article', 'sm', $oArticle) }}"
                                 style="display:block; margin:0; padding:0; border:0;width: 100%;"
                            >
                        </p>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td align="center" valign="top">
            <!-- BEGIN BODY // -->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateContainerMiddle"
                   class="brdBottomPadd">
                <tbody>
                <tr>
                    <td valign="top" class="bodyContent" mc:edit="body_content">
                        <h2>
                            <strong>
                                {{ $oArticle->title }}
                            </strong>
                        </h2>
                        <p>
                            {{ $oArticle->description }}
                        </p>
                        <a class="blue-btn"
                           href="{{ $oArticle->getLinkToArticle() }}"
                           target="_blank"
                        >
                            <strong>Прочитать</strong>
                        </a>
                    </td>
                </tr>
                </tbody>
            </table>
            <!-- // END BODY -->
        </td>
    </tr>
    <tr>
        <td align="center" valign="top" id="bodyCellFooter" class="unSubContent">
            <table width="100%" border="0" cellpadding="0" cellspacing="0" id="templateContainerFooter">
                <tbody>
                <tr>
                    <td valign="top" width="100%" mc:edit="footer_unsubscribe">
                        <p style="text-align:center;">
                            <img src="{{ asset('img/logo.png') }}"
                                 style="margin:0 auto 0 auto;display:inline-block;" width="60" height="40">
                        </p>
                        <h6 style="text-align:center;margin-top: 9px;">{{ config('app.name') }}</h6>
                        <h6 style="text-align:center;margin-top: 7px;">
                            <a href="{{ $linkUnsubscribe }}" target="_blank">
                                Отписаться
                            </a>
                        </h6>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
@endsection
