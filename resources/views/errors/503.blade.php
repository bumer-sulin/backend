<!DOCTYPE html>
<html>
<head>
    <title>Ведутся технические работы - {{ config('app.name', 'Laravel') }}</title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:100" rel="stylesheet" type="text/css">

    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            color: #464646;
            display: table;
            font-weight: 100;
            font-family: 'Roboto', sans-serif;
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .title {
            font-size: 60px;
            margin-bottom: 40px;
        }
        .description {
            font-size: 30px;
            text-align: right;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="content">
        <div class="title">Ведутся технические работы.</div>
        <div class="description">{{ config('app.url', 'Laravel') }}</div>
    </div>
</div>
</body>
</html>
