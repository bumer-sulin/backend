@extends('admin.layouts.admin')

@section('content.title')
    @include('admin.components.pages.title', [
        'title' => 'Раздел в разработке'
    ])
@endsection

@section('content')
    <div style="text-align: center;">
        <img src="/img/construction.png" alt="">
    </div>
@endsection
