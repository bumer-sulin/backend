<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">Редактирование</h4>
        <div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: right;">
            <div class="col-12">
                {{ $oItem->title }}
            </div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12 mb-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#post-tab-1" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Заголовок</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#post-tab-2" role="tab" aria-controls="profile" aria-expanded="false" data-hidden-submit="0">Текст</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#post-tab-3" role="tab" aria-controls="messages" aria-expanded="false" data-hidden-submit="1">Галерея</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane tab-submit active" id="post-tab-1" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          id="{{$sComposerRouteView}}-from"
                          action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
                          data-counter=".admin-table-counter"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Заголовок<i class="r">*</i></label>
                                <input type="text" class="form-control" name="title" placeholder="Заголовок" value="{{ $oItem->title or '' }}" required>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane tab-submit" id="post-tab-2" role="tabpanel" aria-expanded="false">
                    <form class="ajax-form"
                          action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        @include('admin.components.summernote', [
                            'url' => route($sComposerRouteView.'.text.save.post', ['id' => $oItem->id]),//url('/admin/posts/action/upload-wysiwyg'),
                            'model' => $sComposerRouteView,
                            'oItem' => $oItem,
                            'id' => 'summernote-'.$oItem->id.'-main'
                        ])
                    </form>
                </div>
                <div class="tab-pane tab-submit" id="post-tab-3" role="tabpanel" aria-expanded="false">
                    @include('admin.components.gallery.gallery', [
                        'priorityUrl' => route($sComposerRouteView.'.action.post', ['name' => 'imagePriority']),
                        'uploadUrl' => route($sComposerRouteView.'.image.upload.post', ['id' => $oItem->id]),
                        'model' => $sComposerRouteView,
                        'oItem' => $oItem,
                        'form' => '#{{$sComposerRouteView}}-form'
                    ])
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit">Сохранить</button>
    </div>
</div>
