@if(count($oItems) !== 0)
    <table class="table table-striped __with-edit-delete">
        <thead>
        <tr>
            <th class="is-id">#</th>
            <th>Заголовок</th>
            <th>Тип</th>
            <th class="is-status">Статус</th>
            <th style="width: 50px">Параметры</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th>Заголовок</th>
            <th>Тип</th>
            <th>Статус</th>
            <th>Параметры</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach($oItems as $oItem)
            <tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
                <td>{{ $oItem->id }}</td>
                <td>
                    {{ $oItem->title ?? '' }} @if(!is_null($oItem->description))({{ $oItem->description }})@endif
                </td>
                <td>
                    {{ $oItem->type_text ?? '' }}
                </td>
                <td>
                    @include('admin.content.components.table.status', [
                        'oItem' => $oItem
                    ])
                </td>
                <td style="text-align: center;">
                    @include('admin.content.components.table.edit', [
                        'oItem' => $oItem,
                        'init' => 'uploader',
                        'text' => $oItem->isList() ? 'Кол-во: '.$oItem->parameters->count() : 'Отсутствуют',
                        'disabled' => !$oItem->isList(),
                        'url' => route($sComposerRouteView.'.action.item.post', ['id' => $oItem->id, 'name' => 'getParameters'])
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.edit', [
                        'oItem' => $oItem,
                        'init' => 'uploader'
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.delete', [
                        'oItem' => $oItem,
                        'deleteKey' => 'параметр',
                        'deleteValue' => $oItem->title,
                    ])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @include('admin.content.components.table.pagination', [
        'oItems' => $oItems
    ])
@else
    <div class="text-muted page-desc">Список пуст</div>
@endif
