<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">Редактирование</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12 mb-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#post-tab-1" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Основное</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane tab-submit active" id="post-tab-1" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          id="setting-from"
                          action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
                          data-counter=".admin-table-counter"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Тип</label>
                                <select class="form-control" name="type">
                                    @foreach(Model::init($sComposerRouteView)->getTypes() as $key => $type)
                                        <option value="{{ $key }}" @if($oItem->type === $key) selected @endif>{{ $type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label>Название<i class="r">*</i></label>
                                <input type="text" class="form-control" name="title" placeholder="Заголовок" value="{{ $oItem->title ?? '' }}" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Описание</label>
                                <input type="text" class="form-control" name="description" placeholder="Описание" value="{{ $oItem->description ?? '' }}">
                            </div>
                        </div>
                        {{--<div class="col-12">--}}
                            {{--<div class="form-group">--}}
                                {{--<label>Unit</label>--}}
                                {{--<input type="text" class="form-control" name="unit" placeholder="Unit" value="{{ $oItem->unit ?? '' }}">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-12">--}}
                            {{--<div class="form-group">--}}
                                {{--<label>Ключ<i class="r">*</i></label>--}}
                                {{--<input type="text" class="form-control" name="key" placeholder="Ключ" value="{{ $oItem->key ?? '' }}" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit">Сохранить</button>
    </div>
</div>
{{--
<form class="modal-content dialog__content ajax-form"
      action="{{ route($sComposerRouteView.'.store') }}"
      data-counter=".admin-table-counter"
      data-list=".admin-table"
      data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
      data-callback="closeModalAfterSubmit, refreshAfterSubmit"
>
    <div class="modal-header">
        <h4 class="modal-title">Добавить</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label>Заголовок</label>
                        <input type="text" class="form-control" name="title" placeholder="Заголовок" value="" required>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Текст</label>
                        <textarea class="form-control" name="text" placeholder="Текст" rows="5"></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <label for="email" class="label">Иконка</label>
                    <div class="field" style="width: 101%;">
                        <p class="control" style="width: 70%;display: inline-block;">
                            <input class="form-control" name="icon" type="text" placeholder="Иконка" value="">
                        </p>
                        <p class="control" style="width: 28%;display: inline-block;">
                            <button class="btn btn-primary trigger is-primary" data-dialog="#fa-icons" type="button" style="width: 100%;">
                                Выбрать
                            </button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-primary inner-form-submit">Сохранить</button>
    </div>
</form>
--}}
