<form class="modal-content dialog__content ajax-form"
      action="{{ route($sComposerRouteView.'.store') }}"
      data-counter=".admin-table-counter"
      data-list=".admin-table"
      data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
      data-callback="closeModalAfterSubmit, refreshAfterSubmit"
      {{--data-edit-after-create="1"--}}
      {{--data-append="1"--}}
      {{--data-append-container="#custom-edit-modal .modal-content"--}}
      novalidate
>
    <div class="modal-header">
        <h4 class="modal-title">Добавить</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label>Тип</label>
                        <select class="form-control" name="type">
                            @foreach(Model::init($sComposerRouteView)->getTypes() as $key => $type)
                                <option value="{{ $key }}">{{ $type }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Название<i class="r">*</i></label>
                        <input type="text" class="form-control" name="title" placeholder="Заголовок" value="{{ $oItem->title ?? '' }}" required>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Описание</label>
                        <input type="text" class="form-control" name="description" placeholder="Описание" value="{{ $oItem->description ?? '' }}">
                    </div>
                </div>
                {{--<div class="col-12">--}}
                    {{--<div class="form-group">--}}
                        {{--<label>Unit</label>--}}
                        {{--<input type="text" class="form-control" name="unit" placeholder="Unit" value="{{ $oItem->unit ?? '' }}">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-12">--}}
                    {{--<div class="form-group">--}}
                        {{--<label>Ключ<i class="r">*</i></label>--}}
                        {{--<input type="text" class="form-control" name="key" placeholder="Ключ" value="{{ $oItem->key ?? '' }}" required>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-primary inner-form-submit">Сохранить</button>
    </div>
</form>
