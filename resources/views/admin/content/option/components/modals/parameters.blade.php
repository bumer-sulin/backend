<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">Параметры</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12 mb-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#post-tab-1" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Список</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane tab-submit admin-albums-table active" id="post-tab-1" role="tabpanel" aria-expanded="true">
                    <form class="ajax-form"
                          id="admin-audio-from"
                          action="{{ route($sComposerRouteView.'.action.item.post', ['name' => 'saveParameters', 'id' => $oOption->id]) }}"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                    >
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th class="is-id">#</th>
                                <th>Название</th>
                                <th class="is-status">Приоритет</th>
                                <th class="is-status">Статус</th>
                                <th style="width: 50px">Удалить</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Название</th>
                                <th>Приоритет</th>
                                <th>Статус</th>
                                <th>Удалить</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($oItems as $key => $oItem)
                                <tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
                                    <td>{{ $oItem->id }}</td>
                                    <td>
                                        <input class="form-control form-control-sm form-control-focus" type="text" name="name[{{ $oItem->id }}]" value="{{ $oItem->quantity }}">
                                    </td>
                                    <td>
                                        <input class="form-control form-control-sm" type="number" name="priority[{{ $oItem->id }}]" value="{{ $oItem->priority }}">
                                    </td>
                                    <td style="text-align: center;">
                                        @include('admin.content.components.table.status', [
                                            'url' => route($sComposerRouteView.'.action.item.post', ['name' => 'setStatusParameter', 'id' => $oItem->id]),
                                            'oItem' => $oItem,
                                            'list' => '#custom-edit-modal .modal-content',
                                            'action' => route($sComposerRouteView.'.action.item.post', ['name' => 'getParameters', 'id' => $oOption->id]),
                                            'model' => 'option_parameter'
                                        ])
                                    </td>
                                    <td style="text-align: center;">
                                        <a class="btn btn-danger btn-sm is-small ajax-link" action="{{ route($sComposerRouteView.'.action.item.post', ['name' => 'setDestroyParameter', 'id' => $oItem->id]) }}"
                                           data-view="#custom-edit-modal .modal-content"
                                           data-callback="updateView"
                                        >
                                            <i class="fa fa-trash" style="color: #fff;"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            <tr class="clone-tr-container" style="background-color: #fff;">
                                <td colspan="4"></td>
                                <td class="is-icon">
                                    <button type="button" class="btn btn-sm btn-primary clone-tr" data-tr=".temp-ref-value">
                                        <i class="icon-plus"></i>
                                    </button>
                                </td>
                            </tr>
                            <tr class="temp-ref-value" style="display: none;">
                                <td>#</td>
                                <td>
                                    <input type="text" class="form-control form-control-sm form-control-focus" name="name[0][]" value="" disabled>
                                </td>
                                <td>
                                    <input type="text" class="form-control form-control-sm" name="priority[0][]" value="" disabled>
                                </td>
                                <td>
                                    <select class="form-control form-control-sm" name="status[0][]" disabled>
                                        @foreach(Model::init('option_parameter')->getStatuses() as $key => $status)
                                            <option value="{{ $key }}" @if($key === 1) selected @endif>{{ $status }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td class="is-icon">
                                    <button type="button" class="btn btn-sm btn-danger delete-clone-tr">
                                        <i class="icon-trash"></i>
                                    </button>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit">Сохранить</button>
    </div>
</div>
