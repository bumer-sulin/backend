пример: <code>app.powered</code><br>
Ключ по которому подставляется значение, ранее прописанное в коде разработчиками.<br>
Существующие в коде:
<ul>
    @foreach($oComposerSite as $key => $options)
        @foreach($options as $sKey => $option)
            @if(!is_array($option))
                <li><code>{{ $key }}.{{ $sKey }}</code> {{ is_null($option) ? '- нет значения' : ''}}</li>
            @else
                @foreach($option as $nKey => $sub)
                    @if(!is_array($sub) && !is_object($sub))
                        <li><code>{{ $key }}.{{ $sKey }}</code> {{ is_null($sub) ? '- нет значения' : ''}}</li>
                    @endif
                @endforeach
            @endif
        @endforeach
    @endforeach
</ul>
