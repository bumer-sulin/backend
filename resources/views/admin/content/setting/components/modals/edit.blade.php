<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">Редактирование</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12 mb-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#post-tab-1" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Основное</a>
                </li>
                @if($oItem->isImage)
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#post-tab-2" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="1">Изображение</a>
                    </li>
                @endif
                @if($oItem->isText)
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#post-tab-2" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Текст</a>
                    </li>
                @endif
            </ul>

            <div class="tab-content">
                <div class="tab-pane tab-submit active" id="post-tab-1" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          id="setting-from"
                          action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
                          data-counter=".admin-table-counter"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Тип</label>
                                <select class="form-control" name="type">
                                    @foreach(Model::init($sComposerRouteView)->getTypes() as $key => $type)
                                        <option value="{{ $key }}" @if($oItem->type === $key) selected @endif>{{ $type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="form-group">
                                <label>Название<i class="r">*</i></label>
                                <input type="text" class="form-control" name="title" placeholder="Заголовок" value="{{ $oItem->title ?? '' }}" required>
                            </div>
                        </div>
                        @if(!$oItem->isImage && !$oItem->isColor && !$oItem->isText)
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Значение<i class="r">*</i></label>
                                    <input type="text" class="form-control" name="value" placeholder="Значение" value="{{ $oItem->value ?? '' }}" required>
                                </div>
                            </div>
                        @endif
                        @if($oItem->isColor)
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Значение<i class="r">*</i></label>
                                    <input type="text" class="form-control color-input" name="value" placeholder="Значение" value="{{ $oItem->value ?? '' }}" required autocomplete="off">
                                </div>
                            </div>
                        @endif
                        <div class="col-12">
                            <div class="form-group is-disabled">
                                <label>Ключ<i class="r">*</i></label>
                                <input type="text" class="form-control" name="key" placeholder="Ключ" value="{{ $oItem->key ?? '' }}" disabled>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <span class="help">
                                    @include('admin.content.setting.components.modals.components.issetKeys')
                                </span>
                            </div>
                        </div>
                        {{--@if($oItem->isColor)--}}
                            {{--<div class="col-12">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label>Значение<i class="r">*</i></label>--}}
                                    {{--<input type="text" class="form-control" name="value" placeholder="Значение" id="hexVal" value="{{ $oItem->value ?? '' }}" required style="padding-left: 35px;">--}}
                                    {{--<div class="lr" data-color="#hexVal" style="position: absolute;height: 25px;width: 25px;margin-left: 15px;top: 34px;border-radius: 3px;left: 5px; {{ 'background-color: '.$oItem->value.';' }}"></div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--@if(!is_null($oImage))--}}
                                {{--<div class="col-12">--}}
                                    {{--<div class="container-img" data-src="{{ ImagePath::main('setting', 'original', $oImage) }}">--}}
                                        {{--<div class="text-field">--}}
                                            {{--<div class="lr">Цвет пипетки: </div>--}}
                                            {{--<div class="lr" id="preview" style="margin-left: 5px;"></div>--}}
                                        {{--</div>--}}
                                        {{--<div class="image-field">--}}
                                            {{--<canvas id="panel" width="200" height="auto"></canvas>--}}
                                        {{--</div>--}}
                                        {{--<div style="clear:both;"></div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@else--}}
                                {{--<div class="col-12">--}}
                                    {{--<span class="help">--}}
                                        {{--Подбираемый цвет происходит по связанному изображению с ключом <code>{{ $oItem->key }}</code> по типу <code>app.banner.code</code>--}}
                                    {{--</span>--}}
                                {{--</div>--}}
                            {{--@endif--}}
                        {{--@endif--}}
                    </form>
                </div>
                @if($oItem->isImage)
                    <div class="tab-pane tab-submit" id="post-tab-2" role="tabpanel" aria-expanded="false">
                        @include('admin.components.gallery.gallery', [
                            'priorityUrl' => route($sComposerRouteView.'.action.post', ['name' => 'imagePriority']),
                            'uploadUrl' => route($sComposerRouteView.'.image.upload.post', ['id' => $oItem->id]),
                            'model' => 'setting',
                            'oItem' => $oItem,
                            'form' => '#setting-form'
                        ])
                        <div style="position: absolute;bottom: -30px;margin-bottom: 0;">
                            Размеры должны быть 552x100 пикселей
                        </div>
                    </div>
                @endif
                @if($oItem->isText)
                    <div class="tab-pane tab-submit" id="post-tab-2" role="tabpanel" aria-expanded="false">
                        <form class="ajax-form"
                              action="{{ route($sComposerRouteView.'.text.save.post', ['id' => $oItem->id]) }}"
                              data-list=".admin-table"
                              data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                              data-form-data=".pagination-form"
                              data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                        >
                            <input type="hidden" name="id" value="{{ $oItem->id }}">
                            @include('admin.components.summernote', [
                                'url' => route($sComposerRouteView.'.text.save.post', ['id' => $oItem->id]),//url('/admin/posts/action/upload-wysiwyg'),
                                'model' => $sComposerRouteView,
                                'oItem' => $oItem,
                                'itemText' => $oItem->value,
                                'id' => 'summernote-'.$oItem->id.'-main'
                            ])
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit">Сохранить</button>
    </div>
</div>
{{--
<form class="modal-content dialog__content ajax-form"
      action="{{ route($sComposerRouteView.'.store') }}"
      data-counter=".admin-table-counter"
      data-list=".admin-table"
      data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
      data-callback="closeModalAfterSubmit, refreshAfterSubmit"
>
    <div class="modal-header">
        <h4 class="modal-title">Добавить</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label>Заголовок</label>
                        <input type="text" class="form-control" name="title" placeholder="Заголовок" value="" required>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Текст</label>
                        <textarea class="form-control" name="text" placeholder="Текст" rows="5"></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <label for="email" class="label">Иконка</label>
                    <div class="field" style="width: 101%;">
                        <p class="control" style="width: 70%;display: inline-block;">
                            <input class="form-control" name="icon" type="text" placeholder="Иконка" value="">
                        </p>
                        <p class="control" style="width: 28%;display: inline-block;">
                            <button class="btn btn-primary trigger is-primary" data-dialog="#fa-icons" type="button" style="width: 100%;">
                                Выбрать
                            </button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-primary inner-form-submit">Сохранить</button>
    </div>
</form>
--}}
