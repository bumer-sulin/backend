<table class="table table-striped __with-edit-delete">
    <thead>
    <tr>
        <th class="is-id">#</th>
        <th>Заголовок</th>
        <th>Ключ</th>
        <th>Значение</th>
        <th class="is-status">Статус</th>
        <th>Изменить</th>
        <th>Удалить</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>#</th>
        <th>Заголовок</th>
        <th>Ключ</th>
        <th>Значение</th>
        <th>Статус</th>
        <th>Изменить</th>
        <th>Удалить</th>
    </tr>
    </tfoot>
    <tbody>
    @foreach($oItems as $oItem)
        <tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
            <td>{{ $oItem->id }}</td>
            <td>
                {{ $oItem->title ?? '' }}
            </td>
            <td>
                {{ $oItem->key ?? '' }}
            </td>
            <td>
                @if($oItem->isImage)
                    @include('admin.content.components.table.image', [
                        'oItem' => $oItem,
                    ])
                @elseif($oItem->isColor)
                    <div style="background-color: {{ $oItem->value }}; width: 100%;height: 19px;margin: 5px 0;"></div>
                @else
                    <b>{{ str_limit($oItem->value, 30) }}</b>
                @endif
            </td>
            <td>
                @include('admin.content.components.table.status', [
                    'oItem' => $oItem
                ])
            </td>
            <td>
                @include('admin.content.components.table.edit', [
                    'oItem' => $oItem,
                    'init' => 'uploader, initSummernote, colorpicker'
                ])
            </td>
            <td>
                @include('admin.content.components.table.delete', [
                    'oItem' => $oItem,
                    'deleteKey' => 'параметр',
                    'deleteValue' => $oItem->title,
                ])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@include('admin.content.components.table.pagination', [
    'oItems' => $oItems
])
