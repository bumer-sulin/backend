@if(count($oItems) !== 0)
    <table class="table table-striped __with-edit-delete">
        <thead>
        <tr>
            <th class="is-id">#</th>
            <th>Email</th>
            <th>Тип</th>
            <th class="is-status">Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th>Email</th>
            <th>Тип</th>
            <th>Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach($oItems as $oItem)
            <tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
                <td>{{ $oItem->id }}</td>
                <td>
                    @if($oItem->hasUser)
                        @include('admin.components.tooltip.question', [
                            'title' => 'Есть пользователь с таким email',
                            'icon' => 'user',
                            'type' => 'success'
                        ])
                    @endif
                    <b>{{ $oItem->email }}</b>
                </td>
                <td>
                    {{ Model::init($sComposerRouteView)->getTypes()[$oItem->type]['title'] }}
                </td>
                <td>
                    @include('admin.content.components.table.status', [
                        'oItem' => $oItem
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.edit', [
                        'oItem' => $oItem,
                        'init' => 'multiselect, tooltip'
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.delete', [
                        'oItem' => $oItem,
                        'deleteKey' => 'подписку',
                        'deleteValue' => $oItem->email,
                    ])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @include('admin.content.components.table.pagination')
@else
    <div class="text-muted page-desc">Список пуст</div>
@endif
