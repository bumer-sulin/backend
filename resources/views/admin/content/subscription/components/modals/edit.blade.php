<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">
            Редактирование
            @include('admin.components.tooltip.question', [
                'title' => 'Вкладки сохраняются по отдельности.<br>Вкладка "Заголовок" закрывает панель.'
            ])
        </h4>
        <div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: right;">
            <div class="col-12">
                {{ $oItem->email }} :: {{ Model::init($sComposerRouteView)->getTypes()[$oItem->type]['title'] }}
            </div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12 mb-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#post-tab-1" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Заголовок</a>
                </li>
                @if($oItem->isCategories())
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#post-tab-5" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Список</a>
                    </li>
                @endif
            </ul>

            <div class="tab-content">
                <div class="tab-pane tab-submit active" id="post-tab-1" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          id="{{$sComposerRouteView}}-from"
                          action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
                          data-counter=".admin-table-counter"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        <div class="col-12">
                            <div class="form-group is-disabled">
                                <label>Email<i class="r">*</i></label>
                                <input type="text" class="form-control" name="email" placeholder="Email" value="{{ $oItem->email ?? '' }}" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Тип<i class="r">*</i></label>
                                <select class="form-control" name="type" required>
                                    @foreach(Model::init($sComposerRouteView)->getTypes() as $key => $value)
                                        <option value="{{ $key }}" {{ $oItem->type === $key ? 'selected' : '' }}>{{ ucfirst($value['title']) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                @if($oItem->isCategories())
                    <div class="tab-pane tab-submit" id="post-tab-5" role="tabpanel" aria-expanded="true">
                        <form class="row ajax-form"
                              id="admin-audio-from"
                              action="{{ route($sComposerRouteView.'.action.item.post', ['name' => 'saveCategories', 'id' => $oItem->id]) }}"
                              data-list=".admin-table"
                              data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                              data-form-data=".pagination-form"
                              data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                        >
                            <div class="col-12">
                                <div class="form-group">
                                    <label>Категории<i class="r">*</i></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-comments"></i></span>
                                        <select class="selectpicker form-control" title="Выберите категории..." multiple name="categories[]" required>
                                            @foreach($oCategories->where('parent_id', null) as $key => $oCategory)
                                                @include('admin.components.select.parentable', [
                                                    'oItem' => $oCategory,
                                                    'selected' => true,
                                                    'oRelation' => $oItem->categories,
                                                    'selectAll' => isset($oItem->selectAllCategories) && $oItem->selectAllCategories,
                                                ])
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit">Сохранить</button>
    </div>
</div>
