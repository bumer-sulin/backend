@if(count($oItems) !== 0)
    <table class="table table-striped __with-edit-delete">
        <thead>
        <tr>
            <th class="is-id">#</th>
            <th class="is-image text-center">
                @include('admin.components.tooltip.question', [
                    'title' => 'Опубликовать статью, после этого начинается рассылка этой статьи на каждый email в разделе Подписки и статья будет видна на сайте. Отменить это действие невозможно.',
                    'type' => 'success',
                ])
            </th>
            <th class="is-image"></th>
            <th>Заголовок</th>
            <th>Категория</th>
            <th>Тип</th>
            <th nowrap>
                Дата публикации
                @include('admin.components.tooltip.question', [
                    'title' => 'Сверху: Дата публикации статьи. <br>Снизу: Дата рассылки.',
                    'type' => 'success',
                ])
            </th>
            <th class="text-center" style="width: 150px">Статистика</th>
            <th class="is-status">Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th></th>
            <th></th>
            <th>Заголовок</th>
            <th>Категория</th>
            <th>Тип</th>
            <th>Дата публикации</th>
            <th class="text-center">Статистика</th>
            <th>Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach($oItems as $oItem)
            <tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
                <td>{{ $oItem->id }}</td>
                <td>
                    @if(is_null($oItem->publication_at))
                        <a class="btn btn-primary btn-sm {{ isset($oItem->status) && !$oItem->status ? 'disabled' : 'ajax-link' }}"
                           style="height: 29px;"
                           action="{{ route($sComposerRouteView.'.action.item.post', ['name' => 'setPublication', 'id' => $oItem->id]) }}"
                           data-callback="refreshAfterSubmit"
                           data-list=".admin-table"
                           data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                           title="Опубликовать статью"
                           data-loading="1"
                        >
                            <i class="fa fa-check" aria-hidden="true" style="color: #fff;"></i>
                        </a>
                    @else
                        <a class="btn btn-sm btn-success disabled"
                           style="height: 29px;"
                        >
                            <i class="fa fa-check" aria-hidden="true" style="color: #fff;"></i>
                        </a>
                    @endif
                </td>
                <td>
                    @include('admin.content.components.table.image', [
                        'oItem' => $oItem,
                        'path' => 'square'
                    ])
                </td>
                <td>
                    <b>
                        {{ $oItem->title }}
                    </b>
                </td>
                <td>
                    {{ $oItem->category->title ?? '' }}
                </td>
                <td>
                    {{ $oItem->type_title }}
                </td>
                <td nowrap>
                    @if(!is_null($oItem->published_at))
                        {{ $oItem->published_at->format('d.m.Y') }}
                    @else
                        -
                    @endif
                        <br>
                    @if(!is_null($oItem->publication_at))
                        {{ $oItem->publication_at->format('d.m.Y H:i:s') }}
                    @else
                        -
                    @endif
                </td>
                <td class="text-center">
                    <div class="d-flex justify-content-between">
                        <button class="btn btn-light btn-sm" type="button">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                            {{ $oItem->statistic->views }}
                        </button>
                        <button class="btn btn-sm {{ $oItem->statistic->comments === 0 ? 'btn-light is_disabled' : 'btn-primary trigger' }}" type="button"
                                @if($oItem->statistic->comments !== 0)
                                data-dialog="#custom-edit-modal" data-ajax
                                data-action="{{ route($sComposerRouteView.'.action.item.post', ['id' => $oItem->id, 'name' => 'getCommentsModal']) }}"
                                data-ajax-init="tooltip"
                                data-tippy-popover
                                data-tippy-content="Комментарии"
                                @endif
                        >
                            <i class="fa fa-comment-o" aria-hidden="true"></i>
                            {{ $oItem->statistic->comments }}
                        </button>
                    </div>
                </td>
                <td>
                    @include('admin.content.components.table.status', [
                        'oItem' => $oItem,
                        'exceptId' => $oItem->isSlider() ? 2 : null,
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.edit', [
                        'oItem' => $oItem,
                        'init' => 'uploader, initSummernote, multiselect, clipboard, datetimepicker, tooltip, counter'
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.delete', [
                        'oItem' => $oItem,
                        'deleteKey' => 'статью',
                        'deleteValue' => $oItem->title,
                    ])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {{--{{ dd($oItems->currentPage(), $oItems->perPage(), $oItems->lastItem()) }}--}}
    {{--{{ dd($oItems) }}--}}
    @include('admin.content.components.table.pagination')
@else
    <div class="text-muted page-desc">Список пуст</div>
@endif
