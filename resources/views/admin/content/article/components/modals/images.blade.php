<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">
            Размеры изображений
        </h4>
        <button type="button" class="close dialog__close" data-dismiss="modal" aria-label="Close" data-dialog="#pages-dialogs-ajax">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-12" style="margin-bottom: 5px;display: flex;align-items: end;">
                <img src="{{ ImagePath()->image($model, 'original', $image) }}" width="100px">
                <div style="width: 300px;margin: 0 20px;">
                    <div class="text-uppercase"><b>original</b></div>
                    <div style="margin-bottom: 5px;">Какой был загружен</div>
                    <input type="text" class="form-control" value="{{ ImagePath()->image($model, 'original', $image) }}">
                </div>
            </div>
            @foreach(cmfData()->{$model}()->filters() as $name => $filter)
                <div class="col-12" style="margin-bottom: 5px;display: flex;align-items: center;">
                    <img src="{{ ImagePath()->image($model, $name, $image) }}" width="100px">
                    <div style="width: 300px;margin: 0 20px;">
                        <div class="text-uppercase"><b>{{ $name }}</b></div>
                        @if(isset($filter['options']['size']))
                            <div style="margin-bottom: 5px;">{{ $filter['options']['size'] }}x{{ $filter['options']['size'] }}</div>
                        @endif
                        <input type="text" class="form-control" value="{{ ImagePath()->image($model, $name, $image) }}">
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
