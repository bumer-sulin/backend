<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">
            Редактирование
            @include('admin.components.tooltip.question', [
                'title' => 'Вкладки сохраняются по отдельности.'
            ])
        </h4>
        <div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: right;">
            <div class="col-12">
                {{ $oItem->title }}
            </div>
        </div>
        {{--<div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: left;">--}}
            {{--<div class="col-12 alert alert-success" style="padding-left: 0;">--}}
                {{--Вкладки сохраняются по отдельности, вкладки 'Заголовок' закрывает панель--}}
            {{--</div>--}}
        {{--</div>--}}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12 mb-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#article-tab-1" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Заголовок</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#article-tab-2" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Параметры</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#article-tab-2-seo" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Сео</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#article-tab-3" role="tab" aria-controls="profile" aria-expanded="false" data-hidden-submit="0">Текст</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#article-tab-4" role="tab" aria-controls="messages" aria-expanded="false" data-hidden-submit="1">Галерея</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane tab-submit active" id="article-tab-1" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          id="{{$sComposerRouteView}}-from"
                          action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
                          data-counter=".admin-table-counter"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="refreshAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Заголовок<i class="r">*</i></label>
                                <input type="text" class="form-control" name="title" placeholder="Заголовок" value="{{ $oItem->title ?? '' }}"
                                       maxlength="100"
                                       data-counter="1"
                                       data-counter-label="Количество допустимых символов: {remaining}."
                                       required
                                >
                                <span class="help">
                                    Заголовок не может быть больше 100 символов.
                                </span>
                                <br>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Подзаголовок</label>
                                <textarea class="form-control" name="description" id="" cols="15" rows="5">{{ $oItem->description ?? '' }}</textarea>
                            </div>
                        </div>
                        {{--<div class="col-12">--}}
                            {{--<div class="form-group">--}}
                                {{--<label>Тип</label>--}}
                                {{--<select class="form-control" name="type_id">--}}
                                    {{--@foreach(Model::init($sComposerRouteView)->getTypes() as $key => $value)--}}
                                        {{--<option value="{{ $key }}" {{ $oItem->type_id === $key ? 'selected' : '' }}>{{ $value }}</option>--}}
                                    {{--@endforeach--}}
                                {{--</select>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="col-12">
                            <div class="form-group">
                                <label>Категория</label>
                                <select class="form-control" name="category_id">
                                    <option value="">Нет категории</option>
                                    @foreach($oCategories->where('parent_id', null) as $key => $oCategory)
                                        @include('admin.components.select.parentable', [
                                            'oItem' => $oCategory,
                                            'value' => $oItem->category_id
                                        ])
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Тэги</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-tags"></i></span>
                                    <select class="selectpicker form-control" multiple name="tags[]">
                                        @foreach($oTags as $oTag)
                                            <option value="{{ $oTag->id }}" @if(!is_null($oItem->tags->where('id', $oTag->id)->first())) selected @endif>{{ $oTag->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Дата публикации</label>
                                <input type="date" class="form-control" name="published_at" placeholder="Дата публикации" value="{{ !is_null($oItem->published_at) ? $oItem->published_at->format('Y-m-d') : now()->format('Y-m-d') }}">
                                <span class="help">
                                    Публикуются статьи только у которых дата публикации меньше "сегодня". Новым считается статья, которая была добавлена не позже 7 дней назад.
                                </span>
                            </div>
                        </div>
                        @foreach($oMainOptions as $oOption)
                            <div class="col-12">
                                <div class="form-group">
                                    <label>
                                        {{ $oOption->title }} @if(!is_null($oOption->description))({{ $oOption->description }})@endif
                                    </label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-tags"></i></span>
                                        @if($oOption->isList())
                                            <select class="selectpicker form-control" multiple name="options[{{ $oOption->key }}][]">
                                                @foreach($oOption->parameters as $oParameter)
                                                    <option value="{{ $oParameter->id }}" @if(!is_null($oItem->values->where('parameter_id', $oParameter->id)->first())) selected @endif>{{ $oParameter->quantity }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <input type="text" class="form-control" name="options[{{ $oOption->key }}]" placeholder="{{ $oOption->title }}" value="{{ isset($oValues[$oOption->key]) ? $oValues[$oOption->key]->value : ''}}">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </form>
                </div>
                <div class="tab-pane tab-submit" id="article-tab-2" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          action="{{ route($sComposerRouteView.'.action.item.post', ['id' => $oItem->id, 'action' => 'updateArticleParameters']) }}"
                          data-counter=".admin-table-counter"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="refreshAfterSubmit"
                            {{--data-with-form="#post-tab-4 form"--}}
                            {{--data-callback="refreshAfterSubmit"--}}
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        @foreach($oOptions as $oOption)
                            <div class="col-12">
                                <div class="form-group">
                                    <label>
                                        {{ $oOption->title }} @if(!is_null($oOption->description))({{ $oOption->description }})@endif
                                    </label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-tags"></i></span>
                                        @if($oOption->isList())
                                            <select class="selectpicker form-control" multiple name="options[{{ $oOption->key }}][]">
                                                @foreach($oOption->parameters as $oParameter)
                                                    <option value="{{ $oParameter->id }}" @if(!is_null($oItem->values->where('parameter_id', $oParameter->id)->first())) selected @endif>{{ $oParameter->quantity }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <input type="text" class="form-control" name="options[{{ $oOption->key }}]" placeholder="{{ $oOption->title }}" value="{{ isset($oValues[$oOption->key]) ? $oValues[$oOption->key]->value : ''}}">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </form>
                </div>
                <div class="tab-pane tab-submit" id="article-tab-2-seo" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          id="{{$sComposerRouteView}}-from"
                          action="{{ route($sComposerRouteView.'.action.item.post', ['id' => $oItem->id, 'action' => 'updateArticleSeo']) }}"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="refreshAfterSubmit"
                    >
                        <div class="col-12">
                            <div class="form-group">
                                <label>Ссылка (url)<i class="r">*</i></label>
                                <input type="text" class="form-control" name="name" placeholder="Ссылка" value="{{ $oItem->name ?? '' }}" required>
                                <span class="help">
                                    <a href="http://hozyindachi.ru/kak-url-adres-vliyaet-na-seo-prodvizhenie/" target="_blank">Как URL-адрес влияет на SEO продвижение</a>
                                    <br>
                                    Рекомендуется оставлять url, который был автоматически сгенерирован.
                                </span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Ключевые слова (keywords)</label>
                                <textarea class="form-control" name="keywords" cols="15" rows="5" placeholder="Ключевые слова">{{ $oItem->keywords ?? '' }}</textarea>
                                <span class="help">
                                    Этот мета-тег не влияет на ранжирование, но, поскольку Яндекс пишет, что meta keywords может учитываться — рекомендую заполнять его, добавляя 3–5 релевантных контенту фраз, разделённых между собой запятыми.
                                    <br>
                                    Представитель Google в своём блоге заявил, что поисковик не учитывает в результатах ранжирования метатег Keywords
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane tab-submit" id="article-tab-3" role="tabpanel" aria-expanded="false">
                    <a href="{{ route('index.article.preview', ['id' => $oItem->id, 'hash' => Sentinel::getUser()->hash]) }}"
                       target="_blank"
                       style="position: absolute;top: 10px;right: 40px;"
                       data-tippy-popover
                       data-tippy-content="{{ route('index.article.preview', ['id' => $oItem->id, 'hash' => Sentinel::getUser()->hash]) }}"
                    >
                        Предпросмотр
                    </a>
                    <form class="ajax-form"
                          action="{{ route($sComposerRouteView.'.text.save.post', ['id' => $oItem->id]) }}"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="refreshAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        @include('admin.components.summernote', [
                            'url' => route($sComposerRouteView.'.text.save.post', ['id' => $oItem->id]),//url('/admin/posts/action/upload-wysiwyg'),
                            'model' => $sComposerRouteView,
                            'oItem' => $oItem,
                            'id' => 'summernote-'.$oItem->id.'-main'
                        ])
                    </form>
                </div>
                <div class="tab-pane tab-submit" id="article-tab-4" role="tabpanel" aria-expanded="false">
                    @include('admin.components.gallery.gallery', [
                        'priorityUrl' => route($sComposerRouteView.'.action.post', ['name' => 'imagePriority']),
                        'uploadUrl' => route($sComposerRouteView.'.image.upload.post', ['id' => $oItem->id]),
                        'model' => $sComposerRouteView,
                        'oItem' => $oItem,
                        'form' => '#'.$sComposerRouteView.'-form',
                    ])
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit"
                {{--data-with-form="#article-tab-1 form, #article-tab-2 form, #article-tab-3 form"--}}
        >Сохранить</button>
    </div>
</div>
