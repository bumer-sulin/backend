<div class="post-comments {{ $oItem->approved ? 'shadow-success' : 'shadow-danger' }}" id="comment-{{ $oItem->id }}">
    <p class="meta">
        # {{ $oItem->id }}
        {{ $oItem->created_at->format('d.m.Y H:i:s') }}
        @if(!is_null($oItem->commented))
            <a href="{{ route('user.index', ['id' => $oItem->commented->id]) }}"
               target="_blank"
               data-tippy-popover
               data-tippy-content="Найти пользователя"
            >{{ $oItem->commented->first_name }}</a>
        @else
            <span style="color: #6b6e80">Гость</span>
        @endif
        <span class="text-success" style="margin-left: 5px;">
            <i class="fa fa-thumbs-o-up" aria-hidden="true"></i> {{ $oItem->loveUsers->sum('like') }}
        </span>
        <span class="text-danger" style="margin-left: 5px;">
            <i class="fa fa-thumbs-o-down" aria-hidden="true"></i> {{ $oItem->loveUsers->sum('dislike') }}
        </span>

        {{--оставил комментарий:--}}
        <span class="pull-right">
            <button class="btn btn-link text-info btn-sm toggle-item" type="button"
                    data-item=".--comment-{{$oItem->id}}"
                    data-hide="1"
                    data-tippy-popover
                    data-tippy-content="Изменить комментарий"
            >
                <i class="fa fa-pencil" aria-hidden="true"></i>
            </button>
            <button class="btn btn-link text-success btn-sm {{ !$oItem->approved ? 'ajax-link' : 'is-disabled' }}" type="button"
                    action="{{ route($sComposerRouteView.'.action.item.post', ['id' => $oItem->id, 'name' => 'approveComment']) }}"
                    data-loading="1"
                    data-callback="refreshAfterSubmit"
                    data-list="#custom-edit-modal .modal-content.--inner"
                    data-list-action="{{ route($sComposerRouteView.'.action.item.post', ['id' => $oItem->commentable->id, 'name' => 'getCommentsModal']) }}"
                    data-ajax-list-init="tooltip"
                    @if(!$oItem->approved)
                        data-tippy-popover
                        data-tippy-content="Одобрить"
                    @endif
            >
                <i class="fa fa-check" aria-hidden="true"></i>
            </button>
            <button class="btn btn-link text-danger btn-sm {{ $oItem->approved ? 'ajax-link' : 'is-disabled' }}" type="button"
                    action="{{ route($sComposerRouteView.'.action.item.post', ['id' => $oItem->id, 'name' => 'disapproveComment']) }}"
                    data-loading="1"
                    data-callback="refreshAfterSubmit"
                    data-list="#custom-edit-modal .modal-content.--inner"
                    data-list-action="{{ route($sComposerRouteView.'.action.item.post', ['id' => $oItem->commentable->id, 'name' => 'getCommentsModal']) }}"
                    data-ajax-list-init="tooltip"
                    @if($oItem->approved)
                        data-tippy-popover
                        data-tippy-content="Заблокировать"
                    @endif
            >
                <i class="fa fa-close" aria-hidden="true"></i>
            </button>
            <button class="btn btn-link text-danger btn-sm ajax-link" type="button"
                    action="{{ route($sComposerRouteView.'.action.item.post', ['id' => $oItem->id, 'name' => 'deleteComment']) }}"
                    data-loading="1"
                    data-callback="refreshAfterSubmit"
                    data-list="#custom-edit-modal .modal-content.--inner"
                    data-list-action="{{ route($sComposerRouteView.'.action.item.post', ['id' => $oItem->commentable->id, 'name' => 'getCommentsModal']) }}"
                    data-ajax-list-init="tooltip"
                    data-tippy-popover
                    data-tippy-content="Удалить комментарий и все ответы на него"
            >
                <i class="fa fa-trash" aria-hidden="true"></i>
            </button>
            @if(!$oItem->approved)

            @else

            @endif
        </span>
        {{--<i class="pull-right">--}}
            {{--<a href="#">--}}
                {{--<small>Удалить</small>--}}
            {{--</a>--}}
        {{--</i>--}}
    </p>
    <p>
        <div class="--comment-{{$oItem->id}} --off">
            @if(!is_null($oItem->reply_id))
                <a href="#"
                   data-tippy-popover
                   data-tippy-content="{{ $oItem->reply->comment }}"
                >
                    #{{ $oItem->reply_id }}
                </a>
            @endif

            {{ $oItem->comment }}
        </div>
        <form class="ajax-form --comment-{{$oItem->id}} --on hidden" action="{{ route($sComposerRouteView.'.action.item.post', ['id' => $oItem->id, 'name' => 'editComment']) }}"
              data-loading="1"
              data-callback="replaceView"
              data-view="#comment-{{ $oItem->id }}"
              data-ajax-list-init="tooltip"
        >
            <div class="form-group">
                <textarea name="comment_original" cols="30" rows="5" class="form-control hidden">{{ $oItem->comment }}</textarea>
            </div>
            <div class="form-group">
                <textarea name="comment" cols="30" rows="5" class="form-control">{{ $oItem->comment }}</textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary inner-form-submit">
                    Сохранить
                </button>
                <button type="button" class="btn btn-secondary toggle-item"
                        data-item=".--comment-{{$oItem->id}}"
                        data-reset-from='.--comment-{{$oItem->id}} [name="comment_original"]'
                        data-reset-to='.--comment-{{$oItem->id}} [name="comment"]'
                >
                    Отмена
                </button>
            </div>
        </form>
    </p>
</div>
@if($oItem->hasChildren())
    <ul class="comments">
        <li class="clearfix">
            @foreach($oItem->children as $oChildren)
                @include('admin.content.article.components.modals.comments.comment', [
                    'oItem' => $oChildren,
                    'prefix' => isset($prefix) ? $prefix.' - ' : ' - '
                ])
            @endforeach
        </li>
    </ul>
@endif
