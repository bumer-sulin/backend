<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">
            Комментарии
        </h4>
        <div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: right;">
            <div class="col-12">
                {{ $oItem->title }}
            </div>
        </div>
        {{--<div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: left;">--}}
            {{--<div class="col-12 alert alert-success" style="padding-left: 0;">--}}
                {{--Вкладки сохраняются по отдельности, вкладки 'Заголовок' закрывает панель--}}
            {{--</div>--}}
        {{--</div>--}}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12 mb-12">
            {{--<ul class="nav nav-tabs" role="tablist">--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link active" data-toggle="tab" href="#article-tab-1" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Заголовок</a>--}}
                {{--</li>--}}
            {{--</ul>--}}

            {{--<div class="tab-content">--}}
                {{--<div class="tab-pane tab-submit active" id="article-tab-1" role="tabpanel" aria-expanded="true">--}}
                    {{----}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="row">
                <div class="col-md-12">
                    <div class="blog-comment">
                        <ul class="comments">
                            @foreach($oComments->where('parent_id', null) as $oComment)
                                <li class="clearfix">
                                    @include('admin.content.article.components.modals.comments.comment', [
                                        'oItem' => $oComment
                                    ])
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        {{--<button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit"--}}
                {{--data-with-form="#article-tab-1 form, #article-tab-2 form, #article-tab-3 form"--}}
        {{-->Сохранить</button>--}}
    </div>
</div>
