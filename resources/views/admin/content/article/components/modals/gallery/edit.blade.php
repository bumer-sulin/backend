<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">
            Редактирование
            @include('admin.components.tooltip.question', [
                'title' => 'Вкладки сохраняются по отдельности.'
            ])
        </h4>
        <div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: right;">
            <div class="col-12">
                {{ $oItem->title }}
            </div>
        </div>
        {{--<div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: left;">--}}
            {{--<div class="col-12 alert alert-success" style="padding-left: 0;">--}}
                {{--Вкладки сохраняются по отдельности, вкладки 'Заголовок' закрывает панель--}}
            {{--</div>--}}
        {{--</div>--}}
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12 mb-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#article-tab-1" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Заголовок</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#article-tab-4" role="tab" aria-controls="messages" aria-expanded="false" data-hidden-submit="1">Галерея</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane tab-submit active" id="article-tab-1" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          id="{{$sComposerRouteView}}-from"
                          action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
                          data-counter=".admin-table-counter"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="refreshAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Заголовок<i class="r">*</i></label>
                                <input type="text" class="form-control" name="title" placeholder="Заголовок" value="{{ $oItem->title ?? '' }}"
                                       maxlength="100"
                                       data-counter="1"
                                       data-counter-label="Количество допустимых символов: {remaining}."
                                       required
                                >
                                <span class="help">
                                    Заголовок не может быть больше 100 символов.
                                </span>
                                <br>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Дата публикации</label>
                                <input type="date" class="form-control" name="published_at" placeholder="Дата публикации" value="{{ !is_null($oItem->published_at) ? $oItem->published_at->format('Y-m-d') : now()->format('Y-m-d') }}">
                                <span class="help">
                                    Публикуются статьи только у которых дата публикации меньше "сегодня". Новым считается статья, которая была добавлена не позже 7 дней назад.
                                </span>
                            </div>
                        </div>
                        @foreach($oMainOptions as $oOption)
                            <div class="col-12">
                                <div class="form-group">
                                    <label>
                                        {{ $oOption->title }} @if(!is_null($oOption->description))({{ $oOption->description }})@endif
                                    </label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-tags"></i></span>
                                        @if($oOption->isList())
                                            <select class="selectpicker form-control" multiple name="options[{{ $oOption->key }}][]">
                                                @foreach($oOption->parameters as $oParameter)
                                                    <option value="{{ $oParameter->id }}" @if(!is_null($oItem->values->where('parameter_id', $oParameter->id)->first())) selected @endif>{{ $oParameter->quantity }}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <input type="text" class="form-control" name="options[{{ $oOption->key }}]" placeholder="{{ $oOption->title }}" value="{{ isset($oValues[$oOption->key]) ? $oValues[$oOption->key]->value : ''}}">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </form>
                </div>
                <div class="tab-pane tab-submit" id="article-tab-4" role="tabpanel" aria-expanded="false">
                    @include('admin.components.gallery.gallery', [
                        'priorityUrl' => route($sComposerRouteView.'.action.post', ['name' => 'imagePriority']),
                        'uploadUrl' => route($sComposerRouteView.'.image.upload.post', ['id' => $oItem->id]),
                        'model' => $sComposerRouteView,
                        'oItem' => $oItem,
                        'form' => '#'.$sComposerRouteView.'-form'
                    ])
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit"
                {{--data-with-form="#article-tab-1 form, #article-tab-2 form, #article-tab-3 form"--}}
        >Сохранить</button>
    </div>
</div>
