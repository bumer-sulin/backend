@extends('admin.layouts.admin')

@section('content.title')
    @include('admin.components.pages.title', [
        'title' => isset($aComposerContent[$sComposerRouteView]['title']) ? $aComposerContent[$sComposerRouteView]['title'] : '',
    ])
@endsection

@section('content')
    <section class="section" style="padding-top: 0;">
        <div class="container">
            <nav class="level">
                <!-- Left side -->
                <div class="level-left">
                    <div class="level-item">
                        <p class="subtitle is-5">
                            @include('admin.content.components.index.find', [
                                'oItems' => $oItems,
                                'units' => ['партнер', 'партнера', 'партнеров'],
                                'gender' => false
                            ])
                        </p>
                    </div>
                </div>

                <!-- Right side -->
                <div class="level-right">
                    <p class="level-item">
                        <a class="button trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route($sComposerRouteView.'.create.modal.post') }}">Добавить</a>
                    </p>
                </div>
            </nav>
            <div class="admin-table table-component-pagination">
                @include('admin.content.'.$sComposerRouteView.'.components.table', [
                    'oItems' => $oItems
                ])
            </div>
        </div>
    </section>
@endsection


@section('modals')
    @include('admin.pages.dialogs.components.ajax')
    @include('admin.pages.dialogs.components.confirm')
    @include('admin.pages.dialogs.components.form')
    @include('admin.pages.dialogs.components.info')
@endsection
