<a class="button is-small trigger" data-dialog="#pages-dialogs-ajax"
   data-ajax data-action="{{ route($sComposerRouteView.'.files.post', ['id' => $oItem->id]) }}"
   data-ajax-init="uploader"
>
    <span class="icon is-small">
        <i class="fa fa-file-o"></i>
    </span>
</a>