<?php
    $model = isset($model) ? $model : $sComposerRouteView;
    $list = isset($list) ? $list : '.admin-table';
    $action = isset($action) ? $action : route($model.'.view.post');
    $alias = isset($alias) ? $alias : 'status';
?>
<div class="">
    <select class="form-control form-control-sm ajax-select" action="{{ isset($url) ? $url : route($model.'.status', ['id' => $oItem->id]) }}"
            data-status="1"
            name="status"
            data-list="{{ $list }}"
            data-list-action="{{ $action }}"
            data-form-data=".pagination-form"
            data-callback="refreshAfterSubmit {{ isset($sCallbacks) ? ', '.$sCallbacks : '' }}"
    >
        @foreach(Model::init($model)->getStatuses() as $key => $status)
            @if(isset($exceptId) && $exceptId === $key)

            @else
                <option value="{{ $key }}" @if($key === (int)$oItem->{$alias}) selected @endif>{{ $status }}</option>
            @endif
        @endforeach
    </select>
</div>
