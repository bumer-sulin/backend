<div class="form-group float-right">
    <select class="form-control ajax-select" name="{{ $name }}" style="height: 35px;width: 200px;margin-right: 5px;"
            action="{{ route($sComposerRouteView.'.query') }}"
            data-list=".admin-table"
            data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
            data-callback="refreshAfterSubmit"
            data-loading-container=".admin-table"
            data-loading-container=".admin-table"
            data-null-value="{{ isset($nullValue) ? $nullValue : 0 }}"
    >
        <option value="{{ isset($nullValue) ? $nullValue : 0 }}">Все @if(isset($title)){{ mb_strtolower($title) }}@endif</option>
        @if(isset($parentable) && $parentable)
            @if(isset($keyId) && $keyId)
                @foreach($values->where('parent_id', null) as $key => $oValue)
                    <?php
                    $oItem = $values->get($oValue->id);
                    $value = (Session::exists($sComposerRouteView) && Session::has($sComposerRouteView.'.query') && isset(Session::get($sComposerRouteView.'.query')[$name])) ? intval(Session::get($sComposerRouteView.'.query')[$name]) : 0;
                    $prefix = '';
                    ?>
                    <option value="{{ $oItem->id }}" @if(isset($value) && intval($oItem->id) === $value) selected @endif>{{ $prefix ?? '' }}{{ $oItem->title }}</option>
                    @if(count($values->get($oItem->id)->children) !== 0)
                        @foreach($values->get($oItem->id)->children as $oChildren)
                            <?php
                            $oItem = $values->get($oChildren->id);
                            $value = (Session::exists($sComposerRouteView) && Session::has($sComposerRouteView.'.query') && isset(Session::get($sComposerRouteView.'.query')[$name])) ? intval(Session::get($sComposerRouteView.'.query')[$name]) : 0;
                            $prefix = ' - ';
                            ?>
                            <option value="{{ $oItem->id }}" @if(isset($value) && intval($oItem->id) === $value) selected @endif>{{ $prefix ?? '' }}{{ $oItem->title }}</option>
                            @if(count($values->get($oItem->id)->children) !== 0)
                                @foreach($values->get($oItem->id)->children as $oChild)
                                    <?php
                                    $oItem = $values->get($oChild->id);
                                    $value = (Session::exists($sComposerRouteView) && Session::has($sComposerRouteView.'.query') && isset(Session::get($sComposerRouteView.'.query')[$name])) ? intval(Session::get($sComposerRouteView.'.query')[$name]) : 0;
                                    $prefix = ' - - ';
                                    ?>
                                    <option value="{{ $oItem->id }}" @if(isset($value) && intval($oItem->id) === $value) selected @endif>{{ $prefix ?? '' }}{{ $oItem->title }}</option>
                                @endforeach
                            @endif
                        @endforeach
                    @endif
                @endforeach
            @else
                @foreach($values->where('parent_id', null) as $key => $oValue)
                    @include('admin.components.select.parentable', [
                        'oItem' => $values,
                        'value' => (Session::exists($sComposerRouteView) && Session::has($sComposerRouteView.'.query') && isset(Session::get($sComposerRouteView.'.query')[$name])) ? intval(Session::get($sComposerRouteView.'.query')[$name]) : 0
                    ])
                @endforeach
            @endif
        @else
            <?php
            $keyTitle = isset($keyTitle) ? $keyTitle : 'title';
            ?>
            @foreach($values as $key => $oValue)
                @if(isset($oValue->id) && isset($oValue->{$keyTitle}))
                    <option value="{{ $oValue->id }}"
                            @if(Session::exists($sComposerRouteView) && Session::has($sComposerRouteView.'.query') &&
                                isset(Session::get($sComposerRouteView.'.query')[$name]) &&
                                intval(Session::get($sComposerRouteView.'.query')[$name]) === $oValue->id)
                            selected
                            @endif
                    >{{ $oValue->{$keyTitle} }}</option>
                @else
                    <option value="{{ $key }}"
                            @if(Session::exists($sComposerRouteView) && Session::has($sComposerRouteView.'.query') &&
                                isset(Session::get($sComposerRouteView.'.query')[$name]) &&
                                intval(Session::get($sComposerRouteView.'.query')[$name]) === intval($key))
                            selected
                            @endif
                    >{{ isset($oValue['title']) ? $oValue['title'] : $oValue }}</option>
                @endif
            @endforeach
        @endif
    </select>
</div>
