<button class="btn btn-sm float-right ajax-link"
        action="{{ route($sComposerRouteView.'.query', ['all' => true]) }}"
        data-search-container=".breadcrumb + div"
        data-callback="resetSearchFilters, refreshAfterSubmit"
        data-list=".admin-table"
        data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
        style="margin: 5px 10px 5px 5px;"
        title="Очистить фильтры"
        data-loading="1"
>
    <i class="fa fa-close"></i>
</button>
