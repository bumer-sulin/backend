<td class="is-select">
    <p class="control">
        <span class="select is-small">
            <select class="ajax-select" action="{{ route($sComposerRouteView.'.status', ['id' => $oItem->id]) }}"
                    data-status="1"
                    name="status"
                    data-list=".admin-table"
                    data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                    data-form-data=".pagination-form"
                    data-callback="refreshAfterSubmit"
            >
                @if($oItem->status)
                    <option value="1" selected>Опубликовано</option>
                    <option value="0">Не опубликовано</option>
                @else
                    <option value="1">Опубликовано</option>
                    <option value="0" selected>Не опубликовано</option>
                @endif
            </select>
        </span>
    </p>
</td>