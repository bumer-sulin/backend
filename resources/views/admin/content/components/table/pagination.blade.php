@if($oItems instanceof \Illuminate\Pagination\LengthAwarePaginator && ($oItems->currentPage() === $oItems->lastPage() || $oItems->hasMorePages()))
    @if(isset($aSearch))
        {{ $oItems->appends($aSearch)->links('admin.components.pagination.bootstrap') }}
    @else
        {{ $oItems->links('admin.components.pagination.bootstrap') }}
    @endif
@endif
