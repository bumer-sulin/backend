<a class="button is-small trigger" data-dialog="#pages-dialogs-ajax" data-ajax
   data-action="{{ route($sComposerRouteView.'.text.modal.post', ['id' => $oItem->id]) }}"
   {{--data-id="123"--}}
>
    <span class="icon is-small">
        <i class="fa fa-edit"></i>
    </span>
</a>
{{ str_limit(strip_tags($oItem->text), 20) }}