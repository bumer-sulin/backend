<?php
$model = isset($model) ? $model : $sComposerRouteView;
?>
<a class="btn btn-primary btn-sm trigger {{ isset($disabled) && $disabled ? 'disabled' : '' }}" data-dialog="#custom-edit-modal" data-ajax
   data-action="{{ $url ?? route($model.'.edit.modal.post', ['id' => $oItem->id]) }}"
   data-ajax-init="{{ $init ?? '' }}"
   data-edit="{{ $oItem->id }}"
   {{--data-id="123"--}}
>
    @if(isset($text))
        <span style="color: #fff;">{{ $text }}</span>
    @else
        {{--<i class="fa {{ $fa or 'fa-pencil'}}" style="color: #fff;"></i>--}}
        <i class="icon-pencil" style="color: #fff;"></i>
    @endif
</a>
