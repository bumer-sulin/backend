<table class="table table-admin table--mobile">
    <thead>
    <tr>
        @if($oComposerGenerateTable && isset($oComposerModelColumns[$sComposerRouteView]['table']))
            @foreach($oComposerModelColumns[$sComposerRouteView]['table'] as $value)
                <th>{{ $value }}</th>
            @endforeach
        @else
            <th>#</th>
            <th>IMG</th>
            <th>Заголовок</th>
            <th></th>
            <th>Статус</th>
        @endif
        <th>Изменить</th>
        <th>Удалить</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        @if($oComposerGenerateTable && isset($oComposerModelColumns[$sComposerRouteView]['table']))
            @foreach($oComposerModelColumns[$sComposerRouteView]['table'] as $value)
                <th>{{ $value }}</th>
            @endforeach
        @else
            <th>#</th>
            <th>IMG</th>
            <th>Заголовок</th>
            <th></th>
            <th>Статус</th>
        @endif
        <th>Изменить</th>
        <th>Удалить</th>
    </tr>
    </tfoot>
    <tbody>
    @foreach($oItems as $oItem)
        <tr>
            @if($oComposerGenerateTable && isset($oComposerModelColumns[$sComposerRouteView]['table']))
                @foreach($oComposerModelColumns[$sComposerRouteView]['table'] as $key => $column)
                    @if(View::exists('admin.content.components.table.columns.'.$key))
                        @include('admin.content.components.table.columns.'.$key, [
                            'oItem' => $oItem
                        ])
                    @endif
                @endforeach
            @else
                <td>{{ $oItem->id }}</td>
                <td style="width: 30px;">
                    <a class="button is-small trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route($sComposerRouteView.'.image.post', ['id' => $oItem->id]) }}"
                       data-ajax-init="uploader"
                       data-id="123"
                       style="padding: 0; border: none;"
                    >
                        <img src="{{ ImagePath::cache()->main($sComposerRouteView, 'original', $oItem) }}" width="30">
                    </a>
                </td>
                <td>
                    <a href="{{ route($sComposerRouteView.'.show', ['id' => $oItem->id]) }}">{{ $oItem->title }}</a>
                </td>
                <td></td>
            @endif
            <td>
                <a class="button is-small trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route($sComposerRouteView.'.edit.modal.post', ['id' => $oItem->id]) }}" data-id="123">
                    <span class="icon is-small">
                        <i class="fa fa-pencil"></i>
                    </span>
                </a>
            </td>
            <td>
                <a class="button is-small trigger" data-dialog="#pages-dialogs-confirm" data-confirm
                   data-text='Действительно хотите удалить "{{ $oItem->id }}" ?'
                   data-action="{{ route($sComposerRouteView.'.destroy', ['id' => $oItem->id]) }}"
                   data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                >
                    <span class="icon is-small">
                        <i class="fa fa-trash"></i>
                    </span>
                </a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@include('admin.content.components.table.pagination', [     'oItems' => $oItems ]))
    @if(isset($aSearch))
        {{ $oItems->appends($aSearch)->links('admin.components.pagination.ajax') }}
    @else
        {{ $oItems->links('admin.components.pagination.ajax') }}
    @endif
@endif
