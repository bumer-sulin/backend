{{--
    'model' => 'products',
    'aQuery' => [
        'status' => [
            'value' => '1',
            'title' => 'Опубликованные'
        ]
    ]
--}}

<p class="level-item level-link-query">
    <a
            @if(!Session::exists($model) || !Session::has($model.'.query'))
            class="is-active ajax-link"
            @else
            class="ajax-link"
            @endif
            action="{{ route($model.'.query') }}"
            data-items=".level-link-query a, .level-link-query select"
            data-has-active="1"
            data-all="1"
            data-list=".admin-table"
            data-counter=".admin-table-counter"
            data-list-action="{{ route($model.'.view.post') }}"
            data-form-data=".pagination-form"
            data-callback="refreshAfterSubmit"
    >
        Все
    </a>
</p>
@foreach($aQuery as $key => $query)
    <p class="level-item level-link-query">
        @if(isset($query['type']) && $query['type'] === 'select' && isset($query['category']))
            <span class="select">
                <select @if(Session::exists($model) && Session::has($model.'.query') && isset(Session::get($model.'.query')[$key]))
                        class="is-active ajax-select"
                        @else
                        class="ajax-select"
                        @endif
                        name="{{ $key }}"
                        action="{{ route($model.'.query') }}"
                        data-items=".level-link-query a, .level-link-query select"
                        data-has-active="1"
                        data-{{$key}}="{{ $query['value'] }}"
                        data-counter=".admin-table-counter"
                        data-list=".admin-table"
                        data-list-action="{{ route($model.'.view.post') }}"
                        data-form-data=".pagination-form"
                        data-callback="refreshAfterSubmit"
                >
                    <option value="0">Категория не выбрана</option>
                    @foreach($oComposerCategories->where('parent_id', null) as $oCategory)
                        @if(Session::exists($model) && Session::has($model.'.query') && isset(Session::get($model.'.query')[$key]) && intval(Session::get($model.'.query')[$key]) === $oCategory->id)
                            @include('admin.content.components.form.select.parentable', [
                                'oItem' => $oCategory,
                                'id' => intval(Session::get($model.'.query')[$key])
                            ])
                        @else
                            @include('admin.content.components.form.select.parentable', [
                                'oItem' => $oCategory,
                            ])
                        @endif
                    @endforeach
                </select>
            </span>
        @else
            @if(is_array($query) && !isset($query['value']))
                @foreach($query as $q)
                    <a
                            @if(Session::exists($model) && Session::has($model.'.query') && isset(Session::get($model.'.query')[$key]) && Session::get($model.'.query')[$key] === $q['value'])
                            class="is-active ajax-link"
                            @else
                            class="ajax-link"
                            @endif
                            action="{{ route($model.'.query') }}"
                            data-items=".level-link-query a, .level-link-query select"
                            data-has-active="1"
                            data-{{$key}}="{{ $q['value'] }}"
                            data-counter=".admin-table-counter"
                            data-list=".admin-table"
                            data-list-action="{{ route($model.'.view.post') }}"
                            data-form-data=".pagination-form"
                            data-callback="refreshAfterSubmit"
                    >
                        {{ $q['title'] }}
                    </a>
                    &emsp;
                @endforeach
            @else
                <a
                        @if(Session::exists($model) && Session::has($model.'.query') && isset(Session::get($model.'.query')[$key]))
                        class="is-active ajax-link"
                        @else
                        class="ajax-link"
                        @endif
                        action="{{ route($model.'.query') }}"
                        data-items=".level-link-query a, .level-link-query select"
                        data-has-active="1"
                        data-counter=".admin-table-counter"
                        data-{{$key}}="{{ $query['value'] }}"
                        data-list=".admin-table"
                        data-list-action="{{ route($model.'.view.post') }}"
                        data-form-data=".pagination-form"
                        data-callback="refreshAfterSubmit"
                >
                    {{ $query['title'] }}
                </a>
                &emsp;
            @endif
        @endif
    </p>
@endforeach
