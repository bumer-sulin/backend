@if(method_exists($oItems, 'total'))
{!! Helper::wordWithCounts($oItems->total(), $units, $gender) !!}
@else
{!! Helper::wordWithCounts(count($oItems), $units, $gender) !!}
@endif

