@if(!isset($column['icon']))
    <label for="email" class="label" style="margin-bottom: 0;">{{ $column['title'] }}</label>
@endif
<p @if(isset($column['icon'])) class="control has-icon" @else class="control" @endif>
    <input
            class="input"
            name="{{ $key }}"
            type="email"
            placeholder="{{ $column['title'] }}"
            value="{{ $oItem->$key or ''}}"
            {{--required--}}
    >
    @if(isset($column['icon']))
        <span class="icon is-small">
            <i class="fa {{ $column['icon'] }}"></i>
        </span>
    @endif
</p>