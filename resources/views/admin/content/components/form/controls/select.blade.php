@if(!isset($column['icon']))
    <label for="email" class="label" style="margin-bottom: 0;">{{ $column['title'] }}</label>
@endif
<p class="control">
    <span class="select" style="width: 100%;">
        <select name="{{ $key }}" style="width: 100%;">
            @if(isset(${$column['select']['cache']}))
                @foreach(${$column['select']['cache']} as $oSelect)
                    <option value="{{ $oSelect->{$column['select']['key']} }}" @if(isset($oItem) && $oSelect->{$column['select']['key']} === $oItem->{$key}) selected @endif>{{ $oSelect->{$column['select']['value']} }}</option>
                @endforeach
            @endif
        </select>
    </span>
</p>