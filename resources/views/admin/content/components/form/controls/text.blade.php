@if(!isset($column['icon']))
    <label for="email" class="label" style="margin-bottom: 0;">{{ $column['title'] }}</label>
@endif
<p @if(isset($column['icon'])) class="control has-icon" @else class="control" @endif>
    <textarea id="parent" class="textarea" name="{{ $key }}" placeholder="{{ $column['title'] }}">{{ $oItem->$key or ''}}</textarea>
    @if(isset($column['icon']))
        <span class="icon is-small">
            <i class="fa {{ $column['icon'] }}"></i>
        </span>
    @endif
</p>