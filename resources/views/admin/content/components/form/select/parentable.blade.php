<option value="{{ $oItem->id }}" @if(isset($id) && $oItem->id === $id) selected @endif>@if(isset($prefix)) {{ $prefix }} @endif {{ $oItem->title }}</option>
@if(!empty($oItem->children) && !empty($oItem->children[0]))
    @foreach($oItem->children as $oChildren)
        @include('admin.content.components.form.select.parentable', [
            'oItem' => $oChildren,
            'prefix' => isset($prefix) ? $prefix.' - ' : ' - '
        ])
    @endforeach
@endif
