@extends('admin.layouts.admin')

@section('content.title')
    @include('admin.components.pages.title', [
        'title' => isset($aComposerContent[$sComposerRouteView]['title']) ? $aComposerContent[$sComposerRouteView]['title'] : '1',
    ])
@endsection

@section('breadcrumb-right')
    <button class="btn btn-primary float-right trigger"
            data-ajax
            data-action="{{ route($sComposerRouteView.'.create.modal.post') }}"
            data-dialog="#custom-edit-modal"
    >
        <i class="fa fa-plus" style="margin-right: 10px;"></i>
        Добавить
    </button>
    @include('admin.content.components.table.search.reset')
    @include('admin.content.components.table.search.query', [
        'title' => 'Статусы',
        'name' => 'status',
        'values' => Model::init($sComposerRouteView)->getStatuses(),
        'keyId' => true,
        'nullValue' => '--'
    ])
    @include('admin.content.components.table.search.query', [
        'title' => 'id',
        'name' => 'id',
        'values' => $oComposerUsers->sortBy('id'),
        'keyTitle' => 'id',
    ])
@endsection

@section('content')

    <div class="row hidden">
        <div class="col-12">
            <form class="card ajax-form" action="{{ route($sComposerRouteView.'.search.post', ['page' => 1]) }}" method="post"
                  data-loading-container=".admin-table"
                  data-append="1"
                  data-append-container=".admin-table"
            >
                <div class="card-block">
                    <div class="row">
                        <div class="col-12 col-lg-8">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Поиск" name="search">
                            </div>
                        </div>
                        <div class="col-12 col-lg-4">
                            <div class="form-group row" style="margin-right: 0px;">
                                <label class="col-md-3 form-control-label" style="margin-top: 7px;margin-bottom: 5px;">Статус:</label>
                                <select class="col-md-9 form-control" name="status">
                                    <option value="">Все</option>
                                    @foreach(Model::init($sComposerRouteView)->getStatuses() as $key => $status)
                                        <option value="{{ $key }}">{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row float-right">
                        <div class="col-12 col-lg-4">
                            <div class="form-group" style="margin-bottom: 0;">
                                <button type="submit" class="btn btn-primary inner-form-submit">
                                    <i class="fa fa-search" style="margin-right: 10px;"></i>
                                    Найти
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                    <div class="row">
                        <div class="col-12">
                            <div class="admin-table table-component-pagination">
                                @if(count($oItems) !== 0)
                                    @include('admin.content.'.$sComposerRouteView.'.components.table', [
                                        'oItems' => $oItems
                                    ])
                                @else
                                    <div class="text-muted page-desc">Список пуст</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
