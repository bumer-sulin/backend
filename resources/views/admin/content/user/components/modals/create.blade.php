<form class="modal-content dialog__content ajax-form"
      action="{{ route($sComposerRouteView.'.store') }}"
      data-counter=".admin-table-counter"
      data-list=".admin-table"
      data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
      data-callback="closeModalAfterSubmit, refreshAfterSubmit"
      novalidate
>
    <div class="modal-header">
        <h4 class="modal-title">Добавить</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label>Имя<i class="r">*</i></label>
                        <input type="text" class="form-control" name="first_name" placeholder="Имя" value="" required>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Фамилия<i class="r">*</i></label>
                        <input type="text" class="form-control" name="last_name" placeholder="Фамилия" value="" required>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Email<i class="r">*</i></label>
                        <input type="text" class="form-control" name="email" placeholder="Email" value="" required>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Роль</label>
                        <select class="form-control" name="role_id">
                            @foreach($oRoles as $key => $oRole)
                                <option value="{{ $oRole->id }}" {{ $oRole->slug === 'user' ? 'selected' : '' }}>{{ $oRole->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Новый Пароль<i class="r">*</i></label>
                        <input type="password" class="form-control" name="password" placeholder="Новый Пароль" value="" required>
                    </div>
                </div><div class="col-12">
                    <div class="form-group">
                        <label>Повторить пароль<i class="r">*</i></label>
                        <input type="password" class="form-control" name="password_confirmation" placeholder="Повторить пароль" value="" required>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-primary inner-form-submit">Сохранить</button>
    </div>
</form>
