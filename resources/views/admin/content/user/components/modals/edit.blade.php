<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">Редактирование</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12 mb-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#user-edit-tab-1" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Данные</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#user-edit-tab-2" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Доступы</a>
                </li>
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" data-toggle="tab" href="#user-edit-tab-4" role="tab" aria-controls="profile" aria-expanded="false" data-hidden-submit="0">Соцсети</a>--}}
                {{--</li>--}}
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#user-edit-tab-5" role="tab" aria-controls="messages" aria-expanded="false" data-hidden-submit="1">Галерея</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane tab-submit active" id="user-edit-tab-1" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          id="user-from"
                          action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
                          data-counter=".admin-table-counter"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Имя<i class="r">*</i></label>
                                <input type="text" class="form-control" name="first_name" placeholder="Имя" value="{{ $oItem->first_name }}" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Фамилия<i class="r">*</i></label>
                                <input type="text" class="form-control" name="last_name" placeholder="Фамилия" value="{{ $oItem->last_name }}" required>
                            </div>
                        </div>
                        {{--<div class="col-12">--}}
                            {{--<div class="form-group">--}}
                                {{--<label>Фамилия<i class="r">*</i></label>--}}
                                {{--<input type="text" class="form-control" name="last_name" placeholder="Фамилия" value="{{ $oItem->last_name }}" required>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="col-12">
                            <div class="form-group">
                                <label>Email<i class="r">*</i></label>
                                <input type="text" class="form-control" name="email" placeholder="Email" value="{{ $oItem->email }}" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Роль</label>
                                <select class="form-control" name="role_id">
                                    <option value="">Нет роли</option>
                                    @foreach($oRoles as $key => $oRole)
                                        <option value="{{ $oRole->id }}" {{ $oRole->id === $oItem->role->id ? 'selected' : '' }}>{{ $oRole->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane tab-submit" id="user-edit-tab-2" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
                          data-callback="closeModalAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        <input type="hidden" name="first_name" value="{{ $oItem->first_name }}">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Email<i class="r">*</i></label>
                                <input type="text" class="form-control" name="email" placeholder="Email" value="{{ $oItem->email }}" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Текущий Пароль<i class="r">*</i></label>
                                <input type="password" class="form-control" name="old_password" placeholder="Текущий Пароль" value="" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Новый Пароль<i class="r">*</i></label>
                                <input type="password" class="form-control" name="password" placeholder="Новый Пароль" value="" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Повторить пароль<i class="r">*</i></label>
                                <input type="password" class="form-control" name="password_confirmation" placeholder="Повторить пароль" value="" required>
                            </div>
                        </div>
                    </form>
                </div>
                {{--<div class="tab-pane tab-submit" id="user-edit-tab-4" role="tabpanel" aria-expanded="true">--}}
                    {{--<form class="row ajax-form"--}}
                          {{--action="{{ route($sComposerRouteView.'.action.item.post', ['id' => $oItem->id, 'action' => 'updateUserSocials']) }}"--}}
                          {{--data-callback="closeModalAfterSubmit"--}}
                    {{-->--}}
                        {{--@foreach($oItem->socials as $oSocial)--}}
                            {{--<div class="col-8">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label>{{ $oSocial->title }} <small>ссылка</small></label>--}}
                                    {{--<input type="text" class="form-control" name="socials[{{ $oSocial->id }}][link]" placeholder="Ссылка" value="{{ $oSocial->link }}">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-4">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label>Статус</label>--}}
                                    {{--<select class="form-control"--}}
                                            {{--name="socials[{{ $oSocial->id }}][status]"--}}
                                    {{-->--}}
                                        {{--@foreach(Model::init('social')->getStatuses() as $key => $status)--}}
                                            {{--<option value="{{ $key }}" @if($key === $oSocial->status) selected @endif>{{ $status }}</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--@endforeach--}}

                        {{--@if($oItem->role->slug === 'admin')--}}
                            {{--<div class="col-12">--}}
                                {{--<div class="form-group">--}}
                                    {{--<label>Сайт</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--@foreach($oComposerSocials as $oSocial)--}}
                                {{--<div class="col-8">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label>{{ $oSocial->title }} <small>ссылка</small></label>--}}
                                        {{--<input type="text" class="form-control" name="socials[{{ $oSocial->id }}][link]" placeholder="Ссылка" value="{{ $oSocial->link }}">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-4">--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label>Статус</label>--}}
                                        {{--<select class="form-control"--}}
                                                {{--name="socials[{{ $oSocial->id }}][status]"--}}
                                        {{-->--}}
                                            {{--@foreach(Model::init('social')->getStatuses() as $key => $status)--}}
                                                {{--<option value="{{ $key }}" @if($key === $oSocial->status) selected @endif>{{ $status }}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--@endforeach--}}
                        {{--@endif--}}
                    {{--</form>--}}
                {{--</div>--}}
                <div class="tab-pane tab-submit" id="user-edit-tab-5" role="tabpanel" aria-expanded="false">
                    @include('admin.components.gallery.gallery', [
                        'priorityUrl' => route($sComposerRouteView.'.action.post', ['name' => 'imagePriority']),
                        'uploadUrl' => route($sComposerRouteView.'.image.upload.post', ['id' => $oItem->id]),
                        'model' => 'user',
                        'oItem' => $oItem,
                        'form' => '#user-form'
                    ])
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit">Сохранить</button>
    </div>
</div>

{{--
<div class="dialog__content is-vcentered box" style="min-width: 300px; max-width: 300px;">
    <h3 class="title">
        Изменить
    </h3>
    <form class="ajax-form" role="form" method="POST" action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
          data-counter=".admin-table-counter"
          data-list=".admin-table"
          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
          data-form-data=".pagination-form"
          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
          style="text-align: left;"
    >
        {{ csrf_field() }}
        <p class="control has-icon" >
            <input
                    class="input"
                    type="text"
                    name="first_name"
                    placeholder="Name"
                    value="{{ $oItem->first_name }}"
                    required
            >
                <span class="icon is-small">
                    <i class="fa fa-user"></i>
                </span>
        </p>
        <p class="control has-icon" >
            <input
                    class="input"
                    name="email"
                    type="email"
                    placeholder="Email"
                    value="{{ $oItem->email }}"
                    required
            >
                <span class="icon is-small">
                    <i class="fa fa-envelope"></i>
                </span>
        </p>
        <p class="control" >
            <span class="select" style="width: 100%;">
                <select name="role_id" style="width: 100%;">
                    @foreach($oComposerRoles as $role)
                        <option value="{{ $role->id }}" @if($role->id === $oItem->role->id) selected @endif>{{ $role->name }}</option>
                    @endforeach
                </select>
            </span>
        </p>
        <p class="control has-icon" >
            <input
                    class="input"
                    type="password"
                    name="password"
                    placeholder="New password"

            >
                <span class="icon is-small">
                    <i class="fa fa-lock"></i>
                </span>
        </p>
        <p class="control has-icon" >
            <input
                    class="input"
                    type="password"
                    name="password_confirmation"
                    placeholder="Confirm password"

            >
                <span class="icon is-small">
                    <i class="fa fa-lock"></i>
                </span>
        </p>
        <p class="control">
            <button class="button is-primary inner-form-submit" type="submit">
                Изменить
            </button>
            <button class="dialog__close button" type="button" data-dialog="#pages-dialogs-ajax">
                Cancel
            </button>
        </p>
    </form>
    <div class="close--container">
        <a class="dialog__close icon" data-dialog="#pages-dialogs-ajax">
            <i class="fa fa-times"></i>
        </a>
    </div>
</div>
 --}}
