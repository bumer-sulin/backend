<table class="table table-admin table--mobile __with-edit-delete">
    <thead>
    <tr>
        <th class="is-id">#</th>
        <th class="is-image"></th>
        <th>Имя</th>
        <th>Email</th>
        <th>Роль</th>
        <th class="is-status">Статус</th>
        <th>Изменить</th>
        <th>Удалить</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>#</th>
        <th></th>
        <th>Имя</th>
        <th>Email</th>
        <th>Роль</th>
        <th>Статус</th>
        <th>Изменить</th>
        <th>Удалить</th>
    </tr>
    </tfoot>
    <tbody>
    @foreach($oItems as $oItem)
        <tr @if(!$oItem->status) style="opacity: .5;" @endif>
            <td>{{ $oItem->id }}</td>
            <td>
                @include('admin.content.components.table.image', [
                    'oItem' => $oItem,
                    'path' => 'square'
                ])
            </td>
            <td>
                @if(isset($oItem->activation))
                    @if(!$oItem->activation->completed)
                        @include('admin.components.tooltip.question', [
                            'title' => 'Пользователь не активирован',
                            'icon' => 'exclamation',
                            'type' => 'danger'
                        ])
                        <span class="icon is-danger tippy" title="Пользователь не активирован">
                            <i class="fa fa-exclamation"></i>
                        </span>
                    @endif
                @else
                    @include('admin.components.tooltip.question', [
                        'title' => 'Пользователь не активирован',
                        'icon' => 'exclamation',
                        'type' => 'danger'
                    ])
                @endif
                @if(!Sentinel::guest() && Sentinel::getUser()->id === $oItem->id)
                    @include('admin.components.tooltip.question', [
                        'title' => 'Текущий пользователь',
                        'icon' => 'user',
                        'type' => 'success'
                    ])
                @endif
                {{ $oItem->first_name }}
            </td>
            <td>
                {{ $oItem->email }}
            </td>
            <td>
                {{ $oItem->role->name ?? 'Неизвестно' }}
            </td>
            <td style="text-align: center;">
                @include('admin.content.components.table.status', [
                    'oItem' => $oItem
                ])
            </td>
            <td>
                @include('admin.content.components.table.edit', [
                    'oItem' => $oItem
                ])
            </td>
            <td>
                @include('admin.content.components.table.delete', [
                    'oItem' => $oItem,
                    'deleteKey' => 'пользователя',
                    'deleteValue' => $oItem->first_name,
                    'disabled' => Sentinel::getUser()->id === $oItem->id || (isset($oItem->role->slug) && $oItem->role->slug === 'developer') ? true : false
                ])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
@include('admin.content.components.table.pagination', [
    'oItems' => $oItems
])
