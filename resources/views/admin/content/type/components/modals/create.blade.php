<form class="modal-content dialog__content ajax-form"
      action="{{ route($sComposerRouteView.'.store') }}"
      data-counter=".admin-table-counter"
      data-list=".admin-table"
      data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
      data-callback="closeModalAfterSubmit, refreshAfterSubmit"
      novalidate
>
    <div class="modal-header">
        <h4 class="modal-title">Добавить</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label>Заголовок<i class="r">*</i></label>
                        <input type="text" class="form-control" name="title" placeholder="Заголовок" value="" required>
                    </div>
                </div>
                {{--<div class="col-12">--}}
                    {{--<div class="form-group">--}}
                        {{--<label>Системное имя<i class="r">*</i></label>--}}
                        {{--<input type="text" class="form-control" name="url" placeholder="Системное имя" value="" required>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="col-12">
                    <div class="form-group">
                        <label>Приоритет<i class="r">*</i></label>
                        <input type="number" class="form-control" name="priority" placeholder="Приоритет" value="0" required>
                        <span class="help">
                            Чем больше, тем выше будет находится тип внутри своего родителя.
                        </span>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Родитель</label>
                        <select class="form-control" name="parent_id">
                            <option value="">Главная (Нет родителя)</option>
                            @foreach($oTypes->where('parent_id', null) as $key => $oType)
                                @include('admin.components.select.parentable', [
                                    'oItem' => $oType
                                ])
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Статус</label>
                        <select class="form-control" name="status">
                            @foreach(Model::init($sComposerRouteView)->getStatuses() as $key => $value)
                                <option value="{{ $key }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-primary inner-form-submit">Сохранить</button>
    </div>
</form>
