<tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
    <td>{{ $oItem->id }}</td>
    <td>
        {{ $prefix ?? '' }}
        <b>{{ $oItem->title }}</b>
    </td>
    <td style="text-align: center">
        {{ count($oItem->articles) }}
    </td>
    <td style="text-align: center">
        {{ $oItem->priority }}
    </td>
    <td>
        @include('admin.content.components.table.status', [
            'oItem' => $oItem
        ])
    </td>
    <td>
        @include('admin.content.components.table.edit', [
            'oItem' => $oItem,
            'init' => 'uploader, initSummernote'
        ])
    </td>
    <td>
        @include('admin.content.components.table.delete', [
            'oItem' => $oItem,
            'deleteKey' => 'категорию',
            'subtitle' => 'При удалении категорию - удаляться все её подкатегории',
            'deleteValue' => $oItem->title,
        ])
    </td>
</tr>
@if($oItem->hasChildren())
    @foreach($oItem->children as $oChildren)
        @include('admin.content.'.$sComposerRouteView.'.components.parentable', [
            'oItem' => $oChildren,
            'prefix' => isset($prefix) ? $prefix.' - ' : ' - '
        ])
    @endforeach
@endif
