@if(count($oItems) !== 0)
    <table class="table table-striped __with-edit-delete">
        <thead>
        <tr>
            <th class="is-id">#</th>
            {{--<th class="is-image"></th>--}}
            <th>Заголовок</th>
            <th style="width: 120px">Кол-во статей</th>
            <th class="is-status">Цвет</th>
            <th class="is-status">Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            {{--<th></th>--}}
            <th>Заголовок</th>
            <th>Кол-во статей</th>
            <th>Цвет</th>
            <th>Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach($oItems as $oItem)
            <tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
                <td>{{ $oItem->id }}</td>
                {{--<td>--}}
                    {{--@include('admin.content.components.table.image', [--}}
                        {{--'oItem' => $oItem,--}}
                        {{--'path' => 'square'--}}
                    {{--])--}}
                {{--</td>--}}
                <td>
                    <b>{{ $oItem->title }}</b>
                </td>
                <td class="text-center">
                    {{ count($oItem->activeArticles) }}
                </td>
                <td>
                    <div style="background-color: {{ $oItem->color }}; width: 100%;height: 19px;margin: 5px 0;"></div>
                </td>
                <td>
                    @include('admin.content.components.table.status', [
                        'oItem' => $oItem
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.edit', [
                        'oItem' => $oItem,
                        'init' => 'uploader, initSummernote, colorpicker'
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.delete', [
                        'oItem' => $oItem,
                        'deleteKey' => 'альбом',
                        'deleteValue' => $oItem->title,
                    ])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @include('admin.content.components.table.pagination')
@else
    <div class="text-muted page-desc">Список пуст</div>
@endif
