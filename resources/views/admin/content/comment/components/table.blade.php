@if(count($oItems) !== 0)
    <table class="table table-striped __with-edit-delete">
        <thead>
        <tr>
            <th class="is-id">#</th>
            <th>Пользователь</th>
            <th>Текст</th>
            <th>Статья</th>
            <th>Дата</th>
            <th>
                <span class="text-success">
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                </span>
            </th>
            <th>
                <span class="text-danger">
                    <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                </span>
            </th>
            <th class="is-status">Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th>Пользователь</th>
            <th>Текст</th>
            <th>Статья</th>
            <th>Дата</th>
            <th>
                <span class="text-success">
                    <i class="fa fa-thumbs-o-up" aria-hidden="true"></i>
                </span>
            </th>
            <th>
                <span class="text-danger">
                    <i class="fa fa-thumbs-o-down" aria-hidden="true"></i>
                </span>
            </th>
            <th>Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach($oItems as $oItem)
            <tr @if(isset($oItem->approved) && !$oItem->approved) style="opacity: .5;" @endif>
                <td>{{ $oItem->id }}</td>
                <td>
                    @if(!is_null($oItem->commented))
                        <b>
                            <a href="{{ route('user.index', ['id' => $oItem->commented->id]) }}" target="_blank"
                               data-tippy-popover
                               data-tippy-content="Найти пользователя"
                            >
                                {{ $oItem->commented->first_name }}
                            </a>
                        </b>
                    @else
                        <b>Гость</b>
                    @endif
                </td>
                <td>
                    @include('admin.components.tooltip.question', [
                        'title' => $oItem->comment,
                        'icon' => 'eye'
                    ])
                    {{ \Illuminate\Support\Str::limit($oItem->comment, 70) }}
                </td>
                <td>
                    @if(!is_null($oItem->commentable))
                        <a href="{{ route('article.index', ['id' => $oItem->commentable->id]) }}" target="_blank"
                           data-tippy-popover
                           data-tippy-content="Найти статью"
                        >
                            {{ \Illuminate\Support\Str::limit($oItem->commentable->title, 70) }}
                        </a>
                    @else
                        Неизвестно
                    @endif
                </td>
                <td>{{ $oItem->created_at->format('d.m.Y H:i:s') }}</td>
                <td class="text-success">
                    {{ $oItem->loveUsers->sum('like') }}
                </td>
                <td class="text-danger">
                    {{ $oItem->loveUsers->sum('dislike') }}
                </td>
                <td>
                    @include('admin.content.components.table.status', [
                        'oItem' => $oItem,
                        'alias' => 'approved'
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.edit', [
                        'oItem' => $oItem,
                        'init' => 'tooltip'
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.delete', [
                        'oItem' => $oItem,
                        'deleteKey' => 'комментарий',
                        'deleteValue' => $oItem->comment,
                    ])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @include('admin.content.components.table.pagination')
@else
    <div class="text-muted page-desc">Список пуст</div>
@endif
