@extends('admin.layouts.admin')

@section('content.title')
    @include('admin.components.pages.title', [
        'title' => isset($aComposerContent[$sComposerRouteView]['title']) ? $aComposerContent[$sComposerRouteView]['title'] : '',
    ])
@endsection

@section('breadcrumb-right')
    <button class="btn btn-primary float-right disabled"
            data-ajax
            data-action="{{ route($sComposerRouteView.'.create.modal.post') }}"
            data-dialog="#custom-edit-modal"
            disabled
    >
        <i class="fa fa-plus" style="margin-right: 10px;"></i>
        Добавить
    </button>
    @include('admin.content.components.table.search.reset')
    @include('admin.content.components.table.search.query', [
        'title' => 'Статусы',
        'name' => 'approved',
        'values' => Model::init($sComposerRouteView)->getStatuses(),
        'keyId' => true,
        'nullValue' => '--'
    ])
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                    <div class="row">
                        <div class="col-12">
                            <div class="admin-table table-component-pagination">
                                @include('admin.content.'.$sComposerRouteView.'.components.table', [
                                    'oItems' => $oItems
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
