@if(count($oItems) !== 0)
    <table class="table table-striped __with-edit-delete">
        <thead>
        <tr>
            <th class="is-id">#</th>
            <th class="is-image"></th>
            <th>Заголовок</th>
            <th>Ссылка</th>
            <th class="text-center" style="width: 120px">Приоритет</th>
            <th class="is-status">Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th></th>
            <th>Заголовок</th>
            <th>Ссылка</th>
            <th class="text-center">Приоритет</th>
            <th>Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach($oItems as $oItem)
            <tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
                <td>{{ $oItem->id }}</td>
                <td>
                    <i class="fa {{ Model::init($sComposerRouteView)->getTypes()[$oItem->type]['fa-icon'] }}"></i>
                </td>
                <td>
                    <b>{{ $oItem->title }}</b>
                </td>
                <td>
                    <a href="{{ $oItem->link }}" target="_blank">{{ str_limit($oItem->link) }}</a>
                </td>
                <td style="text-align: center">
                    {{ $oItem->priority }}
                </td>
                <td>
                    @include('admin.content.components.table.status', [
                        'oItem' => $oItem
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.edit', [
                        'oItem' => $oItem,
                        'init' => 'multiselect'
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.delete', [
                        'oItem' => $oItem,
                        'deleteKey' => 'ссылку',
                        'deleteValue' => $oItem->title,
                    ])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @include('admin.content.components.table.pagination')
@else
    <div class="text-muted page-desc">Список пуст</div>
@endif
