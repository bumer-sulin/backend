<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">Редактирование</h4>
        <div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: right;">
            <div class="col-12">
                {{ $oItem->title }} <i class="fa {{ Model::init($sComposerRouteView)->getTypes()[$oItem->type]['fa-icon'] }}"></i>
            </div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12 mb-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#post-tab-1" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Заголовок</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane tab-submit active" id="post-tab-1" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          id="{{$sComposerRouteView}}-from"
                          action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
                          data-counter=".admin-table-counter"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Заголовок<i class="r">*</i></label>
                                <input type="text" class="form-control" name="title" placeholder="Заголовок" value="{{ $oItem->title ?? '' }}" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Тип</label>
                                <div style="padding: 0.5rem 0.75rem;border: 1px solid rgba(0, 0, 0, 0.15);">
                                    <i class="fa {{ Model::init($sComposerRouteView)->getTypes()[$oItem->type]['fa-icon'] }}"></i>
                                    - {{ Model::init($sComposerRouteView)->getTypes()[$oItem->type]['title'] }}
                                </div>
                            </div>
                        </div>
                        {{--<div class="col-12">--}}
                            {{--<div class="form-group">--}}
                                {{--<label>Тип<i class="r">*</i></label>--}}
                                {{--<div class="input-group">--}}
                                    {{--<span class="input-group-addon"><i class="fa fa-comments"></i></span>--}}
                                    {{--<select class="selectpicker form-control" name="type" title="Выберите тип социальной сети..." required>--}}
                                        {{--@foreach(Model::init($sComposerRouteView)->getTypes() as $key => $value)--}}
                                            {{--<option value="{{ $key }}" data-icon="fa {{ $value['fa-icon'] }}" {{ $oItem->type === $key ? 'selected' : '' }}>--}}
                                                {{--- {{ ucfirst($value['title']) }}--}}
                                            {{--</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="col-12">
                            <div class="form-group">
                                <label>Ссылка<i class="r">*</i></label>
                                <input type="text" class="form-control" name="link" placeholder="Ссылка" value="{{ $oItem->link ?? '' }}" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Приоритет<i class="r">*</i></label>
                                <input type="number" class="form-control" name="priority" placeholder="Приоритет" value="{{ $oItem->priority ?? '' }}" required>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit">Сохранить</button>
    </div>
</div>
