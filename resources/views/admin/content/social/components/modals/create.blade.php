<form class="modal-content dialog__content ajax-form"
      action="{{ route($sComposerRouteView.'.store') }}"
      data-counter=".admin-table-counter"
      data-list=".admin-table"
      data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
      data-callback="closeModalAfterSubmit, refreshAfterSubmit"
>
    <div class="modal-header">
        <h4 class="modal-title">Добавить</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-12">
            <div class="row">
                {{--<div class="col-12">--}}
                    {{--<div class="form-group">--}}
                        {{--<label>Ключ<i class="r">*</i></label>--}}
                        {{--<input type="text" class="form-control" name="name" placeholder="Ключ" value="" required>--}}
                    {{--</div>--}}
                {{--</div>--}}
                <div class="col-12">
                    <div class="form-group">
                        <label>Заголовок<i class="r">*</i></label>
                        <input type="text" class="form-control" name="title" placeholder="Заголовок" value="" required>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Тип<i class="r">*</i></label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-comments"></i></span>
                            <select class="selectpicker form-control" name="type" title="Выберите тип социальной сети..." required>
                                @foreach(Model::init($sComposerRouteView)->getTypes() as $key => $value)
                                    <option value="{{ $key }}" data-icon="fa {{ $value['fa-icon'] }}">
                                        - {{ ucfirst($value['title']) }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Ссылка<i class="r">*</i></label>
                        <input type="text" class="form-control" name="link" placeholder="Ссылка" value="" required>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Приоритет<i class="r">*</i></label>
                        <input type="number" class="form-control" name="priority" placeholder="Приоритет" value="0" required>
                    </div>
                </div>
                {{--<div class="col-12">--}}
                    {{--<div class="form-group">--}}
                        {{--<label>Иконка<i class="r">*</i></label>--}}
                        {{--<input type="text" class="form-control" name="icon" placeholder="Иконка" value="">--}}
                        {{--<span class="help">--}}
                            {{--Иконки брять отсюда <a href="https://material.io/icons/" target="_blank">https://material.io/icons/</a>--}}
                        {{--</span>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-12">--}}
                    {{--<div class="form-group">--}}
                        {{--<label>Цвет<i class="r">*</i></label>--}}
                        {{--<input type="text" class="form-control color-input" name="color" placeholder="Цвет" value="">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-12">--}}
                    {{--<div class="form-group">--}}
                        {{--<label>Подсказка</label>--}}
                        {{--<input type="text" class="form-control" name="tooltip" placeholder="Подсказка" value="">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-12">--}}
                    {{--<div class="form-group">--}}
                        {{--<label>Статус</label>--}}
                        {{--<select class="form-control" name="status">--}}
                            {{--@foreach(Model::init($sComposerRouteView)->getStatuses() as $key => $value)--}}
                                {{--<option value="{{ $key }}">{{ $value }}</option>--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-primary inner-form-submit">Сохранить</button>
    </div>
</form>
