@if(count($oItems) !== 0)
    <table class="table table-striped __with-edit-delete">
        <thead>
        <tr>
            <th class="is-id">#</th>
            <th class="is-image"></th>
            <th>Заголовок</th>
            <th>Тип</th>
            <th>Категория</th>
            <th style="width: 150px;">Дата публикации</th>
            <th class="is-status">Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th></th>
            <th>Заголовок</th>
            <th>Тип</th>
            <th>Категория</th>
            <th>Дата публикации</th>
            <th>Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach($oItems as $oItem)
            <tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
                <td>{{ $oItem->id }}</td>
                <td>
                    @include('admin.content.components.table.image', [
                        'oItem' => $oItem,
                        'path' => 'square'
                    ])
                </td>
                <td>
                    <b>{{ $oItem->title }}</b>
                </td>
                <td>
                    @if(!is_null($oItem->type))
                        @foreach($oItem->type->tree() as $cKey =>  $oType)
                            {{ $oType->title }} @if(is_null($oType->parent_id) && $cKey !== 0) @else | @endif
                        @endforeach
                        {{ $oItem->type->title }}
                    @endif
                </td>
                <td>
                    @if(!is_null($oItem->category))
                        @foreach($oItem->category->tree() as $cKey =>  $oCategory)
                            {{ $oCategory->title }} @if(is_null($oCategory->parent_id) && $cKey !== 0) @else | @endif
                        @endforeach
                        {{ $oItem->category->title }}
                    @else
                        Нет категории
                    @endif
                </td>
                <td>
                    {{ $oItem->published_at->format('d.m.Y H:i:s') }}
                </td>
                <td>
                    @include('admin.content.components.table.status', [
                        'oItem' => $oItem
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.edit', [
                        'oItem' => $oItem,
                        'init' => 'uploader, initSummernote, multiselect'
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.delete', [
                        'oItem' => $oItem,
                        'deleteKey' => 'альбом',
                        'deleteValue' => $oItem->title,
                    ])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @include('admin.content.components.table.pagination')
@else
    <div class="text-muted page-desc">Список пуст</div>
@endif
