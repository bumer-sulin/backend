<form class="modal-content dialog__content ajax-form"
      action="{{ route($sComposerRouteView.'.store') }}"
      data-counter=".admin-table-counter"
      data-list=".admin-table"
      data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
      data-callback="closeModalAfterSubmit, refreshAfterSubmit"
>
    <div class="modal-header">
        <h4 class="modal-title">Добавить</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label>Заголовок<i class="r">*</i></label>
                        <input type="text" class="form-control" name="title" placeholder="Заголовок" value="" required>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Тип</label>
                        <select class="form-control" name="type_id">
                            @foreach($oTypes->where('parent_id', null) as $key => $oType)
                                @include('admin.components.select.parentable', [
                                    'oItem' => $oType
                                ])
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Категория</label>
                        <select class="form-control" name="category_id">
                            <option value="">Нет категории</option>
                            @foreach($oCategories->where('parent_id', null) as $key => $oCategory)
                                @include('admin.components.select.parentable', [
                                    'oItem' => $oCategory
                                ])
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-primary inner-form-submit">Сохранить</button>
    </div>
</form>
