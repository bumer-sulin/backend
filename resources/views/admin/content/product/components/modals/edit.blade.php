<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">Редактирование</h4>
        <div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: right;">
            <div class="col-12">
                {{ $oItem->title }}
            </div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12 mb-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#post-tab-1" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Заголовок</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#post-tab-4" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Цена</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#post-tab-5" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Параметры</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#post-tab-2" role="tab" aria-controls="profile" aria-expanded="false" data-hidden-submit="0">Текст</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#post-tab-3" role="tab" aria-controls="messages" aria-expanded="false" data-hidden-submit="1">Галерея</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane tab-submit active" id="post-tab-1" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          id="{{$sComposerRouteView}}-from"
                          action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
                          data-counter=".admin-table-counter"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          {{--data-with-form="#post-tab-5 form, #post-tab-4 form"--}}
                          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Заголовок<i class="r">*</i></label>
                                <input type="text" class="form-control" name="title" placeholder="Заголовок" value="{{ $oItem->title or '' }}" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Тип</label>
                                <select class="form-control" name="type_id">
                                    @foreach($oTypes->where('parent_id', null) as $key => $oType)
                                        @include('admin.components.select.parentable', [
                                            'oItem' => $oType,
                                            'value' => $oItem->type_id
                                        ])
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Категория</label>
                                <select class="form-control" name="category_id">
                                    <option value="">Нет категории</option>
                                    @foreach($oCategories->where('parent_id', null) as $key => $oCategory)
                                        @include('admin.components.select.parentable', [
                                            'oItem' => $oCategory,
                                            'value' => $oItem->category_id
                                        ])
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Приоритет</label>
                                <input type="text" class="form-control" name="priority" placeholder="Приоритет" value="{{ $oItem->priority or 0 }}" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Дата публикации</label>
                                <input type="date" class="form-control" name="published_at" placeholder="Дата публикации" value="{{ !is_null($oItem->published_at) ? $oItem->published_at->format('Y-m-d') : '' }}">
                                <span class="help">
                                    Публикуются товары только у которых дата публикации меньше "сегодня". Новым считается товар, который был добавлен не позже 7 дней назад.
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane tab-submit" id="post-tab-4" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          action="{{ route($sComposerRouteView.'.action.item.post', ['id' => $oItem->id, 'action' => 'updateProductPrice']) }}"
                          data-counter=".admin-table-counter"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          {{--data-with-form="#post-tab-5 form"--}}
                          {{--data-callback="refreshAfterSubmit"--}}
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Цена<i class="r">*</i></label>
                                <input type="text" class="form-control" name="price[amount]" placeholder="Цена" value="{{ $oItem->price->amount or '' }}">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Старая цена</label>
                                <input type="text" class="form-control" name="price[old]" placeholder="Старая цена" value="{{ $oItem->price->old or '' }}">
                                <span class="help">
                                    {{--Если существует Старая цена--}}
                                </span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Бренд<i class="r">*</i></label>
                                <select class="form-control" name="brand_id">
                                    <option value="0">Не указан</option>
                                    @foreach($oBrands as $key => $oBrand)
                                        <option value="{{ $oBrand->id }}" @if(!is_null($oItem->brand) && $oBrand->id === $oItem->brand->id) selected @endif>{{ $oBrand->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Тэги</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-tags"></i></span>
                                    <select class="selectpicker form-control" multiple name="tags[]">
                                        @foreach($oTags as $oTag)
                                            <option value="{{ $oTag->id }}" @if(!is_null($oItem->tags->where('id', $oTag->id)->first())) selected @endif>{{ $oTag->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane tab-submit" id="post-tab-5" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          action="{{ route($sComposerRouteView.'.action.item.post', ['id' => $oItem->id, 'action' => 'updateProductParameters']) }}"
                          data-counter=".admin-table-counter"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          {{--data-with-form="#post-tab-4 form"--}}
                          {{--data-callback="refreshAfterSubmit"--}}
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        @foreach($oOptions as $oOption)
                            <div class="col-12">
                                <div class="form-group">
                                    <label>
                                        {{ $oOption->title }} @if(!is_null($oOption->description))({{ $oOption->description }})@endif
                                    </label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-tags"></i></span>
                                        <select class="selectpicker form-control" multiple name="options[{{ $oOption->key }}][]">
                                            @foreach($oOption->parameters as $oParameter)
                                                <option value="{{ $oParameter->id }}" @if(!is_null($oItem->values->where('parameter_id', $oParameter->id)->first())) selected @endif>{{ $oParameter->quantity }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </form>
                </div>
                <div class="tab-pane tab-submit" id="post-tab-2" role="tabpanel" aria-expanded="false">
                    <form class="ajax-form"
                          action="{{ route($sComposerRouteView.'.text.save.post', ['id' => $oItem->id]) }}"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        @include('admin.components.summernote', [
                            'url' => route($sComposerRouteView.'.text.save.post', ['id' => $oItem->id]),//url('/admin/posts/action/upload-wysiwyg'),
                            'model' => $sComposerRouteView,
                            'oItem' => $oItem,
                            'id' => 'summernote-'.$oItem->id.'-main'
                        ])
                    </form>
                </div>
                <div class="tab-pane tab-submit" id="post-tab-3" role="tabpanel" aria-expanded="false">
                    @include('admin.components.gallery.gallery', [
                        'priorityUrl' => route($sComposerRouteView.'.action.post', ['name' => 'imagePriority']),
                        'uploadUrl' => route($sComposerRouteView.'.image.upload.post', ['id' => $oItem->id]),
                        'model' => $sComposerRouteView,
                        'oItem' => $oItem,
                        'form' => '#'.$sComposerRouteView.'-form'
                    ])
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit">Сохранить</button>
    </div>
</div>
