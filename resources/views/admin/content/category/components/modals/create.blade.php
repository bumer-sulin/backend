<form class="modal-content dialog__content ajax-form"
      action="{{ route($sComposerRouteView.'.store') }}"
      data-counter=".admin-table-counter"
      data-list=".admin-table"
      data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
      data-callback="closeModalAfterSubmit, refreshAfterSubmit"
      novalidate
>
    <div class="modal-header">
        <h4 class="modal-title">Добавить</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label>Заголовок<i class="r">*</i></label>
                        <input type="text" class="form-control" name="title" placeholder="Заголовок" value="" required>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Приоритет<i class="r">*</i></label>
                        <input type="number" class="form-control" name="priority" placeholder="Приоритет" value="0" required>
                        <span class="help">
                            Чем больше, тем выше будет находится категория внутри своего родителя.
                        </span>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Цвет<i class="r">*</i></label>
                        <input type="text" class="form-control color-input" name="color" placeholder="Цвет" value="#FFFFFF" required>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Родитель</label>
                        <select class="form-control disabled" name="parent_id">
                            @if(!$fast)
                                <option value="">Главная (Нет родителя)</option>
                                @foreach($oCategories->where('parent_id', null) as $key => $oCategory)
                                    @include('admin.components.select.parentable', [
                                        'oItem' => $oCategory
                                    ])
                                @endforeach
                            @else
                                @foreach($oCategories as $key => $oCategory)
                                    @include('admin.components.select.parentable', [
                                        'oItem' => $oCategory
                                    ])
                                @endforeach
                            @endif
                        </select>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Статус</label>
                        <select class="form-control" name="status">
                            @foreach(Model::init($sComposerRouteView)->getStatuses() as $key => $value)
                                <option value="{{ $key }}" @if($key === 1) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-primary inner-form-submit">Сохранить</button>
    </div>
</form>
