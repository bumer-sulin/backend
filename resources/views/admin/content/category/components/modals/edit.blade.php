<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">Редактирование</h4>
        <div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: right;">
            <div class="col-12">
                {{ $oItem->title }}
            </div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12 mb-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#post-tab-1" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Основные</a>
                </li>
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" data-toggle="tab" href="#post-tab-2" role="tab" aria-controls="profile" aria-expanded="false" data-hidden-submit="0">Текст</a>--}}
                {{--</li>--}}
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link" data-toggle="tab" href="#post-tab-3" role="tab" aria-controls="profile" aria-expanded="false" data-hidden-submit="1">Галерея</a>--}}
                {{--</li>--}}
            </ul>

            <div class="tab-content">
                <div class="tab-pane tab-submit active" id="post-tab-1" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          id="group-from"
                          action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
                          data-counter=".admin-table-counter"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Заголовок<i class="r">*</i></label>
                                <input type="text" class="form-control" name="title" placeholder="Заголовок" value="{{ $oItem->title }}" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Приоритет<i class="r">*</i></label>
                                <input type="number" class="form-control" name="priority" placeholder="Приоритет" value="{{ $oItem->priority }}" required>
                                <span class="help">
                                    Чем больше, тем выше будет находится категория внутри своего родителя.
                                </span>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Цвет<i class="r">*</i></label>
                                <input type="text" class="form-control color-input" name="color" placeholder="Цвет" value="{{ $oItem->color ?? '' }}" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Родитель<i class="r">*</i></label>
                                <select class="form-control" name="parent_id">
                                    <option value="" @if(is_null($oItem->parent_id)) selected @endif>Главная (Нет родителя)</option>
                                    @foreach($oCategories->where('parent_id', null) as $key => $oCategory)
                                        @include('admin.components.select.parentable', [
                                            'oItem' => $oCategory,
                                            'value' => $oItem->parent_id
                                        ])
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Описание</label>
                                <textarea class="form-control" name="text" id="" cols="15" rows="5">{{ $oItem->text or '' }}</textarea>
                            </div>
                        </div>
                    </form>
                </div>
                {{--<div class="tab-pane tab-submit" id="post-tab-2" role="tabpanel" aria-expanded="false">--}}
                    {{--<form class="ajax-form"--}}
                          {{--action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"--}}
                          {{--data-list=".admin-table"--}}
                          {{--data-list-action="{{ route($sComposerRouteView.'.view.post') }}"--}}
                          {{--data-form-data=".pagination-form"--}}
                          {{--data-callback="closeModalAfterSubmit, refreshAfterSubmit"--}}
                    {{-->--}}
                        {{--<input type="hidden" name="id" value="{{ $oItem->id }}">--}}
                        {{--@include('admin.components.summernote', [--}}
                            {{--'url' => route($sComposerRouteView.'.text.save.post', ['id' => $oItem->id]),//url('/admin/posts/action/upload-wysiwyg'),--}}
                            {{--'model' => $sComposerRouteView,--}}
                            {{--'oItem' => $oItem,--}}
                            {{--'id' => 'summernote-'.$oItem->id.'-main'--}}
                        {{--])--}}
                    {{--</form>--}}
                {{--</div>--}}
                {{--<div class="tab-pane tab-submit" id="post-tab-3" role="tabpanel" aria-expanded="false">--}}
                    {{--@include('admin.components.gallery.gallery', [--}}
                        {{--'priorityUrl' => route($sComposerRouteView.'.action.post', ['name' => 'imagePriority']),--}}
                        {{--'uploadUrl' => route($sComposerRouteView.'.image.upload.post', ['id' => $oItem->id]),--}}
                        {{--'model' => $sComposerRouteView,--}}
                        {{--'oItem' => $oItem,--}}
                        {{--'form' => '#'.$sComposerRouteView.'-form'--}}
                    {{--])--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit">Сохранить</button>
    </div>
</div>
{{--
<form class="modal-content dialog__content ajax-form"
      action="{{ route($sComposerRouteView.'.store') }}"
      data-counter=".admin-table-counter"
      data-list=".admin-table"
      data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
      data-callback="closeModalAfterSubmit, refreshAfterSubmit"
>
    <div class="modal-header">
        <h4 class="modal-title">Добавить</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="form-group">
                        <label>Заголовок</label>
                        <input type="text" class="form-control" name="title" placeholder="Заголовок" value="" required>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label>Текст</label>
                        <textarea class="form-control" name="text" placeholder="Текст" rows="5"></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <label for="email" class="label">Иконка</label>
                    <div class="field" style="width: 101%;">
                        <p class="control" style="width: 70%;display: inline-block;">
                            <input class="form-control" name="icon" type="text" placeholder="Иконка" value="">
                        </p>
                        <p class="control" style="width: 28%;display: inline-block;">
                            <button class="btn btn-primary trigger is-primary" data-dialog="#fa-icons" type="button" style="width: 100%;">
                                Выбрать
                            </button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="submit" class="btn btn-primary inner-form-submit">Сохранить</button>
    </div>
</form>
--}}
