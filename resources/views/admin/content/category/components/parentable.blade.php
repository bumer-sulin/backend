<tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
    <td>{{ $oItem->id }}</td>
    {{--<td>--}}
        {{--@if(imagePath()->checkMain($sComposerRouteView, 'square', $oItems->get($oItem->id)))--}}
            {{--<img src="{{ ImagePath::main($sComposerRouteView, 'square', $oItems->get($oItem->id)) }}" width="30"--}}
                 {{--data-fancybox="gallery-post-{{ $sComposerRouteView }}"--}}
                 {{--href="{{ ImagePath::main($sComposerRouteView, 'original', $oItems->get($oItem->id)) }}"--}}
                 {{--style="cursor: pointer"--}}
            {{-->--}}
        {{--@else--}}
            {{--<img src="{{ ImagePath::main($sComposerRouteView, 'square', $oItems->get($oItem->id)) }}" width="30">--}}
        {{--@endif--}}
    {{--</td>--}}
    <td>
        {{ $prefix ?? '' }}
        <b>{{ $oItem->title }}</b>
    </td>
    <td style="text-align: center">
        <a href="{{ route('article.index', ['category_id' => $oItem->id]) }}"
           data-tippy-popover
           data-tippy-content="Отобрать статьи с этой категорией"
           target="_blank"
        >
            {{ count($oItem->activeArticles) }}
        </a>
    </td>
    <td>
        <div style="background-color: {{ $oItem->color }}; width: 100%;height: 19px;margin: 5px 0;"></div>
    </td>
    <td style="text-align: center">
        {{ $oItem->priority }}
    </td>
    <td>
        <div class="">
            <select class="form-control form-control-sm ajax-select" action="{{ $url ?? route($sComposerRouteView.'.status', ['id' => $oItem->id]) }}"
                    data-status="1"
                    name="status"
                    data-list=".admin-table"
                    data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                    data-form-data=".pagination-form"
                    data-callback="refreshAfterSubmit @if(isset($sCallbacks)), {{ $sCallbacks }}@endif"
            >
                @foreach(Model::init($sComposerRouteView)->getStatuses() as $key => $status)
                    <option value="{{ $key }}" @if($key === $oItem->status) selected @endif>{{ $status }}</option>
                @endforeach
            </select>
        </div>
    </td>
    <td style="text-align: center;">
        <a class="btn btn-primary btn-sm trigger" data-dialog="#custom-edit-modal" data-ajax
           data-action="{{ route($sComposerRouteView.'.create.modal.post', ['parent_id' => $oItem->id]) }}"
           {{--data-id="123"--}}
            style="height: 29px;"
        >
            <i class="icon-plus" style="color: #fff;"></i>
        </a>
    </td>
    <td>
        <a class="btn btn-primary btn-sm trigger" data-dialog="#custom-edit-modal" data-ajax
           data-action="{{ route($sComposerRouteView.'.edit.modal.post', ['id' => $oItem->id]) }}"
           data-ajax-init="uploader, initSummernote, colorpicker"
                {{--data-id="123"--}}
        >
            <i class="icon-pencil" style="color: #fff;"></i>
        </a>
    </td>
    <td>
        <a class="btn btn-danger btn-sm is-small trigger" data-dialog="#pages-dialogs-confirm" data-confirm
           data-text='Действительно хотите удалить категорию "{{ $oItem->title }}" ?'
           data-action="{{ route($sComposerRouteView.'.destroy', ['id' => $oItem->id]) }}"
           data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
           data-subtitle="При удалении категорию - удаляться все её подкатегории"
        >
            <i class="icon-trash icons" style="color: #fff;"></i>
            {{--<i class="fa fa-trash" style="color: #fff;"></i>--}}
        </a>
    </td>
</tr>
@if($oItem->hasChildren())
    @foreach($oItems->get($oItem->id)->children as $oChildren)
        @include('admin.content.'.$sComposerRouteView.'.components.parentable', [
            'oItem' => $oChildren,
            'prefix' => isset($prefix) ? $prefix.' - ' : ' - '
        ])
    @endforeach
@endif
