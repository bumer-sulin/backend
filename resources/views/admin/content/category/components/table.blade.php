@if(count($oItems) !== 0)
    <table class="table table-striped __with-edit-delete">
        <thead>
        <tr>
            <th class="is-id">#</th>
            {{--<th class="is-image"></th>--}}
            <th>Заголовок</th>
            <th style="width: 120px">Кол-во статей</th>
            <th class="is-status">Цвет</th>
            <th class="text-center" style="width: 120px">
                Приоритет
                @include('admin.components.tooltip.question', [
                    'title' => 'Чем больше, тем выше внутри своего родителя'
                ])
            </th>
            <th class="is-status">Статус</th>
            <th style="width: 50px">Добавить</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            {{--<th></th>--}}
            <th>Заголовок</th>
            <th>Кол-во статей</th>
            <th>Цвет</th>
            <th>Приоритет</th>
            <th>Статус</th>
            <th>Добавить</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach($oItems->where('parent_id', null) as $oItem)
            @include('admin.content.'.$sComposerRouteView.'.components.parentable', [
                'oItem' => $oItem
            ])
        @endforeach
        </tbody>
    </table>
    @include('admin.content.components.table.pagination')
@else
    <div class="text-muted page-desc">Список пуст</div>
@endif
