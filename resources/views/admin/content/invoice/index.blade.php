@extends('admin.layouts.admin')

@section('content.title')
    @include('admin.components.pages.title', [
        'title' => isset($aComposerContent[$sComposerRouteView]['title']) ? $aComposerContent[$sComposerRouteView]['title'] : '',
    ])
@endsection

@section('breadcrumb-right')

@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-block">
                    <div class="row">
                        <div class="col-12">
                            <div class="admin-table table-component-pagination" data-name="{{ $sComposerRouteView }}">
                                @include('admin.content.'.$sComposerRouteView.'.components.table', [
                                    'oItems' => $oItems
                                ])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
@endpush
