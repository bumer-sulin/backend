@if(count($oItems) !== 0)
    <table class="table table-striped __with-edit-delete">
        <thead>
        <tr>
            <th class="is-id">#</th>
            <th>Заголовок</th>
            <th>Цена</th>
            <th style="width: 150px;">Открыт/Закрыт</th>
            <th class="is-status">Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th>Заголовок</th>
            <th>Цена</th>
            <th>Открыт/Закрыт</th>
            <th>Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach($oItems as $oItem)
            <tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
                <td>{{ $oItem->id }}</td>
                <td>
                    <b>{{ $oItem->title }}</b>
                </td>
                <td>
                    {{ number_format($oItem->amount, 2, '.', '') }}
                </td>
                <td>
                    {{ optional($oItem->opened_at)->format('d.m.Y H:i:s') }}
                    <hr style="margin: 5px 0;">
                    {{ optional($oItem->closed_at)->format('d.m.Y H:i:s') }}
                </td>
                <td>
                    {{ array_get(Model::init($sComposerRouteView)->getStatuses(), $oItem->status, 'Неизвестно') }}
                    {{--@include('admin.content.components.table.status', [--}}
                        {{--'oItem' => $oItem--}}
                    {{--])--}}
                </td>
                <td>
                    @include('admin.content.components.table.edit', [
                        'oItem' => $oItem,
                        'init' => 'uploader, initSummernote'
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.delete', [
                        'oItem' => $oItem,
                        'deleteKey' => 'альбом',
                        'deleteValue' => $oItem->title,
                    ])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @include('admin.content.components.table.pagination')
@else
    <div class="text-muted page-desc">Список пуст</div>
@endif
