<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">Редактирование</h4>
        <div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: right;">
            <div class="col-12">
                {{ $oItem->title }}
            </div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12 mb-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#post-tab-1" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Заголовок</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#post-tab-2" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="1">Контакты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#post-tab-3" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="1">Список товаров</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane tab-submit active" id="post-tab-1" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          id="{{$sComposerRouteView}}-from"
                          action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
                          data-counter=".admin-table-counter"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Заголовок<i class="r">*</i></label>
                                <input type="text" class="form-control" name="title" placeholder="Заголовок" value="{{ $oItem->title ?? '' }}" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Назначение</label>
                                <textarea class="form-control" name="purpose" id="" cols="15" rows="5">{{ $oItem->purpose ?? '' }}</textarea>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Общая цена</label>
                                <input type="text" class="form-control" name="amount" placeholder="Общая цена" value="{{ number_format($oItem->amount, 2, '.', '') }}" disabled>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Дата открытия</label>
                                <input type="text" class="form-control" name="opened_at" placeholder="Дата открытия" value="{{ optional($oItem->opened_at)->format('d.m.Y H:i:s') }}" disabled>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Дата закрытия</label>
                                <input type="text" class="form-control" name="closed_at" placeholder="Дата закрытия" value="{{ optional($oItem->closed_at)->format('d.m.Y H:i:s') }}" disabled>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane tab-submit" id="post-tab-2" role="tabpanel" aria-expanded="true">
                    <div class="row">
                        <div class="col-12">
                            Клиент
                            <hr style="margin: 5px 0;">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Имя</label>
                                        <input type="text" class="form-control" name="first_name" placeholder="Имя" value="{{ $oItem->client->first_name ?? '' }}">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Фамилия</label>
                                        <input type="text" class="form-control" name="last_name" placeholder="Фамилия" value="{{ $oItem->client->last_name ?? '' }}">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Отчество</label>
                                        <input type="text" class="form-control" name="second_name" placeholder="Отчество" value="{{ $oItem->client->second_name ?? '' }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" class="form-control" name="email" placeholder="Email" value="{{ $oItem->client->email ?? '' }}">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Телефон</label>
                                        <input type="text" class="form-control" name="phone" placeholder="Телефон" value="{{ $oItem->client->phone ?? '' }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            Адрес
                            <hr style="margin: 5px 0;">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Индекс</label>
                                        <input type="text" class="form-control" name="index" placeholder="Индекс" value="{{ $oItem->options->index ?? '' }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">

                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Адрес введенный пользователем</label>
                                <input type="text" class="form-control" name="address" placeholder="Адрес введенный пользователем" value="{{ $oItem->options->address ?? '' }}">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Адрес от яндекса</label>
                                <input type="text" class="form-control" name="address_yandex" placeholder="Адрес от яндекса" value="{{ $oItem->options->address_yandex ?? '' }}">
                            </div>
                        </div>
                        <div class="col-12">
                            <div id="map" style="width: 100%; height: 400px;"></div>
                            <script>
                                ymaps.ready(init);

                                function init () {
                                    var myMap = new ymaps.Map("map", {
                                            center: [{{ $oItem->options->coordinates['lon'] }}, {{ $oItem->options->coordinates['lat'] }}],
                                            zoom: 13
                                        }, {
                                            searchControlProvider: 'yandex#search'
                                        }),
                                        myPlacemark = new ymaps.Placemark([{{ $oItem->options->coordinates['lon'] }}, {{ $oItem->options->coordinates['lat'] }}], {
                                            // Чтобы балун и хинт открывались на метке, необходимо задать ей определенные свойства.
                                            balloonContentHeader: "Балун метки",
                                            balloonContentBody: "Содержимое <em>балуна</em> метки",
                                            balloonContentFooter: "Подвал",
                                            hintContent: "Хинт метки"
                                        });

                                    myMap.geoObjects.add(myPlacemark);
                                    myMap.behaviors.disable('scrollZoom');

                                    // Открываем балун на карте (без привязки к геообъекту).
                                    {{--
                                    myMap.balloon.open([{{ $oItem->options->coordinates['lon'] }}, {{ $oItem->options->coordinates['lat'] }}], "Содержимое балуна", {
                                        // Опция: не показываем кнопку закрытия.
                                        closeButton: false
                                    });
                                    --}}
                                }
                            </script>
                        </div>
                    </div>
                </div>
                <div class="tab-pane tab-submit" id="post-tab-3" role="tabpanel" aria-expanded="true">
                    <div class="row">
                        <div class="col-12">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th class="is-id">#</th>
                                    <th class="is-image"></th>
                                    <th>Заголовок</th>
                                    <th>Тип</th>
                                    <th>Категория</th>
                                    <th>Количество</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th></th>
                                    <th>Заголовок</th>
                                    <th>Тип</th>
                                    <th>Категория</th>
                                    <th>Количество</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @foreach($oProducts as $oProduct)
                                    <tr @if(isset($oProduct->status) && !$oProduct->status) style="opacity: .5;" @endif>
                                        <td>{{ $oProduct->id }}</td>
                                        <td>
                                            @include('admin.content.components.table.image', [
                                                'oItem' => $oProduct,
                                                'path' => 'square',
                                                'model' => 'product'
                                            ])
                                        </td>
                                        <td>
                                            <b>
                                                <a href="{{ route('index.item', ['id' => $oProduct->id]) }}" target="_blank">
                                                    {{ $oProduct->title }}
                                                </a>
                                            </b>
                                        </td>
                                        <td>
                                            @if(!is_null($oProduct->type))
                                                @foreach($oProduct->type->tree() as $cKey =>  $oType)
                                                    {{ $oType->title }} @if(is_null($oType->parent_id) && $cKey !== 0) @else | @endif
                                                @endforeach
                                                {{ $oProduct->type->title }}
                                            @endif
                                        </td>
                                        <td>
                                            @if(!is_null($oProduct->category))
                                                @foreach($oProduct->category->tree() as $cKey =>  $oCategory)
                                                    {{ $oCategory->title }} @if(is_null($oCategory->parent_id) && $cKey !== 0) @else | @endif
                                                @endforeach
                                                {{ $oProduct->category->title }}
                                            @else
                                                Нет категории
                                            @endif
                                        </td>
                                        <td>
                                            {{ $oProduct->amount }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">
                                            @foreach(json_decode($oProduct->product_options) as $option)
                                                {{ $option }} <br>
                                            @endforeach
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit">Сохранить</button>
    </div>
</div>
