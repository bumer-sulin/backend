<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">
            Редактирование
            @include('admin.components.tooltip.question', [
                'title' => 'Вкладки сохраняются по отдельности.<br>Вкладка "Заголовок" закрывает панель.'
            ])
        </h4>
        <div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: right;">
            <div class="col-12">
                {{ $oItem->title }} :: {{ Model::init($sComposerRouteView)->getTypes()[$oItem->type]['title'] }}
            </div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="col-md-12 mb-12">
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#post-tab-1" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Заголовок</a>
                </li>
                @if($oItem->isImage())
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#post-tab-4" role="tab" aria-controls="messages" aria-expanded="false" data-hidden-submit="1">Галерея</a>
                    </li>
                @endif
                @if($oItem->isStrings())
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#post-tab-5" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Список</a>
                    </li>
                @endif
            </ul>

            <div class="tab-content">
                <div class="tab-pane tab-submit active" id="post-tab-1" role="tabpanel" aria-expanded="true">
                    <form class="row ajax-form"
                          id="{{$sComposerRouteView}}-from"
                          action="{{ route($sComposerRouteView.'.update', ['id' => $oItem->id]) }}"
                          data-counter=".admin-table-counter"
                          data-list=".admin-table"
                          data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                          data-form-data=".pagination-form"
                          data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                    >
                        <input type="hidden" name="id" value="{{ $oItem->id }}">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Заголовок<i class="r">*</i></label>
                                <input type="text" class="form-control" name="title" placeholder="Заголовок" value="{{ $oItem->title ?? '' }}" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label>Тип<i class="r">*</i></label>
                                <select class="form-control" name="type" required>
                                    @foreach(Model::init($sComposerRouteView)->getTypes() as $key => $value)
                                        <option value="{{ $key }}" {{ $oItem->type === $key ? 'selected' : '' }}>{{ ucfirst($value['title']) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            {{--<div class="form-group">--}}
                                {{--<label>Тип</label>--}}
                                {{--<div style="padding: 0.5rem 0.75rem;border: 1px solid rgba(0, 0, 0, 0.15);">--}}
                                    {{--- {{ Model::init($sComposerRouteView)->getTypes()[$oItem->type]['title'] }}--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        {{--<div class="col-12">--}}
                            {{--<div class="form-group">--}}
                                {{--<label>Тип<i class="r">*</i></label>--}}
                                {{--<div class="input-group">--}}
                                    {{--<span class="input-group-addon"><i class="fa fa-comments"></i></span>--}}
                                    {{--<select class="selectpicker form-control" name="type" title="Выберите тип социальной сети..." required>--}}
                                        {{--@foreach(Model::init($sComposerRouteView)->getTypes() as $key => $value)--}}
                                            {{--<option value="{{ $key }}" data-icon="fa {{ $value['fa-icon'] }}" {{ $oItem->type === $key ? 'selected' : '' }}>--}}
                                                {{--- {{ ucfirst($value['title']) }}--}}
                                            {{--</option>--}}
                                        {{--@endforeach--}}
                                    {{--</select>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="col-12">
                            <div class="form-group">
                                <label>Ссылка</label>
                                <input type="text" class="form-control" name="link" placeholder="Ссылка" value="{{ $oItem->link ?? '' }}">
                            </div>
                        </div>
                    </form>
                </div>
                @if($oItem->isImage())
                    <div class="tab-pane tab-submit" id="post-tab-4" role="tabpanel" aria-expanded="false">
                        @include('admin.components.gallery.gallery', [
                            'priorityUrl' => route($sComposerRouteView.'.action.post', ['name' => 'imagePriority']),
                            'uploadUrl' => route($sComposerRouteView.'.image.upload.post', ['id' => $oItem->id]),
                            'model' => $sComposerRouteView,
                            'oItem' => $oItem,
                            'form' => '#'.$sComposerRouteView.'-form'
                        ])
                    </div>
                @endif
                @if($oItem->isStrings())
                    <div class="tab-pane tab-submit" id="post-tab-5" role="tabpanel" aria-expanded="false">
                        <form class="ajax-form"
                              id="admin-audio-from"
                              action="{{ route($sComposerRouteView.'.action.item.post', ['name' => 'saveParameters', 'id' => $oItem->id]) }}"
                              data-list=".admin-table"
                              data-list-action="{{ route($sComposerRouteView.'.view.post') }}"
                              data-form-data=".pagination-form"
                              data-callback="refreshAfterSubmit"
                        >
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th class="is-id" style="border-bottom: none;">#</th>
                                    <th style="border-bottom: none;">Название</th>
                                    <th style="width: 50px;border-bottom: none;">Удалить</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>#</th>
                                    <th>Название</th>
                                    <th>Удалить</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                @if(!is_null($oItem->options))
                                    @foreach($oItem->options['text'] as $key => $value)
                                        <tr>
                                            <td>{{ $key + 1}}</td>
                                            <td>
                                                <input class="form-control form-control-sm form-control-focus" type="text" name="options[{{ $key }}]" value="{{ $value }}">
                                            </td>
                                            <td style="text-align: center;">
                                                <button type="button" class="btn btn-sm btn-danger delete-clone-tr">
                                                    <i class="icon-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                <tr class="clone-tr-container" style="background-color: #fff;">
                                    <td colspan="2"></td>
                                    <td class="is-icon">
                                        <button type="button" class="btn btn-sm btn-primary clone-tr" data-tr=".temp-ref-value">
                                            <i class="icon-plus"></i>
                                        </button>
                                    </td>
                                </tr>
                                <tr class="temp-ref-value" style="display: none;">
                                    <td>#</td>
                                    <td>
                                        <input type="text" class="form-control form-control-sm form-control-focus" name="options[]" value="" disabled>
                                    </td>
                                    <td class="is-icon">
                                        <button type="button" class="btn btn-sm btn-danger delete-clone-tr">
                                            <i class="icon-trash"></i>
                                        </button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit">Сохранить</button>
    </div>
</div>
