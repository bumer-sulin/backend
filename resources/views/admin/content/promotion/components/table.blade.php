@if(count($oItems) !== 0)
    <table class="table table-striped __with-edit-delete">
        <thead>
        <tr>
            <th class="is-id">#</th>
            <th class="is-image"></th>
            <th>Заголовок</th>
            <th>Тип</th>
            <th class="is-status">Ссылка</th>
            <th>Размер</th>
            <th class="is-status">Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th></th>
            <th>Заголовок</th>
            <th>Тип</th>
            <th>Ссылка</th>
            <th>Размер</th>
            <th>Статус</th>
            <th>Изменить</th>
            <th>Удалить</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach($oItems as $oItem)
            <tr @if(isset($oItem->status) && !$oItem->status) style="opacity: .5;" @endif>
                <td>{{ $oItem->id }}</td>
                <td>
                    @include('admin.content.components.table.image', [
                        'oItem' => $oItem,
                        'path' => 'square'
                    ])
                </td>
                <td>
                    <b>{{ $oItem->title }}</b>
                </td>
                <td>
                    {{ Model::init($sComposerRouteView)->getTypes()[$oItem->type]['title'] }}
                </td>
                <td>
                    @if(!is_null($oItem->link))
                        <a href="{{ $oItem->link }}" target="_blank"
                            title="{{ $oItem->link }}"
                        >
                            {{ str_limit($oItem->link, 20) }}
                        </a>
                    @else
                        Нет ссылки
                    @endif
                </td>
                <td>
                    {{ Model::init($sComposerRouteView)->getTypes()[$oItem->type]['size'] ?? '' }}
                </td>
                <td>
                    @include('admin.content.components.table.status', [
                        'oItem' => $oItem
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.edit', [
                        'oItem' => $oItem,
                        'init' => 'uploader, multiselect, tooltip'
                    ])
                </td>
                <td>
                    @include('admin.content.components.table.delete', [
                        'oItem' => $oItem,
                        'deleteKey' => 'рекламу',
                        'deleteValue' => $oItem->title,
                    ])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @include('admin.content.components.table.pagination')
@else
    <div class="text-muted page-desc">Список пуст</div>
@endif
