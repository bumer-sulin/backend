@extends('admin.layouts.app')

@section('content')
    <section class="section {{-- is-medium --}}">
        <div class="container">
            <div class="columns">
                <div class="column is-half is-offset-one-quarter box" style="padding: 20px 30px 30px 30px;">
                    <h1 class="title">Login</h1>
                    <form role="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <label for="email" class="label">E-Mail Address</label>
                        <p class="control">
                            <input id="email" type="email" class="input {{ $errors->has('email') ? 'is-danger' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help is-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </p>

                        <label for="password" class="label">Password</label>
                        <p class="control">
                            <input id="password" type="password" class="input {{ $errors->has('password') ? 'is-danger' : '' }}" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help is-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </p>

                        <p class="control">
                            <label class="checkbox">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                Remember me
                            </label>
                        </p>

                        <div class="control is-grouped">
                            <p class="control">
                                <button type="submit" class="button is-primary">Login</button>
                            </p>
                            <p class="control">
                                <a class="button is-link" href="{{ route('password.request') }}">Forgot Your Password?</a>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
