@extends('admin.layouts.app')

@section('content')
    <section class="section {{-- is-medium --}}">
        <div class="container">
            <div class="columns">
                <div class="column is-half is-offset-one-quarter box" style="padding: 20px 30px 30px 30px;">
                    <h1 class="title">Register</h1>
                    <form role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <label for="email" class="label">Name</label>
                        <p class="control">
                            <input id="name" type="text" class="input {{ $errors->has('name') ? 'is-danger' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                            @if ($errors->has('name'))
                                <span class="help is-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </p>

                        <label for="email" class="label">E-Mail Address</label>
                        <p class="control">
                            <input id="email" type="email" class="input {{ $errors->has('email') ? 'is-danger' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="help is-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </p>

                        <label for="password" class="label">Password</label>
                        <p class="control">
                            <input id="password" type="password" class="input {{ $errors->has('password') ? 'is-danger' : '' }}" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help is-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </p>

                        <label for="password-confirm" class="label">Confirm Password</label>
                        <p class="control">
                            <input id="password-confirm" type="password" class="input" name="password_confirmation" required>
                        </p>

                        <div class="control is-grouped">
                            <p class="control">
                                <button type="submit" class="button is-primary">Register</button>
                            </p>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
