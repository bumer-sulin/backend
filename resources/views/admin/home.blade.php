@extends('admin.layouts.admin')

@section('content.title')
    @include('admin.components.pages.title', [
        'title' => 'Это Ваш личный кабинет '.Sentinel::getUser()->first_name.'!'
    ])
@endsection

@section('content')
    <section class="section {{-- is-medium --}}">
        <div class="container">
            <h3 class="subtitle">Добро пожаловать!</h3>
        </div>
    </section>
@endsection
