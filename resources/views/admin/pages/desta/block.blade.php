@include('admin.components.pages.title', [
    'title' => 'Деста слайдер'
])
<section class="section {{-- is-medium --}}">
    <div class="container">

        <div class="columns">
            <div class="column is-6">
                <div class="mockup-slider mockup-slider--current is-pulled-right id-1">
                    <img src="http://laravel.mix.loc/imagecache/original/cover.png" alt="img13" />
                    <!-- <h3 class="mockup-slider__title">Интернет-магазин футбольной атрибутики</h3> -->
                    <!-- <h5 class="mockup-slider__subtitle">Правило 1</h5> -->
                </div>
                <div class="mockup-slider is-pulled-right id-2">
                    <img src="http://laravel.mix.loc/imagecache/original/cover1.png" alt="img13" />
                    <!-- <h3 class="mockup-slider__title">Портал музыкальных групп г.Ростов-на-Дону</h3> -->
                    <!-- <h5 class="mockup-slider__subtitle">Правило 2</h5> -->
                </div>
                <div class="mockup-slider is-pulled-right id-3">
                    <img src="http://laravel.mix.loc/imagecache/original/cover2.png" alt="img13" />
                    <!-- <h3 class="mockup-slider__title">"Белый баскетбол"</h3> -->
                    <!-- <h5 class="mockup-slider__subtitle">Правило 3</h5> -->
                </div>
            </div>
            <div class="column">
                <nav class="nav--desta">
                    <button class="nav__item nav__item--current" aria-label="Item 1" data-article-id="#id-1" data-article-class=".id-1">
                        <svg class="nav__icon"><use xlink:href="#icon-triangle"></use></svg>
                        <span class="nav__item-title">gfootball.ru</span>
                    </button>
                    <button class="nav__item" aria-label="Item 2" data-article-id="#id-2" data-article-class=".id-2">
                        <svg class="nav__icon"><use xlink:href="#icon-triangle"></use></svg>
                        <span class="nav__item-title">hearmeplease.ru</span>
                    </button>
                    <button class="nav__item" aria-label="Item 3" data-article-id="#id-3" data-article-class=".id-3">
                        <svg class="nav__icon"><use xlink:href="#icon-triangle"></use></svg>
                        <span class="nav__item-title">happyday.ru</span>
                    </button>
                </nav>
                <article class="mockup-article article--current" id="id-1">

                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

                </article>
                <article class="mockup-article" id="id-2">

                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo

                </article>
                <article class="mockup-article" id="id-3">

                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod

                </article>
            </div>
        </div>
    </div>
</section>
