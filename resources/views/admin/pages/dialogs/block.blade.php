@include('admin.components.pages.title', [
    'title' => 'Диалоговые окна'
])
<section class="section">
    <div class="container">
        <p class="control">
            <a class="button trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="" data-id="123">Ajax</a>
        </p>
        <p class="control">
            <a class="button trigger" data-dialog="#pages-dialogs-confirm">Confirm</a>
        </p>
        <p class="control">
            <a class="button trigger" data-dialog="#pages-dialogs-form">Form</a>
        </p>
        <p class="control">
            <a class="button trigger" data-dialog="#pages-dialogs-info" data-disabled-overlay>Info</a>
        </p>
    </div>
</section>
