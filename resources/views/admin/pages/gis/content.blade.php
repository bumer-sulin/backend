@extends('admin.layouts.admin')

@push('scripts')
<script src="http://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>
<script type="text/javascript">
    var map;

    DG.then(function () {
        map = DG.map('map', {
            center: [54.98, 82.89],
            zoom: 13
        });
    });
</script>
@endpush

@section('content.title')
    @include('admin.components.pages.title', [
        'title' => 'Карты 2Gis'
    ])
@endsection

@section('content')
    <section class="section" style="padding-top: 0;">
        <div class="container">
            <center>
                <div id="map" class="box" style="width:100%; height:400px"></div>
            </center>
        </div>
    </section>
@endsection
