@extends('admin.layouts.admin')

@section('content.title')
    @include('admin.components.pages.title', [
        'title' => 'Таблицы'
    ])
@endsection

@section('content')
    <section class="section" style="padding-top: 0;">
        <div class="container">
            <nav class="level">
                <!-- Left side -->
                <div class="level-left">
                    <div class="level-item">
                        <p class="subtitle is-5">
                            <strong>123</strong> posts
                        </p>
                    </div>
                    <div class="level-item">
                        <p class="control has-addons">
                            <input class="input" type="text" placeholder="Find a post">
                            <button class="button">
                                Search
                            </button>
                        </p>
                    </div>
                </div>

                <!-- Right side -->
                <div class="level-right">
                    <p class="level-item"><strong>All</strong></p>
                    <p class="level-item is-active"><a>Published</a></p>
                    <p class="level-item"><a>Drafts</a></p>
                    <p class="level-item"><a>Deleted</a></p>
                    <p class="level-item"><a class="button is-dark">New</a></p>
                </div>
            </nav>
            <table class="table table-admin">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Имя</th>
                    <th>Email</th>
                    <th>Статус</th>
                    <th>Изменить</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Имя</th>
                    <th>Email</th>
                    <th>Статус</th>
                    <th>Изменить</th>
                    <th>Удалить</th>
                </tr>
                </tfoot>
                <tbody>
                    @for($i=0;$i<10;$i++)
                        <tr>
                            <td>{{ $i }}</td>
                            <td>Имя</td>
                            <td>Email</td>
                            <td class="is-select">
                                <p class="control">
                                    <span class="select is-small">
                                        <select>
                                            <option>Опубликовано</option>
                                            <option>Не опубликовано</option>
                                        </select>
                                    </span>
                                </p>
                            </td>
                            <td>
                                <a class="button is-small trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="" data-id="123">
                                    <span class="icon is-small">
                                        <i class="fa fa-pencil"></i>
                                    </span>
                                </a>
                            </td>
                            <td>
                                <a class="button is-small trigger" data-dialog="#pages-dialogs-confirm" data-confirm
                                   data-text="Действительно хотите удалить id{{ $i }} ?"
                                   data-action="{{ route('confirm') }}"
                                >
                                    <span class="icon is-small">
                                        <i class="fa fa-trash"></i>
                                    </span>
                                </a>
                            </td>
                        </tr>
                    @endfor
                </tbody>
            </table>
        </div>
    </section>
@endsection


@section('modals')
    @include('admin.pages.dialogs.components.ajax')
    @include('admin.pages.dialogs.components.confirm')
    @include('admin.pages.dialogs.components.form')
    @include('admin.pages.dialogs.components.info')
@endsection
