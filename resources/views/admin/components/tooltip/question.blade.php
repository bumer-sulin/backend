<a role="button"
   class="{{ isset($type) ? 'text-'.$type : '' }}"
   data-tippy-popover
   data-tippy-content="{{ $title ?? '' }}"
   {{--data-tippy="{{ $title ?? '' }}"--}}
   href="#"
>
    <i class="fa fa-{{ isset($icon) ? $icon : 'question' }}" aria-hidden="true"></i>
</a>
