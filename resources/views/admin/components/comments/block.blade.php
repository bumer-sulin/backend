<article class="media">
    <figure class="media-left">
        <p class="image is-64x64">
            <img src="http://placehold.it/128x128">
        </p>
    </figure>
    <div class="media-content">
        <div class="content">
            <p>
                <strong>{{ $oComment->commented->name }}</strong>
                @if($oComment->created_at->isToday())
                    <small> · {{ LocalizedCarbon::instance($oComment->created_at)->diffForHumans() }}</small>
                @else
                    <small> · {{ $oComment->created_at->format('d.m.Y H:i:s') }}</small>
                @endif
                <br>
                {{ $oComment->comment }}
                <br>
            </p>
        </div>
        <div class="level" style="margin-bottom: 5px;">
            <div class="level-left">
                @if(!is_null($oComment->rate))
                    <a class="level-item" style="cursor: default;">
                        @include('bulma.components.comments.rate', [
                            'nRate' => $oComment->rate
                        ])
                    </a>
                @endif
                <a class="level-item">
                    <span class="icon is-small"><i class="fa fa-reply"></i></span>
                </a>
                {{--
                <a class="level-item">
                    <span class="icon is-small"><i class="fa fa-heart"></i></span>
                </a>
                --}}

            </div>
        </div>
        @if(isset($oComment->comments) && !empty($oComment->comments[0]))
            @include('bulma.components.comments.list', [
                'oComments' => $oComment->comments
            ])
        @endif
    </div>
</article>