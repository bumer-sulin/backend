<?php
$adminModel = isset($adminModel) ? $adminModel : 'posts';
$form = isset($form) ? $form : '#post-form';
?>
@foreach($oItem->gallery as $image)
    <div class="col-{{ $col }}" style="position: relative; margin-top: 10px;">
        <img src="{{ ImagePath::image($model, 'square', $image) }}"
             class="drag-image drag-pointer"
             data-id="{{ $image->id }}"
             style="width: 100%;"
        >
        <button type="button" class="btn btn-sm icon __l-t ajax-link {{ intval($image->is_main) === 1 ? 'btn-primary' : 'btn-outline-primary' }}"
            action="{{ route($model.'.image.main.post', ['id' => $oItem->id, 'image_id' => $image->id]) }}"
            data-image_id="{{ $image->id }}"
            data-loading="1"
            data-view=".is-gallery-row"
            data-callback="updateView"
            title="Назначить главной"
        >
            <i class="icon-check"></i>
        </button>
        <button type="button" class="btn btn-sm icon __r-t btn-outline-danger ajax-link"
                action="{{ route($model.'.image.destroy.post', ['id' => $oItem->id, 'image_id' => $image->id]) }}"
                data-image_id="{{ $image->id }}"
                data-loading="1"
                data-view=".is-gallery-row"
                data-callback="updateView"
                title="Удалить изображение"
        >
            <i class="icon-trash"></i>
        </button>
        <button type="button" class="btn btn-sm icon __c-b btn-outline-primary" data-fancybox="gallery-post-{{$oItem->id}}" href="{{ ImagePath::image($model, 'original', $image) }}"
                title="Посмотреть изображение"
        >
            <i class="icon-eye"></i>
        </button>
        <button type="button" class="btn btn-sm icon __r-b btn-outline-success trigger" data-dialog="#pages-dialogs-ajax" data-ajax
                data-action="{{ route('info.action.item.post', ['id' => $oItem->id, 'action' => 'getLinkImagesModal', 'image_id' => $image->id, 'model' => $model]) }}"
                title="Ссылки на изображения"
        >
            <i class="icon-link"></i>
        </button>
        {{--<button type="button" class="btn btn-sm icon __l-b btn-outline-success"--}}
                {{--data-toggle="popover"--}}
                {{--data-template='<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>'--}}
                {{--data-content='@include('admin.components.gallery.popover.author')'--}}
                {{--data-html="true"--}}
                {{--data-container=".is-gallery-row"--}}

        {{-->--}}
            {{--<i class="icon-link"></i>--}}
        {{--</button>--}}
        {{--<input id="clipboard-{{ $image->id }}" type="text" value="{{ ImagePath::image($model, 'original', $image) }}" style="display: none;">--}}
    </div>
@endforeach
<div class="col-{{ $col }} --empty-placeholder" style="position: relative; margin-top: 10px; height: 79px;"></div>
