{{--
    'priorityUrl' => url('/admin/posts/action/save-gallery-priority'),
    'uploadUrl' => url('/admin/posts/action/upload-gallery'),
    'model' => 'post',
    'oItem' => $oPost,
--}}
<div id="gallery-container" data-id="file-image-{{ $model }}-{{ $oItem->id }}">
    <div class="row">
        <div class="col-12">
            <div id="dropzone" class="dropzone fade well" data-id="file-image-{{ $model }}-{{ $oItem->id }}">
                <i class="fa fa-plus" style="margin-right: 10px;"></i>
                Для загрузки, перетащите файл сюда
            </div>
            {{--
            <div class="alert alert-info" role="alert">
                Сортировки изображений по типу <strong>"drag and drop"</strong>
            </div>
            --}}
        </div>
    </div>
    <div class="row is-gallery-row"
         data-save-action="{{ $priorityUrl }}"
         data-name="image-priority"
         data-item_id="{{ $oItem->id }}"
         data-type="{{ $model ?? '' }}"
         data-col="{{ $col ?? 3 }}"
    >
        @include('admin.components.gallery.block', [
            'oItem' => $oItem,
            'col' => isset($col) ? $col : 3,
            'model' => isset($model) ? $model : 'post',
            'adminModel' => isset($adminModel) ? $adminModel : 'posts',
            'form' => isset($form) ? $form : '#post-form',
        ])
    </div>
    <div class="row hidden" style="margin-top: 15px;">
        <div class="col-12">
            <input type="file" name="images[]" id="file-image-{{ $model }}-{{ $oItem->id }}" class="btn btn-primary file-uploader is-file" accept="image/*"
                   data-dropzone='#dropzone[data-id="file-image-{{ $model }}-{{ $oItem->id }}"]'
                   data-url="{{ $uploadUrl }}"
                   data-id="{{ $oItem->id }}"
                   data-uid=""
                   data-col="{{ $col ?? 3 }}"
                   data-imageable_type="{{ $model }}"
                   data-upload_type="gallery"

                   data-loading-container='#gallery-container[data-id="file-image-{{ $model }}-{{ $oItem->id }}"]'
                   multiple
            >
        </div>
    </div>
</div>
