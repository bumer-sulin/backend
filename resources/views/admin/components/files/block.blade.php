@if(count($oItem->{$relation}) !== 0)
    <table class="table table-striped">
        <thead>
        <tr>
            <th class="is-id">#</th>
            <th>Название</th>
            <th class="is-status">Приоритет</th>
            <th class="is-status">Статус</th>
            <th style="width: 50px">Удалить</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>#</th>
            <th>Название</th>
            <th>Приоритет</th>
            <th>Статус</th>
            <th>Удалить</th>
        </tr>
        </tfoot>
        <tbody>
        @foreach($oItem->{$relation} as $key => $oFile)
            <tr @if(isset($oFile->status) && !$oFile->status) style="opacity: .5;" @endif>
                <td>{{ $oFile->id }}</td>
                <td>
                    <input class="form-control form-control-sm form-control-focus" type="text" name="name[{{ $oFile->id }}]" value="{{ $oFile->name }}">
                </td>
                <td>
                    <input class="form-control form-control-sm" type="number" name="priority[{{ $oFile->id }}]" value="{{ $oFile->priority }}">
                </td>
                <td style="text-align: center;">
                    @include('admin.content.components.table.status', [
                        'oItem' => $oFile,
                        'list' => '.is-files-row',
                        'action' => route($sComposerRouteView.'.action.item.post', ['name' => 'fileGetFiles', 'id' => $oItem->id, 'relation' => $relation]),
                        'model' => 'file'
                    ])
                </td>
                <td style="text-align: center;">
                    <a class="btn btn-danger btn-sm is-small ajax-link" action="{{ route($sComposerRouteView.'.file.destroy.post', ['id' => $oItem->id, 'file_id' => $oFile->id]) }}"
                       data-view=".is-files-row"
                       data-callback="updateView"
                    >
                        <i class="fa fa-trash" style="color: #fff;"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <div class="col-12">
        <div class="text-muted page-desc">Список пуст</div>
    </div>
@endif

