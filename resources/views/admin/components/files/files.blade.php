{{--
    'priorityUrl' => url('/admin/posts/action/save-gallery-priority'),
    'uploadUrl' => url('/admin/posts/action/upload-gallery'),
    'model' => 'post',
    'oItem' => $oPost,
--}}
<div id="gallery-container" data-id="file-{{ $model }}-{{ $oItem->id }}">
    <div class="row">
        <div class="col-12">
            <div id="dropzone" class="dropzone fade well" data-id="file-{{ $model }}-{{ $oItem->id }}">
                <i class="fa fa-plus" style="margin-right: 10px;"></i>
                Для загрузки, перетащите файл сюда
            </div>
            {{--
            <div class="alert alert-info" role="alert">
                Сортировки изображений по типу <strong>"drag and drop"</strong>
            </div>
            --}}
        </div>
    </div>
    <div class="row is-gallery-row is-files-row"
         data-save-action="{{ $priorityUrl ?? '' }}"
         data-name="image-priority"
         data-item_id="{{ $oItem->id }}"
         data-type="{{ $model ?? '' }}"
         data-col="{{ $col ?? 3 }}"
         style="margin-top: 1rem;"
    >
        @if(View::exists('admin.content.'.$sComposerRouteView.'.components.modals.files.'.$relation.'.block'))
            @include('admin.content.'.$sComposerRouteView.'.components.modals.files.'.$relation.'.block', [
                'oItem' => $oItem,
                'relation' => isset($relation) ? $relation : 'files',
                'model' => isset($model) ? $model : 'post',
            ])
        @else
            @include('admin.components.files.block', [
                'oItem' => $oItem,
                'relation' => isset($relation) ? $relation : 'files',
                'model' => isset($model) ? $model : 'post',
            ])
        @endif
    </div>
    <div class="row hidden" style="margin-top: 15px;">
        <div class="col-12">
            <input type="file" name="files[]" id="file-{{ $model }}-{{ $oItem->id }}" class="btn btn-primary file-uploader is-file" accept="image/*"
                   data-dropzone='.dropzone[data-id="file-{{ $model }}-{{ $oItem->id }}"]'
                   data-url="{{ $uploadUrl }}"
                   data-id="{{ $oItem->id }}"
                   data-uid=""
                   data-view-init="1"
                   data-col="{{ $col ?? 3 }}"
                   data-fileable_type="{{ $model }}"
                   data-upload_type="gallery"

                   data-loading-container='#gallery-container[data-id="file-{{ $model }}-{{ $oItem->id }}"]'
                   multiple
            >
        </div>
    </div>
</div>
