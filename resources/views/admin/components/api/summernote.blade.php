<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>Bulma Mix</title>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet">


    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>

    <link href="{{ asset('assets/summernote/summernote.css') }}" rel="stylesheet">
    <script src="{{ asset('assets/summernote/summernote.js') }}"></script>

</head>
<body>
    <section class="section {{-- is-medium --}}">
        <div class="container">
            <textarea id="summernote" class="textarea"></textarea>
        </div>
    </section>


    <script>
        $(function() {
            $('#summernote').summernote();
        });
    </script>
</body>
</html>
