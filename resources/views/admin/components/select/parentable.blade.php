<option value="{{ $oItem->id }}"
        @if(isset($value) && intval($oItem->id) === $value) selected @endif
        @if(isset($selected) && !is_null($oRelation->where('id', $oItem->id)->first())) selected @endif
        @if(isset($selectAll) && $selectAll) selected @endif
>{{ $prefix ?? '' }}{{ $oItem->title }}</option>
@if(isset($oItems) && isset($keyId))
    @if(count($oItems->get($oItem->id)->children) !== 0)
        @foreach($oItems->get($oItem->id)->children as $oChildren)
            @include('admin.components.select.parentable', [
                'oItem' => $oItems->get($oChildren->id),
                'prefix' => isset($prefix) ? $prefix.' - ' : ' - ',
                'value' => isset($value) ? $value : null,
            ])
        @endforeach
    @endif
@else
    @if(count($oItem->children) !== 0)
        @foreach($oItem->children as $oChildren)
            @include('admin.components.select.parentable', [
                'oItem' => $oChildren,
                'prefix' => isset($prefix) ? $prefix.' - ' : ' - ',
                'value' => isset($value) ? $value : null,
            ])
        @endforeach
    @endif
@endif
