<section @if(isset($id)) id="{{$id}}" @endif class="section section--loader hidden" style="height: 100%;position: fixed;top: 0;left: 0;background-color: #000;width: 100%;z-index: 10;opacity: .7;">
    <div class="container-loader vertical-center" style="position: fixed;top: 50%;left: 50%;">
        <div class="square-spin is-dark">
            <div></div>
        </div>
    </div>
</section>