{{--
	'title'     => 'Hello world!',
	'body'      => 'How's it hangin'?',
	'icon'      => 'icon.png',
	'timeout'   => 4000,
--}}
<script>
    Push.create("{{ $title ?? 'Заголовок'}}", {
        body: "{{ $body ?? 'Текст сообщения'}}",
        icon: "{{ $icon ?? asset('img/cover.png')}}",
        timeout: "{{ $timeout ?? 10000}}",
        vibrate: [200,100],
        onClick: function () {
            window.focus();
            this.close();
        }
    });

</script>
