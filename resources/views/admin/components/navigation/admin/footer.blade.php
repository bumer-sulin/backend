@hasSection('footer')
    @yield('footer')
@else
    <footer class="app-footer">
        {{--{{ $oComposerSite->app->title }} © 2017 {{ \Tremby\LaravelGitVersion\GitVersionHelper::getVersion() }}@include('git-version::version-comment')--}}
        <span class="float-right">
            Powered by <a href="http://webregul.ru" target="_blank">{{ $oComposerSite->app->title }}</a>
        </span>
    </footer>
@endif

