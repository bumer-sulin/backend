<nav class="nav has-shadow nav-desta">
    <div class="container">
        <div class="nav-left" style="overflow: visible;">
            <a class="nav-item nav-logo" href="{{ url('/') }}">
                @if(Option::exists('app.name'))
                    {{ option('app.name') }}
                @else
                    {{ config('app.name', 'Laravel') }}
                @endif
            </a>
        </div>
        <span class="nav-toggle">
            <span></span>
            <span></span>
            <span></span>
        </span>
        <div class="nav-right nav-menu">

            @if (Sentinel::bypassCheckpoints(function() { return Sentinel::guest(); }))
                <span class="nav-item">
                    <a class="button is-primary is-inverted trigger" data-dialog="#login">
                        <span class="icon is-small">
                            <i class="fa fa-sign-in"></i>
                        </span>
                        <span>Войти</span>
                    </a>

                    <a class="button is-primary is-inverted trigger" data-dialog="#register">
                        Регистрация
                    </a>
                </span>
            @else
                <a class="nav-item nav-logo tippy" data-dialog="#pages-dialogs-ajax">
                    {{ Sentinel::bypassCheckpoints(function() { return Sentinel::getUser()->email; }) }}
                </a>
                <span class="nav-item">
                    <a class="button is-inverted ajax-link" action="{{ route('logout.post') }}" data-loading="1">
                        <span class="icon is-small">
                           <i class="fa fa-user"></i>
                        </span>
                        <span>Выйти</span>
                    </a>
                </span>

            @endif
        </div>
    </div>
</nav>