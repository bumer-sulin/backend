<header class="app-header navbar">
    <button class="navbar-toggler mobile-sidebar-toggler hidden-lg-up" type="button">☰</button>
    <a class="navbar-brand" href="#" style="background-size: 140px auto;background: #141519;color: #fff;text-align: center">
        {{ $oComposerSite->app->title }}
    </a>
    <ul class="nav navbar-nav hidden-md-down">
        <li class="nav-item">
            <a class="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>
        </li>

        <li class="nav-item px-1">
            <a class="nav-link" href="{{ url('/admin/dashboard') }}">
                {{ $oComposerSite->app->title }}
            </a>
        </li>
        <li class="nav-item px-1">
            <a class="nav-link" href="{{ url('/admin/users') }}">
                {{ $oComposerSite->app->title }}
            </a>
        </li>
        <li class="nav-item px-1">
            <a class="nav-link" href="{{ url('/admin/settings') }}">
                {{ $oComposerSite->app->title }}
            </a>
        </li>
    </ul>
    <ul class="nav navbar-nav ml-auto">
        {{--
        <li class="nav-item dropdown hidden-md-down">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="icon-bell"></i>
                <span class="badge badge-pill badge-danger">5</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">

                <div class="dropdown-header text-center">
                    <strong>You have 5 notifications</strong>
                </div>

                <a href="#" class="dropdown-item">
                    <i class="icon-user-follow text-success"></i> New user registered
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-user-unfollow text-danger"></i> User deleted
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-chart text-info"></i> Sales report is ready
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-basket-loaded text-primary"></i> New client
                </a>
                <a href="#" class="dropdown-item">
                    <i class="icon-speedometer text-warning"></i> Server overloaded
                </a>

                <div class="dropdown-header text-center">
                    <strong>Server</strong>
                </div>

                <a href="#" class="dropdown-item">
                    <div class="text-uppercase mb-q">
                        <small><b>CPU Usage</b>
                        </small>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                    <small class="text-muted">348 Processes. 1/4 Cores.</small>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="text-uppercase mb-q">
                        <small><b>Memory Usage</b>
                        </small>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 70%" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                    <small class="text-muted">11444GB/16384MB</small>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="text-uppercase mb-q">
                        <small><b>SSD 1 Usage</b>
                        </small>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-danger" role="progressbar" style="width: 95%" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                    <small class="text-muted">243GB/256GB</small>
                </a>

            </div>
        </li>
        <li class="nav-item dropdown hidden-md-down">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="icon-list"></i>
                <span class="badge badge-pill badge-warning">15</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">

                <div class="dropdown-header text-center">
                    <strong>You have 5 pending tasks</strong>
                </div>

                <a href="#" class="dropdown-item">
                    <div class="small mb-q">Upgrade NPM &amp; Bower
                            <span class="float-right">
                                <strong>0%</strong>
                            </span>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="small mb-q">ReactJS Version
                            <span class="float-right">
                                <strong>25%</strong>
                            </span>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-danger" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="small mb-q">VueJS Version
                            <span class="float-right">
                                <strong>50%</strong>
                            </span>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-warning" role="progressbar" style="width: 50%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="small mb-q">Add new layouts
                            <span class="float-right">
                                <strong>75%</strong>
                            </span>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-info" role="progressbar" style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="small mb-q">Angular 2 Cli Version
                            <span class="float-right">
                                <strong>100%</strong>
                            </span>
                    </div>
                        <span class="progress progress-xs">
                            <div class="progress-bar bg-success" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                        </span>
                </a>

                <a href="#" class="dropdown-item text-center">
                    <strong>View all tasks</strong>
                </a>
            </div>
        </li>
        <li class="nav-item dropdown hidden-md-down">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <i class="icon-envelope-letter"></i>
                <span class="badge badge-pill badge-info">7</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg">

                <div class="dropdown-header text-center">
                    <strong>You have 4 messages</strong>
                </div>

                <a href="#" class="dropdown-item">
                    <div class="message">
                        <div class="py-1 mr-1 float-left">
                            <div class="avatar">
                                <img src="{{ asset('img/admin/avatars/7.jpg') }}" class="img-avatar" alt="admin@bootstrapmaster.com">
                                <span class="avatar-status badge-success"></span>
                            </div>
                        </div>
                        <div>
                            <small class="text-muted">John Doe</small>
                            <small class="text-muted float-right mt-q">Just now</small>
                        </div>
                        <div class="text-truncate font-weight-bold">
                            <span class="fa fa-exclamation text-danger"></span>Important message</div>
                        <div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
                    </div>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="message">
                        <div class="py-1 mr-1 float-left">
                            <div class="avatar">
                                <img src="{{ asset('img/admin/avatars/6.jpg') }}" class="img-avatar" alt="admin@bootstrapmaster.com">
                                <span class="avatar-status badge-warning"></span>
                            </div>
                        </div>
                        <div>
                            <small class="text-muted">John Doe</small>
                            <small class="text-muted float-right mt-q">5 minutes ago</small>
                        </div>
                        <div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
                        <div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
                    </div>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="message">
                        <div class="py-1 mr-1 float-left">
                            <div class="avatar">
                                <img src="{{ asset('img/admin/avatars/5.jpg') }}" class="img-avatar" alt="admin@bootstrapmaster.com">
                                <span class="avatar-status badge-danger"></span>
                            </div>
                        </div>
                        <div>
                            <small class="text-muted">John Doe</small>
                            <small class="text-muted float-right mt-q">1:52 PM</small>
                        </div>
                        <div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
                        <div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
                    </div>
                </a>
                <a href="#" class="dropdown-item">
                    <div class="message">
                        <div class="py-1 mr-1 float-left">
                            <div class="avatar">
                                <img src="{{ asset('img/admin/avatars/4.jpg') }}" class="img-avatar" alt="admin@bootstrapmaster.com">
                                <span class="avatar-status badge-info"></span>
                            </div>
                        </div>
                        <div>
                            <small class="text-muted">John Doe</small>
                            <small class="text-muted float-right mt-q">4:03 PM</small>
                        </div>
                        <div class="text-truncate font-weight-bold">Lorem ipsum dolor sit amet</div>
                        <div class="small text-muted text-truncate">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt...</div>
                    </div>
                </a>

                <a href="#" class="dropdown-item text-center">
                    <strong>View all messages</strong>
                </a>
            </div>
        </li>
        <li class="nav-item hidden-md-down">
            <a class="nav-link" href="#"><i class="icon-location-pin"></i></a>
        </li>
        --}}
        <li class="nav-item dropdown" style="padding-right: 20px;">
            <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <img src="{{ asset('img/admin/default/avatar.png') }}" class="img-avatar" alt="{{ Sentinel::getUser()->email }}">
                <span class="hidden-md-down">{{ Sentinel::getUser()->first_name }}</span>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                {{--
                <div class="dropdown-header text-center">
                    <strong>Account</strong>
                </div>

                <a class="dropdown-item" href="#"><i class="fa fa-bell-o"></i> Updates<span class="badge badge-info">42</span></a>
                <a class="dropdown-item" href="#"><i class="fa fa-envelope-o"></i> Messages<span class="badge badge-success">42</span></a>
                <a class="dropdown-item" href="#"><i class="fa fa-tasks"></i> Tasks<span class="badge badge-danger">42</span></a>
                <a class="dropdown-item" href="#"><i class="fa fa-comments"></i> Comments<span class="badge badge-warning">42</span></a>

                <div class="dropdown-header text-center">
                    <strong>Settings</strong>
                </div>

                <a class="dropdown-item" href="#"><i class="fa fa-user"></i> Profile</a>
                <a class="dropdown-item" href="#"><i class="fa fa-wrench"></i> Settings</a>
                <a class="dropdown-item" href="#"><i class="fa fa-usd"></i> Payments<span class="badge badge-default">42</span></a>
                <a class="dropdown-item" href="#"><i class="fa fa-file"></i> Projects<span class="badge badge-primary">42</span></a>
                <div class="divider"></div>
                <a class="dropdown-item" href="#"><i class="fa fa-shield"></i> Lock Account</a>
                --}}
                <a class="dropdown-item ajax-link" href="#" action="{{ route('logout.post') }}">
                    <i class="fa fa-lock"></i>
                    Выйти
                </a>
            </div>
        </li>
        {{--
        <li class="nav-item hidden-md-down">
            <a class="nav-link navbar-toggler aside-menu-toggler" href="#">☰</a>
        </li>
        --}}

    </ul>
</header>



<nav class="nav has-shadow nav-desta">
    <div class="container">
        <div class="nav-left" style="overflow: visible;">
            <a class="nav-item nav-logo" href="{{ url('/') }}">
                {{ $oComposerSite->app->title }}
                {{--@classIf(isset($oComposerSite), 'is-loading', 'dsd')--}}
            </a>
        </div>
        <span class="nav-toggle">
            <span></span>
            <span></span>
            <span></span>
        </span>
        <div class="nav-right nav-menu">
            @if(!Sentinel::guest())
                {{-- 
                <span class="nav-item">
                    <a class="button is-inverted tippy" title="Количество просмотров" >
                        <span class="icon is-small">
                           <i class="fa fa-users"></i>
                        </span>
                        <span>{{ YandexMetrika::getVisitsViewsUsers()->data['total_rows'] }}</span>
                    </a>
                    <a class="button is-inverted">
                        <span class="icon is-small">
                           <i class="fa fa-heart"></i>
                        </span>
                        <span>10</span>
                    </a>
                    <a class="button is-inverted">
                        <span class="icon is-small">
                           <i class="fa fa-envelope"></i>
                        </span>
                        <span>10</span>
                    </a>
                </span>

                <span class="nav-item">
                    <a class="button is-primary is-inverted trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="">
                        <span class="icon is-small">
                           <i class="fa fa-envelope-o"></i>
                        </span>
                        <span>10</span>
                    </a>
                </span>
                --}}
            @endif



            @if (Sentinel::guest())
                <span class="nav-item">
                    <a class="button is-primary is-inverted trigger" data-dialog="#login">
                        <span class="icon is-small">
                            <i class="fa fa-sign-in"></i>
                        </span>
                        <span>Войти</span>
                    </a>
                    {{--
                    <a class="button is-primary is-inverted trigger" data-dialog="#register">
                        Регистрация
                    </a>
                    --}}
                </span>
            @else
                <a class="nav-item trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route('user'.'.image.post', ['id' => Sentinel::getUser()->id]) }}"
                   data-ajax-init="uploader"
                   data-id="123"
                   style="padding: 0; border: none;"
                >
                    <img data-user-image src="{{ ImagePath::cache()->main('user', 'original', Sentinel::getUser()) }}" width="25" style="border-radius: 100%;">
                </a>
                {{--
                <a class="nav-item nav-logo tippy-html trigger" data-html="tippy-template" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route('user.edit.modal.post', ['id' => Sentinel::getUser()->id]) }}" title="Редактировать" href="{{ url('/') }}">
                    {{ Sentinel::getUser()->email }}
                </a>
                <span id="tippy-template" style="display: none;">
                    <div class="box" style="margin: -10px -15px;">
                        <div>
                            {{ Sentinel::getUser()->first_name }}
                        </div>
                        <a class="button is-inverted ajax-link" action="{{ route('logout.post') }}" data-loading="1">
                        <span class="icon is-small">
                           <i class="fa fa-user"></i>
                        </span>
                            <span>Выйти</span>
                        </a>
                    </div>
                </span>
                --}}
                <a class="nav-item nav-logo tippy-popover trigger" data-dialog="#pages-dialogs-ajax" data-ajax data-action="{{ route('user.edit.modal.post', ['id' => Sentinel::getUser()->id]) }}" title="Редактировать" href="{{ url('/') }}">
                    {{ Sentinel::getUser()->email }}
                </a>
                <span class="nav-item">
                    <a class="button is-inverted ajax-link" action="{{ route('logout.post') }}" data-loading="1">
                        <span class="icon is-small">
                           <i class="fa fa-user"></i>
                        </span>
                        <span>Выйти</span>
                    </a>
                </span>

            @endif
        </div>
    </div>
</nav>