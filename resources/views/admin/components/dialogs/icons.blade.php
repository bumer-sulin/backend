<div id="fa-icons" class="dialog">
    <div class="dialog__overlay"></div>
    <div class="dialog__content is-vcentered box" style="min-width: 300px; width: 900px;">
        <h3 class="title" style="margin-bottom: 10px;">
            Иконки
        </h3>
        <p class="control">
            <iframe src="{{ route('index.iframe', ['name' => 'icons']) }}" width="100%" height="300" align="center"></iframe>
        </p>
        <p class="control">
            <button class="dialog__close button" type="button" data-dialog="#fa-icons">
                Закрыть
            </button>
        </p>
        <div style="position: absolute; top: 10px; right: 10px;">
            <a class="dialog__close icon" data-dialog="#fa-icons">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
</div>