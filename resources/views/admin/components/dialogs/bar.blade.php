<div class="modal fade is-right-bar edit-form" id="custom-edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        {{-- <div class="dialog__loading is-black " style="width: 100%;height: 100%;"></div>--}}
        <div class="modal-content --inner"></div>
    </div>
</div>
<div class="modal fade is-left-bar edit-form" id="custom-edit-left-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="background-color: #fff">
        {{-- <div class="dialog__loading is-black " style="width: 100%;height: 100%;"></div>--}}
        <div class="modal-content --inner"></div>
    </div>
</div>
