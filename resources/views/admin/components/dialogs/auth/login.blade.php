<div id="login" class="dialog">
    <div class="dialog__overlay"></div>
    <div class="dialog__content is-vcentered box" style="width: 300px;">
        <h3 class="title" style="margin-bottom: 10px;">
            {{ trans('auth.enter') }}
        </h3>
        <form class="ajax-form" role="form" method="POST" action="{{ route('login') }}{{-- route('items.action.post',['name' => 'get']) --}}"
              data-callback="closeModalAfterSubmit"
        >
            {{ csrf_field() }}
            <p class="control has-icon" >
                <input
                        class="input"
                        name="email"
                        type="email"
                        placeholder="{{ trans('form.email') }}"
                        {{--required--}}
                >
            <span class="icon is-small">
                <i class="fa fa-envelope"></i>
            </span>
            </p>
            <p class="control has-icon">
                <input
                        class="input"
                        type="password"
                        name="password"
                        placeholder="{{ trans('form.password') }}"
                        {{--required--}}
                >
            <span class="icon is-small">
                <i class="fa fa-lock"></i>
            </span>
            </p>
            <p class="control">
                <button class="button is-primary inner-form-submit" type="submit">
                    {{ trans('auth.login') }}
                </button>
                <button class="dialog__close button" type="button" data-dialog="#login">
                    {{ trans('auth.cancel') }}
                </button>

            </p>
        </form>
        <div style="position: absolute; top: 10px; right: 10px;">
            <a class="dialog__close icon" data-dialog="#login">
                <i class="fa fa-times"></i>
            </a>
        </div>
        {{--
        <p class="dialog__content--helper-link" style="width: 100%;left: 0;">
            <a class="trigger" data-dialog="#register">
                {{ trans('auth.register_account') }}
            </a>
            |
            <a class="trigger" data-dialog="#password-email">
                {{ trans('auth.forgot_password') }}
            </a>
        </p>
        --}}
    </div>
</div>