<div id="dialog-search" class="dialog">
    <div class="dialog__overlay"></div>
    <div class="dialog__content is-vcentered box" style="width: 300px;">
        <h3 class="title" style="margin-bottom: 10px;">
            Поиск
        </h3>
        <form class="ajax-form" role="form" method="POST" action="">
            {{ csrf_field() }}
            <p class="control has-icon" >
                <input
                        class="input"
                        name="search"
                        type="text"
                        placeholder="Search"
                        {{--required--}}
                >
                <span class="icon is-small">
                    <i class="fa fa-search"></i>
                </span>
            </p>
            <p class="control">
                <button class="button is-primary inner-form-submit" type="submit">
                    Search
                </button>
                <button class="dialog__close button" type="button" data-dialog="#dialog-search">
                    Cancel
                </button>
            </p>
        </form>
        <div style="position: absolute; top: 10px; right: 10px;">
            <a class="dialog__close icon" data-dialog="#dialog-search">
                <i class="fa fa-times"></i>
            </a>
        </div>
    </div>
</div>