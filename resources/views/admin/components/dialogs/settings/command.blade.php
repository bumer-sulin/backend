<div class="modal fade is-right-bar edit-form" id="user-settings-command-edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            @include('admin.content.user.components.modals.command')
        </div>
    </div>
</div>
