{{--
    'url' => url('/admin/posts/action/upload-wysiwyg'),
    'model' => 'post',
    'oItem' => $oPost,
    'id' => 'summernote',
    'itemText' => $oPost->text
--}}

<div class="row">
    <div class="col-12">
        <div class="form-group" style="margin-bottom: 0;">
            <input id="{{ $id ?? 'summernote' }}-uid" type="hidden" name="uid">
            <textarea id="{{ $id ?? 'summernote' }}-textarea" class="textarea" name="text" style="display: none;">
                @if(isset($itemText))
                    {{ $itemText }}
                @else
                    {{ $oItem->text ?? ''}}
                @endif
            </textarea>
            <div id="{{ $id ?? 'summernote' }}"
                 class="summernote-editor"

                 data-text-area="{{ $id ?? 'summernote' }}-textarea"
                 data-text-block="{{ $id ?? 'summernote' }}-textblock"

                 data-url="{{ $url }}"
                 data-type="wysiwyg"
                 data-imageable_type="{{ $model }}"
            >

            </div>
        </div>
    </div>
</div>
