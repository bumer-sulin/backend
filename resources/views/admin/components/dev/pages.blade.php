@extends('admin.layouts.admin', [
    'breadcrumb' => 'dev.pages'
])

@section('content.title')
    @include('admin.components.pages.title', [
        'title' => 'Системные страницы',
    ])
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body" style="padding: 1rem;">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item flex-column align-items-start">
                            <div>
                                <b>Яндекс.Новости</b>
                            </div>
                            <p class="mb-1 text-muted">
                                Посмотреть вид выходного файла xml для Яндекс.Новостей
                            </p>
                            <p class="mb-0">
                                <a href="{{ route('dev.yandex.news.index') }}">Перейти</a>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
