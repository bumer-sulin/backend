@extends('admin.layouts.admin', [
    'breadcrumb' => 'dev.yandex.news'
])

@section('content.title')
    @include('admin.components.pages.title', [
        'title' => 'Яндекс.Новости',
    ])
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Данные
                </div>
                <div class="card-body" style="padding: 10px">
                    <code>
                        {{ $data }}
                    </code>
                </div>
            </div>
        </div>
{{--        <div class="col-12">--}}
{{--            <div class="embed-responsive embed-responsive-16by9">--}}
{{--                <iframe class="embed-responsive" src="{{ '/rss.xml' }}" height="500px"></iframe>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
@endsection
