@extends('admin.layouts.admin')

@section('content')
    <section class="section {{-- is-medium --}}">
        <div class="container">
            <div class="box">
                <center>
                    {!! $chart->render() !!}
                </center>
            </div>
        </div>
    </section>
@endsection
