@extends('admin.layouts.admin')

@section('content.title')
    @include('admin.components.pages.title', [
        'title' => 'Это Ваш личный кабинет '.$oComposerMember->get('user.first_name').'!'
    ])
@endsection

@section('content')
    {{--<div class="row">--}}
        {{--<div class="col-12">--}}
            {{--Добро пожаловать, {{ $oComposerMember->get('user.first_name') }} {{  $oComposerMember->get('user.last_name') }}!--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Статистика</div>
                <div class="card-body">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="callout callout-info">
                                    <div>
                                        <div class="text-info">Статьи</div>
                                        <small class="text-muted">Активных/Всего</small>
                                    </div>
                                    <strong class="h4">{{ $oComposerArticlesActive->count() ?? 0 }}/{{ $oComposerArticles->count() ?? 0 }}</strong>
                                    <div class="chart-wrapper">
                                        <canvas id="sparkline-chart-1" width="100" height="30"></canvas>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="callout callout-success">
                                    <div>
                                        <div class="text-success">Пользователи</div>
                                        <small class="text-muted">Активных/Всего</small>
                                    </div>
                                    <strong class="h4">{{ $oComposerUsers->where('status', 1)->count() ?? 0 }}/{{ $oComposerUsers->count() ?? 0 }}</strong>
                                    <div class="chart-wrapper">
                                        <canvas id="sparkline-chart-2" width="100" height="30"></canvas>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="callout callout-success">
                                    <div>
                                        <div class="text-success">Подписок</div>
                                        <small class="text-muted">Активных/Всего</small>
                                    </div>
                                    <strong class="h4">{{ $oComposerSubscriptions->where('status', 1)->count() ?? 0 }}/{{ $oComposerSubscriptions->count() ?? 0 }}</strong>
                                    <div class="chart-wrapper">
                                        <canvas id="sparkline-chart-4" width="100" height="30"></canvas>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="callout callout-warning">
                                    <div>
                                        <div class="text-warning">Категории</div>
                                        <small class="text-muted">Активных/Всего</small>
                                    </div>
                                    <strong class="h4">{{ $oComposerCategories->whereIn('status', [1, 2])->count() ?? 0 }}/{{ $oComposerCategories->count() ?? 0 }}</strong>
                                    <div class="chart-wrapper">
                                        <canvas id="sparkline-chart-3" width="100" height="30"></canvas>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="callout callout-warning">
                                    <div>
                                        <div class="text-warning">Тэги</div>
                                        <small class="text-muted">Активных/Всего</small>
                                    </div>
                                    <strong class="h4">{{ $oComposerTags->where('status', 1)->count() ?? 0 }}/{{ $oComposerTags->count() ?? 0 }}</strong>
                                    <div class="chart-wrapper">
                                        <canvas id="sparkline-chart-4" width="100" height="30"></canvas>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="callout callout-danger">
                                    <div>
                                        <div class="text-danger">Настройки</div>
                                        <small class="text-muted">Активных/Всего</small>
                                    </div>
                                    <strong class="h4">{{ $oComposerOptions->where('status', 1)->count() ?? 0 }}/{{ $oComposerOptions->count() ?? 0 }}</strong>
                                    <div class="chart-wrapper">
                                        <canvas id="sparkline-chart-4" width="100" height="30"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
