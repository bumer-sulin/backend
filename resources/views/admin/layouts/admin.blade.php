<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $oComposerSite->app->title ?? 'app.title'}}</title>
    <meta name="description" content="{{ $oComposerSite->app->description ?? 'app.description'}}">
    <meta name="keywords" content="{{ $oComposerSite->app->keywords ?? 'app.keywords'}}">
    <link rel="shortcut icon" href="{{ $oComposerSite->app->favicon['main'] ?? 'app.favicon[main]' }}">

    <link rel="stylesheet" href="{{ asset('css/admin.css') }}?v={{ $sComposerVersion }}">

    @if(!Sentinel::guest())
        <script>
            window.user = {!! json_encode([
                'id' => Sentinel::getUser()->id
            ]) !!};
            window.jsVars = {!! $jsVars ?? '' !!};
        </script>
    @endif
    @stack('styles')
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden {{ $sBodyClass ?? '' }} {{ Session::has('sidebar-toggle') ? 'sidebar-hidden' : '' }}"
        {{-- app flex-row align-items-center --}}
>
@hasSection('body')
    @yield('body')
@else
    <div id="header">
        @include('admin.layouts.components.header')
    </div>

    <div id="app" class="app-body" style="min-height: 100vh;margin-top: 55px;min-width: 1150px;">

        @include('admin.layouts.components.sidebar')

        <main class="main">
            <div class="page-header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            @yield('head')
                            @yield('content.title')
                        </div>
                    </div>
                </div>
            </div>
            <div style="position: relative;">
                @include('admin.components.breadcrumbs')
                <div style="position: absolute;right: 5px;top: 5px;">
                    @yield('breadcrumb-right')
                </div>
            </div>

            <div class="container-fluid">

                {{--
                <div id="current-title" hidden="">Dashboard</div>
                <div id="current-desc" hidden="">Welcome to Root Bootstrap 4 Admin Template</div>
                --}}

                <div class="animated fadeIn">

                    @yield('content')

                </div>
            </div>
        </main>
        @include('admin.layouts.components.aside')
    </div>

    <div id="footer">
        @include('admin.layouts.components.footer')
    </div>
@endif

<div id="modals">
    @include('admin.components.loaders.square')

    @include('admin.components.dialogs.settings.command')
    @include('admin.components.dialogs.ajax')
    @include('admin.components.dialogs.confirm')
    {{--@include('admin.components.dialogs.form')--}}
    @include('admin.components.dialogs.info')
    @include('admin.components.dialogs.bar')

    @yield('modals')
</div>


<div id="scripts">
    @include('admin.components.toastr')
    <script src="{{ asset('js/admin/manifest.js') }}?v={{ $sComposerVersion }}"></script>
    <script src="{{ asset('js/admin/vendor.js') }}?v={{ $sComposerVersion }}"></script>
    <script src="{{ asset('js/admin/app.js') }}?v={{ $sComposerVersion }}"></script>
    @stack('scripts')
</div>
</body>
</html>
