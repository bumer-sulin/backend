$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': window.Laravel }
});

/**
 * Init callbacks
 */



/**
 * @see ./fancybox.js
 */
window['fancybox'] = function() {
    initFancybox();
};




/*
 * Каллбэк для закрытия модального окна после отправки формы.
 */
window['after-closeModalAfterSubmit'] = function (result, $target) {
    if(result.success){
        var $dialog = $target.closest('.dialog');
        $dialog.find('.dialog__overlay').trigger('click');
    }
};
/*
 * Каллбэк для закрытия модального окна после отправки формы.
 */
window['after-closeBootstrapModalAfterSubmit'] = function (result, $target) {
    var $body = $('body');
    if(result.success){
        $body.removeClass('--fixed');
        $body.css('margin-right', 0);
        $target.closest('.modal').modal('hide');
    }
};

/*
 * Каллбэк для закрытия модального окна после отправки формы.
 */
window['closeModalAfterSubmit'] = function (result, $target) {
    var $body = $('body');
    if(result.success){
        $body.removeClass('--fixed');
        $body.css('margin-right', 0);
        $target.closest('.modal').modal('hide');
    }
};

window['before-openModalBodyBeforeSubmit'] = function ($target) {
    if ($target.find('.modal-body.--contact').hasClass('hidden')) {
        $target.find('.modal-body.--contact').removeClass('hidden');
    } else {
        return true;
    }
};

window['after-showBodyMessageAfterSubmit'] = function (result, $target) {
    if(result.success){
        $target.find('.modal-body').remove();
        $target.append('<div class="modal-body" style="text-align: center;">' + result.message + '</div>');
        $target.find('.modal-footer').css('display', 'none');
    }
};
window['after-updateBasketCountAfterSubmit'] = function (result, $target) {
    if (result.success) {
        var $t = $('a[href="#getBasket"]');
        var action = $t.data('action');
        var data = {};
        setTimeout(function() {
            document.getBasketItems(action, data, $('#getBasket'));
        }, 5000);
    }
};



window['after-refreshQuoteCounter'] = function (result, $target) {
    if(result.success){
        var $counter = $($target.data('quote-counter'));
        if (result.count !== 0) {
            $counter.text('(' + result.count + ')');
        } else {
            $counter.text('');
        }
    }
};

/*
 * Каллбэк для закрытия модального окна после отправки формы.
 */
window['after-refreshAfterSubmit'] = function (result, $target) {
    if (result.success && $target.data('list') && $target.data('list-action')){
        var $view = $($target.data('list'));
        var url = $target.data('list-action');
        var data = null;
        if ($target.data('form-data')) {
            console.log('serializeArray');
            data = $($target.data('form-data')).serializeArray();
        }
        $.ajax({
            url: url,
            type: "POST",
            data: data,
            success: function (result) {
                if (result.view) {
                    $view.html(result.view);
                    if ($target.data('counter')) {
                        var $counter = $($target.data('counter'));
                        $counter.html(result.count);
                    }
                } else {
                    $view.html(result);
                }
                if ($target.data('list-loader')) {
                    $($target.data('list-loader')).css('display', 'none');
                }
                if ($target.data('quote-counter')) {
                    var $count = $($target.data('quote-counter'));
                    if (result.count !== 0) {
                        $count.text('(' + result.count + ')');
                    } else {
                        $count.text('');
                    }
                }
            },
            error: function (msg) {
                console.log(msg);
                if ($target.data('list-loader')) {
                    $($target.data('list-loader')).css('display', 'none');
                }
            }
        });
    }
};

window['document-strongRefreshView'] = function($view, url, $data, $counter) {
    var data = null;
    if ($data) {
        data = $data.serializeArray();
    }
    console.log(url);
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        success: function ($list) {
            if ($list.view) {
                $view.html($list.view);
                if ($counter) {
                    $counter.html($list.count);
                }
            } else {
                $view.html($list);
            }
        },
        error: function (msg) {
            console.log(msg);
        }
    });
};

window['after-refreshModalAfterSubmit'] = function(result, $target) {
    if (result.success) {
        var $view = $target.closest('.is-modal-body');
        if (result.view) {
            $view.html(result.view);
        } else {
            $view.html(result);
        }
    }
};


window['after-replaceIconAfterSubmit'] = function(result, $target) {
    if (result.success) {
        var $icon = $target.find('.icon i');
        if ($icon.hasClass($icon.attr('on-class'))) {
            $icon.removeClass();
            $icon.addClass($icon.attr('off-class'));
        } else {
            $icon.removeClass();
            $icon.addClass($icon.attr('on-class'));
        }
    }
};

window['after-replaceFormAttributesAfterSubmit'] = function(result, $target) {
    if (result.success) {
        var title = $target.attr('title');
        if (title == $target.attr('on-title')) {
            $target.attr('title', $target.attr('off-title'));
        } else {
            $target.attr('title', $target.attr('on-title'));
        }
    }
};

window['before-clearModalBeforeSubmit'] = function($target) {
    $('#getBasket .dialog__loading').css('display', 'block');
    $('#getBasket .modal-table').empty();
};


window['initPluginsAfterSubmit'] = function(result, $target) {
    if (result.success) {
        var sInit = $target.data('view-init');
        if (sInit) {
            var aInit = _.split(sInit, ',');
            _.each(aInit, function (val) {
                var sFuncName = _.trim(val);
                if (_.isFunction(window['initPlugin-' + sFuncName])){
                    window['initPlugin-' + sFuncName]();
                }
            });
        }
    }
};

window['initPlugin-refbook'] = function() {

    console.log('initPlugin-refbook');
};