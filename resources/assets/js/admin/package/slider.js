if (typeof MasterSlider === "function") {
    var slider = new MasterSlider();
    slider.setup('masterslider' , {
        width:800,    // slider standard width
        //height:350,   // slider standard height
        autoHeight: true,
        space:5,
        speed: 50,
        // more slider options goes here...
        // check slider options section in documentation for more options.
    });
// adds Arrows navigation control to the slider.
    slider.control('arrows');
}

