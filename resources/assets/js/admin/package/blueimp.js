/*
$(document).ready(function() {
    initFileUploader();
});
*/
/**
 * @see ./blueimp.js
 */
window['uploader'] = function() {
    initFileUploader();
};

var initFileUploader = function() {
    $('#file').fileupload({
        dataType: 'json',
        sequentialUploads: true,
        singleFileUploads: false,
        add: function (e, data) {
            $('#file').closest('form').find('.help').remove();
            console.log(data.formData);
            data.formData = {id: $('#file').data('id')};
            $('#file + label').addClass('is-loading');
            data.submit();
            /*
             $.ajax({
             url: '/image/upload',
             type: 'POST',
             dataType: 'json',
             processData: false,
             data: data.files,
             headers: {'X-CSRF-Token': window.Laravel},
             success: function (result) {
             console.log('success');
             data.submit();
             },
             error: function (message) {
             console.log('error');
             }
             });
             */
        },
        done: function (e, data) {
            console.log(data.result);
            $('#file + label').removeClass('is-loading');
            var $target = $('#file');
            if (data.result.success) {
                $target.closest('form').find('.is-modal-body').html(data.result.view);
                if (data.result.src) {
                    $('img[data-user-image]').attr('src', data.result.src);
                }
            }

            window['document-strongRefreshView'](
                $($target.data('list')),
                $target.data('list-action'),
                $($target.data('form-data')),
                $($target.data('counter'))
            );

        },
        error: function (data, textStatus, errorThrown) {
            for (var key in data.responseJSON) {
                $('#file').closest('form').prepend('<span class="help has-text-centered is-danger">' + data.responseJSON[key][0] + '</span>');
            }

            $('#file + label').removeClass('is-loading');
            console.log('error');
        }
    });
};