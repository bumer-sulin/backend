import tippy from "tippy.js";

$(document).ready(function ($) {
    window['tooltip']();
});

window['tooltip'] = function () {
    const button = document.querySelector('[data-tippy-popover]');
    if (button.length) {
        const instance = tippy(button);
        instance.destroyAll();
    }
    // $('[rel="tooltip"],[data-rel="tooltip"]').tooltip({"placement": "top", delay: {show: 200, hide: 200}});
    $('[data-toggle="popover"]').popover({
        trigger: 'hover',
        placement: 'top'
    }).on('show.bs.popover', function () {
        let $button = $(this);
        setTimeout(function () {
            // console.log($button);
            // console.log($button.attr('aria-describedby'));
            // console.log('#' + $button.attr('aria-describedby'));
            // console.log($('#' + $button.attr('aria-describedby')));

            $('#' + $button.attr('aria-describedby')).find('input').on('keyup', function () {
                $button.data('author',  $(this).val());
                console.log($(this).val());
            });
        }, 100);
    }).on('hide.bs.popover', function () {
        console.log($(this));
    });
    tippy('[data-tippy-popover]', {
        interactive: true,
        //theme: 'light',
        hideOnClick: false,
        animateFill: false,
        //duration: [275, 250000],
    });
    tippy('[data-tippy-input-popover]', {
        theme: 'light',
        animateFill: false,
        hideOnClick: false,
        trigger: 'click',
        placement: 'bottom',
    });


};


$(document).ready(function ($) {
    //window['datetimepicker']();
});

window['datetimepicker'] = function () {
    // $('.datetimepicker').datetimepicker({
    //     locale: 'ru',
    // });
};
