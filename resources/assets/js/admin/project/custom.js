$(document).ready(function(){
    $('body').on('click','.settings .card-header .spoiler',function(){
        $(this).closest('.settings').toggleClass('active');
        $(this).closest('.settings').find('.card-block').slideToggle();
    });

    $('body').on('click', '.--delete-container', function() {
        if ($(this).data('parent')) {
            $(this).closest($(this).data('container')).remove();
        }
    });


    $('body').on('shown.bs.tab', 'a[data-toggle="tab"][data-hidden-submit]', function (e) {
        var tab = $(e.target);
        var submit = tab.closest('.modal-content').find('.modal-footer .ajax-link');
        if (parseInt(tab.data('hidden-submit')) === 0) {
            submit.removeClass('hidden');
        } else {
            submit.addClass('hidden');
        }
    });

    $('body').on('click', '.clone-tr', function() {
        console.log('hi');
        var tbody = $(this).closest('tbody');
        var tr = $(this).data('tr');
        var clone = $(tr).clone().css('display', 'table-row').removeClass('temp-ref-value');
        clone.find('input[disabled]').removeAttr('disabled');
        clone.find('select[disabled]').removeAttr('disabled');
        tbody.children('.clone-tr-container').before(clone);
        if (clone.find('.form-control-focus').length) {
            clone.find('.form-control-focus').focus();
        }
    });
    $('body').on('click', '.delete-clone-tr', function() {
        $(this).closest('tr').remove();
    });
});


