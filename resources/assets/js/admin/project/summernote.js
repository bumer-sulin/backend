$(function () {
    window['initSummernote']();
});

function sendFile(file, editor, welEditable) {
    let data = new FormData();
    let $container = editor;
    let id = $container.attr('id');
    data.append('file', file);
    data.append('upload_type', $container.data('type'));
    data.append('imageable_type', $container.data('imageable_type'));
    data.append('uid', $('#' + id + '-uid').val());
    let url = $container.data('url');
    $.ajax({
        data: data,
        type: 'POST',
        xhr: function () {
            let myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) myXhr.upload.addEventListener('progress', progressHandlingFunction, false);
            return myXhr;
        },
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            if (response.success) {
                $('.note-image-url').val(response.file.url);
                $('#' + id + '-uid').val(response.file.uid);
            }
            $container.summernote('insertImage', response.file.url, response.file.name);
            console.log(response);
            //editor.insertImage(welEditable, url);
        },
        error: function (message) {
            console.log(message);
        }
    });
}

function progressHandlingFunction(e) {
    if (e.lengthComputable) {
        $('progress').attr({value: e.loaded, max: e.total});
        // reset progress on complete
        if (e.loaded == e.total) {
            $('progress').attr('value', '0.0');
        }
    }
}


window['initSummernote'] = function () {
    let $element;
    $element = $('.modal .note-video-btn').closest('.modal');
    if ($element.length) {
        $element.remove();
    }
    $element = $('.modal .note-image-btn').closest('.modal');
    if ($element.length) {
        $element.remove();
    }
    $element = $('.link-dialog');
    if ($element.length) {
        $element.remove();
    }
    $element = $('.note-link-popover');
    if ($element.length) {
        $element.remove();
    }
    $element = $('.note-image-popover');
    if ($element.length) {
        $element.remove();
    }
    $element = $('a[href="http://summernote.org/"]').closest('.modal');
    if ($element.length) {
        $element.remove();
    }

    /**
     * <div id="summernote"
     *  data-text-area="summernote-textarea" // куда будет сохраняться текст
     *  data-text-block="textblock" // где он будет выводиться, только для быстрого просмотра
     * >
     *
     * <div>
     */
    $('.summernote-editor').each(function () {
        initSummernote($('#' + $(this).attr('id')));
    });


};

let initSummernote = function ($container) {
    let $textArea = $('#' + $container.data('text-area'));
    let textArea = $textArea.attr('id');
    let $textBlock = $('#' + $container.data('text-block'));
    let textBlock = $textBlock.attr('id');
    let editor = '.note-editable';
    let $editor = $('.note-editable');
    $container.summernote({
        lang: 'ru-RU',
        height: 500,                 // set editor height
        imageTitle: {
            specificAltField: true,
        },
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor

        focus: true,                // set focus to editable area after initializing summernote
        dialogsInBody: true,
        // https://codemirror.net/doc/manual.html
        codemirror: {
            mode: 'text/html',
            htmlMode: true,
            lineNumbers: true,
            lineWrapping: true,
            spellcheck: true,
            autocorrect: true,
            autocapitalize: true,
            theme: 'default',
        },
        colors: [
            window.colors,
        ],
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear', 'hr']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            //['height', ['height']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['view', [
                'codeview',
                'picture',
                'link',
                'video',
                'fullscreen',
            ]],
        ],
        popover: {
            image: [
                ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']],
                ['custom', ['imageTitle']],
            ],
            link: [
                ['link', ['linkDialogShow', 'unlink']]
            ],
            air: [
                ['color', ['color']],
                ['font', ['bold', 'underline', 'clear']],
                ['para', ['ul', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture']]
            ]
        },
        callbacks: {
            onInit: function () {
                let parent = document.getElementById(textArea);
                if (parent) {
                    $container.summernote('code', parent.value);
                }

                $('.note-video-url').closest('.modal').addClass('summernote-video-modal');
                $('.note-image-btn').closest('.modal').addClass('summernote-image-modal');

                let $altText = $('.note-image-alt-text');
                $altText.closest('.modal').addClass('summernote-image-text-modal');
                $altText.closest('.form-group').find('label').text('Автор фотографии');

                let $altButton = $('.note-image-title-btn');
                $altButton.text('Сохранить');
                $altButton.click(function () {
                    setTimeout(function () {
                        $('.note-editable').trigger('input');
                    }, 10);
                });
            },
            onChange: function (contents, $editable) {
                let parent = document.getElementById(textArea);
                if (parent) {
                    parent.value = contents;
                }
                let visual = document.getElementById(textBlock);
                if (visual) {
                    visual.innerHTML = contents;
                }
            },
            onBlur: function () {
                /*
                let parent = document.getElementById('parent');
                if (parent) {
                    parent.value = $('#summernote').val();
                }
                let visual = document.getElementById('textblock');
                if (visual) {
                    visual.innerHTML = $('#summernote').val();
                }
                */
            },
            onImageUpload: function (files, editor, welEditable) {
                sendFile(files[0], $container);
                console.log(files);
                console.log(editor);
                console.log(welEditable);
                // upload image to server and create imgNode...
                //$('#summernote').summernote('insertNode', '<img src="https://dummyimage.com/mediumrectangle/222222/eeeeee">');
            },
            onPaste: function (e) {
                let id = 'hidden-summernote-html';
                $('body').append('<div id="' + id + '" style="display: none;"></div>');
                let $id = $('#' + id);
                let bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('text/html');
                $id.html(bufferText);
                let all = document.getElementById(id).getElementsByTagName('*');
                for (let i = -1, l = all.length; ++i < l;) {
                    all[i].setAttribute('style', '');
                    all[i].removeAttribute('style');
                }
                let html = $id.html();
                /*
                let string = [].map.call( all, function(node){
                        return $(node).get(0).outerHTML;
                        //return '<' + node.tagName + '>' + node.textContent || node.innerText || "" + '</' + node.tagName + '>';
                    }).join("");
                */
                e.preventDefault();
                setTimeout(function () {
                    let parent = document.getElementById(textArea);
                    $container.summernote('code', parent.value + html);
                    $id.remove();
                    $('.note-editable').trigger('input');
                }, 10);
            }
        }
    });
};

let VideoButton = function (context) {
    let ui = $.summernote.ui;

    // create button
    let button = ui.button({
        contents: '<i class="fa fa-film"/> Hello',
        tooltip: 'Вставить видео',
        click: function () {
            // invoke insertText method with 'hello' on editor module.
            context.invoke('editor.insertText', 'hello');
        }
    });

    return button.render();   // return button as jquery object
};
