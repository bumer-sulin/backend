const shortAndSweet = require('short-and-sweet/dist/short-and-sweet.min.js');

$(document).ready(function ($) {
    window['counter']();
});

window['counter'] = function () {
    if ($('input[data-counter]').length) {
        shortAndSweet('input[data-counter]', {
            counterClassName: 'my-short-and-sweet-counter'
        });
    }
};
