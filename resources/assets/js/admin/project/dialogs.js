$.ajaxSetup({
    headers: { 'X-CSRF-TOKEN': window.Laravel }
});

window['closeDialogCallback'] = function($dialog) {
    $dialog.find('input.is-danger').removeClass('is-danger');
    $dialog.find('.help.is-danger').remove();
    $dialog.find('input').empty();
};

let ajaxDialogs = {

    settings : {
        openDialogClass: 'dialog--open',

        ajax: false,
        confirm: false,
        ajaxClass: 'dialog-ajax',
        disableOverlayClass: 'dialog-disabled_overlay',
        loadingClass: '.dialog__loading',
        loading: '<div class="dialog__loading"><div></div></div>',
        isBootstrap: false
    },

    target : null,

    bind : function (sElem, sDelegateFrom, sAction, oSettings) {
        sDelegateFrom = sDelegateFrom || '';
        sAction = sAction || 'submit';
        let fn = function (event) {
            event.preventDefault();
            this.target = $(event.currentTarget);
            this.send();
            return false;
        };
        fn = _.bind(fn, this);
        if(sDelegateFrom){
            $(sDelegateFrom).on(sAction, sElem, fn);
        } else {
            $(sElem).on(sAction, fn);
        }
        let self = this;
        _.each(oSettings, function(field, key) {
            self.settings[key] = field;
        });
        //console.log(ajaxDialogs.settings);
    },

    send : function () {
        console.log('send click');
        let $dialog = $(this.target.attr('data-dialog'));
        let $body = $('body');
        console.log($dialog);
        console.log($dialog.data());

        this.settings.isBootstrap = $dialog.hasClass('modal');

        if (this.settings.isBootstrap) {
            $body.addClass('--fixed');
            $body.css('margin-right', document.getScrollbarWidth() + 'px');
            $body.find('.app-header').css('padding-right', document.getScrollbarWidth() + 'px');
        }

        if (this.settings.isBootstrap) {
            try {
                $dialog.modal('show');
            } catch($e) {
                console.log('Error: Modal is transitioning.');
            }
        } else {
            $dialog.addClass(this.settings.openDialogClass);
        }

        let self = this;
        $dialog.off().on('click', '[data-dismiss="modal"]', function() {
            if (self.settings.isBootstrap) {

                $body.removeClass('--fixed');
                $body.css('margin-right', 0);
                $body.find('.app-header').css('padding-right', 0);
            }
            if (self.target.attr('data-without-footer') !== undefined) {
                if (self.settings.isBootstrap) {
                    $dialog.find('.modal-footer').removeClass('hidden');
                }
            }

            try {
                $(this).closest('.modal').modal('hide');
            } catch($e) {
                console.log('Error: Modal is transitioning.');
            }
        });

        //$dialog.modal('show');

        if (this.settings.ajax) {
            console.log(this.settings.ajax);
            this.addAjax($dialog, this.target);
        }

        if (this.settings.confirm) {
            console.log(this.settings.confirm);
            this.addConfirm($dialog, this.target);
        }

        if (this.target.attr('data-disabled-overlay') !== undefined) {
            $dialog.addClass(this.settings.disableOverlayClass);
        }

    },

    addAjax: function ($dialog, $target) {
        let self = this;
        this.removeAjax($dialog);
        $dialog.addClass(this.settings.ajaxClass);
        if (this.settings.isBootstrap) {
            $dialog.find('.modal-content').append(this.settings.loading);
            $dialog.find('.dialog__loading').addClass('is-black');
        } else {
            $dialog.append(this.settings.loading);
        }

        $dialog.addClass(this.settings.disableOverlayClass);

        setTimeout(function() {
            $dialog.removeClass(self.settings.disableOverlayClass);
        }, 7000);


        $.ajax({
            url: $target.attr('data-action'),
            type: "POST",
            data: self.dataAjax($target),
            success: function (result) {
                $dialog.find(self.settings.loadingClass).remove();
                $dialog.removeClass(self.settings.disableOverlayClass);
                if (result.view) {
                    self.appendAjax($dialog, result.view);
                } else {
                    self.appendAjax($dialog, result);
                }
                self.afterAppend($dialog, result);

            },
            error: function(data, status, headers, config) {
                $dialog.removeClass(self.settings.disableOverlayClass);
                self.removeAjax($dialog);
                self.afterError($dialog);
            }
        });
    },
    afterAppend: function($dialog, result) {
        if (this.target.attr('data-ajax-init') !== undefined) {
            let aInit = _.split(this.target.attr('data-ajax-init'), ',');
            if(_.first(aInit) === '@'){
                aInit = _.drop(aInit);
            }
            _.each(aInit, function (val) {
                console.log(val);
                let sFuncName = _.trim(val);
                if (_.isFunction(window[sFuncName])){
                    window[sFuncName]();
                }
            });
        }
        if (this.target.attr('data-callback') !== undefined) {
            let aInit = _.split(this.target.attr('data-callback'), ',');
            if(_.first(aInit) === '@'){
                aInit = _.drop(aInit);
            }
            let self = this;
            let $target = self.target;
            _.each(aInit, function (val) {
                let sFuncName = _.trim(val);
                if (_.isFunction(window[sFuncName])){
                    window[sFuncName](result, $target);
                }
            });
        }

        if (this.target.attr('data-without-footer') !== undefined) {
            if (this.settings.isBootstrap) {
                $dialog.find('.modal-footer').addClass('hidden');
            }
        }
    },
    afterError: function($dialog) {
        let error =
            '<div class="modal-header">' +
                '<h4 class="modal-title">Ошибка сервера</h4>' +
                '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                    '<span aria-hidden="true">×</span>' +
                '</button>' +
            '</div>';

        if (this.settings.isBootstrap) {
            $dialog.find('.modal-content').html(error);
        } else {
            $dialog.append(error);
        }
    },
    removeAjax: function ($dialog) {
        if (!this.settings.isBootstrap) {
            $dialog.children()
                .filter(function() {
                    return (!$(this).hasClass('dialog__overlay'));
                }).remove();
            $('#custom-edit-modal').css('display', 'none');
        } else {
            $dialog.find('.modal-content').children().remove();
        }
    },
    appendAjax: function($dialog, view) {
        this.removeAjax($dialog);
        if (this.settings.isBootstrap) {
            $dialog.find('.modal-content').html(view);
        } else {
            $dialog.append(view);
        }
    },
    dataAjax: function($target) {
        return $target.data();
    },
    addConfirm: function($dialog, $target) {
        let subtitle = $dialog.find('.--subtitle');
        subtitle.addClass('hidden');

        if ($target.data('text') !== undefined) {
            $dialog.find('.--text').text($target.data('text'));
        }
        if ($target.data('subtitle') !== undefined) {
            subtitle.html($target.data('subtitle'));
            subtitle.removeClass('hidden');
        }
        if ($target.data('action') !== undefined) {
            $dialog.find('.ajax-form').attr('action', $target.data('action'));
        }
        if ($target.data('list-action') !== undefined) {
            $dialog.find('.ajax-form').attr('data-list-action', $target.data('list-action'));
        }
        if ($target.data('id') !== undefined) {
            $dialog.find('.ajax-form input[name="id"]').val($target.data('id'));
        }
    },
    afterClose($dialog) {
        let self = this;
        console.log(!$dialog.hasClass(self.settings.disableOverlayClass));
        if (!$dialog.hasClass(self.settings.disableOverlayClass)) {
            $dialog.removeClass(self.settings.openDialogClass);
            if (_.isFunction(window['closeDialogCallback'])){
                window['closeDialogCallback']($dialog);
            }
            if ($dialog.hasClass(self.settings.ajaxClass)) {
                $dialog.removeClass(self.settings.ajaxClass);
                self.removeAjax($dialog);
            }
        }
        $('#custom-edit-modal').css('display', 'block');
    }
};



/*
 |----------------------------------------
 | Dialogs
 |
 | Template:
 |
 | Button
 | <a class="trigger" data-dialog="#register">Регистрация</a>
 | <a class="trigger" data-dialog="#register" data-ajax data-action="url" data-ajax-init="callback, callback">Регистрация</a>
 | <a class="trigger" data-dialog="#register" data-disabled-overlay>Регистрация</a>
 |----------------------------------------
 */
$(document).ready(function() {
    window['bulkInit']();
});

window['bulkInit'] = function() {
    $('body').on('click', '.dialog .dialog__close', function() {
        console.log('dialog__close');
        let $dialog = $(this).closest('.dialog');
        ajaxDialogs.afterClose($dialog);
    });
    $('body').on('click', '.dialog .dialog__overlay', function() {
        console.log('dialog__overlay');
        let $dialog = $(this).closest('.dialog');
        ajaxDialogs.afterClose($dialog);
    });
    $('body').on('click', '.modal-backdrop', function() {
        $('.is-right-bar').find('.close[data-dismiss="modal"]').trigger('click');
        // setTimeout(function () {
        //     ajaxDialogs.afterClose($dialog);
        // }, 1000);
        // try {
        //     $('.modal.is-right-bar.show').modal('hide');
        // } catch($e) {
        //     console.log('Error: Modal is transitioning.');
        // }
    });


    let simpleDialog = $.extend(true, {}, ajaxDialogs);
    let ajaxDialog = $.extend(true, {}, ajaxDialogs);
    let confirmDialog = $.extend(true, {}, ajaxDialogs);


    simpleDialog.bind('.trigger[data-modal]', 'body', 'click', {
        openDialogClass: 'dialog--open'
    });

    ajaxDialog.bind('.trigger[data-ajax]', 'body', 'click', {
        openDialogClass: 'dialog--open',
        ajax: true
    });
    ajaxDialog.bind('.trigger-ajax[data-ajax]', 'body', 'click', {
        openDialogClass: 'dialog--open',
        ajax: true
    });
    confirmDialog.bind('.trigger[data-confirm]', 'body', 'click', {
        openDialogClass: 'dialog--open',
        confirm: true
    });
};
