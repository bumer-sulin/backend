$(document).ready(function() {
    //window['uploader']();

    $(document).bind('dragover', function (e) {
        var dropZones = $('.dropzone'),
            timeout = window.dropZoneTimeout;
        if (timeout) {
            clearTimeout(timeout);
        } else {
            dropZones.addClass('in');
        }
        var hoveredDropZone = $(e.target).closest(dropZones);
        dropZones.not(hoveredDropZone).removeClass('hover');
        hoveredDropZone.addClass('hover');
        window.dropZoneTimeout = setTimeout(function () {
            window.dropZoneTimeout = null;
            dropZones.removeClass('in hover');
        }, 100);
    });
});

/**
 * @see ./blueimp.js
 */
window['uploader'] = function() {
    console.log('init uploader');
    /**
     * <div class="file-uploader" id="uploader-1212"
     *  data-dropzone="#dropzone-1212"
     * >
     *
     * <div>
     */
    $('.file-uploader').each(function() {
        if (!$(this).hasClass('--is-init')) {
            $(this).addClass('--is-init');
            initFileUploader($(this));
        }
    });
    window['drag-image']();
    $('[data-toggle="popover"]').popover();
};

var initFileUploader = function($container) {
    console.log($container);
    var id = '#' + $container.attr('id');
    var $dropzone = $($container.data('dropzone'));
    var $loadingContainer = $($container.data('loading-container'));

    $container.fileupload({
        dropZone: $dropzone,
        dataType: 'json',
        sequentialUploads: true,
        singleFileUploads: false,
        replaceFileInput: false,
        add: function (e, data) {
            console.log('fileupload add');
            var $form = $container;
            $loadingContainer.addClass('dialog__loading is-black is-container');
            $form.closest('#gallery-container').find('.help').remove();
            data.formData = _.merge(data.formData, $container.data());
            console.log(data.formData);
            $(id + ' + label').addClass('is-loading');
            data.submit();
        },
        done: function (e, data) {
            console.log('fileupload done');
            console.log(data.result);
            $(id + ' + label').removeClass('is-loading');
            var update = $container.data('view-init');
            console.log($container.data('loading-container'));
            console.log($loadingContainer);
            if (data.result.success) {
                $loadingContainer.find('.is-gallery-row').html(data.result.view);

                if (data.result.src) {
                    $('img[data-user-image]').attr('src', data.result.src);
                }
            }
            $loadingContainer.removeClass('dialog__loading is-black is-container');
            if (update) {
                setTimeout(function() {
                    window['uploader']();
                    /* ---------- Tooltip ---------- */
                }, 100);
            }
        },
        error: function (data, textStatus, errorThrown) {
            for (var key in data.responseJSON) {
                $container.closest('#gallery-container').prepend('<div class="help text-center is-danger" style="margin-bottom: 5px;">' + data.responseJSON[key][0] + '</div>');
            }
            $(id + ' + label').removeClass('is-loading');
            console.log('error');
            $loadingContainer.removeClass('dialog__loading is-black is-container');
            var update = $container.data('view-init');
            if (update) {
                setTimeout(function() {
                    window['uploader']();
                    /* ---------- Tooltip ---------- */
                }, 100);
            }
        }
    });
    $dropzone.click(function() {
        $container.trigger('click');
    });
};