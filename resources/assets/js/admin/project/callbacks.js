/**
 * Init callbacks
 */

/*
 * Каллбэк для закрытия модального окна после отправки формы.
 */
window['closeModalAfterSubmit'] = function (result, $target) {
    let $body = $('body');
    if (result.success) {
        $body.removeClass('--fixed');
        $body.css('margin-right', 0);
        $target.closest('.modal').modal('hide');
    }
};
/*
 * Каллбэк для закрытия модального окна после отправки формы.
 */
window['closeAndTriggerDialogAfterSubmit'] = function (result, $target) {
    if (result.success) {
        $target.closest('.modal').modal('hide');
        let action = $target.data('trigger-action');
        let id = $target.data('trigger-action-id');
        $('button[data-action="' + action + '"][data-id="' + id + '"]').trigger('click');
    }
};
/*
 * Каллбэк для закрытия модального окна после отправки формы.
 */
window['closeDialogAfterSubmit'] = function (result, $target) {
    if (result.success) {
        let $dialog = $target.closest('.dialog');
        $dialog.find('.dialog__overlay').trigger('click');
    }
};

/*
 * Каллбэк для закрытия модального окна после отправки формы.
 */
window['refreshAfterSubmit'] = function (result, $target) {
    if (result.success && $target.data('list') && $target.data('list-action')) {
        let aLists = _.split($target.data('list'), ',');
        let aListActions = _.split($target.data('list-action'), ',');
        _.each(aLists, function (list, key) {
            let $view = $(list);
            let url = aListActions[key];
            let data = null;
            if ($target.data('form-data')) {
                console.log('serializeArray');
                data = $($target.data('form-data')).serializeArray();
            }
            if ($view.data('name') && $target.data('list-name') && trim($view.data('name')) !== trim($target.data('list-name'))) {
                return;
            }
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                success: function ($list) {
                    if ($list.view) {
                        $view.html($list.view);
                        if ($target.data('counter')) {
                            let $counter = $($target.data('counter'));
                            $counter.html($list.count);
                        }
                        if ($list.id !== undefined && $target.data('edit-after-create')) {
                            // начать повторы с интервалом 2 сек
                            let triggerOenModal = setInterval(function() {
                                console.log('triggerOenModal');
                                let $button = $('.admin-table a[data-edit=' + $list.id + ']');
                                if (!$('body').hasClass('modal-open') && $button.length) {
                                    $button.trigger('click');
                                    clearInterval(triggerOenModal);
                                }
                            }, 100);
                            setTimeout(function() {
                                clearInterval(triggerOenModal);
                            }, 5000);
                        }
                    } else {
                        $view.html($list);
                    }

                    let sCallbacks = $target.data('ajax-list-init');
                    if (sCallbacks) {
                        let aCallbacks = _.split(sCallbacks, ',');
                        _.each(aCallbacks, function (val) {
                            let sFuncName = _.trim(val);
                            if (_.isFunction(window[sFuncName])) {
                                window[sFuncName]();
                            }
                        });
                    }
                    window['tooltip']();
                    if ($list.count !== undefined) {
                        let $count = $('.--count-table-view .--get');
                        if ($count.length) {
                            $count.html($list.count);
                        }
                    }
                    if ($target.data('loading-container')) {
                        for (let key in window.ajaxForm.loading.container) {
                            $($target.data('loading-container')).removeClass(window.ajaxForm.loading.container[key])
                        }
                    }
                },
                error: function (msg) {
                    if ($target.data('loading-container')) {
                        for (let key in window.ajaxForm.loading.container) {
                            $($target.data('loading-container')).removeClass(window.ajaxForm.loading.container[key])
                        }
                    }
                    console.log(msg);
                }
            });
        });
    }
};

window['strongRefreshView'] = function ($view, url, $data, $counter) {
    let data = null;
    if ($data) {
        data = $data.serializeArray();
    }
    console.log(url);
    $.ajax({
        url: url,
        type: "POST",
        data: data,
        success: function ($list) {
            if ($list.view) {
                $view.html($list.view);
                if ($counter) {
                    $counter.html($list.count);
                }
            } else {
                $view.html($list);
            }
        },
        error: function (msg) {
            console.log(msg);
        }
    });
};

window['refreshModalAfterSubmit'] = function (result, $target) {
    if (result.success) {
        let $view = $target.closest('.is-modal-body');
        if (result.view) {
            $view.html(result.view);
        } else {
            $view.html(result);
        }
    }
};


window['replaceIconAfterSubmit'] = function (result, $target) {
    if (result.success) {
        let $icon = $target.find('.icon i');
        if ($icon.hasClass($icon.attr('on-class'))) {
            $icon.removeClass();
            $icon.addClass($icon.attr('off-class'));
        } else {
            $icon.removeClass();
            $icon.addClass($icon.attr('on-class'));
        }
    }
};

window['replaceFormAttributesAfterSubmit'] = function (result, $target) {
    if (result.success) {
        let title = $target.attr('title');
        if (title == $target.attr('on-title')) {
            $target.attr('title', $target.attr('off-title'));
        } else {
            $target.attr('title', $target.attr('on-title'));
        }
    }
};


window['updateView'] = function (result, $target) {
    if (result.success && result.view) {
        $($target.data('view')).html(result.view);
    }
    if (result.success && result.src) {
        $('img[data-user-image]').attr('src', result.src);
    }
};

window['replaceView'] = function (result, $target) {
    if (result.success && result.view) {
        $($target.data('view')).replaceWith(result.view);
    }
};


window['resetSearchFilters'] = function (result, $target) {
    if (result.success) {
        $($target.data('search-container') + ' select').each(function () {
            console.log($(this).data('null-value'));
            $(this).val($(this).data('null-value'));
        });
    }
};

window['updateBasket'] = function (result, $target) {
    if (result.success) {
        $.ajax({
            url: '/basket/count',
            type: "POST",
            success: function (response) {
                if (response.success) {
                    $('.cart__count').text(response.count);
                }
            },
            error: function (msg) {
                console.log(msg);
            }
        });
    }
};

window['initItemSwitch'] = function (result, $target) {
    //initItemSwitch();
    console.log('initItemSwitch');
};
