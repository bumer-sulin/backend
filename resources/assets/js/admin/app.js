require('jquery-ui/ui/widgets/sortable.js');
require('jquery-ui/ui/widgets/draggable.js');

window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

/**
 * -------------------------------------------
 * Summernote
 * -------------------------------------------
 *
 */
require('codemirror/lib/codemirror.js');
require('summernote-webpack-fix/dist/summernote.min.js');
require('summernote-webpack-fix/dist/lang/summernote-ru-RU.min.js');
require('codemirror');
require('codemirror/mode/htmlembedded/htmlembedded');
require('summernote-image-title/summernote-image-title.js');


require('bootstrap-select/dist/js/bootstrap-select.js');
require('bootstrap-select/dist/js/i18n/defaults-ru_RU.js');


require('blueimp-file-upload/js/vendor/jquery.ui.widget.js');
require('blueimp-file-upload/js/jquery.iframe-transport.js');
require('blueimp-file-upload/js/jquery.fileupload.js');

/**
 * -------------------------------------------
 * Jquery fancybox
 * -------------------------------------------
 *
 */
//require('jquery-fancybox/source/js/jquery.fancybox.pack.js');

//require('./project/jquery.fancybox.min.js');

window.toastr = require('toastr/build/toastr.min.js');
require('./plugins/notification.js');
if (window.toastrOptions !== undefined) {
    window.toastr.options = window.toastrOptions;
}
if (window.toastrNotification !== undefined) {
    window.notification.send(window.toastrNotification);
}
//window.toastr = require('../common/components/toastr.js');

require('../common/components/table/icon.js');
require('../common/components/cleave-masks.js');
require('../common/components/helpers.js');
require('../common/components/crop.js');
require('../common/components/file-button.js');
require('../common/components/desta.js');
require('../common/components/callbacks.js');
require('./package/app.js');
require('./package/slider.js');
require('./package/fancybox.js');
require('./project/helpers.js');
require('./project/custom.js');

require('./project/dialogs.js');
require('./project/fancybox.js');

require('./project/blueimp.js');
require('./project/summernote.js');
require('./project/drag/image.js');
require('./project/multiselect.js');
require('./template/app.js');
require('./template/libs/gauge.min.js');
require('./template/libs/daterangepicker.js');
require('./template/views/main.js');
require('./template/views/draggable-cards.js');
require('./project/tooltip.js');
require('./project/colorpicker.js');
require('./project/counter.js');
require('../common/components/picker.js');
require('./project/callbacks.js');
require('./project/form.js');
require('./project/comment.js');

window.colors = [ '#F13D37', '#7FBFFB', '#7FB6B7', '#E66FC7', '#D24861', '#1DA25D', '#E8CA20' ];

/**
 * -------------------------------------------
 * Append laravel token
 * -------------------------------------------
 *
 */
window.Laravel = $('meta[name="csrf-token"]').attr('content');

$.ajaxSetup({
    headers: {'X-CSRF-TOKEN': window.Laravel}
});

/**
 * -------------------------------------------
 * Bootstrap colorpicker
 * -------------------------------------------
 * require('bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js');
 */

/**
 * -------------------------------------------
 * Jquery fancybox
 * -------------------------------------------
 * require('jquery-fancybox/source/js/jquery.fancybox.pack.js');
 */



/**
 * -------------------------------------------
 * Clipboard, copy text in area
 * -------------------------------------------
 *
 */
const ClipboardJS = require('clipboard/dist/clipboard.js');

let clipboard = new ClipboardJS('.btn-clipboard');

clipboard.on('success', function (e) {
    console.log('copied');
});

/**
 * -------------------------------------------
 * Mixitup, sort blocks
 * -------------------------------------------
 *
 */
const mixitup = require('mixitup');

if ($('.mixitup').length) {
    var config = {
        controls: {
            toggleDefault: 'all'
        },
        classNames: {
            block: '',
            elementFilter: 'is',
            modifierActive: 'primary'
        },
        selectors: {
            target: '.mix'
        }
    };
    var mixer = mixitup('.mixitup', config);

    mixer.toggleOn('.category-b')
        .then(function () {
            // Deactivate all active toggles

            //return mixer.toggleOff('.category-b')
        })
        .then(function (state) {
            console.log(state.activeFilter.selector); // 'all'
            console.log(state.totalShow); // 12
        });
}

/**
 * -------------------------------------------
 * Move To
 * -------------------------------------------
 *
 * <a href="#app" class="js-trigger" data-mt-duration="300">Trigger</a>
 */
const MoveToPlugin = require('moveto/dist/moveTo.min.js');
const moveTo = new MoveToPlugin();

var trigger = document.getElementsByClassName('js-trigger')[0];
moveTo.registerTrigger(trigger);
