<?php
return [
    'store' => [
        'title' => 'Успех',
        'text' => 'Данные успешно добавлены',
        'type' => 'success'
    ],
    'update' => [
        'title' => 'Успех',
        'text' => 'Данные успешно изменены',
        'type' => 'success'
    ],
    'destroy' => [
        'title' => 'Успех',
        'text' => 'Данные успешно удалены',
        'type' => 'success'
    ],
    'status'    => [
        [
            'title' => 'Успех',
            'text' => 'Данные успешно скрыты',
            'type' => 'success',
        ],
        [
            'title' => 'Успех',
            'text' => 'Данные успешно опубликованы',
            'type' => 'success',
        ]
    ],
];