<?php

namespace App\Registries;

use App\Models\User;
use App\Services\Image\Facades\ImagePath;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel as Sentinel;
//use Sentinel;
use Illuminate\Support\Facades\Config;

class Member
{
    /**
     * Пользователь с которым работает данный экземпляр класса
     * @var User
     */
    public $user;

    /**
     * @var integer
     */
    public $nUserId;

    /**
     * Роль пользователя
     * @var object
     */
    private $role;

    /**
     * Member constructor.
     */
    public function __construct()
    {
        $this->user = Sentinel::getUser();
        $this->role = Cache::remember('user_'.$this->user->id.'-role', 600, function () {
           return $this->user->id;//->toArray();
        });
    }

    /**
     * Возвращает экземпляр класса для текущего авторизованного юзера.
     * @return Member
     */
    public static function current()
    {
        $user = Sentinel::getUser();
        $self = new self($user);
        return $self;
    }

    /**
     * Возвращает экземпляр класса для юзера по id
     * @param $nId
     * @return Member
     */
    public static function getByUser($nId)
    {
        $user = Sentinel::getUser($nId);
        $self = new self($user);
        return $self;
    }


    /**
     * Возвращает всю информацию о пользователе данного экземпляра класса
     * @return \Illuminate\Support\Collection
     */
    public function get()
    {
        if (!Config::get('site.cache.member')) {
            $aMember = $this->getMember();
        } else {
            $aMember = Cache::remember('user_'.$this->user->id, 60, function () {
                return $this->getMember();
            });
        }
        return collect($aMember);
    }

    /**
     * собирает и возращает информацию о пользователе по id
     * @return array
     */
    private function getMember()
    {
        $aResult['user'] = $this->user->toArray();
        $aResult['user']['role'] = $this->user->roles()->first()->toArray();
        $aResult['role'] = $this->user->roles()->first()->toArray();
        $aResult['user']['image'] = ImagePath::main('user', 'square', $this->user);
        return $aResult;
    }

    /**
     * меню для разных пользователей в зависимости от типа.
     * @param string $sMenuType
     * @return array|mixed
     */
    public function getUserMenu($sMenuType = '')
    {
        $sRoleName = $this->role['slug'];
        $aMenu = [
            'partner'=>[],
            'client'=>[],
            'admin'=>[]
        ];
        switch($sRoleName) {
            case 'partner':
                $aMenu['partner'] = Config::get('lepta.menu.partner');
                break;
            case 'client':

                break;
            case 'admin':
                $aMenu['admin'] = Config::get('lepta.menu.admin');
                break;
            case 'cashier':

                break;
            default :
                break;
        }
        return ($sMenuType) ? array_get($aMenu, $sMenuType, []) : $aMenu;
    }



}
