<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Tag;
use App\Models\Type;
use App\Models\User;
use App\Observers\ArticleObserver;
use App\Observers\CategoryObserver;
use App\Observers\CommentObserver;
use App\Observers\TagObserver;
use App\Observers\TypeObserver;
use App\Observers\UserObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

//use LaravelTrailingSlash\RoutingServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * -------------------------------------
     * Schema::defaultStringLength(191);
     * https://laravel-news.com/laravel-5-4-key-too-long-error
     * -------------------------------------
     * Error migrations for laravel 5.4 for long unique key.
     * Error: "Syntax error or access violation: 1071 Specified key was too long; max key length is 767 bytes".
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        //BladeDirectives::switchCase();
        //BladeDirectives::custom();


        \Carbon\Carbon::setLocale(config('app.locale'));
        Category::observe(CategoryObserver::class);
        User::observe(UserObserver::class);
        Type::observe(TypeObserver::class);
        Article::observe(ArticleObserver::class);
        Tag::observe(TagObserver::class);
        Comment::observe(CommentObserver::class);

        app(\Dingo\Api\Auth\Auth::class)->extend('custom', function ($app) {
            return new ApiAuthCustomServiceProvider();
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->rebinding('request', function ($app, $request) {
            $request->setUserResolver(function () use ($app) {
                return $app['sentinel']->getUser();
            });
        });
        //DomainService::handle();



        if ($this->app->environment() == 'local') {
            //$this->app->register(\Bpocallaghan\Generators\GeneratorsServiceProvider::class);
        }
    }
}
