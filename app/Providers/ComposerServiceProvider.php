<?php

namespace App\Providers;

use App\Http\Composers\AdminComposer;
use App\Http\Composers\AppComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
        View::composer([
            'layouts.mobile',
            'layouts.defaults.mobile.*'
        ], 'App\Http\Composers\MobileComposer');
        */
        View::composer('app.*', AppComposer::class);
        View::composer('admin.*', AdminComposer::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
