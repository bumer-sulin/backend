<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class JavascriptVarsProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('admin.layouts.admin', function($view) {
            return $view->with('jsVars', json_encode(trans('jsvars'), JSON_UNESCAPED_UNICODE));
        });
    }
}
