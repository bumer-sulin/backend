<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ContactName implements Rule
{
    /**
     * Сообщения по ошибкам
     *
     * @var array
     */
    private $messages = [
        'count'     => ':attribute должно быть не менее трех символов.',
        //'symbols'   => ':attribute должно содержать только буквы, цифры и пробелы.'
    ];

    /**
     * Активное сообщение
     *
     * @var null
     */
    private $message = null;

    /**
     * ContactName constructor.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = trim($value);
        if (!$this->count($value)) {
            $this->message = $this->messages['count'];
            return false;

        }
        /*
        if (!$this->symbols($value)) {
            $this->message = $this->messages['symbols'];
            return false;
        }
        */
        return true;
    }

    /**
     * Не меньше 3х символов
     *
     * @param $value
     * @return bool
     */
    private function count($value)
    {
        return mb_strlen($value) > 2;
    }

    /**
     * Только буквы, цифры и пробелы
     *
     * @param $value
     * @return bool
     */
    private function symbols($value)
    {
        return preg_match("/^[a-zа-яё\d]{1}[a-zа-яё\d\s]*[a-zа-яё\d]{1}$/iu", $value) === 1; // буквы, цифры и пробелы
        //return preg_match("/^[a-zа-яё\d]{1}[a-zа-яё\d\s]*[a-zа-яё\d]{1}$/iu", $value) === 1 && empty(preg_replace("/[^0-9]/",'',$value)); // буквы и пробелы
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
