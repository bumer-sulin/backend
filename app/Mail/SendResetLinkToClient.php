<?php

namespace App\Mail;

use App\Models\Article;
use App\Models\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendResetLinkToClient extends Mailable
{
    use Queueable, SerializesModels;

    private $token;
    private $isAdmin = false;

    /**
     * SendResetLinkToClient constructor.
     * @param $token
     * @param $isAdmin
     */
    public function __construct($token, $isAdmin)
    {
        $this->token = $token;
        $this->isAdmin = $isAdmin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from(config('mail.from.address'), config('mail.from.name'));
        $title = config('app.name').' - Сброс пароля';

        if ($this->isAdmin) {
            $url = route('password.reset', ['token' => $this->token, 'email' => $this->to[0]['address']]);
        } else {
            $url = route('index.welcome', ['reset_token' => $this->token, 'email' => $this->to[0]['address']]);
        }

        return $this->view('email.auth.reset')->with([
            'title' => $title,
            'url' => $url,
        ])->subject($title);
    }
}
