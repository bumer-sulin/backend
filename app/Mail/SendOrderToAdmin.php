<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOrderToAdmin extends Mailable
{
    use Queueable, SerializesModels;

    private $oInvoice = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($oInvoice)
    {
        $this->oInvoice = $oInvoice;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.invoice.to_admin')->with([
            'oInvoice' => $this->oInvoice,
        ])->subject('Оплачен счет №'.$this->oInvoice->id);
    }
}
