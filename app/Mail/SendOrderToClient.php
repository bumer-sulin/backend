<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOrderToClient extends Mailable
{
    use Queueable, SerializesModels;

    private $oInvoice = null;
    private $oProducts = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($oInvoice)
    {
        $this->oInvoice = $oInvoice;

        $this->oProducts = $this->oInvoice->products;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('email.invoice.to_client')->with([
            'oInvoice' => $this->oInvoice,
            'oProducts' => $this->oProducts,
        ])->subject('Заказ успешно совершен');
    }
}
