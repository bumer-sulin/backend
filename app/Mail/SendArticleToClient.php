<?php

namespace App\Mail;

use App\Models\Article;
use App\Models\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SendArticleToClient extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Article|null
     */
    private $oArticle = null;
    private $email = null;

    /**
     * SendArticleToClient constructor.
     * @param Article $oArticle
     */
    public function __construct(Article $oArticle, $email)
    {
        $this->oArticle = $oArticle;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from(config('mail.from.address'), config('mail.from.name'));
        $title = config('app.name').' - '.$this->oArticle->title;

        $linkUnsubscribe = (new Subscription())->getUnsubscribeUrl($this->email);

        return $this->view('email.article.to_client')->with([
            'oArticle' => $this->oArticle,
            'title' => $title,
            'linkUnsubscribe' => $linkUnsubscribe,
        ])->subject($title)->to($this->email);
    }
}
