<?php

namespace App\Console\Commands;

use App\Services\Logger;
use Illuminate\Console\Command;
use Monolog\Handler\SlackHandler;

class CheckQueuesCommand extends Command
{
    use CommonCommandTrait;

    /**
     * The name and signature of the console command.
     *
     * php artisan queues:check
     *
     * @var string
     */
    protected $signature = 'queues:check {--down} {--scope=restart}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверяет, запущены ли необходимые очереди и запускает новые, если нужно';

    /**
     * @var Logger|null
     */
    private $logger = null;

    /**
     * @var bool
     */
    private $log = true;

    /**
     * CheckQueuesCommand constructor.
     */
    public function __construct()
    {
        @parent::__construct();

        $this->logger = (new Logger())->setName('queries')->log();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->start();

        $this->log('Start '.__CLASS__);

        if (stripos(PHP_OS, 'linux') !== false) {
            exec('ps -aux |grep queue', $output);
            $output = array_map(function (string $output) {
                $fullExplode = explode(' ', $output);

                return [
                    $fullExplode[1],
                    implode(' ', \array_slice($fullExplode, -3))
                ];
            }, $output);
            //$this->log(implode(',', $output));
            //dd(implode(',', $output));
            //dd($output);
            if ($output) {
                $this->option('down') ? $this->down($output, $this->option('scope')) : $this->up($output, $this->option('scope'));
            }
        }
        $this->finish();
    }

    /**
     * @param array $output
     * @param string $scope
     */
    private function up(array $output, string $scope = 'restart'): void
    {
        $commands = $this->commands($scope);
        foreach ($commands as $key => $command) {
            $exec = true;
//            foreach ($output as $outputLine) {
//                $this->log($outputLine[1].':'.$key);
//                if (strpos($outputLine[1], $key) !== false) {
//                    $exec = false;
//                }
//            }
            if ($exec) {
                //$this->log($command);
                //$this->log($key);
                //$this->log(implode(',', $output));
                exec($command);
                $this->log('Command was execute: '.$command);
            }
        }
    }

    /**
     * @param array $output
     * @param string $scope
     */
    private function down(array $output, string $scope = 'work'): void
    {
        $commands = $this->commands($scope);
        foreach ($commands as $key => $command) {
            foreach ($output as $outputLine) {
                if (stripos($outputLine[1], $key) !== false) {
                    exec('kill -9 ' . $outputLine[0]);
                    $this->log('Command was kill: '.$command);
                }
            }
        }
    }

    private function commands(string $scope = 'work'): array
    {
        return [
            "queue:restart" => env('PHP_ALIAS', 'php')." artisan queue:restart > /dev/null &",
            "queue:work" => env('PHP_ALIAS', 'php')." artisan queue:work > /dev/null &",
            //"queue:$scope" => env('PHP_ALIAS', 'php')." artisan queue:$scope",
//            "queue:$scope --queue=calendar:notifications -- notifications" => "php artisan queue:$scope --queue=calendar:notifications -- notifications > /dev/null &",
//            "queue:$scope --queue=planes:general-report:generate" => "php artisan queue:$scope --queue=planes:general-report:generate --timeout=720 > /dev/null &",
//            "queue:$scope --queue=planes:general-report:echo" => "php artisan queue:$scope --queue=planes:general-report:echo > /dev/null &",
//            "queue:$scope --queue=photos" => "php artisan queue:$scope --queue=photos --timeout=180 > /dev/null &",
//            "queue:$scope --queue=contacts" => "php artisan queue:$scope --queue=contacts --timeout=180 > /dev/null &",
//            "queue:$scope --queue=online-users" => "php artisan queue:$scope --queue=online-users --timeout=180 > /dev/null &",
//            "queue:$scope --queue=export" => "php artisan queue:$scope --queue=export --timeout=180 > /dev/null &",
//            "queue:$scope --queue=parse-templates" => "php artisan queue:$scope --queue=parse-templates --timeout=180 > /dev/null &",
//            "queue:$scope --queue=mail" => "php artisan queue:$scope --queue=mail --timeout=180 > /dev/null &",
//            "queue:$scope --queue=ads" => "php artisan queue:$scope --queue=ads --timeout=180 > /dev/null &",
        ];
    }
}
