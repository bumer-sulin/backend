<?php

namespace App\Console\Commands\Common;


use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Chumper\Zipper\Zipper;
use Maatwebsite\Excel\Facades\Excel;
use App\Domain\Reports\Export;

trait ReportTrait
{
    protected $mail = [
        'view' => 'mail.report.simple',
        'data' => [],
        'title' => 'Ежедневный отчет по пополнениям балансов',
        'emails' => [
            'dev' => ['dposkachei@yandex.ru'],
            'prod' => ['dposkachei@yandex.ru']
        ]
    ];

    /**
     * ДЛя файлов
     *
     * @var null
     */
    private $storagePath = null;
    private $folderName = null;
    private $fileName = null;

    private $path = null;

    /**
     * Все файлы
     *
     * @var array
     */
    private $aFiles = [];

    /**
     * Для zip
     *
     * @var null
     */
    private $storageZip = null;
    private $fileZip = null;
    private $withZip = false;


    public function setPath($path)
    {
        $this->path = $path;
        $this->storagePath = $this->storageDir();
    }


    /**
     * Генерация Excel документа
     *
     * @param array $aData
     * @param $filename
     */
    public function xls($aData = [], $filename = null)
    {
        $this->log('Start xls');

        $model = new Export();

        if (!is_null($filename)) {
            $this->setFileName($filename);
        }
        $this->log('File name: '.$this->fileName);

        $keys = [
            '', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P'
        ];
        if (!empty($aData)) {
            $last = $keys[count(array_keys($aData[0]))];
        } else {
            $last = 'B';
        }
        //dd($last);
        $test = Excel::create($this->fileName, function($excel) use (&$model, $aData, $last) {
            $excel->sheet("Лист 1", function($sheet) use ($aData, $last) {

                $sheet->setColumnFormat([
                    //'I' => '0.00',
                    //'F' => '0.00'
                ]);
                $sheet->fromArray($aData, null, 'A1', true, true);
                //$sheet->fromArray($data, null, null, false, true);
                $sheet->cells('A1:'.$last.'1', function($cells) {
                    $cells->setFontWeight('bold');
                });
            });
        });

        $test->store('xls', $this->folderName);

        $this->aFiles[] = $this->getFile(null, 'xls');

        $this->log('Finish xls');
    }

    /**
     * Удалить файл
     *
     * @param $storageFile
     */
    public function deleteFile($storageFile)
    {
        if (File::exists($storageFile)) {
            File::delete($storageFile);
        }
    }

    /**
     * Присвоить путь и имя файла
     *
     * @param $name
     */
    public function setFileName($name)
    {
        if (is_null($this->path)) {
            abort(500, '$this->path is null. Run $this->setPath($path) after.');
        }
        $this->folderName = $this->storagePath;
        $this->fileName = $name;
    }


    /**
     * Storage директория с проверкой директории
     *
     * @return string
     */
    public function storageDir()
    {
        if (!Storage::disk('local')->exists($this->path)) {
            Storage::disk('local')->makeDirectory($this->path);
        }
        return Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix().$this->path;
    }

    private function getFile($name = null, $ext = 'csv')
    {
        $name = !is_null($name) ? $name : $this->fileName;
        return $this->folderName.'/'.$name.'.'.$ext;
    }

    /**
     * Отправка сообщений
     */
    public function mail()
    {
        $this->log('Start mail');

        if (!$this->test && config('app.env') === 'production') {
            $emails = $this->mail['emails']['prod'];
        } else {
            $emails = $this->mail['emails']['dev'];
        }
        if (!is_null($this->email)) {
            $emails = [$this->email];
        }
        Mail::send($this->mail['view'], [
            'title' => $this->mail['title']
        ], function($message) use ($emails) {
            $message->from('zayavka@century21.ru', 'Century 21');
            foreach($emails as $key => $email) {
                if ($key === 0) {
                    $message->to($email);
                } else {
                    $message->cc($email);
                }
            }
            if ($this->withZip) {
                $message->attach($this->storageZip);
            } else {
                foreach ($this->aFiles as $file) {
                    $message->attach($file);
                }
            }
            $message->subject($this->mail['title']);
        });

        $this->log('Finish mail');
    }



    /**
     * Присвоить путь и имя zip архива
     *
     * @param null $name
     */
    private function setZipName($name = null)
    {
        $name = !is_null($name) ? $name : $this->fileName;
        $this->storageZip = storage_path('app/'.$this->path.'/'.$name.'.zip');
        $this->fileZip = $this->folderName.'/'.$name.'.zip';
    }


    public function zip($filename, $aFiles = [])
    {
        $this->log('Start zip');

        $this->withZip = true;

        $this->setZipName($filename);

        $this->log('File to: '.$this->fileZip);

        $this->deleteFile($this->storageZip);

        $zipper = new Zipper;

        if (empty($aFiles)) {
            $aFiles = $this->aFiles;
        }
        $zipper->make($this->fileZip)->add($aFiles);

        $this->log('Finish zip');
    }

    public function deleteFiles()
    {
        foreach($this->aFiles as $aFile) {
            $this->deleteFile($aFile);
        }
    }
}