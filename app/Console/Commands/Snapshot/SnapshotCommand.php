<?php

namespace App\Console\Commands\Snapshot;

use App\Console\Commands\Common\CommandTrait;
use App\Services\Snapshot;
use Illuminate\Console\Command;

class SnapshotCommand extends Command
{
    use CommandTrait;

    /**
     * The name and signature of the console command.
     *
     *
     * --name Имя файла, по умолчанию 10.01.2018
     *
     * php artisan snapbackup:project create --name=10.01.2018
     * php artisan snapbackup:project create --table=brands
     *
     * php artisan snapbackup:project load --name=10.01.2018
     *
     * @var string
     */
    protected $signature = 'snapbackup:project {action} {--name=} {--table=} {--log} {--template=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup project by snapshot.';

    /**
     * Записывать в логи
     *
     * @var bool
     */
    public $log = false;

    /**
     * Метод
     *
     * @var string
     */
    public $action = '';

    /**
     * Имя файла, по умолчанию 10.01.2018
     *
     * @var null
     */
    public $name = null;

    /**
     * Имя таблицы
     *
     * @var null
     */
    public $table = '*';


    /**
     * Шаблон с таблицами
     *
     * @var null
     */
    public $template = null;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @see SnapshotCommand::create
     * @see SnapshotCommand::load
     *
     * @return mixed
     */
    public function handle()
    {
        $this->start();

        $this->action = $this->argument('action');

        if (is_null($this->name)) {
            $this->name = now()->format('d.m.Y');
        }

        if (method_exists($this, $this->action)) {
            $this->{$this->action}();
        }


        $this->finish();
    }

    /**
     * Создание
     */
    private function create()
    {
        try {
            (new Snapshot($this->name))->create([$this->table]);
        } catch (\Exception $e) {
            $this->log('Failed '.__METHOD__);
            $this->log($e->getMessage().':'.$e->getLine());
        }
    }

    /**
     * Загрузка
     */
    private function load()
    {
        try {
            (new Snapshot($this->name))->load([$this->table]);
        } catch (\Exception $e) {
            $this->log('Failed '.__METHOD__);
            $this->log($e->getMessage().':'.$e->getLine());
        }
    }
}
