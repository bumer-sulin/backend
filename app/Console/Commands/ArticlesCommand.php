<?php

namespace App\Console\Commands;

use App\Cmf\Project\Article\ArticleSettingsTrait;
use App\Models\Article;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;

class ArticlesCommand extends Command
{
    use CommonCommandTrait;
    use ArticleSettingsTrait;


    /**
     * The name and signature of the console command.
     *
     * php artisan articles resize
     *
     *
     *
     * @var string
     */
    protected $signature = 'articles {action} {--name=} {--log}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup project by snapshot.';

    public $log = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->start();

        switch($this->argument('action')) {
            case 'resize':
                $this->log('resize...');
                $this->resize();
                break;
            default:
                $this->log('default');
                break;
        }

        $this->finish();
    }

    public function resize()
    {
        $oArticles = Article::all();

        $filters = $this->image['filters'];

        $bar = $this->bar(count($oArticles));

        foreach ($oArticles as $oArticle) {

            foreach ($oArticle->images as $image) {

                $sFileName = $image->filename;

                $path = 'images/article/'.$oArticle->id;

                $originalPath = public_path($path.'/original/');



                foreach ($filters as $key => $filter) {

                    $file = $path.'/'.$key.'/'.$sFileName;

                    if (file_exists($file)) {
                        File::Delete($file);
                    }



                    if ($key !== 'square') {
                        $oObject = new $filter['filter']($filter['options']);
                        $oObject->resize($sFileName, $originalPath, $path, $key);
                    }

                }
            }

            $bar->advance();
        }

        $bar->finish();
    }
}
