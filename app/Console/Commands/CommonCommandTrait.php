<?php

namespace App\Console\Commands;


trait CommonCommandTrait
{
    public $start = null;

    /**
     * Вывод в консоли и запись в лог
     *
     * @param $message
     * @param bool $log
     * @param bool $info
     * @param bool $skipLogger
     */
    public function log($message, $log = true, $info = true, $skipLogger = false)
    {
        if ($info) {
            $this->info($message);
        }
        if (property_exists($this, 'log') && $this->log && $log && !$this->logger) {
            info($message);
        }
        if (isset($this->logger) && !$skipLogger) {
            $this->logger->info($message);
        }
    }

    /**
     * Присвоить конкретные флаги
     *
     */
    public function setOptions()
    {
        foreach (get_object_vars($this) as $key => $var) {
            if (!$this->hasOption($key)) {
                continue;
            }
            if (is_bool($var)) {
                $this->{$key} = $this->option($key);
            } else {
                if (!empty($this->option($key))) {
                    $this->{$key} = $this->option($key);
                }
            }
        }
    }

    /**
     * До выполнения скрипта
     */
    public function start()
    {
        $this->setOptions();

        $this->log('------------------- '.__CLASS__.' -------------------', true, true, true);

        $this->start = microtime(true);
    }

    /**
     * После выполнения скрипта
     * @param $messages
     */
    public function finish($messages = [])
    {
        $time = microtime(true) - $this->start;

        if (property_exists($this, 'log')) {
            if ($this->log) {
                $this->log('Time: '.gmdate("H:i:s", $time).'.', true, true, true);
            } else {
                $this->log("\n".'Time: '.gmdate("H:i:s", $time).'.', true, true, true);
            }
        }

        if (!empty($messages)) {
            foreach($messages as $message) {
                $this->log($message, true, true, true);
            }
        }

        $this->log('------------------- '.__CLASS__.' -------------------', true, true, true);
    }

    /**
     * Создание прогресс бара
     *
     * $bar = $this->bar(count([]))
     * $bar->advance();
     * $bar->finish();
     *
     * @param $count
     * @return mixed
     */
    public function bar($count, $showCount = true)
    {
        if ($showCount) {
            $this->log('Count: '.$count);
        }
        $bar = $this->output->createProgressBar($count);
        $bar->setFormat('Progress: %percent%%');
        return $bar;
    }
}
