<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class Backup extends Command
{
    use CommonCommandTrait;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backup:project {action} {--name=} {--log}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup project by snapshot.';

    public $log = false;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->start();

        switch($this->argument('action')) {
            case 'snapshot':
                $this->log('snapshot...');
                $this->snapshot();
                break;
            default:
                $this->log('default');
                break;
        }

        $this->finish();
    }

    public function snapshot()
    {
        $name = $this->option('name');
        $sName = Carbon::now()->format('d.m.Y');
        if (!empty($name)) {
            $sName = $sName.'-'.$name;
        }
        Artisan::call('snapshot:create', [
            'name' => $sName,
        ]);
        /*
        Mail::send('email.welcome', [], function ($message) {
            $message->from('dposkachei@gmail.com', 'Dmitri mail');
            $message->to('dimapos@bk.ru');
        });
        */
    }
}
