<?php

namespace App\Console;

use App\Console\Commands\ArticlesCommand;
use App\Console\Commands\CheckQueuesCommand;
use App\Console\Commands\Snapshot\SnapshotCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\Helper;
use App\Console\Commands\Backup;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Helper::class,
        Backup::class,
        CheckQueuesCommand::class,

        ArticlesCommand::class,
        SnapshotCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $this->byEnv(function() use ($schedule) {

            //$schedule->command('snapshot:create')->dailyAt('06:00');

            //$schedule->command('link-checker:run')->dailyAt('06:15');

            $schedule->command('queues:check')->everyFiveMinutes();

            //$schedule->command('horizon:snapshot')->everyFiveMinutes();
        }, 'production');
    }

    /**
     * Напускать задачу после проверки по env
     *
     * @param $function
     * @param string|array $env *, production, ['production', 'local']
     */
    private function byEnv($function, $env = '*')
    {
        if (is_array($env)) {
            if (in_array(config('app.env'), $env)) {
                $function();
            }
        } else {
            if ($env === '*' || config('app.env') === $env) {
                $function();
            }
        }
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
