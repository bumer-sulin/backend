<?php

namespace App\Listeners;

use App\Events\ArticleStatisticEvent;
use App\Http\Controllers\ApiController;
use App\Models\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

class ArticleStatisticListener
{
    /**
     * @var string
     */
    private $sessionPrefix = 'article-';

    /**
     * @var User
     */
    private $user;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  ArticleStatisticEvent $event
     * @return void
     */
    public function handle(ArticleStatisticEvent $event)
    {
        /**
         * @see ApiController::articles():137
         * в $oArticle записывается ip запроса
         */
        $cacheKey = md5($this->sessionPrefix.$event->oArticle->id.$event->oArticle->ip);
        if (Cache::has($cacheKey)) {
            return;
        }
        Cache::put($cacheKey, 1, 5);
        switch ($event->type) {
            case 'views':
                $event->oArticle->statistic->increment('views');
                break;
            case 'comments':
                $event->oArticle->statistic->increment('comments');
                break;
        }
    }
}
