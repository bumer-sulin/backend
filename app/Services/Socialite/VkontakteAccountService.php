<?php

namespace App\Services\Socialite;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Http\Controllers\Auth\Sentinel\RegistersUsers;
use App\Services\Image\ImageService;
use App\Models\SocialAccount;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Contracts\User as ProviderUser;

class VkontakteAccountService
{
    use ImageableTrait;
    use RegistersUsers;

    private const NAME = 'vkontakte';

    public function createOrGetUser(Request $request)
    {
        $account = SocialAccount::whereProvider(self::NAME)
            ->whereProviderUserId($request->get('uid'))
            ->first();
        if ($account) {
            return $account->user;
        } else {
            try {
                $account = new SocialAccount([
                    'provider_user_id' => $request->get('uid'),
                    'provider' => self::NAME,
                ]);
                $fakeLogin = $request->get('uid');
                $user = User::where('login', $fakeLogin)->first();
                if (!$user) {
                    $user = User::create([
                        'login' => $fakeLogin,
                        'email' => $fakeLogin,
                        'first_name' => $request->get('first_name'),
                        'last_name' => $request->get('last_name'),
                        'password' => Hash::make(md5(rand(1, 10000))),
                    ]);

                    $this->saveImage($request->get('photo'), $user);

                    $this->activateUser($user);
                }
                $account->user()->associate($user);
                $account->save();
                return $user;
            } catch (\Exception $e) {
                Log::critical($e->getMessage());
            }
        }
    }
}
