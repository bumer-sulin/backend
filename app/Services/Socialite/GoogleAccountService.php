<?php

namespace App\Services\Socialite;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Http\Controllers\Auth\Sentinel\RegistersUsers;
use App\Services\Image\ImageService;
use App\Models\SocialAccount;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Contracts\User as ProviderUser;

class GoogleAccountService
{
    use ImageableTrait;
    use RegistersUsers;

    private const NAME = 'google';

    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialAccount::whereProvider(self::NAME)
            ->whereProviderUserId($providerUser->getId())
            ->first();
        if ($account) {
            return $account->user;
        } else {
            try {
                $account = new SocialAccount([
                    'provider_user_id' => $providerUser->getId(),
                    'provider' => self::NAME,
                ]);
                $user = User::whereEmail($providerUser->getEmail())->first();
                if (!$user) {
                    $user = User::create([
                        'email' => $providerUser->getEmail(),
                        'first_name' => $providerUser->getName(),
                        'password' => Hash::make(md5(rand(1, 10000))),
                    ]);

                    $this->saveImage($providerUser->getAvatar(), $user);

                    $this->activateUser($user);
                }
                $account->user()->associate($user);
                $account->save();
                return $user;
            } catch (\Exception $e) {
                Log::critical($e->getMessage());
            }
        }
    }
}
