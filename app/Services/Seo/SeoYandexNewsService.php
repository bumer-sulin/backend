<?php

namespace App\Services\Seo;

use App\Models\Article;
use App\Models\Setting;
use Illuminate\Support\Facades\File;

class SeoYandexNewsService
{
    use YouTubeParameterTrait;

    /**
     * @param string $data
     * @return string
     */
    private function container(string $data): string
    {
        return '<?xml version="1.0" encoding="UTF-8"?><rss xmlns:yandex="http://news.yandex.ru" xmlns:media="http://search.yahoo.com/mrss/" version="2.0">' . $data . '</rss>';
    }

    /**
     * @param string $title
     * @param string $url
     * @param string $description
     * @param string $data
     * @return string
     */
    private function channel(string $title, string $url, string $description, string $data): string
    {
        return '<channel><title>' . $title . '</title><link>' . $url . '</link><description>' . $description . '</description><language>ru</language>' . $data . '</channel>';
    }

    /**
     * @param Article $oItem
     * @return string
     */
    private function item(Article $oItem): string
    {
        $item = '';
        $item = $this->itemStart($item);

        $item .= '<title>' . $this->itemClearTitle($oItem->title) . '</title>';
        $item .= '<link>' . $oItem->getUrl() . '</link>';
        $item .= '<pdalink>' . $oItem->getUrl() . '</pdalink>';
        $item .= '<description>' . $oItem->description . '</description>';
        if (!is_null($oItem->valueAuthor)) {
            $item .= '<author>' . $oItem->valueAuthor . '</author>';
        }
        if (!is_null($oItem->category)) {
            $item .= '<category>' . $oItem->category->title . '</category>';
        }
        $item .= '<enclosure url="' . imagePath()->main('article', 'sm', $oItem) . '" type="image/jpeg"/>';
        $item .= '<pubDate>' . $oItem->published_at->toRssString() . '</pubDate>';
        $item .= '<yandex:genre>' . $this->itemGenre() . '</yandex:genre>';
        $item .= '<yandex:full-text>' . $this->itemClearText($oItem->text) . '</yandex:full-text>';

        $item = $this->itemMedia($oItem, $item);

        $item = $this->itemEnd($item);
        return $item;
    }

    /**
     * Вставка youtube видео из текста статьи
     *
     * @param Article $oItem
     * @param string $item
     * @return string
     */
    private function itemMedia(Article $oItem, string $item): string
    {
        if (!empty($this->youtubeGetEmbedId($oItem->text))) {
            $embed = $this->getYoutubeEmbedUrl($oItem->text);
            $meta = $this->youtubeGetMetaByEmbed($embed);
            if (!is_null($meta)) {
                $video = $embed;
                $preview = $meta['thumbnail_url'];
                $item .= '<media:group><media:content url="' . $video . '"/><media:thumbnail url="' . $preview . '"/></media:group>';
            }
        }
        return $item;
    }

    /**
     * Указывается латиницей:
     * - lenta — короткое новостное сообщение (50–80 символов);
     * - message — более развернутое новостное сообщение;
     * - article — статья;
     * - interview — интервью.
     *
     * @return string
     */
    private function itemGenre(): string
    {
        return 'message';
    }

    /**
     * @param string $item
     * @return string
     */
    private function itemStart(string $item): string
    {
        $item .= '<item>';
        return $item;
    }

    /**
     * @param string $item
     * @return string
     */
    private function itemEnd(string $item): string
    {
        $item .= '</item>';
        return $item;
    }

    /**
     * Чистка
     * - full text
     * - url
     *
     * @param string $value
     * @return string
     */
    private function itemClearValue(string $value): string
    {
        $value = str_replace('&', '&amp;', $value);
        $value = str_replace('>', '&gt;', $value);
        $value = str_replace('<', '&lt;', $value);
        $value = str_replace('"', '&quot;', $value);
        $value = str_replace("'", '&apos;', $value);
        return $value;
    }

    /**
     * В элементе yandex:full-text запрещено передавать:
     * - название источника;
     * - дату или время сообщения;
     * - контактную информацию;
     * - ссылки на изображения, аудио- и видеофайлы (для этих ссылок нужно формировать отдельные теги enclosure, media:group).
     * @param string $value
     * @return string
     */
    private function itemClearText(string $value): string
    {
        $value = strip_tags($value);
        $value = $this->itemClearValue($value);
        $value = trim($value);
        return $value;
    }

    /**
     * Максимальная длина заголовка — 200 символов.
     * Запрещено:
     * - передавать заголовок сообщения, написанный полностью прописными буквами;
     * - ставить точку в конце заголовка;
     * - указывать в заголовке название источника, дату или время сообщения;
     * - включать в заголовок служебные примечания (например, «обновлено», «дополнено», «фоторепортаж», «видео») и неинформативные обороты, которые не являются неотъемлемой частью заголовка (например, «Срочно!» или «Сенсация»).
     *
     * @param string $value
     * @return string
     */
    private function itemClearTitle(string $value): string
    {
        $dot = mb_substr($value, -1);
        // нельзя чтобы была точка в конце
        if ($dot === '.') {
            $value = mb_substr($value, 0, -1);
        }
        return $value;
    }

    /**
     * @param $oArticles
     * @return string
     */
    private function parseArticles($oArticles)
    {
        $data = '';
        foreach ($oArticles as $oArticle) {
            $data .= $this->item($oArticle);
        }
        $title = Setting::where('key', Setting::KEY_APP_TITLE)->first()->value;
        $description = Setting::where('key', Setting::KEY_APP_DESCRIPTION)->first()->value;

        $data = $this->channel($title, config('app.url'), $description, $data);
        return $this->container($data);
    }

    /**
     *
     */
    public function feedArticles()
    {
        $oArticles = Article::active()
            ->article()
            ->where('name', 'not like', '%vremya-novostey%')
            ->where('title', 'not like', '%выпуск №%')
            ->ordered()
            ->take(8)
            ->get();

        $data = $this->parseArticles($oArticles);
        $file = public_path('feed.xml');
        File::put($file, $data);

        $file = public_path('feed.rss');
        File::put($file, $data);
    }
}
