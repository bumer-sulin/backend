<?php

namespace App\Services;

trait ResponseCommonTrait
{
    /**
     * Ajax return success with default properties
     *
     * return $this->success([
     *
     * ], 'Message')
     *
     * @param array $aData
     * @param null $message
     * @return array
     */
    public function success(array $aData = [], $message = null)
    {
        $success = (new ResponseCommon())->success($aData);

        if (!is_null($message)) {
            $success = $success->withMessage($message);
        }
        return $success->build();
    }

    /**
     * Ajax return error with default properties
     *
     * return $this->error([
     *
     * ], 'Message')
     *
     * @param array $aData
     * @param null $message
     * @return array
     */
    public function error(array $aData = [], $message = null) : array
    {
        $success = (new ResponseCommon())->error($aData);

        if (!is_null($message)) {
            $success = $success->withMessage($message);
        }
        return $success->build();
    }

    /**
     * Ajax return json error with status 422
     *
     * @param $text
     * @param string $key
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonError($text, $key = 'error')
    {
        return response()->json([
            $key => [$text]
        ], 422);
    }



    /**
     * Log info with CPU value
     *
     * @param string $message
     */
    public function cpuLog($message = '')
    {
        $load = sys_getloadavg();
        info(json_encode($load).' - '.$message);
    }

    /**
     * Log info with memory value
     *
     * @param string $message
     */
    public function memoryUsageLog($message = '')
    {
        $load = memory_get_usage();
        info(json_encode($load).' - '.$message);
    }

}