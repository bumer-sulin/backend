<?php

namespace App\Services\Comment;

use App\Models\Comment;

trait CanComment
{

    /**
     * @param $commentable
     * @param string $commentText
     * @param int $rate
     * @param $parent_id
     * @return $this
     */
    public function comment($commentable, $commentText = '', $rate = 0, $parent_id = null)
    {
        Comment::create([
            'comment' => $commentText,
            'rate' => $rate,
            'approved' => false,
            'commented_id' => $this->id,
            'commented_type' => get_class(),
            'commentable_id' => $commentable->id,
            'commentable_type' => $commentable->getShortName(),
            'parent_id' => $parent_id,
        ]);
        return $this;
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return false;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commented');
    }
}
