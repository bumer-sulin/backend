<?php


namespace App\Services\Article;


trait ArticleTrait
{
    /**
     * @param $query
     * @return mixed
     */
    public function activeQuery($query)
    {
        return $query->whereIn('status', [1, 2])
            ->where('published_at', '<=', now()->startOfDay())
            ->whereNotNull('publication_at');
    }
}
