<?php

namespace App\Services\Toastr;

use Bulk\Toastr\Facades\Toastr as ToastrFacade;

class Toastr
{
    private $toastrMessages = [
        'success' => [
            'title' => 'Успех',
            'text' => 'Данные успешно добавлены',
            'type' => 'success'
        ],
        'info' => [
            'title' => 'Информация',
            'text' => 'Данные успешно добавлены',
            'type' => 'info'
        ],
        'warning' => [
            'title' => 'Внимание',
            'text' => 'Данные успешно добавлены',
            'type' => 'warning'
        ],
        'primary' => [
            'title' => 'И так',
            'text' => 'Данные успешно добавлены',
            'type' => 'primary'
        ],
        'error' => [
            'title' => 'Ошибка',
            'text' => 'Данные успешно добавлены',
            'type' => 'error'
        ],
    ];

    private $message = '';
    private $options = [];

    public function __construct($message)
    {
        $this->message = $message;
    }

    private function get($type, $json = true)
    {
        $this->toastrMessages[$type]['text'] = $this->message;
        if (!empty($this->options)) {
            $this->toastrMessages[$type]['options'] = $this->options;
        }
        if (!$json) {
            $data['options'] = [
                'timeOut' => 5000
            ];
            $data = array_merge($data, $this->toastrMessages[$type]);
            if ($type === 'primary') {
                $type = 'info';
            }
            return ToastrFacade::{$type}($data['text'], $data['title'], $data['options']);
        }
        return $this->toastrMessages[$type];
    }

    public function options($data)
    {
        if (isset($data['message'])) {
            unset($data['message']);
        }
        if (isset($data['textable'])) {
            $data['toastClass'] = config('toastr.options.toastClass').' __textable';
        }
        $this->options = array_merge($this->options, $data);

        return $this;
    }

    public function success($json = true)
    {
        return $this->get('success', $json);
    }

    public function error($json = true)
    {
        return $this->get('error', $json);
    }

    public function info($json = true)
    {
        return $this->get('info', $json);
    }

    public function warning($json = true)
    {
        return $this->get('warning', $json);
    }

    public function primary($json = true)
    {
        return $this->get('primary', $json);
    }


}
