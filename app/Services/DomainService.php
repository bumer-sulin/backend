<?php

namespace App\Services;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;

class DomainService {

    public static function handle()
    {
        $domain = env('ADMIN_URL');
        if (starts_with($domain, Request::root())) {
            Config::set('dev-booter.dev_environments', ['local', 'dev', 'testing',]);
        }
    }


    public static function isAdmin()
    {
        $domain = env('ADMIN_URL');
        return starts_with($domain, Request::root()) ? true : false;
    }
}