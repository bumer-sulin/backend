<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

class LayoutService
{
    public function __construct()
    {

    }

    public function theme()
    {
        return $this->remember('theme', function () {
            return Config::get('site.theme');
        });
    }

    public function layout()
    {
        return $this->remember('layout', function () {
            return Config::get('site.layout');
        });
    }

    private function remember($name, $function, $time = 3600)
    {
        return Cache::remember($name, $time, $function);
    }
}
