<?php

namespace App\Services\Yandex;

use App\Mail\SendOrderToAdmin;
use App\Mail\SendOrderToClient;
use App\Models\Invoice;
use App\Models\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use App\Services\Cashier;
use Illuminate\Support\Facades\Mail;

class HttpNotification
{
    private $aRecParams = [
        'notification_type',
        'operation_id',
        'amount',
        'currency',
        'datetime',
        'sender',
        'codepro',
        'notification_secret',
        'label'
    ];

    private $message = '';

    public $invoice = null;

    private $log = null;

    public function __construct()
    {
        $this->log = new BillingLogger();
    }


    public function genSign($aParams)
    {
        $aParams['notification_secret'] = Config::get('services.yandex.money.notification_secret');
        $strParams = implode('&', array_values($aParams));
        return sha1($strParams);
    }

    public function check($aRequest)
    {
        $this->log->start();
        $this->log->info('Notification: '.json_encode($aRequest));

        $aParams = $this->getRecParams($aRequest);
        $nId = (!empty($aRequest['label'])) ? $aRequest['label'] : null;
        if (!isset($aRequest['sha1_hash'])) {
            $this->setMessage('sha1_hash not found');
            return $this->error();
        }
        if ($aRequest['sha1_hash'] !== $this->genSign($aParams)) {
            $this->setMessage('sha1_hash !== genSign');
            return $this->error();
        }
        if ($aRequest['codepro'] !== 'false') {
            $this->setMessage('codepro = false');
            return $this->error();
        }
        if (is_null($nId)) {
            $this->setMessage('invoice_id not found');
            return $this->error();
        }
        $oInvoice = $this->getInvoice($nId, 1);
        if (is_null($oInvoice)) {
            $this->setMessage('invoice not found');
            return $this->error();
        }
        $this->invoice = $oInvoice;
        $this->log->info('Invoice: '.json_encode($this->invoice));

        if ($oInvoice->status === 2) {
            return $this->closeInvoice();
        }
        if (floatval($oInvoice->amount) === floatval($aRequest['withdraw_amount'])) {
            return $this->closeInvoice();
        } else {
            $this->setMessage('price not equal withdraw_amount');
            return $this->error();
        }
    }

    public function getInvoice($nId, $status = null)
    {
        if (!is_null($status)) {
            if (is_array($status)) {
                return Invoice::where('id', $nId)->whereIn('status', $status)->first();
            } else {
                return Invoice::where('id', $nId)->where('status', $status)->first();
            }
        } else {
            return Invoice::where('id', $nId)->where('status')->first();
        }
    }

    public function error($aData = [])
    {
        if (!is_null($this->invoice)) {
            $this->log->info('Invoice id: '.$this->invoice->id);
        }
        $this->log->info(__FUNCTION__);
        $this->log->finish();
        return array_merge([
            'result' => false
        ], $aData);
    }

    public function success($aData = [])
    {
        $this->log->info(__FUNCTION__);
        $this->log->info('Invoice: '.json_encode($this->invoice));
        $this->log->finish();
        return array_merge([
            'result' => true
        ], $aData);
    }

    private function getRecParams($aRequest)
    {
        $aResult = [];
        foreach ($this->aRecParams as $aRecParam) {
            $aResult[$aRecParam] = !empty($aRequest[$aRecParam]) ? $aRequest[$aRecParam] : '';
        }
        return $aResult;
    }

    private function setMessage($message)
    {
        $this->log->info($message);
        $this->message = $message;
    }

    public function setLog($message)
    {
        $this->log->info($message);
    }



    public function sendEmail($oInvoice)
    {
        try {
            $this->sendEmailToAdmin($oInvoice);
            $this->sendEmailToClient($oInvoice);
            $this->log->info('sendEmail: success');
        } catch(\Exception $e) {
            $this->log->info('sendEmail: '.$e->getMessage());
        }
    }

    public function sendEmailToAdmin($oInvoice, $render = false)
    {
        if ($render) {
            return (new SendOrderToAdmin($oInvoice))->render();
        } else {
            Mail::to(config('mail.from.address'))
                ->send(new SendOrderToAdmin($oInvoice));
            return null;
        }
    }

    public function sendEmailToClient($oInvoice, $render = false)
    {
        $oClient = $oInvoice->client;
        if ($render) {
            return (new SendOrderToClient($oInvoice))->render();
        } else {
            Mail::to($oClient->email)
                ->send(new SendOrderToClient($oInvoice));
            return null;
        }
    }

    private function closeInvoice()
    {
        $oInvoice = $this->invoice;
        if (!is_null($oInvoice)) {
            if ($oInvoice->status === 1) {
                $oInvoice->update([
                    'status' => 2
                ]);
                $oPayment = Payment::create([
                    'price' => $oInvoice->amount,
                    'purpose' => $oInvoice->title,
                    'status' => $oInvoice->status
                ]);
                $oInvoice->update([
                    'payment_id' => $oPayment->id,
                    'closed_at' => Carbon::now()
                ]);
                $this->sendEmail($oInvoice);
            }
        }
        return $this->success();
    }
}