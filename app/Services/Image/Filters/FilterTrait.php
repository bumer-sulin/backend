<?php

namespace App\Services\Image\Filters;

use Illuminate\Support\Facades\File;

trait FilterTrait
{
    /**
     * Проверить на существование директории
     * @param $path
     */
    public function checkDirectory($path)
    {
        if (!File::isDirectory($path)) {
            File::makeDirectory($path, 493, true);
        }
    }
}