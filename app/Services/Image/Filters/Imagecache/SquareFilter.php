<?php

namespace App\Services\Image\Filters\Imagecache;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;
use App\Services\Image\Filters\FilterTrait;
use Intervention\Image\ImageManagerStatic as ImageStatic;

class SquareFilter implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(120, 120);
    }
}