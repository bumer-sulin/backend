<?php

namespace App\Services\Image\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;
use App\Services\Image\Filters\FilterTrait;
use Intervention\Image\ImageManagerStatic as ImageStatic;

class SquareFilter implements FilterInterface
{
    use FilterTrait;

    private $options = [];

    private $image = null;

    protected $size = null;

    public function __construct($options)
    {
        $this->options = $options;
    }

    public function applyFilter(Image $image)
    {
        if (isset($this->options['dimension'])) {
            $width = $image->width();
            $height = $image->height();
            $dimension = explode(':', $this->options['dimension']);
            $q = $dimension[1] / $dimension[0];
            if ($width > $height) {
                $image = $image->fit(intval($height), intval($height * $q));
            } else {
                $image = $image->fit(intval($width), intval($width * $q));
            }
        } elseif (isset($this->options['size'])) {
            $this->size = intval($this->options['size']);
            $image = $this->fit($image, $this->size);
        } else {
            $image = $this->fit($image, $this->size);
        }
        return $image;
    }

    public function resize($sFileName, $originalPath, $path, $key)
    {
        $this->image = ImageStatic::make($originalPath.$sFileName);
        $this->image = $this->applyFilter($this->image);
        $sPath = public_path($path.'/'.$key.'/');
        $this->checkDirectory($sPath);
        $this->image->save($sPath.$sFileName);
    }

    private function fit($image, $width)
    {
        $image = $image->resize($width, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        return $image->fit($width, $width);
    }
}
