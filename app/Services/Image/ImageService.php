<?php

namespace App\Services\Image;

use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class ImageService
{
    /**
     * Директория с оригиналами
     * @var string
     */
    protected $originalPath = 'original';
    protected $path;

    public $sFileName = '';
    protected $image = null;

    /**
     * Загрузить и обрезать
     * @param $file
     * @param $sType
     * @param $nId
     * @param $options
     * @param $fileFull
     * @return string
     */
    public function upload($file, $sType, $nId, $options = null, $fileFull = null, $fileName = null)
    {
        $this->path = 'images/'.$sType.'/'.$nId;
        $this->originalPath = public_path($this->path.'/original/');

        //dd($fileFull, $fileName, $this->originalPath);
        if (!is_null($fileFull)) {
            if (!File::exists($this->originalPath)) {
                File::makeDirectory($this->originalPath, 0777, true);
            }
            $sFileName = str_random(12).''.substr(strrchr($fileName, '.'), 0);
            File::copy($fileFull, $this->originalPath.$sFileName);
            $this->sFileName = $sFileName;
        } else {
            $this->sFileName = $this->uploadOriginalFile($file, $this->originalPath);
        }

        if (!is_null($options)) {
            foreach($options as $key => $option) {
                $oObject = new $option['filter']($option['options']);
                $oObject->resize($this->sFileName, $this->originalPath, $this->path, $key);
            }
        }

        return $this->sFileName;
    }

    /**
     * Загрузить оригинальное изображение
     * @param $file
     * @param $path
     * @return string
     */
    public function uploadOriginalFile($file, $path)
    {
        $sFileName = $file->getClientOriginalName();
        $sFileName = str_random(12).''.substr(strrchr($sFileName, '.'), 0);
        $file->move($path,$sFileName);
        return $sFileName;
    }

    /**
     * Удалить изображение со всех папок
     * @param $sFileName
     * @param $sType
     * @param $options
     * @param $nId
     */
    public function deleteImages($sFileName, $sType, $nId, $options = null)
    {
        $aSizes[] = 'original';
        $path = 'images/'.$sType.'/'.$nId;

        if (!is_null($options)) {
            foreach($options['filters'] as $key => $option) {
                $aSizes[] = $key;
            }
        }

        foreach($aSizes as $size) {
            $dir = public_path($path.'/'.$size);
            $file = public_path($path.'/'.$size.'/'.$sFileName);
            if (file_exists($file)) {
                File::Delete($file);
                if (count(File::files($dir)) === 0) {
                    File::deleteDirectory($dir);
                }
            }
        }
    }

}
