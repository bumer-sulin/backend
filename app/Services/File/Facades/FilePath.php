<?php

namespace App\Services\File\Facades;

use Illuminate\Support\Facades\Facade;
use App\Services\File\FilePath as File;

class FilePath extends Facade
{
    protected static function getFacadeAccessor()
    {
        return File::class;
    }
}