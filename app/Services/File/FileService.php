<?php

namespace App\Services\File;

use Illuminate\Support\Facades\File;

class FileService
{
    /**
     * Директория с оригиналами
     * @var string
     */
    protected $originalPath = 'original';
    protected $path;

    public $sFileName = '';
    protected $image = null;

    /**
     * Загрузить и обрезать
     * @param $file
     * @param $sType
     * @param $nId
     * @return string
     */
    public function upload($file, $sType, $nId)
    {
        $this->path = 'files/'.$sType.'/'.$nId;
        $this->originalPath = public_path($this->path.'/original/');

        $this->sFileName = $this->uploadOriginalFile($file, $this->originalPath);
        //$oObject = $this->getClassByType($sType);
        //$oObject->resize($this->sFileName, $this->originalPath, $this->path);
        return $this->sFileName;
    }

    /**
     * Загрузить оригинальное изображение
     * @param $file
     * @param $path
     * @return string
     */
    public function uploadOriginalFile($file, $path)
    {
        $sFileName = $file->getClientOriginalName();
        $sFileName = $this->replaceFileName($sFileName);
        //$sFileName = str_random(12).''.substr(strrchr($sFileName, '.'), 0);
        $file->move($path,$sFileName);
        return $sFileName;
    }

    /**
     * Удалить изображение со всех папок
     * @param $sFileName
     * @param $sType
     * @param $nId
     */
    public function deleteImages($sFileName, $sType, $nId)
    {
        $aSizes[] = [
            'path' => 'original'
        ];
        $path = 'files/'.$sType.'/'.$nId;

        foreach($aSizes as $size) {
            $dir = public_path($path.'/'.$size['path']);
            $file = public_path($path.'/'.$size['path'].'/'.$sFileName);
            if (file_exists($file)) {
                File::Delete($file);
                if (count(File::files($dir)) === 0) {
                    File::deleteDirectory($dir);
                }
                if (count(File::files(public_path($path))) === 0) {
                    File::deleteDirectory(public_path($path));
                }
            }
        }
    }

    /**
     * Сменить название изображения с русского языка
     * @param $filename
     * @return mixed
     */
    public function replaceFileName($filename)
    {
        $search = ["а", "б","в", "г", "д", "е", "ё", "ж", "з", "и", "й", "к", "л", "м", "н", "о", "п", "р", "с", "т", "у", "ф", "х", "ц", "ч", "ш", "щ", "ъ", "ы", "ь", "э", "ю", "я", "А", "Б","В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", " ", "-"];
        $replace = ["a", "b","v", "g", "d", "e", "jo", "zh", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "c", "ch", "sh", "w", "tz", "y", "mz", "je", "ju", "ja", "A", "B","V", "G", "D", "E", "JO", "ZH", "Z", "I", "J", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "H", "C", "CH", "SH", "W", "TZ", "Y", "MZ", "JE", "JU", "_", "-"];
        return str_replace($search, $replace, $filename);
    }

}