<?php

namespace App\Services\File;

use App\Services\File\FileService;
use Illuminate\Support\Facades\File;

class FilePath extends FileService
{

    /**
     * Изображение по умолчанию
     * @return string
     */
    public function default()
    {
        return asset('img/cover.png');
    }

    public function image($key, $size, $model, $filter = null)
    {
        if (is_null($model)) {
            return $this->default();
        }
        $filename = $model->filename;
        if (is_null($filename) || empty($filename)) {
            return $this->default();
        }
        $path = 'files/'.$key.'/'.$model->fileable_id.'/'.$size.'/'.$filename;
        return $this->asset($path);
    }

    public function check($key, $size, $model, $filter = null)
    {
        if (is_null($model)) {
            return false;
        }
        $filename = $model->filename;
        if (is_null($filename) || empty($filename)) {
            return false;
        }
        $path = 'files/'.$key.'/'.$model->fileable_id.'/'.$size.'/'.$filename;
        if (!File::exists($this->publicPath($path))) {
            return false;
        }
        return true;
    }






    private function asset($path)
    {
        return asset($path);
    }

    private function publicPath($path)
    {
        return public_path($path);
    }


}