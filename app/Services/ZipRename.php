<?php

namespace App\Services;


use Illuminate\Support\Facades\File;

class ZipRename
{
    /**
     * @var
     */
    private $zip;

    /**
     * Массив с файлами
     *
     * @var array
     */
    private $files;

    /**
     * Директория куда разархивируются файлы, без уникальной директории
     *
     * @var string
     */
    private $extractTo;

    /**
     * Директория куда разархивируются файлы
     *
     * @var string
     */
    private $extractToPath;

    /**
     * Уникальное имя временной папки, куда разархивируются файлы
     *
     * @var string uniqid()
     */
    private $uniquePathKey;

    /**
     * Уникальный префикс для дублирующего названия
     *
     * image_{$this->uniqueFileKey}.jpg
     *
     * @var string uniqid()
     */
    private $uniqueFileKey;

    /**
     * Список расширений, которые будут переименновываться
     *
     * @var array
     */
    private $aExtensions = ['jpg', 'jpeg', 'png', 'bmp', 'gif'];

    /**
     * ZipRename constructor.
     * @param $zip
     */
    public function __construct($zip)
    {
        $this->uniquePathKey = uniqid();
        $this->uniqueFileKey = uniqid();

        $this->extractTo = public_path('test/extract');

        $this->extractToPath = $this->extractTo . '/' . $this->uniquePathKey;

        $this->zip = $zip;

        $this->clearPath();

        //$this->files = $this->extract();
    }


    /**
     * @return array
     */
    private function extract()
    {
        $zip = new \ZipArchive;
        try {
            if ($zip->open($this->zip) === true) {
                $zip->extractTo($this->extractToPath);
                $zip->close();

                return $this->allFiles();
            }
            return [];
        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * @return mixed
     */
    private function allFiles()
    {
        return File::allFiles($this->extractToPath);
    }

    /**
     * /home/vagrant/home/laravel.codrops/public/test/5ba3c4e3b9c31/images/{image}
     *
     * $reverse = false /home/vagrant/home/laravel.codrops/public/test/5ba3c4e3b9c31
     * $reverse = true  images/{image}
     *
     * @param $path
     * @param bool $reverse
     * @return bool|string
     */
    private function getPathWithoutRelative($path, $reverse = false)
    {
        $position = strpos($path, $this->uniquePathKey);
        if ($position !== false) {
            if (!$reverse) {
                /**
                 * public/test/5ba3c4e3b9c31
                 */
                $path = substr($path, 0, $position + strlen($this->uniquePathKey));
            } else {
                /**
                 * images/.sdsd
                 */
                $path = substr($path, $position + strlen($this->uniquePathKey) + 1);
            }
        }
        return $path;
    }


    /**
     * @return $this
     */
    private function files()
    {
        $aHtmlFiles = $this->getHtml();

        foreach ($this->files as $key => $file) {

            /**
             * jpg
             */
            $extension = $file->getExtension();

            /**
             * anime.jpg
             */
            $name = $file->getBaseName();

            /**
             * /home/vagrant/home/laravel.codrops/public/test/5ba3c4e3b9c31/images
             */
            $path = $file->getPath();

            /**
             * /home/vagrant/home/laravel.codrops/public/test/5ba3c4e3b9c31/images/anime.jpg
             */
            $pathName = $file->getPathName();

            /**
             * images/anime.jpg
             */
            $relative = $file->getRelativePathName();

            if (in_array($extension, $this->aExtensions) && $this->isRussian($relative)) {

                $pathWithoutRelative = $this->getPathWithoutRelative($pathName);

                $newRelative = $this->rename($relative, $pathWithoutRelative, $extension);

                foreach ($aHtmlFiles as $aHtmlFile) {

                    $data = File::get($aHtmlFile);

                    $data = str_replace($relative, $newRelative, $data);

                    $data = File::put($aHtmlFile, $data);
                }
            }
        }

        return $this;
    }

    /**
     * @param $toZip
     * @return array
     */
    public function to($toZip)
    {
        if (File::exists($toZip)) {
            File::delete($toZip);
        }

        $this->files();

        $zip = new \ZipArchive;

        try {
            if ($zip->open($toZip, \ZipArchive::CREATE) === true) {

                $files = $this->allFiles();

                foreach ($files as $file) {

                    $pathName = $file->getPathName();

                    $localname = $this->getPathWithoutRelative($pathName, true);

                    //dd($pathName, $name);
                    $zip->addFile($pathName, $localname);
                }

                $zip->close();
            }

            $this->clearPath();

            return [];
        } catch (\Exception $e) {
            dd($e);
        }
    }

    /**
     * Очистить директорию с
     *
     */
    private function clearPath()
    {
        $directories = File::directories($this->extractTo);

        foreach ($directories as $directory) {
            File::deleteDirectory($directory);
        }
    }


    /**
     * @return array
     */
    private function getHtml()
    {
        $this->files = File::allFiles($this->extractToPath);

        $html = [];

        foreach ($this->files as $file) {

            $extension = $file->getExtension();

            $path = $file->getPathName();
            if ($extension === 'html') {
                $html[] = $path;
            }
        }

        return $html;
    }

    /**
     * @param $text
     * @return false|int
     */
    private function isRussian($text)
    {
        return preg_match('/[А-Яа-яЁё]/u', $text);
    }


    /**
     * @param $str
     * @return string
     */
    private function ru2lat($str)
    {
        $tr = [
            "А" => "a", "Б" => "b", "В" => "v", "Г" => "g", "Д" => "d",
            "Е" => "e", "Ё" => "yo", "Ж" => "zh", "З" => "z", "И" => "i",
            "Й" => "j", "К" => "k", "Л" => "l", "М" => "m", "Н" => "n",
            "О" => "o", "П" => "p", "Р" => "r", "С" => "s", "Т" => "t",
            "У" => "u", "Ф" => "f", "Х" => "kh", "Ц" => "ts", "Ч" => "ch",
            "Ш" => "sh", "Щ" => "sch", "Ъ" => "", "Ы" => "y", "Ь" => "",
            "Э" => "e", "Ю" => "yu", "Я" => "ya", "а" => "a", "б" => "b",
            "в" => "v", "г" => "g", "д" => "d", "е" => "e", "ё" => "yo",
            "ж" => "zh", "з" => "z", "и" => "i", "й" => "j", "к" => "k",
            "л" => "l", "м" => "m", "н" => "n", "о" => "o", "п" => "p",
            "р" => "r", "с" => "s", "т" => "t", "у" => "u", "ф" => "f",
            "х" => "kh", "ц" => "ts", "ч" => "ch", "ш" => "sh", "щ" => "sch",
            "ъ" => "", "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu",
            "я" => "ya", " " => "-", "." => ".", "," => "", "/" => "/",
            ":" => "", ";" => "", "—" => "", "–" => "-"
        ];
        return strtr($str, $tr);
    }


    /**
     * @param $name
     * @param $path
     * @param $extension
     * @return mixed|string
     */
    private function rename($name, $path, $extension)
    {
        $newName = $this->ru2lat($name);

        if (File::exists($path . '/' . $newName)) {
            $newName = str_replace('.' . $extension, '', $newName);
            $newName = $newName . '_' . $this->uniqueFileKey . '.' . $extension;
        }

        File::move($path . '/' . $name, $path . '/' . $newName);

        return $newName;
    }


}