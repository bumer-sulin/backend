<?php

namespace App\Services;

use App\Models\Payment;
use App\Services\Yandex\HttpNotification;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use YandexMoney\ExternalPayment;
use YandexMoney\API;

class Cashier
{


    private $client_id = null;
    private $client_secret = null;
    private $money_wallet = null;
    private $redirect_uri = null;

    protected $notification_secret = null;


    private $scope = ['account-info','operation-history'];
    private $code = 'code';
    private $pattern_id = 'p2p';

    public function __construct()
    {
        $this->client_id = Config::get('services.yandex.money.client_id');
        $this->client_secret = Config::get('services.yandex.money.client_secret');
        $this->money_wallet = Config::get('services.yandex.money.wallet');
        $this->redirect_uri = Config::get('services.yandex.money.redirect_uri');
        $this->notification_secret = Config::get('services.yandex.money.notification_secret');
    }


    /**
     * Получить url для получения токена.
     * Необходимо для получения информации о кошельке
     *
     * @return \YandexMoney\response
     */
    public function getObtainTokenUrl()
    {
        $auth_url = API::buildObtainTokenUrl($this->client_id, $this->redirect_uri, $this->scope);
        return $auth_url;
    }

    /**
     * Получить токен по коду
     *
     * @param $code
     */
    public function getAccessToken($code)
    {
        $access_token_response = API::getAccessToken(
            $this->client_id,
            $code,
            $this->redirect_uri,
            $this->client_secret
        );

        if(property_exists($access_token_response, "error")) {
            // process error
        }

        $access_token = $access_token_response->access_token;

        $api = new API($access_token);

        // get account info
        $acount_info = $api->accountInfo();
        dd($acount_info);
    }

    /**
     * Создать платеж и вернуть форму с post-переходом на страницу
     *
     * @param $amount
     * @param $message
     * @return array
     */
    public function setPayment($amount, $message)
    {
        $result = $this->externalPayment($amount, $message);
        return [
            'form' => $this->createExtAuthForm($result['acs_uri'], $result['params']),
            'cps' => $result['cps']
        ];
    }


    public function externalPayment($amount, $message)
    {
        //$amount = 1;
        $response = ExternalPayment::getInstanceId($this->client_id);
        if($response->status == "success") {
            $instance_id = $response->instance_id;

            $external_payment = new ExternalPayment($instance_id);

            $payment_options = [
                'pattern_id' => $this->pattern_id,
                'to' => $this->money_wallet,
                'amount_due' => $amount,
                'message' => $message,
                'test_payment' => true,
            ];
            $response = $external_payment->request($payment_options);

            if($response->status == "success") {
                $request_id = $response->request_id;
                $process_options = [
                    "request_id" => $request_id,
                    'instance_id' => $instance_id,
                    'ext_auth_success_uri' => $this->redirect_uri,
                    'ext_auth_fail_uri' => $this->redirect_uri,
                    'request_token' => true
                ];
                $result = $external_payment->process($process_options);
                if (isset($result->error)) {
                    dd('throw exception with $result->error');
                } else {
                    if ($result->status === 'ext_auth_required') {
                        $params = [];
                        foreach($result->acs_params as $key => $value) {
                            $params[$key] = $value;
                        }
                        return [
                            'acs_uri' => $result->acs_uri,
                            'params' => $params,
                            'cps' => isset($cps) ? $cps : null
                        ];
                    } else {
                        dd('throw exception with not ext_auth_required');
                    }
                }
            }
            else {
                dd('throw exception with $response->message');
            }
        }
        else {
            dd('throw exception with $response->error message');
        }
        return null;
    }


    public function getPayment($nPaymentId, $status = null)
    {
        if (!is_null($status)) {
            if (is_array($status)) {
                return Payment::where('id', $nPaymentId)->whereIn('status', $status)->first();
            } else {
                return Payment::where('id', $nPaymentId)->where('status', $status)->first();
            }
        } else {
            return Payment::where('id', $nPaymentId)->where('status')->first();
        }
    }



    /**
     * Форма для WebView на yandex с параметрами
     *
     * @param $action
     * @param $params
     * @return string
     */
    public function createExtAuthForm($action, $params)
    {
        return view('defaults.app.yandex.ext_auth', [
            'action' => $action,
            'params' => $params
        ])->render();
    }


    public function getNotification()
    {
        return new HttpNotification();
    }

}