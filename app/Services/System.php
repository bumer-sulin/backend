<?php

namespace App\Services;


use DebugBar\DataCollector\MemoryCollector;
use Illuminate\Support\Facades\File;

class System
{
    public function cpu()
    {
        return sys_getloadavg();
    }


    public function memory()
    {
        return (new MemoryCollector())->collect();
    }

    public function getMemory()
    {
        $free = shell_exec('free');
        $free = (string)trim($free);
        $free_arr = explode("\n", $free);
        $mem = explode(" ", $free_arr[1]);
        $mem = array_filter($mem);
        $mem = array_merge($mem);
        $memory_usage = $mem[2] / $mem[1] * 100;

        return $memory_usage;
    }

    public function getCpu()
    {
        $load = sys_getloadavg();
        return $load[0];
    }

    public function getSizeDirectory()
    {
        $file_size = 0;

        foreach (File::allFiles(public_path('/')) as $file) {
            $file_size += $file->getSize();
        }
        return number_format($file_size / 1048576, 2);
    }
}
