<?php


namespace App\Services;


use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;

class FrontendCommon
{
    /**
     * @var mixed
     */
    private $manifest = [];

    /**
     * @var string
     */
    private $dirToFrontend = '';

    /**
     * @var string
     */
    private $pathToFrontend = '';

    /**
     * FrontendCommon constructor.
     */
    public function __construct($parse = false)
    {
        $this->dirToFrontend = public_path('frontend/build/production');
        $this->pathToFrontend = 'frontend/build/production/';
        if ($parse) {

            $this->manifest = Cache::remember('frontend-'.config('app.version_frontend'), 3600, function () {
                return json_decode(File::get($this->dirToFrontend . '/asset-manifest.json'), true);
            });
        }
    }

    /**
     * @param $name
     * @return string
     */
    public function asset($name)
    {
        if (isset($this->manifest[$name])) {
            return asset($this->pathToFrontend.$this->manifest[$name].'?v='.config('app.version_frontend'));
        }
        return asset($this->pathToFrontend.$name.'?v='.config('app.version_frontend'));
    }
}
