<?php

namespace App\Services\Modelable;


trait Statusable
{
    public function statuses()
    {
        return property_exists($this, 'statuses') ? $this->statuses : [
            0 => 'Не активно',
            1 => 'Активно',
        ];
    }

    public function statusIcons()
    {
        return property_exists($this, 'statusIcons') ? $this->statusIcons : [
            0 => [
                'class' => 'badge badge-default',
            ],
            1 => [
                'class' => 'badge badge-success',
            ],
        ];
    }

    /**
     * Get statuses array
     *
     * @return mixed
     */
    public function getStatuses()
    {
        return $this->statuses();
    }

    /**
     * Accessor for get text status
     *
     * @return string
     */
    public function getStatusTextAttribute()
    {
        return $this->statuses()[$this->status];
    }

    /**
     * Accessor for get text status
     *
     * @return string
     */
    public function getStatusIconAttribute()
    {
        $this->statusIcons()[$this->status]['title'] = $this->statuses()[$this->status];
        return $this->statusIcons()[$this->status];
    }
}
