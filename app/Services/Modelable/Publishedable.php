<?php

namespace App\Services\Modelable;


use Carbon\Carbon;

trait Publishedable
{
    public function formats()
    {
        return property_exists($this, 'formats') ? $this->formats : [
            0 => 'd.m.Y',
            1 => 'Y-m-d',
        ];
    }

    /**
     * Get statuses array
     *
     * @return mixed
     */
    public function getFormats()
    {
        return $this->formats();
    }

    /**
     * Accessor for get text status
     *
     * @return string
     */
    public function getPublishedAtFormatAttribute()
    {
        $value = method_exists($this, 'getFormatPublishedAtAttribute') ? $this->format_published_at : $this->defaultFormatPublishedAt();
        $date = $this->published_at;
        if (is_null($date)) {
            return $date;
        }
        if (strpos($this->formats()[$value], '%B') !== false) {
            return $date->format('d').' '.__('months')[strtolower($date->format('F'))].' '.$date->format('Y').'г';
        } else {
            return $date->format($this->formats()[$value]);
        }
    }

    /**
     * @return int
     */
    private function defaultFormatPublishedAt()
    {
        return 0;
    }
}
