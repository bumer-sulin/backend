<?php

namespace App\Services\Modelable;


trait Typeable
{
    /**
     * @return array
     */
    public function types()
    {
        return property_exists($this, 'types') ? $this->types : [
            0 => 'Нет типа',
            1 => 'Новость',
            2 => 'Статья',
            3 => 'Слайдер',
        ];
    }

    /**
     * Get statuses array
     *
     * @return mixed
     */
    public function getTypes()
    {
        return $this->types();
    }

    /**
     * Accessor for get text status
     *
     * @return string
     */
    public function getStatusTextAttribute()
    {
        return $this->types()[$this->type_id];
    }
}
