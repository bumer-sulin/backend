<?php

namespace App\Services;


use App\Http\Composers\CommonComposersTrait;
use App\Models\Setting;
use App\Services\Image\Facades\ImagePath;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;

class CmfDataService
{
    use CommonComposersTrait;

    private $data = [];

    private $active = [];


    public function __construct()
    {
        $this->data = $this->remember('content', function () {
            return $this->getAliases($this->getFiles(resource_path('views/admin/content'), 'content'));
        });
    }

    public function article()
    {
        $this->active = $this->data['article'];
        return $this;
    }

    public function promotion()
    {
        $this->active = $this->data['promotion'];
        return $this;
    }

    public function user()
    {
        $this->active = $this->data['user'];
        return $this;
    }

    public function filters()
    {
        return $this->active['filters'];
    }
}
