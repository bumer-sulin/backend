<?php

namespace App\Observers;

use App\Models\User;
use App\Registries\MemberRegistry;
use Illuminate\Support\Facades\Cache;

class UserObserver extends MainObserver
{

    private $member;

    public function __construct()
    {
        $this->member = MemberRegistry::getInstance();
    }

    /**
     * Listen to the User created event.
     *
     * @param  \App\Models\User  $oUser
     * @return void
     */
    public function updated(User $oUser)
    {
        Cache::forget('user_'.$oUser->id);
    }

    public function updating(User $oUser)
    {

    }

}
