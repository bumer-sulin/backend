<?php

namespace App\Observers;

use App\Models\Tag;

class TagObserver extends MainObserver
{
    public function creating(Tag $oItem)
    {
        if (empty($oItem->icon)) {
            $oItem->icon = 'brightness_1';
        }
        if (empty($oItem->color)) {
            $oItem->color = '#fff';
        }
    }


}