<?php

namespace App\Observers;

use App\Models\Category;

class CategoryObserver extends MainObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  \App\Models\Category  $oCategory
     * @return void
     */
    public function updated(Category $oCategory)
    {
        if ($this->checkChange($oCategory, 'status', 'int')) {
            if ($oCategory->hasChildren()) {
                foreach ($oCategory->children as $oChildren) {
                    $oChildren->update([
                        'status' => $oCategory->status
                    ]);
                }
            }
        }
    }

    /**
     * Listen to the User created event.
     *
     * @param  \App\Models\Category  $oCategory
     * @return void
     */
    public function deleted(Category $oCategory)
    {
        if ($oCategory->hasChildren()) {
            foreach ($oCategory->children as $oChildren) {
                $oChildren->delete();
            }
        }
    }


}
