<?php

namespace App\Observers;

use App\Models\Comment;

class CommentObserver extends MainObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  \App\Models\Comment $oComment
     * @return void
     */
    public function deleted(Comment $oComment)
    {
        if ($oComment->hasChildren()) {
            foreach ($oComment->children as $oChildren) {
                $oChildren->delete();
            }
        }
    }


}
