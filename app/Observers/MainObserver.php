<?php

namespace App\Observers;

class MainObserver
{
    /**
     * Check change value by column and type
     *
     * @param $oModel
     * @param $column
     * @param null $type
     * @return bool
     */
    protected function checkChange($oModel, $column, $type = null)
    {
        if (!is_null($type) && $type === 'int') {
            return intval($oModel->{$column}) !== intval($oModel->getOriginal($column));
        }
        return $oModel->{$column} !== $oModel->getOriginal($column);
    }
}
