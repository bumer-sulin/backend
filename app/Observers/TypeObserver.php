<?php

namespace App\Observers;

use App\Models\Type;

class TypeObserver extends MainObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  \App\Models\Type  $oItem
     * @return void
     */
    public function updated(Type $oItem)
    {
        if ($this->checkChange($oItem, 'status', 'int')) {
            if ($oItem->hasChildren()) {
                foreach ($oItem->children as $oChildren) {
                    $oChildren->update([
                        'status' => $oItem->status
                    ]);
                }
            }
        }
    }
    /**
     * Listen to the User created event.
     *
     * @param  \App\Models\Type  $oItem
     * @return void
     */
    public function deleted(Type $oItem)
    {
        if ($oItem->hasChildren()) {
            foreach ($oItem->children as $oChildren) {
                $oChildren->delete();
            }
        }
    }


}