<?php

namespace App\Observers;

use App\Models\Article;
use App\Models\ArticleStatistics;

class ArticleObserver extends MainObserver
{
    public function creating(Article $oItem)
    {
        $oItem->creator_id = 1;
        $oItem->status = 0;
    }


    public function created(Article $oItem)
    {
        ArticleStatistics::create([
            'article_id' => $oItem->id
        ]);
        $oItem->update([
            'name' => str_slug($oItem->title),
        ]);
    }


}
