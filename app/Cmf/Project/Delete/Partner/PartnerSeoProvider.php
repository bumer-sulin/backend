<?php

namespace App\Cmf\Project\Partner;

use KekoApp\LaravelMetaTags\Facades\MetaTag;

class PartnerSeoProvider
{
    public function index()
    {
        MetaTag::set('title', 'Partner Index');
    }

    public function create()
    {
        MetaTag::set('title', 'Partner Create');
    }

    public function show($oModel)
    {
        //MetaTag::set('title', $oModel->key);
    }
}
