<?php

namespace App\Cmf\Project\Partner;

use App\Cmf\Core\SettingsTrait;

trait PartnerSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Партнеры',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'partner',
            ],
        ],
        'icon' => 'fa fa-industry'
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'partner';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'partner';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = 'partner';

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = false;

    /**
     * Defaults values for store method
     *
     * @var array
     */
    private $defaults = [
        'store' => [
            'status' => 0,
        ],
    ];

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'title' => 'Заголовок',
        'image' => 'Изображение',
        'file' => 'Файл',
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    private $image = [
        'key' => 'partner',
        'with_main' => true,
        'unique' => false,
    ];

    /**
     * Default files settings in model
     *
     * @var array
     */
    private $file = [
        'key' => 'partner',
        'unique' => false,
    ];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            'title' => 'required|max:255',
        ],
        'update' => [
            'title' => 'required|max:255',
        ],
        'upload' => [
            'id' => 'required|max:255',
            'images' => 'required|max:5000|mimes:jpg,jpeg,gif,png',
        ],
        'uploadFiles' => [
            'id' => 'required|max:255',
            'files' => 'required|max:10000',
        ],
    ];

}
