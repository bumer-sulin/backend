<?php

namespace App\Cmf\Project\Album;

class AlbumController extends AlbumBaseController
{



    public function all()
    {
        $this->seo($this->seo, __FUNCTION__);

        $oItems = $this->paginate($this->class);

        return view($this->getView($this->views, 'index'), [
            'breadcrumb' => 'album.all',
            'oItems' => $oItems
        ]);
    }
}
