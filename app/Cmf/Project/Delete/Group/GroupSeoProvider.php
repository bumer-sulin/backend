<?php

namespace App\Cmf\Project\Group;

use KekoApp\LaravelMetaTags\Facades\MetaTag;

class GroupSeoProvider
{
    public function index()
    {
        MetaTag::set('title', 'Article Index');
    }

    public function create()
    {
        MetaTag::set('title', 'Article Create');
    }

    public function show($oModel)
    {
        //MetaTag::set('title', $oModel->key);
    }
}
