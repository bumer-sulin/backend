<?php

namespace App\Cmf\Project\Group;


use App\Cmf\Project\Album\AlbumController;
use App\Models\Album;
use App\Models\Group;
use App\Models\GroupContact;
use App\Models\GroupVideo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

trait GroupCustomTrait
{
    public function getAlbums(Request $request, $id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);

        $view = view($this->theme . '.content.' . $this->view . '.components.modals.albums', [
            'oItem' => $oItem,
            'oItems' => Album::where('group_id', $oItem->id)->paginate(1)->withPath(route('album.view.post'))
        ])->render();


        return $this->success([
            'view' => $view
        ]);
    }


    public function updateContacts(Request $request)
    {
        $validation = $this->validation($request, $this->rules, $this->attributes, __FUNCTION__);
        if ($validation->fails()) {
            return response()->json($validation->getMessageBag(), 422);
        } else {
            $oGroup = Group::find($request->get('id'));
            $model = new GroupContact;
            $data = array_only($request->all(), $model->getFillable());

            if (!is_null($oGroup->contacts)) {
                $id = $oGroup->contacts->id;
                $model::find($id)->update($data);
            } else {
                $data['group_id'] = $oGroup->id;
                $model::create($data);
            }

            $this->getGenres($request, $oGroup);


            return $this->success([
                'toastr' => [
                    'title' => 'Успех',
                    'text' => 'Данные успешно добавлены',
                    'type' => 'success'
                ]
            ]);
        }
    }

    private function getGenres($request, $oGroup)
    {
        if ($request->exists('genres')) {
            $genres = $request->get('genres');
            if (count($oGroup->genres) !== 0) {
                foreach ($genres as $genre) {
                    if (is_null($oGroup->genres->where('id', $genre)->first())) {
                        $oGroup->genres()->attach($genre);
                    }
                }
                foreach ($oGroup->genres as $genre) {
                    $key = array_search($genre->id, $genres);
                    if (!$key && $key !== 0) {
                        $oGroup->genres()->detach($genre);
                    }
                }
            } else {
                foreach ($genres as $genre) {
                    $oGroup->genres()->attach($genre);
                }
            }
        } else {
            if (count($oGroup->genres) !== 0) {
                foreach ($oGroup->genres as $genre) {
                    $oGroup->genres()->detach($genre);
                }
            }
        }
    }


    public function checkForStatus($oGroup)
    {
        if (count($oGroup->activeAlbums) === 0) {
            return $this->validateMessage(false, 'У группы нет активных альбомов');
        }
        if (count($oGroup->images) === 0) {
            return $this->validateMessage(false, 'У группы нет изображений');
        }
        if (count($oGroup->genres) === 0) {
            return $this->validateMessage(false, 'У группы нет жанров');
        }
        return $this->validateMessage(true);
    }


    private function validateMessage($success, $message = '')
    {
        return $success ? [
            'success' => $success
        ] : [
            'success' => $success,
            'message' => [
                'title' => 'Ошибка',
                'text' => $message,
                'type' => 'error'
            ]
        ];
    }

}