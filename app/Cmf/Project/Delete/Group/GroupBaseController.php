<?php

namespace App\Cmf\Project\Group;

use App\Cmf\Core\Defaults\DatatableTrait;
use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\MainController;
use App\Models\Image;
use App\Services\Toastr\Toastr;
use Chumper\Zipper\Facades\Zipper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use App\Cmf\Core\ControllerCrmTrait;
use App\Cmf\Core\SeoProviderTrait;


use App\Models\Group;
use Spatie\DbSnapshots\SnapshotFactory;
use Spatie\DbSnapshots\SnapshotRepository;

class GroupBaseController extends MainController
{
    use GroupSettingsTrait, ControllerCrmTrait, SeoProviderTrait;
    use ImageableTrait;
    use DatatableTrait;
    use GroupCustomTrait;

    /**
     * Cache model for all items
     *
     * @var mixed|null
     */
    private $cacheModel = null;

    /**
     * Seo Provider
     * @var null
     */
    private $seo = GroupSeoProvider::class;

    /**
     * Class model
     * @var null
     */
    private $class = Group::class;

    /**
     * Pagination url
     * @var string
     */
    private $tableUrl;

    /**
     * Table limit item
     * @var int
     */
    private $tableLimit = 10;

    /**
     * Default order columns
     * @var string
     */
    private $order = 'created_at.desc';

    /**
     * Default query ['status' => 1, ]
     */
    private $aQuery = [];

    /**
     * Order columns
     * @var array
     */
    private $orderBy = [];

    /**
     * Views array, set in @see \App\Cmf\Core\ControllerCrmTrait::setViews()
     *
     * @var array
     */
    private $views = [];

    public function __construct()
    {
        /**
         * Default session query
         */
        $this->aQuery = $this->setQuery($this->session, $this->aQuery);

        /**
         * Route for pagination
         */
        $this->tableUrl = route($this->view.'.view.post');

        /**
         * Default class model
         */
        $this->cacheModel = $this->getCacheModel($this->class, $this->cache);

        /**
         * Order columns
         */
        $this->orderBy = $this->setOrderBy($this->order, $this->session);

        $this->views = $this->setViews();

        //$array = \Bulk\Toastr\Facades\Toastr::success('Message')->toArray();

        //dd($array);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->seo($this->seo, __FUNCTION__);

        $oItems = $this->paginate($this->class);

        //$oItems = datatables()->of($oItems)->toJson();

        //dd(name_of_function1());
        //\App\Jobs\SendLog::dispatch('Привет');
        //dd($oItems);
        //dd($oItems->first()->genres);

        return view($this->views[__FUNCTION__], [
            'oItems' => $oItems
        ]);
    }

    /**
     * Default query items
     *
     * @param $oModel
     * @param null $type
     * @param array $aColumns
     * @return mixed
     */
    public function paginate($oModel, $type = null, $aColumns = [])
    {
        $oItems = $oModel::with('albums')->orderBy($this->orderBy['column'], $this->orderBy['type']);
        if (!is_null($type) && $type === 'search') {
            foreach($aColumns as $key => $value) {
                if (!is_null($value)) {
                    if ($key === 'status') {
                        $oItems = $oItems->where($key, $value);
                    }
                    if ($key === 'title') {
                        $oItems = $oItems->where($key, 'like', '%'.$value.'%');
                    }
                }
            }
        }
        if (!empty($this->aQuery)) {
            foreach($this->aQuery as $key => $value) {
                $oItems = $oItems->where($key, $value);
            }
        }
        //return $oItems->get();
        return $oItems->paginate($this->tableLimit)->withPath($this->tableUrl);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->seo($this->seo, __FUNCTION__);

        return view($this->views[__FUNCTION__]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $this->validation($request, $this->rules, $this->attributes, __FUNCTION__);
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {

            $model = $this->class;
            $data = $this->getSaveColumns($request, $this->view);
            if (!is_null($data)) {
                $model::create($data);
            } else {
                return $this->jsonError(trans('form.errors.cache'));
            }
            $this->afterChange($this->cache);
            return $this->success([
                'toastr' => $this->toastr[__FUNCTION__]
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $this->seo($this->seo, 'show', $oItem);
        return view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        return view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = $this->validation($request, $this->rules, $this->attributes, __FUNCTION__);
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {
            $model = $this->class;
            $data = $this->getSaveColumns($request, $this->view);

            if (!is_null($data)) {
                $model::find($id)->update($data);
            } else {
                return $this->jsonError(trans('form.errors.cache'));
            }
            $this->afterChange($this->cache);
            return $this->success([
                'toastr' => $this->toastr[__FUNCTION__]
            ]);
        }
    }

    /**
     * Update the status in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array
     */
    public function status(Request $request, $id)
    {
        $model = $this->class;
        $oModel = $model::find($id);
        $check = $this->checkForStatus($oModel);
        if ($check['success']) {
            $oModel->update([
                'status' => $request->get('status'),
            ]);
        } else {
            return $this->success([
                'toastr' => $check['message']
            ]);
        }
        $this->afterChange($this->cache);
        return $this->success([
            'toastr' => (new Toastr(trans('toastr')['status'][$request->get('status')]['text']))->success()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = $this->class;
        $model::destroy($id);
        $this->afterChange($this->cache);
        return $this->success([
            'toastr' => $this->toastr[__FUNCTION__]
        ]);
    }

    /**
     * Генерация после добавление/изменения/удаления
     *
     * @param Request $request
     * @return array
     */
    public function view(Request $request)
    {
        $oItems = $this->paginate($this->class);

        return $this->table($oItems);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function search(Request $request)
    {
        $oItems = $this->paginate($this->class, 'search', [
            'title' => $request->get('search'),
            'status' => $request->get('status'),
        ]);

        return $this->table($oItems);
    }



    /**
     * Обновление таблицы после добавление/изменения/удаления
     *
     * @param $oItems
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function table($oItems)
    {
        return $this->success([
            'view' => view($this->views[__FUNCTION__], [
                'oItems' => $oItems
            ])->render(),
            'count' => $oItems->total()
        ]);
    }

    /**
     * Модальное окно для добавления
     *
     * @param Request $request
     * @return array
     * @throws \Throwable
     * @throws \Exception
     */
    public function createModal(Request $request)
    {
        $view = view($this->views[__FUNCTION__])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * Модальное окно для изменений
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function editModal($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }



    /**
     * Get images in modal
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function image($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * Get images in modal
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function files($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * Set sort in session
     *
     * @param Request $request
     * @return array
     */
    public function sort(Request $request)
    {
        if ($request->exists('sort')) {
            Session::put($this->session.'.sort.column', $request->get('sort'));
        }
        if ($request->exists('type')) {
            if (Session::has($this->session.'.sort.type') && Session::get($this->session.'.sort.type') === 'desc') {
                Session::put($this->session.'.sort.type', 'asc');
            } else {
                Session::put($this->session.'.sort.type', $request->get('type'));
            }
        }
        return $this->success();
    }

    /**
     * Ser where query in session
     *
     * @param Request $request
     * @return array
     */
    public function query(Request $request)
    {
        $this->setQuerySearch($request, ['status']);
        return $this->success();
    }

    public function datatables(Request $request)
    {
        $this->seo($this->seo, __FUNCTION__);

        $oItems = $this->paginate($this->class);

        return datatables()->of($oItems)->toJson();

        $data = datatables()->of($oItems)->toArray();

        $data['data'] = $oItems->transform(function($oItem) {
            return $this->getRows($this->columns, [
                'oItem' => $oItem,
            ]);
        })->toArray();

        return response()->json($data);
    }
}