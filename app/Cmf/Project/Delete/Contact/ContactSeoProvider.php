<?php

namespace App\Cmf\Project\Contact;

use KekoApp\LaravelMetaTags\Facades\MetaTag;

class ContactSeoProvider
{
    public function index()
    {
        MetaTag::set('title', 'Contact Index');
    }

    public function create()
    {
        MetaTag::set('title', 'Contact Create');
    }

    public function show($oModel)
    {
        //MetaTag::set('title', $oModel->key);
    }
}
