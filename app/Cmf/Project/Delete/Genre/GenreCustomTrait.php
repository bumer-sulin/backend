<?php

namespace App\Cmf\Project\Genre;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

trait GenreCustomTrait
{
    public function getAlbums(Request $request, $id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view($this->theme.'.content.'.$this->view.'.components.modals.albums', [
            'oItem' => $oItem,
            'oItems' => $oItem->albums
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }
}