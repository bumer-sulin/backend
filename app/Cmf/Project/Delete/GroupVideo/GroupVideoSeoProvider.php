<?php

namespace App\Cmf\Project\GroupVideo;

use KekoApp\LaravelMetaTags\Facades\MetaTag;

class GroupVideoSeoProvider
{
    public function index()
    {
        MetaTag::set('title', 'Article Index');
    }

    public function create()
    {
        MetaTag::set('title', 'Article Create');
    }

    public function show($oModel)
    {
        //MetaTag::set('title', $oModel->key);
    }
}
