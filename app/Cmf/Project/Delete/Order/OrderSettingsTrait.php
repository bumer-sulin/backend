<?php

namespace App\Cmf\Project\Order;

use App\Cmf\Core\SettingsTrait;

trait OrderSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Заказы',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'order',
            ],
        ],
        'icon' => 'fa fa-barcode'
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'order';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'order';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = 'order';

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = false;

    /**
     * Defaults values for store method
     *
     * @var array
     */
    private $defaults = [
        'store' => [
            'phone' => [
                'filter' => 'phone'
            ],
            'status' => 1,
        ],
        'update' => [
            'phone' => [
                'filter' => 'phone'
            ],
        ]
    ];

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'name' => 'ФИО',
        'email' => 'email',
        'phone' => 'телефон',
        'image' => 'Изображение',
        'file' => 'Файл',
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    private $image = [
        'key' => 'order',
        'with_main' => true,
        'unique' => false,
    ];

    /**
     * Default files settings in model
     *
     * @var array
     */
    private $file = [
        'key' => 'order',
        'unique' => false,
    ];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            'name' => 'required|max:255',
            'phone' => 'required_without:email|max:255',
            'email' => 'required_without:phone|max:255',
        ],
        'update' => [
            'title' => 'required|max:255',
        ],
        'upload' => [
            'id' => 'required|max:255',
            'images' => 'required|max:5000|mimes:jpg,jpeg,gif,png',
        ],
        'uploadFiles' => [
            'id' => 'required|max:255',
            'files' => 'required|max:10000',
        ],
    ];



    /**
     * Переопределение свойства $toastr SettingsTrait
     *
     * @var array
     */
    public $_toastr = [
        'status'    => [
            0 => [
                'title' => 'Успех',
                'text' => 'Данные успешно изменены',
                'type' => 'success',
            ],
        ],
    ];


}
