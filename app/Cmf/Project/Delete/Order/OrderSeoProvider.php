<?php

namespace App\Cmf\Project\Order;

use KekoApp\LaravelMetaTags\Facades\MetaTag;

class OrderSeoProvider
{
    public function index()
    {
        MetaTag::set('title', 'Order Index');
    }

    public function create()
    {
        MetaTag::set('title', 'Order Create');
    }

    public function show($oModel)
    {
        //MetaTag::set('title', $oModel->key);
    }
}
