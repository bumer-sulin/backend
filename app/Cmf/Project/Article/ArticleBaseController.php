<?php

namespace App\Cmf\Project\Article;

use App\Cmf\Core\Defaults\FileableTrait;
use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\Defaults\TextableTrait;
use App\Jobs\UpdateSitemap;
use App\Models\Category;
use App\Models\Image;
use App\Models\Option;
use App\Models\Tag;
use App\Models\Type;
use App\Services\Toastr\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cmf\Core\MainController;
use Illuminate\Support\Facades\Session;
use App\Cmf\Core\ControllerCrmTrait;
use App\Cmf\Core\SeoProviderTrait;


use App\Models\Article;

class ArticleBaseController extends MainController
{
    use ArticleSettingsTrait, ControllerCrmTrait, SeoProviderTrait;
    use TextableTrait;
    use ImageableTrait;
    use FileableTrait;
    use ArticleCustomTrait;

    /**
     * Cache model for all items
     *
     * @var mixed|null
     */
     private $cacheModel = null;

    /**
     * Seo Provider
     * @var null
     */
    private $seo = ArticleSeoProvider::class;

    /**
     * Class model
     * @var null
     */
    private $class = Article::class;

    /**
     * Pagination url
     * @var string
     */
    private $tableUrl;

    /**
     * Table limit item
     * @var int
     */
    private $tableLimit = 10;

    /**
     * Default order columns
     * @var string
     */
    private $order = 'created_at.desc';

    /**
     * Default query ['status' => 1, ]
     */
    private $aQuery = [];

    /**
     * Order columns
     * @var array
     */
    private $orderBy = [];

    /**
     * Views array, set in @see \App\Cmf\Core\ControllerCrmTrait::setViews()
     *
     * @var array
     */
    protected $views = [];

    public function __construct()
    {
        /**
         * Default session query
         */
        $this->aQuery = $this->setQuery($this->session, $this->aQuery);

        /**
         * Route for pagination
         */
        $this->tableUrl = route($this->view.'.view.post');

        /**
         * Default class model
         */
        $this->cacheModel = $this->getCacheModel($this->class, $this->cache);

        /**
         * Order columns
         */
        $this->orderBy = $this->setOrderBy($this->order, $this->session);

        /**
         * Set views
         */
        $this->views = $this->setViews();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->seo($this->seo, __FUNCTION__);

        $this->setIndexQuery($request);

        $oItems = $this->paginate($this->class);

        Session::put($this->session . '.count.get', $oItems->total());
        Session::put($this->session . '.count.total', $this->class::count());

        return view($this->views[__FUNCTION__], [
            'oItems' => $oItems,
            'oCategories' => $this->categories(),
        ]);
    }

    public function setIndexQuery(Request $request)
    {
        if ($request->has('category_id')) {
            $this->setQuerySearch($request, ['category_id']);
            $this->aQuery['category_id'] = $request->get('category_id');
        }
        if ($request->has('id')) {
            $this->setQuerySearch($request, ['id']);
            $this->aQuery['id'] = $request->get('id');
        }
    }

    /**
     * Default query items
     *
     * @param $oModel
     * @param null $type
     * @param array $aColumns
     * @return mixed
     */
    public function paginate($oModel, $type = null, $aColumns = [])
    {
        $oItems = $oModel::with('images', 'category', 'statistic')->orderBy($this->orderBy['column'], $this->orderBy['type']);
        if (!is_null($type) && $type === 'search') {
            foreach($aColumns as $key => $value) {
                $oItems = $oItems->where($key, 'like', '%'.$value.'%');
            }
        }
        if (!empty($this->aQuery)) {
            foreach($this->aQuery as $key => $value) {
                $oItems = $oItems->where($key, $value);
            }
        }
        return $oItems->paginate($this->tableLimit)->withPath($this->tableUrl);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->seo($this->seo, __FUNCTION__);

        return view($this->views[__FUNCTION__]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $this->validation($request, $this->rules, $this->attributes, __FUNCTION__);
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {

            $model = $this->class;
            $data = $this->getSaveColumns($request, $this->cache);
            $data = $this->withDefaults($data, $this->defaults[__FUNCTION__]);

            if ((int)$request->get('type_id') === 3 && !is_null($request->get('category_id'))) {
                return response()->json([
                    'category_id' => 'Статья с типом Слайдер не может иметь категорию.'
                ],422);
            }

            if (!is_null($data)) {
                $model = $model::create($data);
            } else {
                return $this->jsonError(trans('form.errors.cache'));
            }
            $this->afterChange($this->cache);

            Session::put('last_create', $model->id);

            return $this->success([
                'toastr' => $this->toastr[__FUNCTION__]
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $this->seo($this->seo, 'show', $oItem);
        return view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        return view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = $this->validation($request, $this->rules, $this->attributes, __FUNCTION__);
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {
            $model = $this->class;
            $data = $this->getSaveColumns($request, $this->cache);
            if (!is_null($data)) {
                $model::find($id)->update($data);
            } else {
                return $this->jsonError(trans('form.errors.cache'));
            }
            $oProduct = $model::find($id);

            $this->syncTags($request, $oProduct);

            $this->updateArticleParameters($request, $id);

            $this->afterChange($this->cache);
            return $this->success([
                'toastr' => $this->toastr[__FUNCTION__]
            ]);
        }
    }

    /**
     * Update the status in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array
     */
    public function status(Request $request, $id)
    {
        $model = $this->class;
        $oModel = $model::find($id);
        if (in_array($request->get('status'), [1, 2])) {
            $check = $this->checkForStatus($oModel);
            if ($check['success']) {
                $oModel->update([
                    'status' => $request->get('status'),
                ]);
            } else {
                return $this->success([
                    'toastr' => $check['message']
                ]);
            }
        } else {
            $oModel->update([
                'status' => $request->get('status'),
            ]);
        }
        $this->afterChange($this->cache);
        $message = 'Статус "'.(new $model())->getStatuses()[$request->get('status')].'" успешно назначен.';
        dispatch(new UpdateSitemap());
        return $this->success([
            'toastr' => (new Toastr($message))->success()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = $this->class;
        $model = $model::find($id);;

        $this->beforeDeleteForImages($model);

        $model::destroy($id);
        $this->afterChange($this->cache);
        dispatch(new UpdateSitemap());
        return $this->success([
            'toastr' => $this->toastr[__FUNCTION__],
            'total' => $this->class::count(),
        ]);
    }

    /**
     * Генерация после добавление/изменения/удаления
     *
     * @param Request $request
     * @return array
     */
    public function view(Request $request)
    {
        $oItems = $this->paginate($this->class);

        return $this->table($oItems);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function search(Request $request)
    {
        $oItems = $this->paginate($this->class, 'search', [
            'title' => $request->get('search'),
            'status' => $request->get('status'),
        ]);
        return $this->table($oItems);
    }



    /**
     * Обновление таблицы после добавление/изменения/удаления
     *
     * @param $oItems
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function table($oItems)
    {
        $data = [
            'view' => view($this->views[__FUNCTION__], [
                'oItems' => $oItems
            ])->render(),
            'count' => $oItems->total()
        ];
        if (Session::exists('last_create')) {
            $data['id'] = Session::get('last_create');
        }

        return $this->success($data);
    }

    /**
     * Модальное окно для добавления
     *
     * @param Request $request
     * @return array
     * @throws \Throwable
     * @throws \Exception
     */
    public function createModal(Request $request)
    {
        $view = view($this->views[__FUNCTION__], [
            'oCategories' => Category::orderBy('title', 'asc')->get(),
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * Модальное окно для изменений
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function editModal($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);

        $oValues = $oItem->values->keyBy('option.key');

        $aOptionNames = [];

        $aOptionNames[] = Option::KEY_AUTHOR;
        $aOptionNames[] = 'avtor';
        $aOptionNames[] = 'podpis-avtora';
        //$aOptionNames[] = Option::KEY_PHOTO_AUTHOR;

        $oOptions = Option::orderBy('title', 'asc')->get();
        $oMainOptions = Option::whereIn('key', $aOptionNames)->orderBy('title', 'asc')->get();

        $idMainOptions = $oMainOptions->pluck('id')->toArray();

        $oOptions = $oOptions->reject(function ($item) use ($idMainOptions) {
            return in_array($item->id, $idMainOptions);
        });

        $view = $this->views[__FUNCTION__];
        if ($oItem->isSlider()) {
            $view = str_replace('.edit', '.gallery.edit', $view);
        }

        $view = view($view, [
            'oItem' => $oItem,
            'oTags' => Tag::all(),
            'oCategories' => Category::orderBy('title', 'asc')->get(),
            'oMainOptions' => $oMainOptions,
            'oOptions' => $oOptions,
            'oValues' => $oValues,
        ])->render();




        return $this->success([
            'view' => $view
        ]);
    }



    /**
     * Get images in modal
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function image($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * Get images in modal
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function files($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * Set sort in session
     *
     * @param Request $request
     * @return array
     */
    public function sort(Request $request)
    {
        if ($request->exists('sort')) {
            Session::put($this->session.'.sort.column', $request->get('sort'));
        }
        if ($request->exists('type')) {
            if (Session::has($this->session.'.sort.type') && Session::get($this->session.'.sort.type') === 'desc') {
                Session::put($this->session.'.sort.type', 'asc');
            } else {
                Session::put($this->session.'.sort.type', $request->get('type'));
            }
        }
        return $this->success();
    }

    /**
     * Ser where query in session
     *
     * @param Request $request
     * @return array
     */
    public function query(Request $request)
    {
        $this->setQuerySearch($request, ['status', 'category_id', 'type_id', 'id']);
        return $this->success();
    }

}
