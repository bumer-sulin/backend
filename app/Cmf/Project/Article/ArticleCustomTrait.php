<?php

namespace App\Cmf\Project\Article;


use App\Jobs\SendSubscriptions;
use App\Jobs\UpdateSitemap;
use App\Models\Article;
use App\Models\ArticleOptionValues;
use App\Models\Comment;
use App\Models\Image;
use App\Models\Option;
use App\Models\OptionParameter;
use App\Models\Product;
use App\Models\ProductOptions;
use App\Models\ProductOptionValues;
use App\Models\ProductPrices;
use App\Models\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

trait ArticleCustomTrait
{
    public function updateArticleParameters(Request $request, $id)
    {
        $oProduct = Article::find($id);
        if ($request->exists('options')) {
            $aOptions = $request->get('options');
            foreach ($aOptions as $key => $value) {
                $oOption = Option::where('key', $key)->first();
                if (!is_null($oOption) && !$oOption->isList()) {
                    $oValue = ArticleOptionValues::where('article_id', $oProduct->id)
                        ->where('option_id', $oOption->id)
                        ->first();
                    if (isset($value)) {
                        if (!is_null($oValue)) {
                            $oValue->update([
                                'value' => $value
                            ]);
                        } else {
                            ArticleOptionValues::create([
                                'article_id' => $oProduct->id,
                                'option_id' => $oOption->id,
                                'value' => $value,
                            ]);
                        }
                    } else {
                        if(!is_null($oValue)) {
                            $oValue->delete();
                        }
                    }
                }
            }
        }
        return responseCommon()->success([], 'Данные успешно обновлены');
    }

    public function checkForStatus($oItem)
    {
        $message = null;
        if (is_null($oItem->description) && !$oItem->isSlider()) {
            $message = $this->setValidateMessage($message, '- подзаголовка');
        }
        if (is_null($oItem->text) && !$oItem->isSlider()) {
            $message = $this->setValidateMessage($message, '- основного текста');
        }
        if (count($oItem->images->where('is_main', 1)) === 0) {
            $message = $this->setValidateMessage($message, '- главного изображения');
        }
        if (is_null($oItem->published_at)) {
            $message = $this->setValidateMessage($message, '- даты публикации');
        }
        return !is_null($message) ?
            $this->validateMessage(false, $message) :
            $this->validateMessage(true);
    }

    private function setValidateMessage($message, $text)
    {
        if (is_null($message)) {
            $message = 'У статьи нет:';
        }
        if (!is_null($message)) {
            $message = !is_null($message) ? $message.'<br>'.$text : $text;
        }
        return $message;
    }



    private function validateMessage($success, $message = '')
    {
        return $success ? [
            'success' => $success
        ] : [
            'success' => $success,
            'message' => [
                'title' => 'Ошибка',
                'text' => $message,
                'type' => 'error'
            ]
        ];
    }

    public function syncTags(Request $request, $oProduct)
    {
        if ($request->exists('tags')) {
            $aTags = $request->get('tags');
            $aId = [];
            foreach ($aTags as $key => $aTag) {
                $aId[] = $aTag;
                $oProduct->tags()->sync([
                    $aTag => [
                        'tag_id' => $aTag,
                    ]
                ], false);
            }
            $aIssetParameters = $oProduct->tags->pluck('id');
            foreach ($aIssetParameters as $id) {
                if (!in_array($id, $aId)) {
                    $oProduct->tags()->detach($id);
                }
            }
        } else {
            $oProduct->tags()->detach();
        }
    }

    public function setPublication(Request $request, $id)
    {
        $oItem = Article::find($id);

        if (is_null($oItem->published_at)) {
            return responseCommon()->error([], [
                'message' => 'Статья не может быть опубликована.<br> Нет даты публикации.',
                'textable' => true,
            ]);
        }

        $oItem->update([
            'publication_at' => now(),
        ]);

        if (!$oItem->isSlider()) {
            $oSubscriptions = Subscription::active()->get();
            $emails = [];

            foreach ($oSubscriptions as $oSubscription) {
                if ($oSubscription->isCategories()) {
                    $aCategories = $oSubscription->categories->pluck('id')->toArray();

                    if (in_array($oItem->category_id, $aCategories)) {
                        $emails[] = $oSubscription->email;
                    }
                } else {
                    $emails[] = $oSubscription->email;
                }
            }

            foreach ($emails as $email) {
                dispatch(new SendSubscriptions($oItem, $email));
            }
        }

        dispatch(new UpdateSitemap());

        return responseCommon()->success([], [
            'message' => 'Рассылка успешно выполнена.<br>Статья успешно опубликована.',
            'textable' => true,
        ]);
    }


    /**
     * @param Request $request
     * @param $id
     * @return mixed
     * @throws \Throwable
     */
    public function getCommentsModal(Request $request, $id)
    {
        $oItem = Article::find($id);

        $view = $this->views['editModal'];
        $view = str_replace('.edit', '.comments.edit', $view);

        $oComments = $oItem->comments;

        $oComments->load(['loveUsers', 'children', 'children.loveUsers', 'commentable']);

        $view = view($view, [
            'oItem' => $oItem,
            'oComments' => $oComments,
        ])->render();

        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return array
     */
    public function approveComment(Request $request, $id)
    {
        $oItem = Comment::find($id);

        $oItem->approve();

        $oArticle = $oItem->commentable;

        $oArticle->statistic()->update([
            'comments' => $oArticle->approvedComments()->count(),
        ]);

        return responseCommon()->success([], 'Комментарий успешно одобрен.');
    }

    /**
     * @param Request $request
     * @param $id
     * @return array
     */
    public function disapproveComment(Request $request, $id)
    {
        $oItem = Comment::find($id);

        $oItem->update([
            'approved' => 0,
        ]);

        $oArticle = $oItem->commentable;

        $oArticle->statistic()->update([
            'comments' => $oArticle->approvedComments()->count(),
        ]);

        return responseCommon()->success([], 'Комментарий успешно заблокирован.');
    }

    public function deleteComment(Request $request, $id)
    {
        $oItem = Comment::find($id);

        $oArticle = $oItem->commentable;

        $oArticle->statistic()->update([
            'comments' => $oArticle->approvedComments()->count(),
        ]);

        $oItem->delete();

        return responseCommon()->success([], 'Комментарий успешно удален.');
    }

    /**
     * @param Request $request
     * @param $id
     * @return array
     */
    public function updateArticleSeo(Request $request, $id)
    {
        $oItem = Article::find($id);

        $oItem->update([
            'name' => $request->get('name'),
            'keywords' => $request->get('keywords'),
        ]);

        return responseCommon()->success([], 'Данные успешно обновлены.');
    }

    /**
     * @param Request $request
     * @param $id
     * @return array
     * @throws \Throwable
     */
    public function editComment(Request $request, $id)
    {
        $oItem = Comment::find($id);

        $oItem->update([
            'comment' => $request->get('comment')
        ]);

        return responseCommon()->success([
            'view' => view('admin.content.article.components.modals.comments.comment', [
                'oItem' => $oItem
            ])->render(),
        ], 'Комментарий успешно изменен.');
    }

}
