<?php

namespace App\Cmf\Project\Article;

use App\Cmf\Core\SettingsTrait;

trait ArticleSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Статьи',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'article',
            ],
        ],
        'icon' => 'fa fa-code-fork',
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'article';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'article';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = ['article', 'article-active'];

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = false;

    /**
     * Defaults values for store method
     *
     * @var array
     */
    private $defaults = [
        'store' => [
            'status' => 1,
        ],
    ];

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'title' => 'Заголовок',
        'image' => 'Изображение',
        'file' => 'Файл',
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    private $image = [
        'key' => 'article',
        'with_main' => true,
        'unique' => false,
        'filters' => [
            'square' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'dimension' => '1:1'
                ]
            ],
            'xs' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'size' => '180'
                ]
            ],
            'sm' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'size' => '540'
                ]
            ],
            'md' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'size' => '720'
                ]
            ],
            'lg' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'size' => '960'
                ]
            ]
        ]
    ];

    public function getFilters()
    {
        $filters = $this->image['filters'];
        //unset($filters['square']);
        return $filters;
    }

    /**
     * Default files settings in model
     *
     * @var array
     */
    private $file = [
        'key' => 'article',
        'unique' => false,
    ];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            'title' => 'required|max:100',
        ],
        'update' => [
            'title' => 'required|max:100',
        ],
        'upload' => [
            'id' => 'required|max:255',
            'images' => 'required|max:15000|mimes:jpg,jpeg,gif,png',
        ],
        'uploadFiles' => [
            'id' => 'required|max:255',
            'files' => 'required|max:10000',
        ],
    ];

}
