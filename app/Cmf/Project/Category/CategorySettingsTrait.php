<?php

namespace App\Cmf\Project\Category;

use App\Cmf\Core\SettingsTrait;

trait CategorySettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Категории',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'GroupVideo',
            ],
        ],
        'icon' => 'fa fa-map'
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'category';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'category';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = ['category'];

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = false;

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'title' => 'Заголовок',
        'text' => 'Текст',
        'image' => 'Изображение',
        'file' => 'Файл',
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    private $image = [
        'key' => 'category',
        'with_main' => true,
        'unique' => false,
        'filters' => [
            'square' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'dimension' => '1:1'
                ]
            ],
            /*
            'banner' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'dimension' => '1:4'
                ]
            ]
            */
        ]
    ];

    /**
     * Default files settings in model
     *
     * @var array
     */
    private $file = [
        'key' => 'category',
        'unique' => false,
    ];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            'title' => 'required|max:255',
        ],
        'update' => [
            //'title' => 'required|max:255',
        ],
        'upload' => [
            'id' => 'required|max:255',
            'images' => 'required|max:5000|mimes:jpg,jpeg,gif,png',
        ],
        'uploadFiles' => [
            'id' => 'required|max:255',
            'files' => 'required|max:10000',
        ],
    ];


    private $columns = ['icon', 'title', 'text', 'created_at'];

}
