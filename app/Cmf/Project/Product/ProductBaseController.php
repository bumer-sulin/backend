<?php

namespace App\Cmf\Project\Product;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\Defaults\TextableTrait;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Image;
use App\Models\Option;
use App\Models\Tag;
use App\Models\Type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cmf\Core\MainController;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use App\Cmf\Core\ControllerCrmTrait;
use App\Cmf\Core\SeoProviderTrait;


use App\Models\Product;

class ProductBaseController extends MainController
{
    use ProductSettingsTrait, ControllerCrmTrait, SeoProviderTrait;
    use ImageableTrait;
    use ProductCustomTrait;
    use TextableTrait;

    /**
     * Cache model for all items
     *
     * @var mixed|null
     */
    private $cacheModel = null;

    /**
     * Seo Provider
     * @var null
     */
    private $seo = ProductSeoProvider::class;

    /**
     * Class model
     * @var null
     */
    private $class = Product::class;

    /**
     * Pagination url
     * @var string
     */
    private $tableUrl;

    /**
     * Table limit item
     * @var int
     */
    private $tableLimit = 10;

    /**
     * Default order columns
     * @var string
     */
    private $order = 'created_at.desc';

    /**
     * Default query ['status' => 1, ]
     */
    private $aQuery = [
        'id' => [
            'operator' => '=',
            'value' => '',
            'request' => true
        ],
    ];

    /**
     * Order columns
     * @var array
     */
    private $orderBy = [];

    /**
     * Views array, set in @see \App\Cmf\Core\ControllerCrmTrait::setViews()
     *
     * @var array
     */
    protected $views = [];

    public function __construct()
    {
        /**
         * Default session query
         */
        $this->aQuery = $this->setQuery($this->session, $this->aQuery);

        /**
         * Route for pagination
         */
        $this->tableUrl = route($this->view.'.view.post');

        /**
         * Default class model
         */
        $this->cacheModel = $this->getCacheModel($this->class, $this->cache);

        /**
         * Order columns
         */
        $this->orderBy = $this->setOrderBy($this->order, $this->session);

        /**
         * Set views
         */
        $this->views = $this->setViews();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->seo($this->seo, __FUNCTION__);

        $oItems = $this->paginate($this->class);

        return view($this->views[__FUNCTION__], [
            'oCategories' => $this->categories(),
            'oTypes' => $this->types(),
            'oItems' => $oItems
        ]);
    }

    /**
     * Default query items
     *
     * @param $oModel
     * @param null $type
     * @param array $aColumns
     * @return mixed
     */
    public function paginate($oModel, $type = null, $aColumns = [])
    {
        $oItems = $oModel::with('type', 'category')->orderBy($this->orderBy['column'], $this->orderBy['type']);
        if (!is_null($type) && $type === 'search') {
            foreach($aColumns as $key => $value) {
                $oItems = $oItems->where($key, 'like', '%'.$value.'%');
            }
        }
        if (!empty($this->aQuery)) {
            foreach($this->aQuery as $key => $value) {
                if ($key === 'category_id') {
                    $oCategory = Category::find($value);
                    if ($oCategory->hasChildren()) {
                        $oSubCategories = Category::where('parent_id', $oCategory->id)->orderBy('id', 'asc')->get()->keyBy('id');
                        $oSubCategories->merge([$oCategory->id => $oCategory]);
                        $aCategoriesId = $oSubCategories->pluck('id')->toArray();
                        $aCategoriesId[] = $oCategory->id;
                        $oItems = $oItems->whereIn('category_id', $aCategoriesId);
                    } else {
                        $oItems = $oItems->where($key, $value);
                    }
                }
                if ($key === 'type_id') {
                    $oCategory = Type::find($value);
                    if ($oCategory->hasChildren()) {
                        $oSubCategories = Type::where('parent_id', $oCategory->id)->orderBy('id', 'asc')->get()->keyBy('id');
                        $oSubCategories->merge([$oCategory->id => $oCategory]);
                        $aCategoriesId = $oSubCategories->pluck('id')->toArray();
                        $aCategoriesId[] = $oCategory->id;
                        $oItems = $oItems->whereIn('type_id', $aCategoriesId);
                    } else {
                        $oItems = $oItems->where($key, $value);
                    }
                }
                if (is_array($value)) {
                    if (isset($value['request'])) {
                        if (Input::has('id')) {
                            $value['value'] = Input::get('id');
                            $oItems = $oItems->where($key, $value['operator'], $value['value']);
                        }
                    }
                }
            }
        }
        return $oItems->paginate($this->tableLimit)->withPath($this->tableUrl);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->seo($this->seo, __FUNCTION__);

        return view($this->views[__FUNCTION__]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $this->validation($request, $this->rules, $this->attributes, __FUNCTION__, $this->validateMessages);
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {

            $model = $this->class;
            $data = $this->getSaveColumns($request, $this->cache);
            $data = $this->withDefaults($data, $this->defaults[__FUNCTION__]);
            if (!is_null($data)) {
                $model::create($data);
            } else {
                return $this->jsonError(trans('form.errors.cache'));
            }
            $this->afterChange($this->cache);
            return $this->success([
                'toastr' => $this->toastr[__FUNCTION__]
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $this->seo($this->seo, 'show', $oItem);
        return view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        return view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = $this->validation($request, $this->rules, $this->attributes, __FUNCTION__);
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {
            $model = $this->class;
            $data = $this->getSaveColumns($request, $this->cache);
            if (!is_null($data)) {
                $model::find($id)->update($data);
            } else {
                return $this->jsonError(trans('form.errors.cache'));
            }
            $this->afterChange($this->cache);
            return $this->success([
                'toastr' => $this->toastr[__FUNCTION__]
            ]);
        }
    }

    /**
     * Update the status in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request, $id)
    {
        $model = $this->class;
        $oModel = $model::find($id);
        if (intval($request->get('status')) === 1) {
            $check = $this->checkForStatus($oModel);
            if ($check['success']) {
                $oModel->update([
                    'status' => $request->get('status'),
                ]);
            } else {
                return $this->success([
                    'toastr' => $check['message']
                ]);
            }
        } else {
            $oModel->update([
                'status' => $request->get('status'),
            ]);
        }
        $this->afterChange($this->cache);
        return $this->success([
            //'toastr' => (new Toastr(trans('toastr')['status'][$request->get('status')]['text']))->success()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = $this->class;
        $model::destroy($id);
        $this->afterChange($this->cache);
        return $this->success([
            'toastr' => $this->toastr[__FUNCTION__]
        ]);
    }

    /**
     * Генерация после добавление/изменения/удаления
     *
     * @param Request $request
     * @return array
     */
    public function view(Request $request)
    {
        $oItems = $this->paginate($this->class);

        return $this->table($oItems);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function search(Request $request)
    {
        $oItems = $this->paginate($this->class, 'search', [
            'title' => $request->get('search'),
            'status' => $request->get('status'),
        ]);
        return $this->table($oItems);
    }



    /**
     * Обновление таблицы после добавление/изменения/удаления
     *
     * @param $oItems
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function table($oItems)
    {
        return $this->success([
            'view' => view($this->views[__FUNCTION__], [
                'oItems' => $oItems
            ])->render(),
            'count' => $oItems->total()
        ]);
    }

    /**
     * Модальное окно для добавления
     *
     * @param Request $request
     * @return array
     * @throws \Throwable
     * @throws \Exception
     */
    public function createModal(Request $request)
    {
        $view = view($this->views[__FUNCTION__], [
            'oCategories' => Category::orderBy('title', 'asc')->get(),
            'oTypes' => Type::orderBy('title', 'asc')->get(),
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * Модальное окно для изменений
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function editModal($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view($this->views[__FUNCTION__], [
            'oItem' => $oItem,
            'oCategories' => Category::orderBy('title', 'asc')->get(),
            'oTypes' => Type::orderBy('title', 'asc')->get(),
            'oOptions' => Option::where('type', 1)->get(),
            'oBrands' => Brand::orderBy('title', 'asc')->get(),
            'oTags' => Tag::all()
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }



    /**
     * Get images in modal
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function image($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * Get images in modal
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function files($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * Set sort in session
     *
     * @param Request $request
     * @return array
     */
    public function sort(Request $request)
    {
        if ($request->exists('sort')) {
            Session::put($this->session.'.sort.column', $request->get('sort'));
        }
        if ($request->exists('type')) {
            if (Session::has($this->session.'.sort.type') && Session::get($this->session.'.sort.type') === 'desc') {
                Session::put($this->session.'.sort.type', 'asc');
            } else {
                Session::put($this->session.'.sort.type', $request->get('type'));
            }
        }
        return $this->success();
    }

    /**
     * Ser where query in session
     *
     * @param Request $request
     * @return array
     */
    public function query(Request $request)
    {
        $this->setQuerySearch($request, ['status', 'category_id', 'type_id']);
        return $this->success();
    }

}