<?php

namespace App\Cmf\Project\Product;


use App\Models\Option;
use App\Models\OptionParameter;
use App\Models\Product;
use App\Models\ProductOptions;
use App\Models\ProductOptionValues;
use App\Models\ProductPrices;
use Illuminate\Http\Request;

trait ProductCustomTrait
{

    public function updateProductPrice(Request $request, $id)
    {
        $oProduct = Product::find($id);
        $oProductPrice = ProductPrices::where('product_id', $id)->first();

        $aPrice = $request->get('price');
        if (isset($aPrice['amount'])) {
            if (is_null($oProductPrice)) {
                $oProductPrice = ProductPrices::create([
                    'product_id' => $id,
                    'amount' => $aPrice['amount'],
                    'old' => $aPrice['old'],
                ]);
            } else {
                $oProductPrice->update([
                    'amount' => $aPrice['amount'],
                    'old' => $aPrice['old'],
                ]);
            }
        }

        if ($request->exists('tags')) {
            $aTags = $request->get('tags');
            $aId = [];
            foreach ($aTags as $key => $aTag) {
                $aId[] = $aTag;
                $oProduct->tags()->sync([
                    $aTag => [
                        'tag_id' => $aTag,
                    ]
                ], false);
            }
            $aIssetParameters = $oProduct->tags->pluck('id');
            foreach ($aIssetParameters as $id) {
                if (!in_array($id, $aId)) {
                    $oProduct->tags()->detach($id);
                }
            }
        } else {
            $oProduct->tags()->detach();
        }


        if ($request->exists('brand_id') && intval($request->brand_id) !== 0) {
            $oProduct->brands()->sync($request->get('brand_id'));
        } else {
            $oProduct->brands()->detach();
        }

        return $this->success([
            'toastr' => [
                'title' => 'Успех',
                'text' => 'Данные успешно обновлены',
                'type' => 'success'
            ]
        ]);
    }

    public function updateProductParameters(Request $request, $id)
    {
        $oProduct = Product::find($id);

        if ($request->exists('options')) {
            $aOptions = $request->get('options');
            $aId = [];
            foreach ($aOptions as $key => $aOption) {
                $oOption = Option::where('key', $key)->first();
                if (!is_null($oOption)) {
                    foreach($aOption as $value) {
                        $oParameter = OptionParameter::find($value);
                        $aId[] = $value;
                        $oProduct->values()->sync([
                            $value => [
                                'parameter_id' => $value,
                                'priority' => $oParameter->priority,
                                'option_id' => $oOption->id
                            ]
                        ], false);
                    }
                    $aIssetParameters = $oProduct->values->where('option_id', $oOption->id)->pluck('id');
                    foreach ($aIssetParameters as $id) {
                        if (!in_array($id, $aId)) {
                            $oProduct->values()->detach($id);
                        }
                    }
                }
            }
            $aIssetParameters = $oProduct->values->pluck('id');
            foreach ($aIssetParameters as $id) {
                if (!in_array($id, $aId)) {
                    $oProduct->values()->detach($id);
                }
            }
        } else {
            $oProduct->values()->detach();
        }
        return $this->success([
            'toastr' => [
                'title' => 'Успех',
                'text' => 'Данные успешно обновлены',
                'type' => 'success'
            ]
        ]);
    }

    public function checkForStatus($oProduct)
    {
        $message = null;
        
        if (is_null($oProduct->price)) {
            $message = $this->setValidateMessage($message, '- цены');
        }
        if (count($oProduct->images) === 0) {
            $message = $this->setValidateMessage($message, '- изображений');
        }
        return !is_null($message) ?
            $this->validateMessage(false, $message) :
            $this->validateMessage(true);
    }

    private function setValidateMessage($message, $text)
    {
        if (is_null($message)) {
            $message = 'У товара нет:';
        }
        if (!is_null($message)) {
            $message = !is_null($message) ? $message.'<br>'.$text : $text;
        }
        return $message;
    }



    private function validateMessage($success, $message = '')
    {
        return $success ? [
            'success' => $success
        ] : [
            'success' => $success,
            'message' => [
                'title' => 'Ошибка',
                'text' => $message,
                'type' => 'error'
            ]
        ];
    }

}