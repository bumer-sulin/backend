<?php

namespace App\Cmf\Project\Info;


use App\Models\Article;
use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

trait InfoCustomTrait
{
    public function getAudio(Request $request, $id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view($this->theme.'.content.'.$this->view.'.components.modals.audio', [
            'oItem' => $oItem,
            'oItems' => $oItem->audio
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     * @throws \Throwable
     */
    public function getLinkImagesModal(Request $request, $id)
    {
        $image = Image::find($request->get('image_id'));
        return $this->success([
            'view' => view('admin.content.article.components.modals.images', [
                'image' => $image,
                'model' => $request->get('model')
            ])->render()
        ]);
    }
}
