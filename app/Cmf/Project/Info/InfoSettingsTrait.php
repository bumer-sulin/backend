<?php

namespace App\Cmf\Project\Info;

use App\Cmf\Core\SettingsTrait;

trait InfoSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Информация',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'setting',
            ],
        ],
        'icon' => 'fa fa-map'
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'info';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'info';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = ['info'];

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = false;

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'title' => 'Заголовок',
        'text' => 'Текст',
        'image' => 'Изображение',
        'file' => 'Файл',
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    private $image = [
        'key' => 'setting',
        'with_main' => true,
        'unique' => false,
        'clear_cache' => 'site',
        'filters' => [
            'square' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'dimension' => '1:1'
                ]
            ]
        ]
    ];

    /**
     * Defaults values for store method
     *
     * @var array
     */
    private $defaults = [
        'store' => [
            'status' => 0,
            'text' => 'replace'
        ],
        'update' => [
            'text' => 'summernote'
        ],
    ];

    /**
     * Default files settings in model
     *
     * @var array
     */
    private $file = [
        'key' => 'album',
        'unique' => false,
    ];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            'key' => 'required|max:255',
            'title' => 'required|max:255',
            //'value' => 'required_unless:type,4',
            'type' => 'required',
        ],
        'update' => [
            'key' => 'required|max:255',
            'title' => 'required|max:255',
            'value' => 'required_unless:type,4',
            'type' => 'required',
        ],
        'upload' => [
            'id' => 'required|max:255',
            'images' => 'required|max:5000|mimes:jpg,jpeg,gif,png',
        ],
    ];


    private $columns = ['icon', 'title', 'text', 'created_at'];

}
