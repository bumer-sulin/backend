<?php

namespace App\Cmf\Project\File;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

trait FileCustomTrait
{
    public function getAudio(Request $request, $id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view($this->theme.'.content.'.$this->view.'.components.modals.audio', [
            'oItem' => $oItem,
            'oItems' => $oItem->audio
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }
}