<?php

namespace App\Cmf\Project\File;

use App\Cmf\Core\SettingsTrait;

trait FileSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Файлы',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'file',
            ],
        ],
        'icon' => 'fa fa-map'
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'file';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'file';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = ['file'];

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = false;

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'title' => 'Заголовок',
        'text' => 'Текст',
        'image' => 'Изображение',
        'file' => 'Файл',
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    private $image = [
        'key' => 'file',
        'with_main' => true,
        'unique' => false,
        'filters' => [
            'square' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'dimension' => '1:1'
                ]
            ]
        ]
    ];

    /**
     * Defaults values for store method
     *
     * @var array
     */
    private $defaults = [
        'store' => [
            'status' => 0,
            'text' => 'replace'
        ],
        'update' => [
            'text' => 'summernote'
        ],
    ];

    /**
     * Default files settings in model
     *
     * @var array
     */
    private $file = [
        'key' => 'file',
        'unique' => false,
    ];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            'title' => 'required|max:255',
        ],
        'update' => [
            //'title' => 'required|max:255',
        ],
        'upload' => [
            'id' => 'required|max:255',
            'images' => 'required|max:5000|mimes:jpg,jpeg,gif,png',
        ],
        'uploadFiles' => [
            'id' => 'required|max:255',
            'files' => 'required|max:10000',
        ],
    ];


    private $columns = ['icon', 'title', 'text', 'created_at'];

}
