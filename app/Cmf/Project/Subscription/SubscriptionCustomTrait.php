<?php

namespace App\Cmf\Project\Subscription;

use App\Models\Promotion;
use App\Models\Subscription;
use Illuminate\Http\Request;

trait SubscriptionCustomTrait
{
    public function saveCategories(Request $request, $id)
    {
        $oItem = Subscription::find($id);

        if ($request->exists('categories')) {
            $oItem->categories()->sync($request->get('categories'));
        } else {
            $oItem->categories()->detach();
        }

        return responseCommon()->success([], 'Данные успешно обновлены');
    }
}
