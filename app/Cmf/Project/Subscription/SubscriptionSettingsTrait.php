<?php

namespace App\Cmf\Project\Subscription;

use App\Cmf\Core\SettingsTrait;

trait SubscriptionSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Подписки',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'subscription',
            ],
        ],
        'icon' => 'fa fa-code-fork'
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'subscription';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'subscription';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = ['subscription'];

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = false;

    /**
     * Defaults values for store method
     *
     * @var array
     */
    private $defaults = [
        'store' => [
            'status' => 1,
        ],
    ];

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'title' => 'Заголовок',
        'type' => 'Тип',
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    private $image = [];

    /**
     * Default files settings in model
     *
     * @var array
     */
    private $file = [];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            //'title' => 'required|max:255',
        ],
        'update' => [
            //'title' => 'required|max:255',
        ],
    ];
}
