<?php

namespace App\Cmf\Project\Social;

use App\Cmf\Core\SettingsTrait;

trait SocialSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Социальные сети',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'social',
            ],
        ],
        'icon' => 'fa fa-code-fork'
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'social';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'social';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = ['social'];

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = false;

    /**
     * Defaults values for store method
     *
     * @var array
     */
    private $defaults = [
        'store' => [
            'status' => 1,
        ],
    ];

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'title' => 'Заголовок',
        'type' => 'Тип',
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    private $image = [];

    /**
     * Default files settings in model
     *
     * @var array
     */
    private $file = [];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            'title' => 'required|max:255',
        ],
        'update' => [
            'title' => 'required|max:255',
        ],
    ];

    public function getRules($method)
    {
        $return = $this->rules[$method];

    }

}
