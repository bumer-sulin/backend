<?php

namespace App\Cmf\Project\User;

use App\Cmf\Core\MainController;
use App\Events\ChangeCacheEvent;
use App\Models\Image;
use App\Registries\MemberRegistry;
use App\Services\Toastr\Toastr;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Roles\EloquentRole;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Cmf\Core\ControllerCrmTrait;
use App\Cmf\Core\SeoProviderTrait;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

use App\Models\User;
use App\Cmf\Core\Defaults\ImageableTrait;

class UserBaseController extends MainController
{
    use UserSettingsTrait, ControllerCrmTrait, SeoProviderTrait;
    use ImageableTrait;//{afterChangeImage as protected afterChangeImage;}

    use UserCustomTrait;

    private $member;

    /**
     * Cache model for all items
     *
     * @var mixed|null
     */
    private $cacheModel = null;

    /**
     * Seo Provider
     * @var null
     */
    private $seo = UserSeoProvider::class;

    /**
     * Class model
     * @var null
     */
    private $class = User::class;

    /**
     * Pagination url
     * @var string
     */
    private $tableUrl;

    /**
     * Table limit item
     * @var int
     */
    private $tableLimit = 10;

    /**
     * Default order columns
     * @var string
     */
    private $order = 'created_at.desc';

    /**
     * Default query ['status' => 1, ]
     */
    private $aQuery = [];

    /**
     * Order columns
     * @var array
     */
    private $orderBy = [];

    /**
     * Views array, set in @see \App\Cmf\Core\ControllerCrmTrait::setViews()
     *
     * @var array
     */
    private $views = [];

    private $sentinel = true;

    private $with = ['images', 'activation', 'roles'];

    public function __construct()
    {
        /**
         * Default session query
         */
        $this->aQuery = $this->setQuery($this->session, $this->aQuery);

        /**
         * Route for pagination
         */
        $this->tableUrl = route($this->view.'.view.post');

        /**
         * Default class model
         */
        $this->cacheModel = $this->getCacheModel($this->class, $this->cache);

        /**
         * Order columns
         */
        $this->orderBy = $this->setOrderBy($this->order, $this->session);

        $this->member = MemberRegistry::getInstance();

        $this->views = $this->setViews();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->seo($this->seo, __FUNCTION__);

        if ($request->has('id')) {
            $this->setQuerySearch($request, ['id']);
            $this->aQuery['id'] = $request->get('id');
        }

        $oItems = $this->paginate($this->class);

        Session::put($this->session . '.count.get', $oItems->total());
        Session::put($this->session . '.count.total', $this->class::count());

        return view($this->views[__FUNCTION__], [
            'oItems' => $oItems
        ]);
    }

    /**
     * Default query items
     *
     * @param $oModel
     * @param null $type
     * @param array $aColumns
     * @return mixed
     */
    public function paginate($oModel, $type = null, $aColumns = [])
    {
        $oItems = $oModel::with($this->with)->orderBy($this->orderBy['column'], $this->orderBy['type']);
        if (!is_null($type) && $type === 'search') {
            foreach($aColumns as $key => $value) {
                $oItems = $oItems->where($key, 'like', '%'.$value.'%');
            }
        }
        if (!empty($this->aQuery)) {
            foreach($this->aQuery as $key => $value) {
                $oItems = $oItems->where($key, $value);
            }
        }
        return $oItems->paginate($this->tableLimit)->withPath($this->tableUrl);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->seo($this->seo, __FUNCTION__);

        return view($this->views[__FUNCTION__]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $this->validation($request, $this->rules, $this->attributes, __FUNCTION__);
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {
            if ($this->sentinel) {
                $credentials = [
                    'first_name' => $request->get('first_name'),
                    'email'    => $request->get('email'),
                    'password' => $request->get('password'),
                ];
                $oUser = Sentinel::register($credentials);

                $oRole = Sentinel::findRoleById($request->role_id);
                $oRole->users()->attach($oUser);


                $oActivation = Activation::create($oUser);
                $oActivation->code = str_random(48);
                $oActivation->save();

                Activation::complete($oUser, $oActivation->code);

                $credential = [
                    'email' => $oUser->email,
                    'password' => $oUser->password,
                ];

                //$oUser = Sentinel::authenticate($credential, false);

            } else {
                $oUser = User::create([
                    'name' => $request->get('name'),
                    'email' => $request->get('email'),
                    'password' => $request->get('password'),
                ]);
            }
            $this->afterChange($this->cache, [
                'user_'.$oUser->id
            ]);
            return $this->success([
                'toastr' => $this->toastr[__FUNCTION__]
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $this->seo($this->seo, 'show', $oItem);
        return view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        return view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = $this->validation($request, $this->rules, $this->attributes, __FUNCTION__);
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {
            if ($this->sentinel) {
                $oUser = Sentinel::findById($id);
                $secondValidate = $this->validateByData($request, $oUser);

                if (!$secondValidate['success']) {
                    return $this->jsonError($secondValidate['message'], 'email');
                }

                $credentials = [
                    'first_name' => $request->get('first_name'),
                    'last_name' => $request->get('last_name'),
                    'email' => $request->get('email'),
                ];
                Sentinel::update($oUser, $credentials);

                if (isset($request->role_id)) {
                    $oRole = Sentinel::findRoleById($request->role_id);
                    if ($oRole->id !== $oUser->role->id) {
                        $oOldRole = Sentinel::findRoleById($oUser->role->id);
                        $oOldRole->users()->detach($oUser);
                        $oRole->users()->attach($oUser);
                    }
                }

                if (isset($request->password) && !is_null($request->password)) {
                    $old = [
                        'email' => $request->email,
                        'password' => $request->old_password,
                    ];
                    if (!Sentinel::authenticate($old)) {
                        return $this->jsonError('Неверный пароль', 'old_password');
                    }
                    $credentials = [
                        'password' => $request->get('password'),
                    ];
                    Sentinel::update($oUser, $credentials);
                }
            } else {
                $oUser = User::find($id);
                $secondValidate = $this->validateByData($request, $oUser);
                if (!$secondValidate['success']) {
                    return $this->jsonError($secondValidate['message'], 'email');
                }

                $oUser->update([
                    'first_name' => $request->get('first_name'),
                    'email' => $request->get('email'),
                ]);

                if (isset($request->password) && !is_null($request->password)) {
                    $oUser->update([
                        'password' => bcrypt($request->get('password')),
                    ]);
                }
            }
            $this->afterChange($this->cache, [
                'user_'.$oUser->id
            ]);
            return $this->success([
                'toastr' => $this->toastr[__FUNCTION__]
            ]);
        }
    }

    public function validateByData(Request $request, $oModel)
    {
        $oUsers = User::where('email', $request->get('email'))->get();
        foreach($oUsers as $oUser) {
            if ($oModel->id !== $oUser->id) {
                return $this->error([
                    'message' => 'Такое значение поля email уже существует.'
                ]);
            }
        }
        return $this->success();
    }

    /**
     * Update the status in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return array
     */
    public function status(Request $request, $id)
    {
        $model = $this->class;

        $oModel = $model::find($id);
        if (intval($oModel->id) === intval($this->member->get('user.id')) && intval($request->get('status')) === 0) {
            return $this->error([
                'toastr' => (new Toastr('Нельзя заблокировать этого пользователя'))->error()
            ]);
        }


        $oModel->update([
            'status' => $request->get('status'),
        ]);
        $message = $request->get('status') ? $this->toastr['status'][1] : $this->toastr['status'][0];
        return $this->success([
            'toastr' => $message
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = $this->class;
        $model = $model::find($id);;

        $this->beforeDeleteForImages($model);
        $model::destroy($id);
        $this->afterChange($this->cache);
        return $this->success([
            'toastr' => $this->toastr[__FUNCTION__],
            'total' => $this->class::count(),
        ]);
    }

    /**
     * Генерация после добавление/изменения/удаления
     *
     * @param Request $request
     * @return array
     */
    public function view(Request $request)
    {
        $oItems = $this->paginate($this->class);

        return $this->table($oItems);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function search(Request $request)
    {
        if (!empty($request->get('search'))) {
            $oItems = $this->paginate($this->class, 'search', [
                'name' => $request->get('search')
            ]);
        } else {
            $oItems = $this->paginate($this->class);
        }
        return $this->table($oItems);
    }



    /**
     * Обновление таблицы после добавление/изменения/удаления
     *
     * @param $oItems
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function table($oItems)
    {
        return $this->success([
            'view' => view($this->views[__FUNCTION__], [
                'oItems' => $oItems
            ])->render(),
            'count' => $oItems->total()
        ]);
    }

    /**
     * Модальное окно для добавления
     *
     * @param Request $request
     * @return array
     * @throws \Throwable
     * @throws \Exception
     */
    public function createModal(Request $request)
    {
        $oRoles = EloquentRole::all()->reject(function ($item) {
            return in_array($item->slug, ['developer']);
        });
        $view = view($this->views[__FUNCTION__], [
            'oRoles' => $oRoles,
        ])->render();

        return $this->success([
            'view' => $view,
        ]);
    }

    /**
     * Модальное окно для изменений
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function editModal($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);

        $oRoles = EloquentRole::all()->reject(function ($item) {
            return in_array($item->slug, ['developer']);
        });

        $view = view($this->views[__FUNCTION__], [
            'oItem' => $oItem,
            'oRoles' => $oRoles,
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }



    /**
     * Get images in modal
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function image($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view($this->views[__FUNCTION__], [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }


    /**
     * Set sort in session
     *
     * @param Request $request
     * @return array
     */
    public function sort(Request $request)
    {
        if ($request->exists('sort')) {
            Session::put($this->session.'.sort.column', $request->get('sort'));
        }
        if ($request->exists('type')) {
            if (Session::has($this->session.'.sort.type') && Session::get($this->session.'.sort.type') === 'desc') {
                Session::put($this->session.'.sort.type', 'asc');
            } else {
                Session::put($this->session.'.sort.type', $request->get('type'));
            }
        }
        return $this->success();
    }

    /**
     * Ser where query in session
     *
     * @param Request $request
     * @return array
     */
    public function query(Request $request)
    {
        $this->setQuerySearch($request, ['status', 'id']);
        return $this->success();
    }


    /**
     * После изменения изображение, перебивает с трейта
     *
     * @param $imageSettings
     * @param $oItem
     */
    private function afterChangeImage($imageSettings, $oItem)
    {
        event(new ChangeCacheEvent('user_'.$oItem->id));
    }
}
