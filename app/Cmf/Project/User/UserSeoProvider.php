<?php

namespace App\Cmf\Project\User;

use KekoApp\LaravelMetaTags\Facades\MetaTag;

class UserSeoProvider
{
    public function index()
    {
        MetaTag::set('title', 'User Index');
    }

    public function create()
    {
        MetaTag::set('title', 'User Create');
    }

    public function show($oModel)
    {
        MetaTag::set('title', $oModel->first_name);
    }
}
