<?php

namespace App\Cmf\Project\User;


use App\Events\ChangeCacheEvent;
use App\Models\Social;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

trait UserCustomTrait
{
    /**
     * Скрыть/показать сайд бар для адмики, запрос в
     * resources\assets\js\admin\template\app.js
     *
     * @param Request $request
     * @return mixed
     */
    public function saveSidebarToggle(Request $request)
    {
        if ($request->exists('toggle') && $request->get('toggle') === 'true') {
            Session::put('sidebar-toggle', $request->get('toggle'));
        } else {
            Session::remove('sidebar-toggle');
        }
        return $this->success();
    }



    /**
     * Модальное окно для изменений
     *
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function getModalCommand(Request $request)
    {
        $view = $this->theme.'.content.'.$this->view;

        $view = view($view.'.components.modals.command', [

        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }


    public function updateUserSocials(Request $request, $id)
    {
        $oUser = User::find($id);

        if ($request->exists('socials') && !empty($request->get('socials'))) {
            $socials = $request->get('socials');
            foreach ($socials as $id => $value) {
                $oSocial = Social::find($id);
                if (!is_null($oSocial)) {
                    $oSocial->update([
                        'link' => $value['link'],
                        'status' => $value['status'],
                    ]);
                }
            }
        } else {

        }


        if($oUser->role->slug === 'admin') {
            event(new ChangeCacheEvent('socials'));
        }

        return $this->success([
            'toastr' => [
                'title' => 'Успех',
                'text' => 'Данные успешно обновлены',
                'type' => 'success'
            ]
        ]);
    }
}