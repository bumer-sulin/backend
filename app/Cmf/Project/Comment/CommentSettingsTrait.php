<?php

namespace App\Cmf\Project\Comment;

use App\Cmf\Core\SettingsTrait;

trait CommentSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Комментарии',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'comment',
            ],
        ],
        'icon' => 'fa fa-code-fork'
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'comment';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'comment';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = ['comment', 'comments-not-approved'];

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = false;

    /**
     * Defaults values for store method
     *
     * @var array
     */
    private $defaults = [
//        'store' => [
//            'status' => 1,
//        ],
    ];

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'comment' => 'Текст',
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    private $image = [];

    /**
     * Default files settings in model
     *
     * @var array
     */
    private $file = [];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            'comment' => 'required',
        ],
        'update' => [
            'comment' => 'required',
        ],
    ];

}
