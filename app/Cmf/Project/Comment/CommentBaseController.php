<?php

namespace App\Cmf\Project\Comment;

use App\Events\ChangeCacheEvent;
use App\Models\Comment;
use App\Models\Image;
use App\Models\Social;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cmf\Core\MainController;
use Illuminate\Support\Facades\Session;
use App\Cmf\Core\ControllerCrmTrait;
use App\Cmf\Core\SeoProviderTrait;


use App\Models\Tag;

class CommentBaseController extends MainController
{
    use CommentSettingsTrait, ControllerCrmTrait, SeoProviderTrait;

    /**
     * Cache model for all items
     *
     * @var mixed|null
     */
    private $cacheModel = null;

    /**
     * Seo Provider
     * @var null
     */
    private $seo = null;

    /**
     * Class model
     * @var null
     */
    private $class = Comment::class;

    /**
     * Pagination url
     * @var string
     */
    private $tableUrl;

    /**
     * Table limit item
     * @var int
     */
    private $tableLimit = 10;

    /**
     * Default order columns
     * @var string
     */
    private $order = 'created_at.desc';

    /**
     * Default query ['status' => 1, ]
     */
    private $aQuery = [];

    /**
     * Order columns
     * @var array
     */
    private $orderBy = [];

    /**
     * Views array, set in @see \App\Cmf\Core\ControllerCrmTrait::setViews()
     *
     * @var array
     */
    protected $views = [];


    protected $with = ['commented', 'commentable', 'loveUsers'];

    public function __construct()
    {
        /**
         * Default session query
         */
        $this->aQuery = $this->setQuery($this->session, $this->aQuery);

        /**
         * Route for pagination
         */
        $this->tableUrl = route($this->view . '.view.post');

        /**
         * Default class model
         */
        $this->cacheModel = $this->getCacheModel($this->class, $this->cache);

        /**
         * Order columns
         */
        $this->orderBy = $this->setOrderBy($this->order, $this->session);

        /**
         * Set views
         */
        $this->views = $this->setViews();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->seo($this->seo, __FUNCTION__);

        if ($request->has('approved')) {
            $this->setQuerySearch($request, ['approved']);
            $this->aQuery['approved'] = $request->get('approved');
        }

        $oItems = $this->paginate($this->class);

        Session::put($this->session . '.count.get', $oItems->total());
        Session::put($this->session . '.count.total', $this->class::count());

        return view($this->views[__FUNCTION__], [
            'oItems' => $oItems,
        ]);
    }

    /**
     * Default query items
     *
     * @param $oModel
     * @param null $type
     * @param array $aColumns
     * @return mixed
     */
    public function paginate($oModel, $type = null, $aColumns = [])
    {
        $oItems = $oModel::with($this->with)->orderBy($this->orderBy['column'], $this->orderBy['type']);
        if (!is_null($type) && $type === 'search') {
            foreach ($aColumns as $key => $value) {
                $oItems = $oItems->where($key, 'like', '%' . $value . '%');
            }
        }
        if (!empty($this->aQuery)) {
            foreach ($this->aQuery as $key => $value) {
                $oItems = $oItems->where($key, $value);
            }
        }
        return $oItems->paginate($this->tableLimit)->withPath($this->tableUrl);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->seo($this->seo, __FUNCTION__);

        return view($this->views[__FUNCTION__]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $this->validation($request, $this->rules, $this->attributes, __FUNCTION__);
        if ($validation->fails()) {
            return response()->json($validation->getMessageBag(), 422);
        } else {

            $model = $this->class;
            $data = $this->getSaveColumns($request, $this->cache);
            $data = $this->withDefaults($data, $this->defaults[__FUNCTION__]);
            if (!is_null($data)) {
                $model::create($data);
            } else {
                return $this->jsonError(trans('form.errors.cache'));
            }
            $this->afterChange($this->cache);
            return $this->success([
                'toastr' => $this->toastr[__FUNCTION__],
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $this->seo($this->seo, 'show', $oItem);
        return view($this->views[__FUNCTION__], [
            'oItem' => $oItem,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        return view($this->views[__FUNCTION__], [
            'oItem' => $oItem,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = $this->validation($request, $this->rules, $this->attributes, __FUNCTION__);
        if ($validation->fails()) {
            return response()->json($validation->getMessageBag(), 422);
        } else {
            $model = $this->class;
            $data = $this->getSaveColumns($request, $this->cache);
            if (!is_null($data)) {
                $model::find($id)->update($data);
            } else {
                return $this->jsonError(trans('form.errors.cache'));
            }
            $this->afterChange($this->cache);
            return $this->success([
                'toastr' => $this->toastr[__FUNCTION__],
            ]);
        }
    }

    /**
     * Update the status in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function status(Request $request, $id)
    {
        $model = $this->class;
        $oItem = $model::find($id);
        $oItem->update([
            'approved' => $request->get('status'),
        ]);
        $message = $request->get('status') ? $this->toastr['status'][1] : $this->toastr['status'][0];

        $oArticle = $oItem->commentable;

        $oArticle->commentsUpdateStatistic();

        event(new ChangeCacheEvent('comments-not-approved'));

        return $this->success([
            'toastr' => $message,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $model = $this->class;
        $oItem = $model::find($id);

        $oArticle = $oItem->commentable;

        $oArticle->commentsUpdateStatistic();

        event(new ChangeCacheEvent('comments-not-approved'));

        $model::destroy($id);

        $this->afterChange($this->cache);
        return $this->success([
            'toastr' => $this->toastr[__FUNCTION__],
            'total' => $this->class::count(),
        ]);
    }

    /**
     * Генерация после добавление/изменения/удаления
     *
     * @param Request $request
     * @return array
     */
    public function view(Request $request)
    {
        $oItems = $this->paginate($this->class);

        return $this->table($oItems);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function search(Request $request)
    {
        $oItems = $this->paginate($this->class, 'search', [
            'title' => $request->get('search'),
            'status' => $request->get('status'),
        ]);
        return $this->table($oItems);
    }


    /**
     * Обновление таблицы после добавление/изменения/удаления
     *
     * @param $oItems
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function table($oItems)
    {
        return $this->success([
            'view' => view($this->views[__FUNCTION__], [
                'oItems' => $oItems,
            ])->render(),
            'count' => $oItems->total(),
        ]);
    }

    /**
     * Модальное окно для добавления
     *
     * @param Request $request
     * @return array
     * @throws \Throwable
     * @throws \Exception
     */
    public function createModal(Request $request)
    {
        $view = view($this->views[__FUNCTION__], [

        ])->render();
        return $this->success([
            'view' => $view,
        ]);
    }

    /**
     * Модальное окно для изменений
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function editModal($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view($this->views[__FUNCTION__], [
            'oItem' => $oItem,
        ])->render();
        return $this->success([
            'view' => $view,
        ]);
    }


    /**
     * Get images in modal
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function image($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view($this->views[__FUNCTION__], [
            'oItem' => $oItem,
        ])->render();
        return $this->success([
            'view' => $view,
        ]);
    }

    /**
     * Get images in modal
     *
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function files($id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);
        $view = view($this->views[__FUNCTION__], [
            'oItem' => $oItem,
        ])->render();
        return $this->success([
            'view' => $view,
        ]);
    }

    /**
     * Set sort in session
     *
     * @param Request $request
     * @return array
     */
    public function sort(Request $request)
    {
        if ($request->exists('sort')) {
            Session::put($this->session . '.sort.column', $request->get('sort'));
        }
        if ($request->exists('type')) {
            if (Session::has($this->session . '.sort.type') && Session::get($this->session . '.sort.type') === 'desc') {
                Session::put($this->session . '.sort.type', 'asc');
            } else {
                Session::put($this->session . '.sort.type', $request->get('type'));
            }
        }
        return $this->success();
    }

    /**
     * Ser where query in session
     *
     * @param Request $request
     * @return array
     */
    public function query(Request $request)
    {
        $this->setQuerySearch($request, ['approved']);
        return $this->success();
    }

}
