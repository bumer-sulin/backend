<?php

namespace App\Cmf\Project\Promotion;

use App\Models\Promotion;
use Illuminate\Http\Request;

trait PromotionCustomTrait
{
    public function saveParameters(Request $request, $id)
    {
        $oPromotion = Promotion::find($id);
        if ($request->exists('options')) {
            $options = $request->get('options');
            $aOptions = [];
            foreach ($options as $option) {
                if (!empty($option)) {
                    $aOptions['text'][] = $option;
                }
            }
            $oPromotion->update([
               'options' => $aOptions
            ]);
        }
        return responseCommon()->success([], 'Данные успешно обновлены');
    }
}
