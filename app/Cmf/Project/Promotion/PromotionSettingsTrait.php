<?php

namespace App\Cmf\Project\Promotion;

use App\Cmf\Core\SettingsTrait;

trait PromotionSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Реклама',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'promotion',
            ],
        ],
        'icon' => 'fa fa-code-fork'
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'promotion';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'promotion';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = ['promotion'];

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = false;

    /**
     * Defaults values for store method
     *
     * @var array
     */
    private $defaults = [
        'store' => [
            'status' => 1,
        ],
    ];

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'title' => 'Заголовок',
        'type' => 'Тип',
        'image' => 'Изображение',
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    private $image = [
        'key' => 'promotion',
        'with_main' => true,
        'unique' => false,
        'filters' => [
            'square' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'dimension' => '1:1'
                ]
            ],
            'rectangle' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'dimension' => '16:9'
                ]
            ],
            'xs' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'size' => '180'
                ]
            ],
//            'sm' => [
//                'filter' => \App\Services\Image\Filters\SquareFilter::class,
//                'options' => [
//                    'size' => '540'
//                ]
//            ],
//            'md' => [
//                'filter' => \App\Services\Image\Filters\SquareFilter::class,
//                'options' => [
//                    'size' => '720'
//                ]
//            ],
            'lg' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'size' => '960'
                ]
            ]
        ]
    ];

    public function getFilters()
    {
        $filters = $this->image['filters'];
        return $filters;
    }

    /**
     * Default files settings in model
     *
     * @var array
     */
    private $file = [];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            'title' => 'required|max:255',
            'type' => [
                'required',
                'min:1',
                'unique:promotions',
            ],
        ],
        'update' => [
            'title' => 'required|max:255',
        ],
        'upload' => [
            'id' => 'required|max:255',
            'images' => 'required|max:15000|mimes:jpg,jpeg,gif,png',
        ],
    ];

    public function getRules($method)
    {
        $return = $this->rules[$method];
    }

}
