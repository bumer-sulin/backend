<?php

namespace App\Cmf\Project\Option;

use App\Cmf\Core\SettingsTrait;

trait OptionSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => 'Опции',
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => 'option',
            ],
        ],
        'icon' => 'fa fa-code-fork'
    ];

    /**
     * Session key
     * @var string
     */
    private $session = 'option';

    /**
     * View path
     *
     * @var string
     */
    private $view = 'option';

    /**
     * Cache key
     *
     * @var string
     */
    private $cache = ['option'];

    /**
     * Enabled get all items by cache
     *
     * @var bool
     */
    private $enabledCacheModel = false;

    /**
     * Defaults values for store method
     *
     * @var array
     */
    private $defaults = [
        'store' => [
            'status' => 1,
        ],
    ];

    /**
     * Validation name return
     * @var array
     */
    private $attributes = [
        'title' => 'Заголовок',
        'image' => 'Изображение',
        'file' => 'Файл',
    ];

    /**
     * Default image settings in model
     *
     * @var array
     */
    private $image = [
        'key' => 'option',
        'with_main' => true,
        'unique' => false,
        'filters' => [
            'square' => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'dimension' => '1:1'
                ]
            ]
        ]
    ];

    /**
     * Default files settings in model
     *
     * @var array
     */
    private $file = [
        'key' => 'option',
        'unique' => false,
    ];

    /**
     * Validation rules
     * @var array
     */
    private $rules = [
        'store' => [
            'title' => 'required|max:255',
        ],
        'update' => [
            'title' => 'required|max:255',
        ],
        'upload' => [
            'id' => 'required|max:255',
            'images' => 'required|max:5000|mimes:jpg,jpeg,gif,png',
        ],
        'uploadFiles' => [
            'id' => 'required|max:255',
            'files' => 'required|max:10000',
        ],
    ];

}
