<?php

namespace App\Cmf\Project\Option;


use App\Models\Option;
use App\Models\OptionParameter;
use App\Models\Product;
use App\Models\ProductOptions;
use App\Models\ProductOptionValues;
use App\Models\ProductPrices;
use Illuminate\Http\Request;

trait OptionCustomTrait
{
    public function getParameters(Request $request, $id)
    {
        $model = $this->class;
        $oItem = $this->enabledCacheModel ? $this->cacheModel->where('id', $id)->first() : $model::find($id);

        $view = view($this->theme . '.content.' . $this->view . '.components.modals.parameters', [
            'oOption' => $oItem,
            'oItems' => $oItem->parameters->sortByDesc('priority')
        ])->render();


        return $this->success([
            'view' => $view
        ]);
    }

    public function setStatusParameter(Request $request, $id)
    {
        OptionParameter::find($id)->update([
            'status' => $request->get('status')
        ]);
        return $this->success();
    }

    public function setDestroyParameter(Request $request, $id)
    {
        $oItem = OptionParameter::find($id);
        $oOption = $oItem->option;
        $oItem->delete();
        $view = view($this->theme . '.content.' . $this->view . '.components.modals.parameters', [
            'oOption' => $oOption,
            'oItems' => $oOption->parameters->sortByDesc('priority')
        ])->render();

        return $this->success([
            'view' => $view
        ]);
    }

    public function saveParameters(Request $request, $id)
    {
        $aParameters = $this->compactByParameters($request->all(), ['name', 'priority']);
        foreach ($aParameters as $key => $aParameter) {
            if (intval($key) === 0) {
                $parameters = $this->compactByParameters($aParameter, ['name', 'priority']);
                foreach($parameters as $parameter) {
                    OptionParameter::create([
                        'option_id' => $id,
                        'quantity' => $parameter['name'],
                        'priority' => $parameter['priority'],
                    ]);
                }
            } else {
                OptionParameter::find($key)->update([
                    'quantity' => $aParameter['name'],
                    'priority' => $aParameter['priority'],
                ]);
            }
        }
        return $this->success();
    }



    /**
     * Скомпоновать параметры
     *
     * @param $aData
     * @param $columns
     * @return array
     */
    private function compactByParameters($aData, $columns)
    {
        $array = [];
        foreach($columns as $column) {
            if (isset($aData[$column])) {
                foreach ($aData[$column] as $key => $value) {
                    $array[$key][$column] = $value;
                }
            }
        }
        return $array;
    }

}