<?php

namespace App\Cmf\Api\Transformers;

use App\Models\Comment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * CommentTransformer constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * @param Comment $item
     * @return array
     */
    public function transform(Comment $item)
    {
        if (isset($this->data['oUser'])) {
            $oLoveComment = $this->data['oUser']->getLoveComment($item->id);
            if (!is_null($oLoveComment)) {
                $item->love = $oLoveComment->like === 1 ? 'like' : 'dislike';
            }
        }
        return [
            'id' => (int)$item->id,
            'image' => imagePath()->main('user', 'square', $item->commented),
            'author' => $item->commented !== null ? $item->commented->first_name : 'Гость',
            'text' => trim($item->comment),
            'children' => $item->hasApprovedChildren() ? $item->approvedChildren->transform(function ($item) {
                return (new CommentTransformer($this->data))->transform($item);
            }) : [],
            'approved' => $item->approved,
            'rate' => $item->rate,
            'likes' => (int)$item->loveUsers()->sum('like'),
            'love' => isset($item->love) ? $item->love : null,
            'dislikes' => (int)$item->loveUsers()->sum('dislike'),
            'created_at' => $item->created_at->format('d.m.Y H:i:s'),
        ];
    }
}
