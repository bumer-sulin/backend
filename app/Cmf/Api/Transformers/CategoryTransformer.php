<?php

namespace App\Cmf\Api\Transformers;

use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;

use League\Fractal\TransformerAbstract;
use App\Models\Category;

class CategoryTransformer extends TransformerAbstract
{
    /**
     * @param Category $category
     * @param bool $withChildren
     * @return array
     */
    public function transform(Category $category, $withChildren = true)
    {
        return [
            'id' => (int)$category->id,
            'title' => trim($category->title),
            'color' => trim($category->color),
            'priority' => (int)$category->priority,
            'status' => (int)$category->status,
            'children' => $withChildren ? $category->children->transform(function ($item) {
                return (new CategoryTransformer())->transform($item);
            }) : [],
            'articles' => (int)$category->activeArticles()->count(),
        ];
    }
}
