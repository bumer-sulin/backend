<?php

namespace App\Cmf\Api\Transformers;

use App\Models\Promotion;
use App\Models\Social;
use App\Models\Type;
use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;

use League\Fractal\TransformerAbstract;
use App\Models\Category;

class PromotionTransformer extends TransformerAbstract
{
    /**
     * @param Promotion $item
     * @return array
     */
    public function transform(Promotion $item)
    {
        return [
            'id' => (int)$item->id,
            'key' => (new Promotion())->getTypes()[$item->type]['key'],
            'title' => trim($item->title),
            'link' => !is_null($item->link) ? trim($item->link) : null,
            'strings' => $item->isStrings() ? $item->options['text'] : null,
            'image' => $item->isImage() ? imagePath()->main('promotion', 'original', $item) : null,
        ];
    }
}
