<?php

namespace App\Cmf\Api\Transformers;

use App\Models\Article;
use App\Models\Setting;
use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;

use League\Fractal\TransformerAbstract;
use App\Models\Category;

class ArticleDetailTransformer extends TransformerAbstract
{
    /**
     * @param Article $item
     * @return array
     */
    public function transform(Article $item)
    {
        $mainImage = $item->images->where('is_main', 1)->first();

        return [
            'id' => (int)$item->id,
            'category_id' => (int)$item->category_id,
            'category' => !is_null($item->category) ? (new CategoryTransformer())->transform($item->category) : null,
            'type_id' => (int)$item->type_id,
            'type' => $item->type_key,
            'tags' => $item->activeTags->transform(function ($item) {
                return (new TagTransformer())->transform($item);
            }),
            'image' => imagePath()->main('article', 'original', $item),
            //'image_author' => $mainImage->options['author'] ?? null,
            'image_author' => $item->value_photo_author,
            'title' => trim($item->title),
            'slug' => trim($item->name),
            'keywords' => !is_null($item->keywords) ? trim($item->keywords) : null,
            'description' => trim($item->description),
            'text' => $item->text,
            'status' => (int)$item->status,
            'published_at' => $item->published_at_format,
            'author' => $item->value_author,
            'count' => [
                'comments' => $item->statistic->comments,
                'views' => $item->statistic->views,
            ],
            'comments' => [],
        ];
    }
}
