<?php

namespace App\Cmf\Api\Transformers;

use App\Models\Setting;
use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;

use League\Fractal\TransformerAbstract;
use App\Models\Category;

class SettingTransformer extends TransformerAbstract
{
    /**
     * @param Setting $item
     * @return array
     */
    public function transform(Setting $item)
    {
        if ($item->isImage) {
            $item->value = imagePath()->main('setting', 'original', $item);
        }
        return [
            'id' => (int)$item->id,
            'title' => trim($item->title),
            'key' => trim($item->key),
            'value' => trim($item->value),
            'type' => (int)$item->type,
            'status' => (int)$item->status,
        ];
    }
}
