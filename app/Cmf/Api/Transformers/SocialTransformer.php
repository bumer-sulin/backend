<?php

namespace App\Cmf\Api\Transformers;

use App\Models\Social;
use App\Models\Type;
use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;

use League\Fractal\TransformerAbstract;
use App\Models\Category;

class SocialTransformer extends TransformerAbstract
{
    /**
     * @param Social $item
     * @return array
     */
    public function transform(Social $item)
    {
        return [
            'id' => (int)$item->id,
            'type' => (new Social())->getTypes()[$item->type],
            'title' => trim($item->title),
            'link' => trim($item->link),
            'priority' => (int)$item->priority,
            'status' => (int)$item->status,
        ];
    }
}
