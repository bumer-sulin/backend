<?php

namespace App\Cmf\Api\Transformers;

use App\Models\Social;
use App\Models\Tag;
use App\Models\Type;
use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;

use League\Fractal\TransformerAbstract;
use App\Models\Category;

class TagTransformer extends TransformerAbstract
{
    /**
     * @param Tag $item
     * @return array
     */
    public function transform(Tag $item)
    {
        return [
            'id' => (int)$item->id,
            'title' => trim($item->title),
            'color' => trim($item->color),
        ];
    }
}
