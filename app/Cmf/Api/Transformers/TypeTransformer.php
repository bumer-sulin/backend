<?php

namespace App\Cmf\Api\Transformers;

use App\Models\Type;
use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;

use League\Fractal\TransformerAbstract;
use App\Models\Category;

class TypeTransformer extends TransformerAbstract
{
    /**
     * @param Type $item
     * @return array
     */
    public function transform(Type $item)
    {
        return [
            'id' => (int)$item->id,
            'title' => trim($item->title),
        ];
    }
}
