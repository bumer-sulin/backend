<?php

namespace App\Cmf\Api\Transformers;

use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;

use League\Fractal\TransformerAbstract;
use App\Models\Category;

class CategorySectionTransformer extends TransformerAbstract
{
    /**
     * @param Category $category
     * @return array
     */
    public function transform(Category $category)
    {
        return [
            'id' => (int)$category->id,
            'title' => trim($category->title),
            'color' => trim($category->color),
            'priority' => (int)$category->priority,
            'status' => (int)$category->status,
            'articles' => $category->articlesSection->transform(function ($item) {
                return (new ArticleTransformer())->transform($item);
            }),
        ];
    }
}
