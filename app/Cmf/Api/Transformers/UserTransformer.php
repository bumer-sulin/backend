<?php

namespace App\Cmf\Api\Transformers;

use App\Models\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * @param User $item
     * @return array
     */
    public function transform(User $item)
    {
        return [
            'id' => (int)$item->id,
            'email' => trim($item->email),
            'name' => trim($item->first_name),
            'last_name' => trim($item->last_name),
            'image' => imagePath()->main('user', 'square', $item),
        ];
    }
}
