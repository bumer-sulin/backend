<?php

namespace App\Cmf\Api\Transformers;

use App\Models\Article;
use App\Models\Setting;
use Dingo\Api\Http\Request;
use Dingo\Api\Transformer\Binding;
use Dingo\Api\Contract\Transformer\Adapter;

use League\Fractal\TransformerAbstract;
use App\Models\Category;

class ArticleTransformer extends TransformerAbstract
{
    /**
     * @param Article $item
     * @return array
     */
    public function transform(Article $item)
    {
        return [
            'id' => (int)$item->id,
            'category_id' => !is_null($item->category) ? (int)$item->category_id : null,
            'category' => !is_null($item->category) ? (new CategoryTransformer())->transform($item->category) : null,
            'type_id' => (int)$item->type_id,
            'type' => $item->type_key,
            'image' => imagePath()->main('article', 'lg', $item),
            'title' => trim($item->title),
            'slug' => trim($item->name),
            'keywords' => !is_null($item->keywords) ? trim($item->keywords) : null,
            'description' => trim($item->description),
            //'text' => trim($item->text),
            'status' => (int)$item->status,
            'published_at' => $item->published_at_format,
            'author' => $item->value_author,
            'count' => [
                'comments' => $item->statistic->comments,
                'views' => $item->statistic->views,
            ],
        ];
    }

    /**
     * @param Article $item
     * @param array $data
     * @return array
     */
    public function transformWithExcept(Article $item, array $data = [])
    {
        $aItem = $this->transform($item);
        foreach ($data as $key) {
            if (isset($aItem[$key])) {
                unset($aItem[$key]);
            }
        }
        return $aItem;
    }
}
