<?php

namespace App\Cmf\Core;

use App\Cmf\Project\Article\ArticleBaseController;
use App\Models\Category;
use App\Models\Type;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Events\ChangeCacheEvent;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cache;
use App\Models\Image;
use App\Models\File;
use App\Services\Image\ImageService;
use App\Services\File\FileService;

trait ControllerCrmTrait
{

    /**
     * Validate mode
     *
     * @param Request $request
     * @param $rules
     * @param $attributes
     * @param $method
     * @param $errors
     * @return mixed
     */
    public function validation(Request $request, $rules, $attributes, $method, $errors = [])
    {
        return isset($rules[$method]) ?
            Validator::make($request->all(), $rules[$method], $errors, $attributes) :
            Validator::make($request->all(), $rules, $errors, $attributes);
    }

    /**
     * После любых изменений очистить кэш по этому ключу
     *
     * @param $key
     */
    public function afterChange($key, $support = null, $tag = null)
    {
        if (is_array($key)) {
            foreach($key as $value) {
                event(new ChangeCacheEvent($value));
            }
        } else {
            if (!is_null($key)) {
                if (!is_null($support)) {
                    if (is_array($support)) {
                        foreach($support as $value) {
                            event(new ChangeCacheEvent($value, $tag));
                        }
                    } else {
                        event(new ChangeCacheEvent($support));
                    }
                }
                event(new ChangeCacheEvent($key));
            }
        }
    }


    /**
     * Get orderBy by session
     *
     * @param $order
     * @param $session
     * @return array
     */
    public function setOrderBy($order, $session)
    {
        if (Session::exists($session) && Session::has($session.'.sort')) {
            $column = Session::has($session.'.sort.column') ? Session::get($session.'.sort.column') : 'created_at';
            $type = Session::has($session.'.sort.type') ? Session::get($session.'.sort.type') : 'desc';
            $order = $column.'.'.$type;
        }

        $orderColumn = strripos($order, '.') ? stristr($order,'.', true) : $order;
        $orderType = substr(stristr($order,'.'), 1);
        $orderBy = [
            'column' => $orderColumn ? $orderColumn : 'created_at',
            'type' => $orderType ? $orderType : 'desc',
        ];
        return $orderBy;
    }

    /**
     * Get query in session
     *
     * @param $session
     * @param null $aQuery
     * @return null
     */
    public function setQuery($session, $aQuery = null)
    {
        if (Session::exists($session) && Session::has($session.'.query')) {
            return Session::get($session.'.query');
        } else {
            return !is_null($aQuery) ? $aQuery : [];
        }
    }

    /**
     * Cache all items
     *
     * @param $class
     * @param $cache
     * @return mixed
     */
    public function getCacheModel($class, $cache)
    {
        return Cache::remember($cache, 3600, function() use ($class) {
            return $class::all();
        });
    }


    /**
     * Загрузить изображение/файл
     *
     * @param $class
     * @param $id
     * @param $aFiles
     * @param $options
     * @param string $type
     * @return null|string
     */
    public function upload($class, $id, $aFiles, $options, $type = 'images')
    {
        switch($type) {
            case 'images':
                return $this->uploadByTypeImages($class, $id, $aFiles, $options);
            case 'files':
                return $this->uploadByTypeFiles($class, $id, $aFiles, $options);
            default:
                return null;
        }
    }

    /**
     * Загрузить изображение
     *
     * @param $class
     * @param $id
     * @param $aFiles
     * @param $options
     * @return string
     */
    public function uploadByTypeImages($class, $id, $aFiles, $options)
    {
        $oService = new ImageService();
        $filters = isset($options['filters']) ? $options['filters'] : null;
        try {
            $files = [];
            foreach($aFiles as $aFile) {
                $files[] = $oService->upload($aFile, $options['key'], $id, $filters);
            }

            if (isset($options['unique']) && $options['unique']) {
                $images = Image::where('type', $options['key'])->where('imageable_id', $id)->get();
                if (!empty($images)) {
                    foreach($images as $image) {
                        $oService->deleteImages($image->filename, $options['key'], $image->imageable_id);
                        $image->delete();
                    }
                }
            }
            foreach($files as $key => $file) {
                Image::create([
                    'type' => $options['key'],
                    'imageable_id' => $id,
                    'imageable_type' => $class,
                    'filename' => $file
                ]);
            }
            if (isset($options['with_main']) && $options['with_main']) {
                $oImages = Image::where('type', $options['key'])->where('imageable_id', $id)->where('is_main', 1)->get();
                if (empty($oImages[0])) {
                    $oImages = Image::where('type', $options['key'])->where('imageable_id', $id)->first();
                    $oImages->update([
                        'is_main' => 1
                    ]);
                }
            }
            return $files[0];
        } catch(\Exception $e) {
            dd($e);
            return 'Image ddd not found.';
        }
    }

    /**
     * Загрузить файл
     *
     * @param $class
     * @param $id
     * @param $aFiles
     * @param $options
     * @return string
     */
    public function uploadByTypeFiles($class, $id, $aFiles, $options)
    {
        if (isset($options['replace_key'])) {
            $options['key'] = $options['replace_key'];
        }
        $oService = new FileService();
        try {
            $files = [];
            foreach ($aFiles as $aFile) {
                $files[] = $oService->upload($aFile, $options['key'], $id);
            }
            if (isset($options['relations'][$options['key']])) {
                $options = array_merge($options, $options['relations'][$options['key']]);
            }
            if (isset($options['unique']) && $options['unique']) {
                $images = File::where('type', $options['key'])->where('fileable_id', $id)->get();
                if (!empty($images)) {
                    foreach ($images as $image) {
                        $oService->deleteImages($image->filename, $options['key'], $image->fileable_id);
                        $image->delete();
                    }
                }
            }
            foreach ($files as $key => $file) {
                File::create([
                    'type' => $options['key'],
                    'fileable_id' => $id,
                    'fileable_type' => $class,
                    'filename' => $file,
                    'name' => $file,
                ]);
            }
            return $files[0];
        } catch (\Exception $e) {
            dd($e);
            return 'File ddd not found.';
        }
    }

    /**
     * Правила для загрузки фотографий, с учетом каждой фотографии
     *
     * @param $images
     * @param $rules
     * @param $rulesKey
     * @param $attributes
     * @param string $validatorKey
     * @param string $validatorAttribute
     * @return array
     */
    public function setRulesForUpload($images, $rules, $rulesKey, $attributes, $validatorKey = 'images', $validatorAttribute = 'image')
    {
        foreach($images as $key => $image) {
            $rules[$rulesKey][$validatorKey.'.'.$key] = $rules[$rulesKey][$validatorKey];
            $attributes[$validatorKey.'.'.$key] = $attributes[$validatorAttribute];
        }
        unset($rules[$rulesKey][$validatorKey]);
        return [
            'rules' => $rules,
            'attributes' => $attributes
        ];
    }


    /**
     * Получить поля для сохранения в модель
     *
     * @param $request
     * @param $key
     * @return array|null
     */
    public function getSaveColumns($request, $key)
    {
        if (Cache::has('model_columns')) {
            $cacheColumns = Cache::get('model_columns');
            if (is_array($key)) {
                $key = $key[0];
            }
            if (isset($cacheColumns[$key]['fillable'])) {
                $array = [];
                foreach($cacheColumns[$key]['fillable'] as $key => $value) {
                    if ($request->exists($value)) {
                        $array[$value] = $request->get($value);
                    }
                }
                return $array;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Рекурсивно получить массив со всеми дочерними id
     *
     * @param $oModel
     * @param array $array
     * @return array
     */
    public function getChildrenId($oModel, $array = [])
    {
        if (!empty($oModel->children) && !empty($oModel->children[0])) {
            foreach($oModel->children as $oChildren) {
                $array[] = $oChildren->id;
                $array = $this->getChildrenId($oChildren, $array);
            }
        }
        return $array;
    }

    /**
     * Обновление родительских категорий после изменений
     *
     * @param $class
     * @param $oModel
     * @param bool|false $replaceParent
     * @param bool|false $needAddToChildren
     * @param null $oldParent
     */
    public function parentAbleChange($class, $oModel, $replaceParent = false, $needAddToChildren = false, $oldParent = null)
    {
        if ($replaceParent) {
            $oFirstChildren = $class::where('parent_id', $oModel->id)->get();
            foreach($oFirstChildren as $oChildren) {
                $oChildren->update([
                    'parent_id' => null
                ]);
            }
        }
        if ($needAddToChildren && !is_null($oldParent)) {
            $oFirstChildren = $class::where('parent_id', $oModel->id)->get();
            foreach($oFirstChildren as $oChildren) {
                $oChildren->update([
                    'parent_id' => $oldParent
                ]);
            }
        }
    }

    /**
     * Корректировка массива с учетом значений по умолчанию
     *
     * @param $data
     * @param $defaults|null
     * @return mixed
     */
    public function withDefaults($data, $defaults = null)
    {
        if (!is_null($defaults)) {
            foreach ($defaults as $key => $default) {
                if (is_array($default)) {
                    if (isset($default['type']) && $default['type'] === 'slug') {
                        $data[$key] = str_slug($data[$default['column']]);
                    }
                    if (isset($default['filter'])) {
                        $method = 'columnFilter' . studly_case('_' . $default['filter']);
                        if (method_exists($this, $method)) {
                            $data[$key] = $this->{$method}($data[$key]);
                        }
                    }
                } else {
                    $data[$key] = $this->switchDefault($default);
                }
            }
        }
        return $data;
    }

    public function switchDefault($default)
    {
        if (is_string($default)) {
            switch ($default) {
                case 'now':
                    return Carbon::now();
                default:
                    return $default;
            }
        } else {
            return $default;
        }
    }



    public function columnFilterPhone($phone)
    {
        return !empty($phone) ? preg_replace("/[^0-9]/", '', $phone) : null;
    }


    public function sendMail($oModel, $oMailObject, $to = null)
    {
        if (is_null($to)) {
            $to = Config::get('mail.to.address');
        }
        Mail::to($to)
            ->send(new $oMailObject($oModel));
    }


    /**
     * Добавить в сессию поиск
     *
     * @param $request
     * @param $key
     */
    protected function setQuerySearch(Request $request, $key = [])
    {
        if (isset($request->all) && !empty($request->all)) {
            Session::forget($this->session.'.query');
            return;
        }
        $session = Session::get($this->session.'.query');
        foreach ($key as $k => $v) {
            if ($request->exists($v)) {
                if ((intval($request->get($v)) === 0 &&
                    ($v !== 'status' && $v !== 'approved') || $request->get($v) === '--')
                ) {
                    if (!is_null($session)) {
                        if (isset($session[$v])) {
                            unset($session[$v]);
                        }
                        Session::put($this->session.'.query', $session);
                    }
                } else {
                    if (!is_null($session)) {
                        $session[$v] = $request->get($v);
                        Session::put($this->session.'.query', $session);
                    } else {
                        Session::put($this->session.'.query', [
                            $v => $request->get($v)
                        ]);
                    }
                }
            }
        }
    }


    protected function setViews()
    {
        return Cache::remember($this->view.'-views', 3600, function() {
            $view = $this->theme.'.content.'.$this->view;
            return [
                'index' => $view.'.index',
                'create' => $view.'.create',
                'show' => $view.'.show',
                'edit' => $view.'.edit',
                'table' => $view.'.components.table',
                'createModal' => $view.'.components.modals.create',
                'editModal' => $view.'.components.modals.edit',
                'image' => $view.'.components.modals.images',
                'files' => $view.'.components.modals.files'
            ];
        });
    }


    public function categories($status = null)
    {
        $oQuery = Category::with('images', 'children', 'children.images', 'parent');
        if (!is_null($status)) {
            $oQuery = $oQuery->where('status', 1);
        }
        return $oQuery
            ->orderBy('title', 'asc')
            ->get()
            ->keyBy('id');
    }

    public function types($status = null)
    {
        $oQuery = Type::with('children');
        if (!is_null($status)) {
            $oQuery = $oQuery->where('status', 1);
        }
        return $oQuery
            ->orderBy('title', 'asc')
            ->get()
            ->keyBy('id');
    }

    public function beforeDeleteForImages($model)
    {
        $oImages = $model->gallery;

        if (count($oImages) !== 0) {
            foreach ($oImages as $oImage) {
                $request = new Request();
                $this->imageDestroy($request, $model->id, $oImage->id);
            }
        }
    }
}

