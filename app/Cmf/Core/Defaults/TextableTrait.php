<?php

namespace App\Cmf\Core\Defaults;

use App\Models\File;
use Illuminate\Http\Request;
use App\Services\File\FileService;

trait TextableTrait
{
    public function textModal($id)
    {
        $class = $this->class;
        $oItem = $class::find($id);
        $view = view($this->theme.'.content.components.modals.text', [
            'oItem' => $oItem
        ])->render();
        return $this->success([
            'view' => $view
        ]);
    }

    public function textSave(Request $request, $id)
    {
        $class = $this->class;
        $oItem = $class::find($id);

        if ($request->has('text')) {
            $key = 'text';
            if (method_exists($class, 'textableKey')) {
                $key = (new $class())->textableKey();
            }
            $oItem->update([
                $key => $request->get('text')
            ]);
            return $this->success([
                'toastr' => $this->toastr['update']
            ]);
        }
        if ($request->has('file')) {

            return $this->success([
                'toastr' => $this->toastr['update']
            ]);
        }
        return $this->success([
            'toastr' => $this->toastr['update']
        ]);
    }
}
