<?php

namespace App\Cmf\Core\Defaults;

use App\Events\ChangeCacheEvent;
use App\Helpers\Model;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Image;
use App\Services\Image\ImageService;
use App\Services\Image\Facades\ImagePath;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

trait ImageableTrait
{
    /*
    private $class = null;

    private $view = null;

    private $image = [
        'key' => 'category',
        'with_main' => true,
        'unique' => false,
    ];

    private $rules = [
        'upload' => [
            'id' => 'required|max:255',
            'images' => 'required|max:5000|mimes:jpg,jpeg,gif,png',
        ],
    ];

    private $attributes = [
        'image' => 'Изображение',
    ];
    */

    /**
     * Upload images in modal
     *
     * @param Request $request
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function imageUpload(Request $request, $id)
    {
        $validateMessages = isset($this->validateMessages) ? $this->validateMessages : [];
        $rules = $this->setRulesForUpload($request->file('images'), $this->rules, 'upload', $this->attributes);
        $this->rules = $rules['rules'];
        $this->attributes = $rules['attributes'];
        $validation = $this->validation($request, $this->rules, $this->attributes, 'upload', $validateMessages);
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {
            $this->upload($this->class, $request->get('id'), $request->file('images'), $this->image);
            $model = $this->class;
            $oItem = $model::find($id);
            $returnData = [];
            if ($this->view === 'user') {
                $returnData['src'] = ImagePath::main($this->view, 'square', $oItem);
            }
            $returnData['view'] = $this->imageGetView($oItem);
            $this->afterChangeImage($this->image, $oItem);
            return $this->success($returnData);
        }
    }

    /**
     * Destroy single image
     *
     * @param Request $request
     * @param $id
     * @param $image_id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function imageDestroy(Request $request, $id, $image_id)
    {
        $oImage = Image::find($image_id);

        $oService = new ImageService();
        $oService->deleteImages($oImage->filename, $this->image['key'], $id,  $this->image);
        $oImage->destroy($oImage->id);
        $this->setDefaultMainImages($this->image['key'], $id);

        $model = $this->class;
        $oItem = $model::find($id);
        $returnData = [];
        if ($this->view === 'user') {
            $returnData['src'] = ImagePath::main($this->view, 'square', $oItem);
        }
        $returnData['view'] = $this->imageGetView($oItem);
        $this->afterChangeImage($this->image, $oItem);
        return $this->success($returnData);
    }

    /**
     * Set main single image
     *
     * @param Request $request
     * @param $id
     * @param $image_id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function imageMain(Request $request, $id, $image_id)
    {
        $oImage = Image::find($image_id);
        $this->clearMainImages($this->image['key'], $id);
        $oImage->update([
            'is_main' => 1
        ]);
        $model = $this->class;
        $oItem = $model::find($id);
        $returnData = [];
        if ($this->view === 'user') {
            $returnData['src'] = ImagePath::main($this->view, 'square', $oItem);
        }
        $returnData['view'] = $this->imageGetView($oItem);
        $this->afterChangeImage($this->image, $oItem);
        return $this->success($returnData);
    }

    public function imagePriority(Request $request)
    {
        $model = Model::init($request->get('type'));
        $oModel = $model::find($request->get('id'));
        if (!is_null($oModel)) {
            $aIds = explode(';', $request->get('positions'));
            $count = count($aIds);
            foreach($aIds as $key => $id) {
                if (empty($id)) {
                    continue;
                }
                Image::find($id)->update([
                    'priority' => $count - $key,
                ]);
            }
        }
        return $this->success();
    }



    private function clearMainImages($key, $id)
    {
        $oMainImages = Image::where('type', $key)->where('imageable_id', $id)->where('is_main', 1)->get();
        foreach($oMainImages as $oMainImage) {
            $oMainImage->update([
                'is_main' => 0
            ]);
        }
    }

    private function setDefaultMainImages($key, $id)
    {
        $oMainImage = Image::where('type', $key)->where('imageable_id', $id)->where('is_main', 1)->first();
        if (is_null($oMainImage)) {
            $oMainImage = Image::where('type', $key)->where('imageable_id', $id)->first();
            if (!is_null($oMainImage)) {
                $oMainImage->update([
                    'is_main' => 1
                ]);
            }
        }
    }


    private function afterChangeImage($imageSettings, $oItem = null)
    {
        if (isset($imageSettings['clear_cache'])) {
            event(new ChangeCacheEvent($imageSettings['clear_cache']));
        }
    }

    private function imageGetView($oItem)
    {
        return view($this->theme.'.components.gallery.block', [
            'oItem' => $oItem,
            'col' => isset($col) ? $col : 3,
            'model' => isset($model) ? $model : $this->view,
            'adminModel' => isset($adminModel) ? $adminModel : $this->view,
            'form' => isset($form) ? $form : '#'.$this->view.'-form',
        ])->render();
    }


    public function getImages($id)
    {
        $model = $this->class;
        $oItem = $model::find($id);
        return $this->imageGetView($oItem);
    }

    public function saveImage($url, $oModel, $type = 'user', $model = User::class)
    {
        $folderName = '/images/user/tmp/';
        $destinationPath = public_path() . $folderName;
        $imageName = Str::random(10).'.'.'jpg';
        $pathFile = $destinationPath . $imageName;

        File::put($pathFile, file_get_contents($url));

        $oService = new ImageService();

        $file = $oService->upload(null, $type, $oModel->id, (new \App\Cmf\Project\User\UserBaseController())->getFilters(), $pathFile, $imageName);

        $this->clearMainImages($type, $oModel->id);

        Image::create([
            'type' => 'user',
            'imageable_id' => $oModel->id,
            'imageable_type' => $model,
            'filename' => $file,
            'is_main' => 1,
        ]);

        File::delete($pathFile);
    }

}
