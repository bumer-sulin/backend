<?php

namespace App\Cmf\Core\Defaults;

use App\Models\File;
use Illuminate\Http\Request;
use App\Services\File\FileService;
use Illuminate\Support\Facades\View;

trait FileableTrait
{
    /*
    private $class = null;

    private $view = null;

    private $file = [
        'key' => null,
        'unique' => false,
    ];

    private $rules = [
        'uploadFiles' => [
            'id' => 'required|max:255',
            'files' => 'required|max:10000',
        ],
    ];

    private $attributes = [
        'file' => 'Файл',
    ];
    */


    /**
     * Upload images in modal
     *
     * @param Request $request
     * @param $id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function fileUpload(Request $request, $id)
    {
        $rules = $this->setRulesForUpload($request->file('files'), $this->rules, 'uploadFiles', $this->attributes, 'files', 'file');
        $this->rules = $rules['rules'];
        $this->attributes = $rules['attributes'];
        $this->file['replace_key'] = $request->get('fileable_type');

        $validation = $this->validation($request, $this->rules, $this->attributes, 'uploadFiles');
        if($validation->fails()) {
            return response()->json($validation->getMessageBag(),422);
        } else {
            $this->upload($this->class, $request->get('id'), $request->file('files'), $this->file, 'files');
            $model = $this->class;
            $oItem = $model::find($id);
            $relation = isset($this->file['replace_key']) ? $this->file['replace_key'] : $this->file['key'];

            if (isset($this->file['relations'][$request->get('fileable_type')]['by'])) {
                $relation = $this->file['relations'][$request->get('fileable_type')]['by'];
            }
            $view = $this->fileGetView($oItem, $relation);
            return $this->success([
                'view' => $view
            ]);
        }
    }

    /**
     * Destroy single image
     *
     * @param Request $request
     * @param $id
     * @param $file_id
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function fileDestroy(Request $request, $id, $file_id)
    {
        $oFile = File::find($file_id);
        $relation = $oFile->type;

        $oService = new FileService();
        if (isset($this->file['relations'])) {
            foreach($this->file['relations'] as $key => $rel) {
                $a = $oFile->{$key};
                if (!is_null($a) && $key !== $relation) {
                    //dd($key);
                    $oService->deleteImages($a->filename, $key, $oFile->id);
                    $a->destroy($a->id);
                }
            }
        }

        $oService->deleteImages($oFile->filename, $this->file['key'], $id);

        $oFile->destroy($oFile->id);

        $model = $this->class;
        $oItem = $model::find($id);
        $view = $this->fileGetView($oItem, $relation);
        return $this->success([
            'view' => $view
        ]);
    }

    private function fileGetView($oItem, $relation = null)
    {
        if (!is_null($relation) && $relation === $this->file['key']) {
            $relation = 'files';
        }
        $view = config('site.theme').'.content.'.$this->view.'.components.modals.files.'.$relation.'.block';
        if (!View::exists($view)) {
            $view = config('site.theme').'.components.files.block';
        }
        return view($view, [
            'oItem' => $oItem,
            'model' => isset($model) ? $model : $this->view,
            'relation' => $relation,
            'uploadUrl' => route($this->view.'.file.upload.post', ['id' => $oItem->id]),
        ])->render();
    }

    public function fileGetFiles(Request $request, $id)
    {
        $model = $this->class;
        $oItem = $model::find($id);
        return $this->fileGetView($oItem, $request->get('relation'));
    }

    public function fileSavePriority(Request $request, $id)
    {
        foreach($request->get('name') as $key => $value) {
            $oFile = File::find($key);
            $oFile->update([
                'priority'  => $request->exists('priority') && isset($request->get('priority')[$key]) ? $request->get('priority')[$key] : 0,
                'name'      => $request->exists('name') && isset($request->get('name')[$key]) ? $request->get('name')[$key] : $oFile->filename
            ]);
        }
        return $this->success([
            'toastr' => [
                'title' => 'Успех',
                'text' => 'Данные успешно сохранены',
                'type' => 'success'
            ]
        ]);
    }
}