<?php

namespace App\Cmf\Core\Defaults;


trait DatatableTrait
{
    private function getRows($aColumns, $aData)
    {
        $aReturnColumns = [];
        foreach($aColumns as $key => $column) {
            $aReturnColumns[$column] = view($this->theme.'.content.'.$this->view.'.components.table.columns.'.$column, $aData)->render();
        }
        return $aReturnColumns;
    }
}