<?php
/**
 * Created by PhpStorm.
 * User: Dmitri
 * Date: 26.04.2017
 * Time: 21:30
 */

namespace App\Cmf\Core;


use App\Services\Toastr\Toastr;

trait SettingsTrait
{
    /**
     * Default toastr messages
     * @var array
     */
    public $toastr = [
        'store'     => [
            'title' => 'Успех',
            'text' => 'Данные успешно добавлены',
            'type' => 'success'
        ],
        'update'    => [
            'title' => 'Успех',
            'text' => 'Данные успешно изменены',
            'type' => 'success'
        ],
        'destroy'   => [
            'title' => 'Успех',
            'text' => 'Данные успешно удалены',
            'type' => 'success'
        ],
        'status'    => [
            [
                'title' => 'Успех',
                'text' => 'Данные успешно скрыты',
                'type' => 'success',
            ],
            [
                'title' => 'Успех',
                'text' => 'Данные успешно опубликованы',
                'type' => 'success',
            ]
        ],
        'error' => [
            'message' => [
                'text' => null,
                'title' => 'Ошибка',
                'type' => 'error',
            ],
            'code' => [
                '500' => [
                    'text' => 'Ошибка сервера. Попробуйте выполнить запрос позже.',
                    'title' => 'Ошибка',
                    'type' => 'error',
                    'options' => [
                        //'positionClass' => 'ns-box ns-growl ns-effect-scale ns-type-notice ns-show is-right'
                    ]
                ],
            ]
        ]
    ];


    /**
     * Только в OrderController
     *
     * @param $key
     * @return mixed
     */
    /**
     * @param $key
     * @param null $value
     * @return mixed
     */
    private function getToastr($key, $value = null)
    {
        if (is_null($value)) {
            (new Toastr(trans('toastr')[$key][$value]['text']))->success();
        }

        return isset($this->_toastr) && isset($this->_toastr[$key]) ? $this->_toastr[$key] : $this->toastr[$key];
    }

}