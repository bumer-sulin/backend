<?php

namespace App\Cmf\Core;

use App\Services\ResponseCommonTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;

class MainController extends Controller
{
    //use ResponseCommonTrait;

    protected $theme = 'admin';

    /**
     * Custom action
     *
     * @param Request $request
     * @param $name
     * @return mixed
     */
    public function action(Request $request, $name)
    {
        $method = Str::camel($name);
        if(!method_exists($this, $method)){
            abort(500, 'Method not found');
        }
        return $this->{$method}($request);
    }


    /**
     * Ajax return success with default properties
     *
     * @param array $aData
     * @return array
     */
    public function success(array $aData = [])
    {
        //return $this->success($aData);


        return array_merge([
            'success' => true
        ], $aData);
    }

    /**
     * Ajax return error with default properties
     *
     * @param array $aData
     * @return array
     */
    public function error(array $aData = [])
    {
        return array_merge([
            'success' => false
        ], $aData);
    }

    /**
     * Ajax return json error with status 422
     *
     * @param $text
     * @param string $key
     * @return \Illuminate\Http\JsonResponse
     */
    public function jsonError($text, $key = 'error')
    {
        return response()->json([
            $key => [$text]
        ], 422);
    }

    /**
     * Log info with CPU value
     *
     * @param string $message
     */
    public function cpuLog($message = '')
    {
        $load = sys_getloadavg();
        info(json_encode($load).' - '.$message);
    }

    /**
     * Log info with memory value
     *
     * @param string $message
     */
    public function memoryUsageLog($message = '')
    {
        $load = memory_get_usage();
        info(json_encode($load).' - '.$message);
    }

    /**
     * @param $seoProvider
     * @param $method
     * @param null $oModel
     */
    public function seo($seoProvider, $method, $oModel = null)
    {
        if (is_null($seoProvider)) {
            return;
        }
        $oSeo = new $seoProvider();
        if(method_exists($oSeo, $method)) {
            if (!is_null($oModel)) {
                $oSeo->{$method}($oModel);
            } else {
                $oSeo->{$method}();
            }
        }
    }

    protected function getView($views, $name)
    {
        return isset($views[$name]) ? $views[$name] : '';
    }
}
