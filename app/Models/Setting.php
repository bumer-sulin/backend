<?php

namespace App\Models;

class Setting extends EloquentModel
{
    protected $table = 'settings';

    protected $fillable = [
        'key', 'value', 'title', 'type', 'status'
    ];

    /**
     * The type attributes for model
     *
     * @var array
     */
    protected $types = [
        //0 => 'Неизвестно',
        1 => 'Обычное поле',
        2 => 'Текстовое поле',
        3 => 'Числовое поле',
        4 => 'Изображение',
        5 => 'Телефон',
        6 => 'Цвет',
        7 => 'Форматируемый текст',
    ];

    const KEY_APP_TITLE = 'app.title';
    const KEY_APP_DESCRIPTION = 'app.description';

    public function textableKey()
    {
        return 'value';
    }

    public function getIsImageAttribute()
    {
        return $this->type === 4;
    }
    public function getIsPhoneAttribute()
    {
        return $this->type === 5;
    }
    public function getIsColorAttribute()
    {
        return $this->type === 6;
    }
    public function getIsTextAttribute()
    {
        return $this->type === 7;
    }

    /**
     * Get statuses array
     *
     * @return mixed
     */
    public function getTypes()
    {
        return $this->types;
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable')->where('type', 'setting');
    }

    public function gallery()
    {
        return $this->morphMany(Image::class, 'imageable')->where('type', 'setting');
    }
}
