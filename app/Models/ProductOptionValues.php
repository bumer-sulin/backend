<?php

namespace App\Models;

class ProductOptionValues extends EloquentModel
{
    protected $table = 'product_option_values';

    protected $fillable = [
        'product_id', 'option_id', 'parameter_id', 'priority'
    ];


    public function parameter()
    {
        return $this->hasOne(OptionParameter::class, 'id', 'parameter_id');
    }

    public function option()
    {
        return $this->hasOne(Option::class, 'id', 'option_id');
    }
}
