<?php

namespace App\Models;

class InvoiceProduct extends EloquentModel
{
    protected $table = 'invoice_products';

    protected $fillable = [
        'invoice_id', 'product_id', 'options', 'amount'
    ];


    public function setOptionsAttribute($value)
    {
        $this->attributes['options'] = json_encode($value);
    }

    public function getOptionsAttribute($value)
    {
        return json_decode($value, true);
    }

}
