<?php

namespace App\Models;

use App\Services\Modelable\Typeable;
use Illuminate\Support\Str;

class Option extends EloquentModel
{
    use Typeable;

    protected $table = 'options';

    protected $fillable = [
        'title', 'description', 'key', 'unit', 'type', 'tooltip', 'status'
    ];

    public const KEY_AUTHOR = 'author';
    public const KEY_PHOTO_AUTHOR = 'photo-author';

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не активно',
        1 => 'Активно',
    ];

    /**
     * The type attributes for model
     *
     * @var array
     */
    protected $types = [
        //0 => 'Неизвестно',
        1 => 'Обычное поле', // например номер на футболку
        2 => 'Список', // связанное с parameters
    ];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        if (!isset($this->attributes['key'])) {
            $this->attributes['key'] = Str::slug($value);
        }
    }

    public function parameters()
    {
        return $this->hasMany(OptionParameter::class, 'option_id', 'id');
    }

    public function isList()
    {
        return $this->type === 2;
    }
}
