<?php

namespace App\Models;

class ProductBrand extends EloquentModel
{
    protected $table = 'product_brands';

    protected $fillable = [
        'product_id', 'brand_id',
    ];

    public $timestamps = false;
}
