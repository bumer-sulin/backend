<?php

namespace App\Models;

class ArticleStatistics extends EloquentModel
{
    protected $table = 'article_statistics';

    protected $fillable = [
        'article_id', 'views', 'downloads', 'comments',
    ];
}
