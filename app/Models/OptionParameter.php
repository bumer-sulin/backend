<?php

namespace App\Models;

class OptionParameter extends EloquentModel
{
    protected $table = 'option_parameters';

    protected $fillable = [
        'option_id', 'quantity', 'priority', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не опубликовано',
        1 => 'Опубликовано',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Не опубликовано'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Опубликовано'
        ],
    ];

    public function option()
    {
        return $this->hasOne(Option::class, 'id', 'option_id');
    }
}
