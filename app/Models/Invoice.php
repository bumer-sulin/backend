<?php

namespace App\Models;

class Invoice extends EloquentModel
{
    protected $table = 'invoices';

    protected $fillable = [
        'payment_id', 'amount', 'code', 'title', 'purpose', 'status', 'expires_at', 'opened_at', 'closed_at'
    ];

    public $dates = [
        'opened_at',
        'closed_at'
    ];

    public $statuses = [
        0 => 'Не активный',
        1 => 'Ждет закрытия',
        2 => 'Закрыт успешно',
        3 => 'Закрыт с провалом',
        4 => 'Истек',
        5 => 'Тест',
        6 => 'Заявка',
    ];

    public function getAmountAttribute($value)
    {
        return floatval($value);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'invoice_products')
            ->withPivot('options as product_options', 'amount as amount');
    }

    public function clients()
    {
        return $this->belongsToMany(Client::class, 'client_invoices');
    }

    public function getClientAttribute()
    {
        return $this->clients()->first();
    }

    public function options()
    {
        return $this->hasOne(InvoiceOption::class, 'invoice_id', 'id');
    }

}
