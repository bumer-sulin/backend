<?php

namespace App\Models;

use App\Services\Modelable\Parentable;

class Type extends EloquentModel
{
    use Parentable;

    protected $table = 'types';

    protected $fillable = [
        'title', 'name', 'text', 'icon', 'color', 'status', 'parent_id', 'priority',
    ];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['name'] = str_slug($value);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }

    public function products()
    {
        return $this->hasMany(Product::class, 'type_id', 'id');
    }

    public function articles()
    {
        return $this->hasMany(Article::class, 'type_id', 'id');
    }
    public function activeProducts()
    {
        return $this->hasMany(Product::class, 'type_id', 'id')->where('status', 1);
    }
}
