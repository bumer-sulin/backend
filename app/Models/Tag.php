<?php

namespace App\Models;

use App\Services\Article\ArticleTrait;

class Tag extends EloquentModel
{
    use ArticleTrait;

    protected $table = 'tags';

    protected $fillable = [
        'name', 'title', 'slug', 'tooltip', 'icon', 'color', 'status',
    ];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['name'] = str_slug($value);
    }

    public function setIconAttribute($value)
    {
        $this->attributes['icon'] = str_replace(' ', '_', trim($value));
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function gallery()
    {
        return $this->morphMany(Image::class, 'imageable')
            ->where('type', 'brand')
            ->orderBy('priority', 'desc');
    }

    public function articles()
    {
        return $this->BelongsToMany(Article::class, 'article_tags')->orderBy('title', 'asc');
    }

    public function activeArticles()
    {
        return $this->activeQuery($this->articles());
    }

}
