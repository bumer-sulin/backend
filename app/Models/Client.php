<?php

namespace App\Models;

class Client extends EloquentModel
{
    protected $table = 'clients';

    protected $fillable = [
        'first_name', 'last_name', 'second_name', 'phone', 'email', 'status'
    ];


    public $statuses = [
        0 => 'Не активный',
        1 => 'Активный',
    ];
}
