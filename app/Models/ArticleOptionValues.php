<?php

namespace App\Models;

class ArticleOptionValues extends EloquentModel
{
    protected $table = 'article_option_values';

    protected $fillable = [
        'article_id', 'option_id', 'parameter_id', 'value', 'priority'
    ];


    public function parameter()
    {
        return $this->hasOne(OptionParameter::class, 'id', 'parameter_id');
    }

    public function option()
    {
        return $this->hasOne(Option::class, 'id', 'option_id');
    }
}
