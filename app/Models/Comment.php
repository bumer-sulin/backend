<?php

namespace App\Models;

use App\Services\Comment\Commentable;
use App\Services\Modelable\Parentable;

/**
 * Class Comment
 * @package Actuallymab\LaravelComment\Models
 *
 * @property string $comment
 * @property float $rate
 * @property boolean $approved
 * @property integer $commentable_id
 * @property string $commentable_type
 * @property integer $commented_id
 * @property string $commented_type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Comment extends EloquentModel
{
    use Commentable;
    use Parentable;

    protected $canBeRated = true;
    protected $mustBeApproved = true;

    protected $fillable = [
        'comment', 'rate', 'approved', 'commented_id', 'commented_type', 'commentable_id', 'commentable_type', 'parent_id', 'reply_id',
    ];

    protected $casts = [
        'approved' => 'boolean'
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не одобрен',
        1 => 'Одобрен',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function commented()
    {
        return $this->morphTo();
    }

    /**
     * @return $this
     */
    public function approve()
    {
        $this->approved = true;
        $this->save();

        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function loveUsers()
    {
        return $this->BelongsToMany(User::class, 'love_user_comment')
            ->withPivot('like as like', 'dislike as dislike');
    }

    public function reply()
    {
        return $this->belongsTo(Comment::class);
    }
}
