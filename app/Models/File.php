<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Modelable\Statusable;

class File extends EloquentModel
{
    protected $table   = 'files';
    protected $guarded = ['id'];

    protected $fillable = [
        'type', 'fileable_id', 'fileable_type', 'filename', 'name', 'priority', 'status'
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Скрыто',
        1 => 'Активно',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Скрыто'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Активно'
        ],
    ];

    public function lyric()
    {
        return $this->hasOne(File::class, 'fileable_id', 'id')->where('type', 'lyric');
    }
    public function gtp()
    {
        return $this->hasOne(File::class, 'fileable_id', 'id')->where('type', 'gtp');
    }
}
