<?php

namespace App\Models;

class InvoiceOption extends EloquentModel
{
    protected $table = 'invoice_options';

    protected $fillable = [
        'invoice_id',
        'coordinates',
        'address',
        'address_yandex',
        'index',
    ];


    public function setCoordinatesAttribute($value)
    {
        $this->attributes['coordinates'] = json_encode($value);
    }

    public function getCoordinatesAttribute($value)
    {
        return json_decode($value, true);
    }

}
