<?php

namespace App\Models;

class Social extends EloquentModel
{
    protected $table = 'socials';

    protected $fillable = [
        'user_id', 'type', 'title', 'link', 'priority', 'status',
    ];

    private $types = [
        //0 => ['title' => 'Неизвестно', 'fa-icon' => ''],
        1 => [
            'title' => 'vk',
            'name' => 'vk',
            'fa-icon' => 'fa-vk',
        ],
        2 => [
            'title' => 'twitter',
            'name' => 'twitter',
            'fa-icon' => 'fa-twitter',
        ],
        3 => [
            'title' => 'telegram',
            'name' => 'telegram',
            'fa-icon' => 'fa-telegram',
        ],
        4 => [
            'title' => 'google-plus',
            'name' => 'google-plus',
            'fa-icon' => 'fa-google-plus',
        ],
        5 => [
            'title' => 'facebook',
            'name' => 'facebook',
            'fa-icon' => 'fa-facebook',
        ],
        6 => [
            'title' => 'odnoklassniki',
            'name' => 'odnoklassniki',
            'fa-icon' => 'fa-odnoklassniki',
        ],
        7 => [
            'title' => 'skype',
            'name' => 'skype',
            'fa-icon' => 'fa-skype',
        ],
        8 => [
            'title' => 'whatsapp',
            'name' => 'whatsapp',
            'fa-icon' => 'fa-whatsapp',
        ],
        9 => [
            'title' => 'slack',
            'name' => 'slack',
            'fa-icon' => 'fa-slack',
        ],
        10 => [
            'title' => 'youtube',
            'name' => 'youtube',
            'fa-icon' => 'fa-youtube',
        ],
        11 => [
            'title' => 'instagram',
            'name' => 'instagram',
            'fa-icon' => 'fa-instagram',
        ],
    ];

    public function getTypes()
    {
        return $this->types;
    }
}
