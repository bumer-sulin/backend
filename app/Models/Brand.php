<?php

namespace App\Models;

class Brand extends EloquentModel
{
    protected $table = 'brands';

    protected $fillable = [
        'title', 'slug', 'status',
    ];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['slug'] = str_slug($value);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function gallery()
    {
        return $this->morphMany(Image::class, 'imageable')
            ->where('type', 'brand')
            ->orderBy('priority', 'desc');
    }

    public function products()
    {
        return $this->BelongsToMany(Product::class, 'product_brands')->orderBy('title', 'asc');
    }

}
