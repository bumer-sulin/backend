<?php

namespace App\Models;

use App\Services\Article\ArticleTrait;
use App\Services\Modelable\Parentable;

class Category extends EloquentModel
{
    use Parentable;
    use ArticleTrait;

    protected $table = 'categories';

    protected $fillable = [
        'title', 'name', 'text', 'status', 'parent_id', 'priority', 'color',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не активно',
        1 => 'Активно',
        2 => 'Выводить секцией',
    ];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['name'] = str_slug($value);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }
    public function gallery()
    {
        return $this->morphMany(Image::class, 'imageable')
            ->where('type', 'category')
            ->orderBy('priority', 'desc');
    }


    public function products()
    {
        return $this->hasMany(Product::class, 'category_id', 'id');
    }

    public function articles()
    {
        return $this->hasMany(Article::class, 'category_id', 'id');
    }

    public function activeArticles()
    {
        return $this->activeQuery($this->hasMany(Article::class, 'category_id', 'id'));
    }

    public function articlesSection()
    {
        return $this->activeQuery($this->hasMany(Article::class, 'category_id', 'id'))
            ->orderBy('published_at', 'desc')
            ->take(8);
    }

    public function activeProducts()
    {
        return $this->hasMany(Product::class, 'category_id', 'id')->where('status', 1);
    }

    public function getParents($type = 'product')
    {
        $oCategory = $this;
        $oParents = collect();
        if ($type === 'product') {
            $oParents->push($oCategory);
        }
        while (!is_null($this->getParent($oCategory))) {
            $oCategory = $this->getParent($oCategory);
            if (!is_null($oCategory)) {
                $oParents->push($oCategory);
            }
        }
        $oParents = $oParents->sortBy(function ($product, $key) {
            return $product;
        });

        return $oParents;
    }

    private function getParent($oCategory)
    {
        if (!is_null($oCategory->parent_id)) {
            return $oCategory->parent;
        } else {
            return null;
        }
    }

}
