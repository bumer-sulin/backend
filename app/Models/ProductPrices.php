<?php
/**
 * Created by PhpStorm.
 * User: Dmitri
 * Date: 20.02.2018
 * Time: 16:27
 */

namespace App\Models;


class ProductPrices extends EloquentModel
{
    protected $table = 'product_prices';

    protected $fillable = [
        'product_id', 'amount', 'old', 'status'
    ];

    public function getAmountFormatAttribute()
    {
        //list($whole, $decimal) = explode('.', $this->amount);
        //list($whole, $decimal) = sscanf($this->amount, '%d.%d');
        //$decimal = intval($decimal) === 0 ? '00' : $decimal;
        return number_format($this->amount, 0, '', ' ');
        //return number_format($whole, 0, '', ' ').'<i>.'.$decimal.'</i>';
    }

    public function getOldFormatAttribute()
    {
        //list($whole, $decimal) = explode('.', $this->old);
        //list($whole, $decimal) = sscanf($this->old, '%d.%d');
        //$decimal = intval($decimal) === 0 ? '00' : $decimal;
        return number_format($this->old, 0, '', ' ');
        //return number_format($whole, 0, '', ' ').'<i>.'.$decimal.'</i>';
    }
}