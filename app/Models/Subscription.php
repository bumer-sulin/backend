<?php

namespace App\Models;

use Illuminate\Support\Facades\Crypt;

class Subscription extends EloquentModel
{
    protected $table = 'subscriptions';

    protected $fillable = [
        'type', 'user_id', 'email', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не активно',
        1 => 'Активно',
        2 => 'Отписан',
    ];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    private $types = [
        //0 => ['title' => 'Неизвестно', 'fa-icon' => ''],
        1 => [
            'title' => 'На все новости',
            'key' => 'ticker',
            'type' => 'full',
        ],
        2 => [
            'title' => 'На отдельные категории',
            'key' => 'logo',
            'type' => 'categories',
        ],
    ];

    public function isFull()
    {
        return $this->types[$this->type]['type'] === 'full';
    }

    public function isCategories()
    {
        return $this->types[$this->type]['type'] === 'categories';
    }

    public function getTypes()
    {
        return $this->types;
    }

    public function categories()
    {
        return $this->BelongsToMany(Category::class, 'subscription_category');
    }

    public function getUnsubscribeUrl($address)
    {
        return url('/unsubscribe/'.Crypt::encrypt($address));
    }
}
