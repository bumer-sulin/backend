<?php

namespace App\Models;

class Payment extends EloquentModel
{
    protected $table = 'payments';

    protected $fillable = [
        'price', 'purpose', 'external_data', 'status'
    ];

    public $statuses = [
        0 => 'Не активный',
        1 => 'Ждет закрытия',
        2 => 'Закрыт успешно',
        3 => 'Закрыт с провалом',
        4 => 'Истек',
        5 => 'Тест',
    ];

}
