<?php

namespace App\Models;

use App\Services\Comment\Commentable;
use App\Services\Modelable\DateFormatable;
use App\Services\Modelable\Publishedable;
use App\Services\Modelable\Typeable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Article
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $description
 * @property string $text
 * @property null|string|Carbon $published_at
 * @property null|string|Carbon $publication_at
 * @property null|string|Carbon $release_at
 * @property int $status
 *
 *
 * @property int|null|Type $type
 * @property int|null|Category $category
 *
 * @property string|null $valueAuthor
 *
 */
class Article extends EloquentModel
{
    use DateFormatable;
    use SoftDeletes;
    use Publishedable;
    use Typeable;
    use Commentable;

    protected $table = 'articles';

    protected $fillable = [
        'title', 'description', 'name', 'category_id', 'type_id', 'creator_id', 'text', 'icon', 'published_at', 'publication_at', 'status', 'keywords',
    ];

    public $types = [
        //0 => 'Нет типа',
        1 => [
            'title' => 'Новость',
            'key' => 'news',
        ],
        2 => [
            'title' => 'Статья',
            'key' => 'article',
        ],
        3 => [
            'title' => 'Слайдер',
            'key' => 'slider',
        ],
    ];

    public $dates = [
        'published_at',
        'publication_at',
        'deleted_at'
    ];

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_MAIN = 2;

    const TYPE_NEWS = 1;
    const TYPE_ARTICLE = 2;
    const TYPE_SLIDER = 3;

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не активно',
        1 => 'Активно',
        2 => 'Выводить главной',
    ];

    /**
     * The formats attributes for model
     *
     * @var array
     */
    protected $formats = [
        0 => 'd.m.Y',
        1 => 'Y-m-d',
        2 => 'd %B Y',
    ];

    public function setTextAttribute($value)
    {
        $value = str_replace('<div class="detail--iframe">', '', $value);
        $value = str_replace('</iframe></div>', '</iframe>', $value);

        $value = str_replace('<iframe', '<div class="detail--iframe"><iframe', $value);
        $value = str_replace('</iframe>', '</iframe></div>', $value);

        preg_match_all('/<div\s+class="detail--image"[^>]+>(.*)<\/div>/', $value, $aImageWrappers);

        $aIssetImages = [];
        if (isset($aImageWrappers[0]) && count($aImageWrappers[0]) !== 0) {
            foreach ($aImageWrappers[0] as $aImageWrapper) {

                preg_match_all('/<img[^>]+>/i', $aImageWrapper, $aImages);
                preg_match_all('/style="(.*)"><img/i', $aImageWrapper, $aStyles);

                if (isset($aStyles[1][0]) && isset($aImages[0][0])) {
                    $value = str_replace($aImageWrapper, '<div class="detail--image" style="'.$aStyles[1][0].'">'.$aImages[0][0].'</div>', $value);
                    $aIssetImages[] = $aImages[0][0];
                } elseif (isset($aImages[0][0])) {
                    $value = str_replace($aImageWrapper, '<div class="detail--image">'.$aImages[0][0].'</div>', $value);
                    $aIssetImages[] = $aImages[0][0];
                }
            }
        }
        preg_match_all('/<img[^>]+>/i', $value, $aImages);
        if (isset($aImages[0]) && count($aImages[0]) !== 0) {
            foreach ($aImages[0] as $image) {
                if (!in_array($image, $aIssetImages)) {
                    $value = str_replace($image, '<div class="detail--image">'.$image.'</div>', $value);
                }
            }
        }

        $value = str_replace('<p></p>', '', $value);

        //preg_match_all('/<div class="detail--image"[^>]+>/i', $value, $aImageWrappers);
        //preg_match_all('/<img[^>]+>/i', $value, $aImages);

        $this->attributes['text'] = $value;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return in_array($this->status, [self::STATUS_ACTIVE, self::STATUS_MAIN])
            && $this->publication_at !== null
            && $this->published_at !== null
            && $this->published_at < now()->startOfDay();
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query
            ->whereIn('status', [self::STATUS_ACTIVE, self::STATUS_MAIN])
            ->where('published_at', '<=', now()->startOfDay())
            ->whereNotNull('publication_at');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeArticle(Builder $query): Builder
    {
        return $query->whereIn('type_id', [self::TYPE_NEWS, self::TYPE_ARTICLE]);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('published_at', 'desc');
    }


    /**
     * @return mixed
     */
    public function getFormatPublishedAtAttribute()
    {
        return array_keys($this->formats)[2];
    }

    public function gallery()
    {
        return $this->morphMany(Image::class, 'imageable')
            ->where('type', 'article')
            ->orderBy('priority', 'desc');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }


    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }

    public function getBeginningAtEventAttribute()
    {
        return $this->dmYHi($this->created_at);
    }

    public function tags()
    {
        return $this->BelongsToMany(Tag::class, 'article_tags');
    }

    public function activeTags()
    {
        return $this->BelongsToMany(Tag::class, 'article_tags')->where('status', 1);
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function type()
    {
        return $this->hasOne(Type::class, 'id', 'type_id');
    }

    public function creator()
    {
        return $this->hasOne(User::class, 'id', 'creator_id');
    }

    public function getPublishedStringAttribute()
    {
        if ($this->published_at->format('d.m.Y') === now()->format('d.m.Y')) {
            return 'Сегодня';
        }
        if ($this->published_at->format('d.m.Y') === now()->subDay()->format('d.m.Y')) {
            return 'Вчера';
        }

        return $this->created_at->format('F d, Y');
    }


    public function values()
    {
        return $this->hasMany(ArticleOptionValues::class, 'article_id')->with('option');
    }

    public function getValueSiteAttribute()
    {
        $value = $this->values->where('option.key', 'site')->first();
        return !is_null($value) ? $value->value : '';
    }

    /**
     * @return string
     */
    public function getValueAuthorAttribute()
    {
        $value = $this->values->whereIn('option.key', [Option::KEY_AUTHOR, 'avtor', 'podpis-avtora'])->first();
        return !is_null($value) ? $value->value : '';
    }

    /**
     * @return string
     */
    public function getValuePhotoAuthorAttribute()
    {
        $value = $this->values->whereIn('option.key', [Option::KEY_PHOTO_AUTHOR])->first();
        return !is_null($value) ? $value->value : null;
    }

    public function getDownloadLinkAttribute()
    {
        $value = $this->values->where('option.key', 'download_link')->first();
        return !is_null($value) ? $value->value : '';
    }

    public function getParameterYoutubeIdAttribute()
    {
        $value = $this->values->where('option.key', 'youtube_id')->first();
        return !is_null($value) ? $value->value : '';
    }

    public function getParameterSourceAttribute()
    {
        $value = $this->values->where('option.key', 'source')->first();
        return !is_null($value) ? $value->value : '';
    }


    public function getDescriptionShortAttribute()
    {
        //return str_limit($this->description, 210);
        return str_limit($this->description, 80);
    }

    /**
     * @return mixed
     */
    public function getTypeTitleAttribute()
    {
        return $this->types[$this->type_id]['title'];
    }

    /**
     * @return mixed
     */
    public function getTypeKeyAttribute()
    {
        return $this->types[$this->type_id]['key'];
    }

    /**
     * @return string
     */
    public function getLinkToArticle()
    {
        return url('/article/'.$this->id.'/'.$this->name);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return route('index.article', ['id' => $this->id, 'slug' => $this->name]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function statistic()
    {
        return $this->hasOne(ArticleStatistics::class, 'article_id', 'id');
    }

    /**
     * @return bool
     */
    public function isSlider()
    {
        return $this->types[$this->type_id]['key'] === 'slider';
    }

    public function commentsUpdateStatistic()
    {
        $this->statistic()->update([
            'comments' => $this->approvedComments()->count(),
        ]);
    }
}
