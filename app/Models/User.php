<?php

namespace App\Models;

use App\Services\Comment\CanComment;
use App\Services\Modelable\Statusable;
use Cartalyst\Sentinel\Activations\EloquentActivation as SentinelEloquentActivation;
use Cartalyst\Sentinel\Reminders\EloquentReminder as SentinelEloquentReminder;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

/**
 * Class User
 * @package App\Models
 *
 * @method User find()
 * @property \Illuminate\Database\Eloquent\Relations\BelongsToMany loveComments()
 *
 */
class User extends EloquentUser
{
    use Notifiable;
    use CanComment;
    use Statusable;

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login',
        'email',
        'password',
        'permissions',
        'first_name',
        'last_name',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Login by phone
     * @var array
     */
    protected $loginNames = ['email'];


    public function getRoleAttribute()
    {
        return $this->roles()->first();
    }

    public function isAdmin()
    {
        return $this->role->slug === 'admin';
    }

    public function isDeveloper()
    {
        return $this->role->slug === 'developer';
    }

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Заблокирован',
        1 => 'Активен',
    ];

    /**
     * User activation
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function activation()
    {
        return $this->hasOne(SentinelEloquentActivation::class, 'user_id', 'id');
    }

    /**
     * User reminder
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function reminder()
    {
        return $this->hasOne(SentinelEloquentReminder::class, 'user_id', 'id');
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function gallery()
    {
        return $this->morphMany(Image::class, 'imageable')
            ->where('type', 'user')
            ->orderBy('priority', 'desc');
    }

    public function socials()
    {
        return $this->hasMany(Social::class, 'user_id')->orderBy('priority', 'desc');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function loveComments()
    {
        return $this->BelongsToMany(Comment::class, 'love_user_comment')
            ->withPivot('like as like', 'dislike as dislike');
    }

    public function hasLoveComment($comment_id)
    {
        return $this->loveComments()->where('comment_id', $comment_id)->count() > 0;
    }

    public function getLoveComment($comment_id)
    {
        return $this->loveComments()->where('comment_id', $comment_id)->first();
    }

    public function setLoveCommentLike($comment_id)
    {
        $this->loveComments()->attach($comment_id, [
            'like' => 1
        ]);
    }

    public function setLoveCommentDislike($comment_id)
    {
        $this->loveComments()->attach($comment_id, [
            'dislike' => -1
        ]);
    }

    /**
     * @param string $hash
     * @return User
     */
    public static function findByHash(string $hash): User
    {
        $email = Crypt::decrypt($hash);
        return User::where('email', $email)->first();
    }

    public function getHashAttribute()
    {
        return Crypt::encrypt($this->email);
    }

    /**
     * Get the e-mail address where password reset links are sent.
     *
     * @return string
     */
    public function getEmailForPasswordReset()
    {
        return $this->email;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @param  string  $isAdmin
     * @return void
     */
    public function sendPasswordResetNotification($token, $isAdmin)
    {
        try {
            Mail::to($this->email)->send(new \App\Mail\SendResetLinkToClient($token, $isAdmin));
        } catch (\Exception $e) {
            Log::critical($e->getMessage());
        }
    }
}
