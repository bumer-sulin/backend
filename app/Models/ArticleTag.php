<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Modelable\Statusable;
use App\Services\Modelable\DateFormatable;

class ArticleTag extends EloquentModel
{
    protected $table = 'article_tags';

    protected $fillable = [
        'article_id', 'tag_id'
    ];
}
