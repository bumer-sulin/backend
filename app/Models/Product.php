<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Services\Comment\Commentable;
use App\Services\Modelable\Statusable;

class Product extends Model
{
    use Commentable;
    use Statusable;

    protected $canBeRated = true;
    protected $mustBeApproved = true;

    protected $fillable = [
        'title', 'status', 'text', 'url', 'category_id', 'type_id', 'pay_count', 'priority', 'published_at'
    ];

    public $dates = [
        'published_at'
    ];

    protected $with = [
        'images'
    ];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['url'] = str_slug($value);
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function gallery()
    {
        return $this->morphMany(Image::class, 'imageable')
            ->where('type', 'product')
            ->orderBy('priority', 'desc');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function type()
    {
        return $this->hasOne(Type::class, 'id', 'type_id');
    }

    public function price()
    {
        return $this->hasOne(ProductPrices::class, 'product_id', 'id');
    }

    public function seo()
    {
        return $this->hasOne(ProductParameters::class, 'product_id', 'id')->where('type', 1);
    }

    public function certificates()
    {
        return $this->BelongsToMany(Certificate::class, 'product_certificates')
            ->withPivot('product_id', 'priority', 'id')
            ->orderBy('product_certificates.priority', 'desc')
            ->withTimestamps();
    }

    public function brands()
    {
        return $this->BelongsToMany(Brand::class, 'product_brands');
    }

    public function activeBrands()
    {
        return $this->BelongsToMany(Brand::class, 'product_brands')->where('status', 1);
    }

    public function tags()
    {
        return $this->BelongsToMany(Tag::class, 'product_tags');
    }

    public function activeTags()
    {
        return $this->BelongsToMany(Tag::class, 'product_tags')->where('status', 1);
    }

    public function getBrandAttribute()
    {
        return $this->brands->first();
    }

    public function getActiveBrandAttribute()
    {
        return $this->activeBrands->first();
    }


    public function activeCertificates()
    {
        return $this->BelongsToMany(Certificate::class, 'product_certificates')
            ->withPivot('product_id', 'priority', 'id')
            ->where('certificates.status', 1)
            ->orderBy('product_certificates.priority', 'desc')
            ->withTimestamps();
    }


    protected $columnsTable = [
        'id' => '#',
        'image' => 'IMG',
        'title' => 'Заголовок',
        'status' => 'Статус'
    ];
    protected $columnsForm = [
        'title' => [
            'type' => 'string',
            'title' => 'Заголовок'
        ],
        'category_id' => [
            'type' => 'select',
            'title' => 'Категория',
            'select' => [
                'key' => 'id',
                'value' => 'title',
                'cache' => 'oComposerCategories'
            ]
        ],
        'description' => [
            'type' => 'text',
            'title' => 'Описание'
        ]
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не опубликовано',
        1 => 'Опубликовано',
    ];

    protected $statusIcons = [
        0 => [
            'class' => 'badge badge-default',
            'title' => 'Не опубликовано'
        ],
        1 => [
            'class' => 'badge badge-success',
            'title' => 'Опубликовано'
        ],
    ];

    public function getColumns($type)
    {
        switch($type) {
            case 'form':
                return $this->columnsForm;
            case 'table':
                return $this->columnsTable;
            case 'fillable':
                return $this->fillable;
            default:
                return null;
        }
    }

    public function values()
    {
        return $this->belongsToMany(OptionParameter::class, 'product_option_values', 'product_id', 'parameter_id')
            ->withPivot('product_id as product_id', 'parameter_id as parameter_id', 'priority as priority', 'id as pivot_id')
            ->orderBy('product_option_values.priority', 'desc');
    }

}
