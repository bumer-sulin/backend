<?php

namespace App\Models;

class Image extends EloquentModel
{
    protected $table = 'images';
    protected $guarded = ['id'];

    public function imageable()
    {
        return $this->morphTo('branches');
    }
}
