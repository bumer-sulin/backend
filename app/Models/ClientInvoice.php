<?php

namespace App\Models;

class ClientInvoice extends EloquentModel
{
    protected $table = 'client_invoices';

    protected $fillable = [
        'invoice_id',
        'client_id',
    ];

}
