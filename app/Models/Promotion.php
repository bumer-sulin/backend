<?php

namespace App\Models;

class Promotion extends EloquentModel
{
    protected $table = 'promotions';

    protected $fillable = [
        'type', 'key', 'title', 'link', 'text', 'options', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не активно',
        1 => 'Активно',
    ];

    private $types = [
        //0 => ['title' => 'Неизвестно', 'fa-icon' => ''],
        1 => [
            'title' => 'Бегущая строка',
            'key' => 'ticker',
            'type' => 'strings',
        ],
        2 => [
            'title' => 'Главный баннер',
            'key' => 'main',
            'type' => 'image',
            'size' => 'Ширина не меньше 1170 пикселей',
        ],
        3 => [
            'title' => 'В сайдбаре',
            'key' => 'sidebar',
            'type' => 'image',
            'size' => '330x400',
        ],
        4 => [
            'title' => 'В детальном просмотре',
            'key' => 'article',
            'type' => 'image',
            'size' => 'Ширина не меньше 860 пикселей',
        ],
    ];

    public function getTypes()
    {
        return $this->types;
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function gallery()
    {
        return $this->morphMany(Image::class, 'imageable')
            ->where('type', 'promotion')
            ->orderBy('priority', 'desc');
    }

    public function isStrings()
    {
        return $this->types[$this->type]['type'] === 'strings';
    }

    public function isImage()
    {
        return $this->types[$this->type]['type'] === 'image';
    }

    public function setOptionsAttribute($value)
    {
        $this->attributes['options'] = json_encode($value);
    }

    public function getOptionsAttribute($value)
    {
        return json_decode($value ,true);
    }
}
