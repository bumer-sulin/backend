<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Blade;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class BladeDirectives
{
    /**
     * Директирвы @switch @case @break @default @endswitch
     */
    public static function switchCase()
    {
        Blade::extend(function($value, $compiler) {
            $value = preg_replace('/(?<=\s)@switch\((.*)\)(\s*)@case\((.*)\)(?=\s)/', '<?php switch($1):$2case $3: ?>', $value);
            $value = preg_replace('/(?<=\s)@endswitch(?=\s)/', '<?php endswitch; ?>', $value);

            $value = preg_replace('/(?<=\s)@case\((.*)\)(?=\s)/', '<?php case $1: ?>', $value);
            $value = preg_replace('/(?<=\s)@default(?=\s)/', '<?php default: ?>', $value);
            $value = preg_replace('/(?<=\s)@break(?=\s)/', '<?php break; ?>', $value);

            return $value;
        });

        Blade::directive('prod', function ($beta) {
            return "<?php if (app()->environment('production')): ?>";
        });
        Blade::directive('endprod', function ($beta) {
            return "<?php endif; ?>";
        });
    }


    public static function custom()
    {
        Blade::directive('wordCount', function($expression) {
            list($number, $keys) = explode(',',str_replace(['(',')',' '], '', $expression));
            $mod = $number % 100;
            $suffix_key = ($mod > 7 && $mod < 20) ? 2: $keys[min($mod % 10, 5)];
            return $keys[$suffix_key];
        });

        Blade::directive('classIf', function($expression) {
            return self::classIf($expression);
        });
        Blade::directive('wordByCount', function($expression) {
            return self::wordByCount($expression);
        });
    }

    public static function classIf($expression)
    {
        //$array = explode(',', $expression);
        $array = explode(',', $expression);
        $expression = $array[0];
        $class1 = $array[1];

        $class1 = str_replace("'",'', $class1);
        $class1 = str_replace('"','', $class1);

        if (isset($array[2])) {
            $class2 = $array[2];
            $class2 = str_replace("'",'', $class2);
            $class2 = str_replace('"','', $class2);
        } else {
            $class2 = '';
        }
        $value = "<?php if ({$expression}): ?>";
        $value .= $class1;
        $value .= "<?php else: ?>";
        $value .= $class2;
        $value .= "<?php endif; ?>";
        return $value;
    }

    public static function wordByCount($expression, $originalCount = null)
    {
        $array = explode(',', $expression);
        $count = $array[0];
        $words = $array[1];

        foreach($array as $key => $value) {
            dd($count);
            dd(array_search('[', $value));
            dd(array_search('[', $value));
        }

        if ($count === 0) {
            return $words[2];
        }
        if ($count === 1) {
            if (!is_null($originalCount)) {
                $counts = $originalCount;
            }
            return $words[0];
        }
        if ($count > 1 && $count < 5) {
            if (!is_null($originalCount)) {
                $count = $originalCount;
            }
            return $words[1];
        }
        if ($count > 5 && $count < 21) {
            if (!is_null($originalCount)) {
                $count = $originalCount;
            }
            return $words[2];
        }
        if ($count > 20) {
            //return self::wordWithCounts(intval(substr($count, 1)), $words, $count);
        }
        return null;
    }
}
