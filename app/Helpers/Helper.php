<?php

namespace App\Helpers;

class Helper
{
    /**
     * Create new class object for get attributes in views
     *
     * @param $class
     * @return string
     */
    public static function wordWithCounts($counts, $words, $gender = null, $originalCount = null)
    {
        $oCount = $counts;

        if (!is_null($originalCount)) {
            $counts = $originalCount;
        }
        $searchWord = '';
        if (!is_null($gender) && $counts === 1) {
            if ($gender) { // жр
                $searchWord = 'Найдена';
            } else { // мр
                $searchWord = 'Найден';
            }
        }
        if ($counts === 0) {
            return title_case($words[2]).' не найдено.';
        }
        if ($counts === 1) {
            return $searchWord.' <strong class="admin-table-counter">'.$oCount.'</strong> '.$words[0];
        }
        if ($counts > 1 && $counts < 5) {
            return 'Найдено <strong class="admin-table-counter">'.$oCount.'</strong> '.$words[1];
        }
        if ($counts >= 5 && $counts < 21) {
            return 'Найдено <strong class="admin-table-counter">'.$oCount.'</strong> '.$words[2];
        }
        if ($counts > 20) {
            return self::wordWithCounts($oCount, $words, $gender, intval(substr($counts, 1)));
        }
        return null;
    }


    public static function formatPhone($sPhone) {
        $sPhone = preg_replace('/[^0-9]/','',$sPhone);
        if(strlen($sPhone) < 10) return(False);
        $nCode = substr($sPhone, 0,1);
        $sArea = substr($sPhone, 1,3);
        $sPrefix = substr($sPhone,4,3);
        $sNumber = substr($sPhone,7,2);
        $sNumberLast = substr($sPhone,9,2);
        $sPhone = '+'.$nCode.' ('.$sArea.') '.$sPrefix.' '.$sNumber.'-'.$sNumberLast;
        return $sPhone;
    }

}