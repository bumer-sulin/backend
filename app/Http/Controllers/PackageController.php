<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\SitemapGenerator;
use Lubusin\Decomposer\Controllers\DecomposerController;

class PackageController extends Controller
{
    /**
     * Sitemap by route /sitemap
     */
    public function sitemap()
    {
        $sitemap = Sitemap::create();

        $aArticles = (new ApiController())->articles((new Request())->merge(['sitemap' => 1]));

        $aArticles = json_decode($aArticles->getContent(), true)['data'];

        $sitemap->add(\Spatie\Sitemap\Tags\Url::create('/')->setLastModificationDate(now()));

        foreach ($aArticles as $aArticle) {
            $sitemap->add(\Spatie\Sitemap\Tags\Url::create('/article/'.$aArticle['id'].'/'.$aArticle['slug'])->setLastModificationDate(now()));
        }

        $sitemap->writeToFile(public_path('sitemap.xml'));

        return redirect('/sitemap.xml');
    }

    /**
     * Decompose packages
     *
     * @return \Illuminate\View\View
     */
    public function decompose()
    {
        return (new DecomposerController())->index();
    }
}
