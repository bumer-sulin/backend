<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Services\ResponseCommonTrait;
use App\Services\Toastr\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Auth\Sentinel\AuthenticatesUsers;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Route;

class LoginController extends AuthController
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    use ResponseCommonTrait;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * @var User
     */
    protected $user;

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view($this->theme.'.auth.login');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showActivateForm()
    {
        return view($this->theme.'.auth.activate');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        @parent::__construct();
        $this->middleware('guest', ['except' => 'logout']);

        $this->redirectTo = route('admin.dashboard.index');
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function logout(Request $request)
    {
        $oUser = Sentinel::bypassCheckpoints(function() { return Sentinel::getUser(); });
        Sentinel::logout($oUser, true);

        return $this->success([
            'redirect' => $this->redirectTo
        ], 'Выход успешно выполнен');
    }

    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @return array
     */
    protected function authenticated(Request $request)
    {
        $this->user->hash = Crypt::encrypt($this->user->email);

        return $this->success([
            'redirect' => $this->redirectTo,
            'hash' => $this->user->hash,
        ], 'Вход успешно выполнен');
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        if ($request->has('adminToken')) {
            $this->isAdmin = true;
            $this->setRedirectTo(route('admin.dashboard.index'));
        } else {
            $this->isAdmin = false;
            $this->setRedirectTo('/');
        }

        $validation = $this->validateLogin($request);

        if ($validation->fails()) {
            return responseCommon()->validationMessages($validation);
        }
        $this->user = $this->authenticate($request);
        if (isset($this->user->email)) {
            return $this->authenticated($request);
        }
        return $this->authenticate($request);
    }

    /**
     * @param Request $request
     * @param $hash
     * @return array
     */
    public function loginByHash(Request $request, $hash)
    {
        $this->user = $this->getUserByHash($hash);

        if (is_null($this->user)) {
            return $this->error([]);
        }

        Sentinel::login($this->user);

        return $this->authenticated($request);
    }

    /**
     * @param $hash
     * @return mixed
     */
    public function getUserByHash($hash)
    {
        $email = Crypt::decrypt($hash);
        return User::where('email', $email)->first();
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function activate(Request $request)
    {
        $oUser = Sentinel::bypassCheckpoints(function() { return Sentinel::getUser(); });
        if (Activation::complete($oUser, $request->get('code'))) {
            return [
                'success' => true,
                'redirect' => $this->redirectTo
            ];
        } else {
            return response()->json([
                'code' => ['Неверный код.']
            ], 401);
        }
    }

    /**
     * @param $redirectTo
     */
    private function setRedirectTo($redirectTo)
    {
        $this->redirectTo = $redirectTo;
    }
}
