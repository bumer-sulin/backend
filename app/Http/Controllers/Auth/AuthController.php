<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    protected $theme = 'admin';

    public function __construct()
    {
        $this->theme = config('site.theme');
    }
}
