<?php

namespace App\Http\Controllers\Auth\Sentinel;

use App\Models\User;
use Illuminate\Http\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use App\Events\ChangeCacheEvent;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

trait RegistersUsers
{

    private $keyMessage = 'error';

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validation = $this->validateRegister($request);

        if ($validation->fails()) {
            return responseCommon()->validationMessages($validation);
        }

        $oUser = $this->create($request);
        $this->afterRegister();

        $this->afterLogin();

        Sentinel::loginAndRemember($oUser);
        $this->beforeLogin();

        return $this->registered($request, $oUser);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     */
    protected function create(Request $request)
    {
        $data = $this->beforeCreate($request);

        $oUser = Sentinel::register($data);

        $role = Sentinel::findRoleBySlug('user');
        $role->users()->attach($oUser);

        $oActivation = Activation::create($oUser);
        $oActivation->code = Str::random(48);
        $oActivation->save();
        Activation::complete($oUser, $oActivation->code);

        $credential = [
            'email' => $request->email,
            'password' => $request->password,
        ];

        $oUser = Sentinel::authenticate($credential, false);

        return $oUser;
    }

    public function activateUser($oUser)
    {
        $role = Sentinel::findRoleBySlug('user');
        $role->users()->attach($oUser);

        $oActivation = Activation::create($oUser);
        $oActivation->code = Str::random(48);
        $oActivation->save();
        Activation::complete($oUser, $oActivation->code);
    }


    protected function beforeCreate(Request $request)
    {
        $input['password'] = $request->password;
        $input['login'] = $request->email;
        $input['first_name'] = $request->first_name;
        $input['last_name'] = $request->has('last_name') ? $request->get('last_name') : null;
        return $input;
    }





    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */

    /**
     * @param $request
     * @return \Illuminate\Validation\Validator
     */
    protected function validateRegister($request): \Illuminate\Validation\Validator
    {
        return Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    protected function beforeLogin()
    {

    }

    protected function afterLogin()
    {

    }

    public function afterRegister()
    {
        event(new ChangeCacheEvent('user'));
    }


    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User  $user
     * @return mixed
     */
    protected function registered(Request $request, User $user)
    {
        return [
            'success' => true,
            'redirect' => url()->previous(),
            'message' => 'Регистрация прошла успешно.',
            'hash' => $user->hash,
        ];
    }
}
