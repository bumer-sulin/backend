<?php

namespace App\Http\Controllers\Auth\Sentinel;

use Illuminate\Http\Request;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Illuminate\Support\Facades\Validator;

trait AuthenticatesUsers
{
    /**
     * @var string
     */
    private $keyMessage = 'error';

    /**
     * @var bool
     */
    protected $isAdmin = false;

    /**
     * @param $oUser
     * @return \Illuminate\Http\JsonResponse
     */
    protected function validateUser($oUser)
    {
        if ($this->isAdmin && !$oUser->isAdmin() && !$oUser->isDeveloper()) {
            Sentinel::logout($oUser, true);
            return $this->errorMessage('Не верный логин или пароль.');
        }
        if ($oUser->status !== 1) {
            Sentinel::logout($oUser, true);
            return $this->errorMessage('Ваш аккаунт заблокирован в связи с нарушением Условий использования.');
        }
        return $oUser;
    }



    /**
     * @param Request $request
     * @return array
     */

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validateLogin(Request $request)
    {
        $validation = Validator::make($request->all(), [
            $this->username() => 'required|email|max:255',
            'password' => 'required|string',
        ]);
        return $validation;

//        $this->validate($request, [
//            $this->username() => 'required|email|max:255',
//            'password' => 'required|string',
//        ]);
    }

    protected function findUser(Request $request)
    {
        $credentials = [
            $this->username() => $request->email
        ];
        $oUser = Sentinel::findByCredentials($credentials);
        return $oUser;
    }

    protected function authenticateUsers(Request $request)
    {
        if (!is_null($this->findUser($request))) {

            $remember = isset($request->remember) && $request->remember ? $request->remember : 0;
            $credentials = [
                $this->username() => $request->email,
                'password'  => $request->password
            ];
            $this->beforeLogin();

            try {
                if ($oUser = Sentinel::authenticateAndRemember($credentials, $remember)) {
                    return $this->validateUser($oUser);
                } else {
                    return $this->errorMessage('Не верный логин или пароль.');
                }
            } catch (NotActivatedException $e) {
                $oUser = Sentinel::bypassCheckpoints(function() use ($credentials, $remember) {
                    return Sentinel::authenticateAndRemember($credentials, $remember);
                });
                return $this->validateUser($oUser);
            }
        } else {
            return $this->errorMessage('Не верный логин или пароль.');
        }
    }


    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }


    protected function authenticate(Request $request)
    {
        return $this->authenticateUsers($request);
    }

    protected function errorMessage($message)
    {
        return response()->json([
            'success' => false,
            'errors' => [
                $this->username() => $message,
            ],
            'status_code' => 422,
        ], 422);
    }

    protected function beforeLogin()
    {

    }

    protected function afterLogin()
    {

    }



}
