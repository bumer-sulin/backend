<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    private $isAdmin = false;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('admin.auth.passwords.reset', [
            'token' => $token,
            'email' => $request->email
        ]);
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        if ($request->has('adminToken')) {
            $this->isAdmin = true;
        }

        $validation = Validator::make($request->all(), $this->rules(), $this->validationErrorMessages());
        if ($validation->fails()) {
            return responseCommon()->validationMessages($validation);
        }

        $oUser = User::where('email', $request->get('email'))->first();

        if (is_null($oUser)) {
            return responseCommon()->validationMessages(null, [
                'email' => 'Пользователь с указанным email не найден.'
            ]);
        }

        if ($reminder = Reminder::complete($oUser, $request->get('token'), $request->get('password'))) {
            if (!$this->isAdmin) {
                return responseCommon()->success([
                    'message' => 'Пароль успешно сброшен.',
                    'hash' => $oUser->hash,
                ]);
            } else {
                $remember = 1;
                $credentials = [
                    'email' => $request->email,
                    'password'  => $request->password
                ];
                $oUser = Sentinel::bypassCheckpoints(function() use ($credentials, $remember) {
                    return Sentinel::authenticateAndRemember($credentials, $remember);
                });

                return responseCommon()->success([
                    'redirect' => '/'
                ], 'Пароль успешно сброшен.');
            }
        } else {
            return responseCommon()->validationMessages(null, [
                'email' => 'Ошибка сброса. Попробуйте попытку позже.'
            ]);
        }
    }
}
