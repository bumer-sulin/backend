<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Cartalyst\Sentinel\Laravel\Facades\Reminder;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends AuthController
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    private $isAdmin = false;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        @parent::__construct();
        $this->middleware('guest');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('admin.auth.passwords.email');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        if ($request->has('adminToken')) {
            $this->isAdmin = true;
        }

        $validation = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
        ]);
        if ($validation->fails()) {
            return responseCommon()->validationMessages($validation);
        }

        $oUser = User::where('email', $request->get('email'))->first();

        if (is_null($oUser)) {
            return responseCommon()->validationMessages(null, [
                'email' => 'Пользователь с указанным email не найден.'
            ]);
        }

        try {
            if (!Reminder::exists($oUser)) {
                $oModel = Reminder::create($oUser);
            } else {
                $oModel = $oUser->reminders->first();
            }

            $oUser->sendPasswordResetNotification($oModel->code, $this->isAdmin);

            if (!$this->isAdmin) {
                return responseCommon()->success([
                    'message' => 'Ссылка успешно отправлена.',
                ]);
            } else {
                return responseCommon()->success([],'Ссылка успешно отправлена.');
            }
        } catch (\Exception $e) {
            return responseCommon()->validationMessages(null, [
                'email' => 'Ошибка отправки. Попробуйте попытку позже.'
            ]);
        }
    }
}
