<?php

namespace App\Http\Controllers;

use App\Jobs\SendLog;
use App\Models\Article;
use App\Models\ArticleStatistics;
use App\Models\ArticleTag;
use App\Models\Category;
use App\Models\Tag;
use App\Models\Type;
use App\Models\Product;
use App\Models\User;
use App\Services\Image\Facades\ImagePath;
use App\Services\ResponseCommonTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use KekoApp\LaravelMetaTags\Facades\MetaTag;
use Faker\Factory as Faker;
use Carbon\Carbon;
use App\Events\TestPusher;
use App\Services\DomainService;
use Spatie\Sitemap\SitemapGenerator;

class IndexController_old extends Controller
{
    use ResponseCommonTrait;


    /**
     * Pagination url
     * @var string
     */
    private $indexPaginationUrl;

    /**
     * Table limit item
     * @var int
     */
    private $indexPaginationLimit = 16;

    public function __construct()
    {
        /**
         * Route for pagination
         */
        $this->indexPaginationUrl = route('index.articles.pagination');

    }

    public function pagination(Request $request)
    {
        $oQuery = $this->getProductsQuery($request);

        $oProducts = $oQuery['oQuery']
            ->paginate($this->indexPaginationLimit)
            ->withPath($this->indexPaginationUrl);

        $aSearch = $oQuery['aSearch'];

        return [
            'success' => true,
            'view' => [
                'list' => view('app.content.product.ul', [
                    'oProducts' => $oProducts
                ])->render(),
                'button' => view('app.content.product.button', [
                    'oProducts' => $oProducts,
                    'aSearch' => $aSearch
                ])->render(),
                'content'=> view('app.content.product.list', [
                    'oProducts' => $oProducts,
                    'aSearch' => $aSearch
                ])->render(),
            ],
            'count' => $oProducts->total()
        ];
    }

    private function getArticles($type)
    {
        $oArticles = collect([]);

        switch ($type) {
            case 'main':
                $oArticles = Article::where('status', 2)->take(3)->get();
                break;
            case 'popular':
                $oArticles = Article::whereIn('status', [1, 2])
                    ->take(6)
                    ->orderByDesc('view_count')
                    ->get();
                break;
            default:
                break;
        }

        return $oArticles;
    }


    public function index(Request $request)
    {
        $oMainArticles = $this->getArticles('main');
        $oPopularArticles = $this->getArticles('popular');


        $oQuery = $this->getArticlesQuery($request);
        $aSearch = $oQuery['aSearch'];

        $now = Carbon::now();

        $oArticles = $oQuery['oQuery']
            ->where('published_at', '<', $now->copy())
            ->orderBy('published_at', 'desc')
            ->paginate($this->indexPaginationLimit)
            ->withPath($this->indexPaginationUrl);


        return view('app.index', [
            'oMainArticles' => $oMainArticles,
            'oPopularArticles' => $oPopularArticles,
            'oArticles' => $oArticles,
            'aSearch' => $aSearch,
        ]);
    }

    private function getArticlesQuery(Request $request)
    {
        $oQuery =  Article::with(['type', 'type.parent', 'category', 'category.parent', 'category.parent.children', 'activeTags'])
            ->where('status', 1);

        $aSearch = [];

        if ($request->exists('type_id')) {
            $oType = Type::find($request->get('type_id'));
            View::share('oType', $oType);

            $treeChildren = $oType->treeChildren();
            $treeChildren = $treeChildren->merge([$oType]);

            $oQuery = $oQuery->whereIn('type_id', $treeChildren->pluck('id'));
            $aSearch['type_id'] = $request->get('type_id');
        }
        if ($request->exists('category_id')) {
            $oCategory = Category::find($request->get('category_id'));
            View::share('oCategory', $oCategory);

            $treeChildren = $oCategory->treeChildren();
            $treeChildren = $treeChildren->merge([$oCategory]);

            $oQuery = $oQuery->whereIn('category_id', $treeChildren->pluck('id'));
            $aSearch['category_id'] = $request->get('category_id');

            $aSearch['parent_id'] = $oCategory->id;
        }

        if ($request->exists('tag_id')) {
            $oTag = Tag::find($request->get('tag_id'));
            View::share('oTag', $oTag);

            $aArticleTags = ArticleTag::where('tag_id', $oTag->id)->get()->pluck('article_id');

            $oQuery = $oQuery->whereIn('id', $aArticleTags);
            $aSearch['tag_id'] = $request->get('tag_id');
        }

        return [
            'oQuery' => $oQuery,
            'aSearch' => $aSearch
        ];
    }

    public function categories()
    {
        return Category::with('images', 'activeChildren', 'activeChildren.images','parent')
            ->where('status', 1)
            ->orderBy('priority', 'desc')
            ->get()
            ->keyBy('id');
    }

    public function types()
    {
        return Type::with('activeChildren')
            ->where('status', 1)
            ->orderBy('priority', 'desc')
            ->get()
            ->keyBy('id');
    }



    public function items(Request $request)
    {
        //$oMainArticles = $this->getMainArticles();
        $oPopularArticles = $this->getArticles('popular');

        $oQuery = $this->getArticlesQuery($request);

        $aSearch = $oQuery['aSearch'];

        $oArticles = $oQuery['oQuery']->paginate($this->indexPaginationLimit)
            ->withPath($this->indexPaginationUrl);

        return view('app.content.items', [
            //'oMainArticles' => $oMainArticles,
            'oPopularArticles' => $oPopularArticles,
            'oArticles' => $oArticles,
            'aSearch' => $aSearch,
        ]);

    }

    public function item(Request $request, $id)
    {
        $oArticle = Article::with('statistic')->where('id', $id)->first();

        //$oArticle->statistic()->attach();

        MetaTag::set('title', $oArticle->title);
        MetaTag::set('description', !is_null($oArticle->text) ? $oArticle->text : $oArticle->title);
        if(ImagePath::checkMain('article', 'original', $oArticle)) {
            MetaTag::set('image', ImagePath::main('article', 'original', $oArticle));
        }

        $oRecommendationArticles = Article::where('type_id', $oArticle->type_id)
            ->where('id', '<>', $oArticle->id)
            ->take(3)
            ->orderBy('published_at')
            ->get();

        $view = 'app.content.article.index';

        if (View::exists('app.content.article.'.$oArticle->type->url)) {
            $view = 'app.content.article.'.$oArticle->type->url;
        }
        $oStatistic = $oArticle->statistic;
        if (is_null($oStatistic)) {
            ArticleStatistics::create([
                'article_id' => $oArticle->id
            ]);
        }
        $oStatistic->views = $oStatistic->views + 1;
        $oStatistic->save();

        return view($view, [
            'oArticle' => $oArticle,
            'oRecommendationArticles' => $oRecommendationArticles,
            'metaController' => true
        ]);
    }

    public function about(Request $request)
    {
        $oPopularArticles = $this->getArticles('popular');

        return view('app.content.about', [
            'oPopularArticles' => $oPopularArticles
        ]);
    }

    public function contact(Request $request)
    {
        $oPopularArticles = $this->getArticles('popular');

        return view('app.content.contact', [
            'oPopularArticles' => $oPopularArticles
        ]);
    }

    public function modal(Request $request, $name)
    {
        return view('app.components.modal.'.$name, [

        ])->render();
    }


    public function download(Request $request)
    {
        if ($request->exists('article_id')) {

            $oArticle = Article::find($request->get('article_id'));

            $oStatistic = $oArticle->statistic;

            $oStatistic->increment('downloads');

            return $this->success([
                'redirect' => $oArticle->value_site,
            ]);
        }
        return $this->success();
    }

    public function subscribe(Request $request)
    {
        return $this->success([], 'Вы успешно подписались на рассылку');
    }

}
