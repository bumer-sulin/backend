<?php

namespace App\Http\Controllers;

use App\Cmf\Api\Transformers\ArticleDetailTransformer;
use App\Cmf\Api\Transformers\ArticleTransformer;
use App\Cmf\Api\Transformers\CategorySectionTransformer;
use App\Cmf\Api\Transformers\CategoryTransformer;
use App\Cmf\Api\Transformers\CommentTransformer;
use App\Cmf\Api\Transformers\PromotionTransformer;
use App\Cmf\Api\Transformers\SettingTransformer;
use App\Cmf\Api\Transformers\SocialTransformer;
use App\Cmf\Api\Transformers\TagTransformer;
use App\Cmf\Api\Transformers\UserTransformer;
use App\Events\ArticleStatisticEvent;
use App\Events\ChangeCacheEvent;
use App\Http\Controllers\Auth\LoginController;
use App\Listeners\ArticleStatisticListener;
use App\Models\Article;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Image;
use App\Models\Promotion;
use App\Models\Setting;
use App\Models\Social;
use App\Models\Subscription;
use App\Models\Tag;
use App\Models\User;
use App\Services\Image\ImageService;
use Carbon\Carbon;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class ApiController extends Controller
{
    use Helpers;

    private const ARTICLES_LIMIT = 10;

    /**
     * @return \Dingo\Api\Http\Response
     */
    public function index()
    {
        return $this->response->noContent();
    }

    public function user(Request $request)
    {
        $controller = new LoginController();

        if ($request->has('hash') && $request->get('hash') !== 'null') {
            $result = $controller->loginByHash($request, $request->get('hash'));

            if (!$result['success']) {
                $this->response->error('Пользователь не авторизован.', 401);
            }

            $oUser = $controller->getUserByHash($request->get('hash'));

            $aItem = (new UserTransformer())->transform($oUser);

            return $this->response->array($aItem);
        }
        $this->response->error('Пользователь не авторизован.', 401);

        return $this->response->array([]);
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function categories(Request $request)
    {
        $oItems = Category::with('children', 'activeArticles')
            ->where('parent_id', null)
            ->whereIn('status', [1, 2])
            ->orderBy('priority', 'desc')
            ->get();

//        $oItems = $oItems->reject(function ($item) {
//            return count($item->activeArticles) === 0;
//        });

        return $this->response->collection($oItems, new CategoryTransformer());
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function sections(Request $request)
    {
        $oItems = Category::with('children', 'activeArticles')
            ->where('parent_id', null)
            ->where('status', 2)
            ->orderBy('priority', 'desc')
            ->get();

        return $this->response->collection($oItems, new CategorySectionTransformer());
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function settings(Request $request)
    {
        $oItems = Setting::where('status', 1)->get();
        return $this->response->collection($oItems, new SettingTransformer());
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function articles(Request $request)
    {
        $oItems = Article::with('category', 'images', 'statistic', 'activeTags')
            ->where('published_at', '<=', now()->startOfDay())
            ->whereNotNull('publication_at');

        $collectTake = null;
        $queryTake = null;
        $hasSortByDesc = null;
        $isPopular = false;

        if ($request->exists('type')) {

            if ($request->get('type') === 'main') {
                $oItems = $oItems->whereIn('status', [2]);
                $queryTake = 20;
            }

            if ($request->get('type') === 'new') {
                $oItems = $oItems->whereIn('status', [1]);

                $queryTake = 4;
            }

            if ($request->get('type') === 'popular') {
                $oItems = $oItems->whereIn('status', [1, 2]);
                $oItems = $oItems->where('published_at', '>=', now()->subWeeks(2)->startOfDay());

                $hasSortByDesc = 'statistic.views';
                $collectTake = 5;
                $isPopular = true;
            }
        } else {
            $oItems = $oItems->whereIn('status', [1, 2]);
        }

        if ($request->exists('type') && $request->get('type') === 'gallery') {
            $oItems = $oItems->where('type_id', 3);
        } else {
            $oItems = $oItems->whereIn('type_id', [1, 2]);

            $oItems = $oItems->whereHas('category', function ($q) {
                $q->where('categories.status', '<>', 0);
            });
        }
        if ($request->exists('date')) {
            $oItems = $oItems->where('published_at', '>=', Carbon::parse($request->get('date')));
        }

        if ($request->exists('category_id')) {
            $oItems = $oItems->where('category_id', $request->get('category_id'));
        }
        if ($request->exists('tags')) {
            $tags = $request->get('tags');

            $oItems = $oItems->whereHas('activeTags', function ($q) use ($tags) {
                $q->whereIn('tags.id', $tags);
            });
        }

        if ($request->exists('search')) {
            $oItems = $oItems->where('title', 'like', '%' . $request->get('search') . '%');
        }

        if ($request->exists('id')) {
            $oItems = $oItems->where('id', $request->get('id'));

            $oItem = $oItems->first();

            if (is_null($oItem)) {
                $this->response->error('Not found.', 404);
            }

            /**
             * @see ArticleStatisticListener::handle()
             */
            $oItem->ip = $request->ip();

            event(new ArticleStatisticEvent('views', $oItem));

            $aItem = (new ArticleDetailTransformer())->transform($oItem);

            return $this->response->array($aItem);
        } else {
            if (!is_null($queryTake)) {
                $oItems = $oItems->take($queryTake);
            }
            if (!$hasSortByDesc) {
                $oItems = $oItems->orderBy('published_at', 'desc');
            }

            if ($this->hasPaginationPage($request)) {
                $oItems = $this->pagination($oItems, $request->url());
            } else {
                $oItems = $oItems->get();
            }
        }

        if (!is_null($hasSortByDesc)) {
            $oItems = $oItems->sortByDesc('statistic.views');
        }
        if (!is_null($collectTake)) {
            $oItems = $oItems->take($collectTake);

            $oCollect = collect([]);
            foreach ($oItems as $oItem) {
                $oCollect->push($oItem);
            }
            $oItems = $oCollect;
        }

        if ($isPopular) {
            $aItems = $oItems->transform(function ($item) {
                return (new ArticleTransformer())->transformWithExcept($item, ['image']);
            })->all();
        } else {
            $aItems = $oItems->transform(function ($item) {
                return (new ArticleTransformer())->transform($item);
            })->all();
        }
        $response['data'] = $aItems;

        if ($this->hasPaginationPage($request)) {
            $response['pagination'] = [
                'total' => $oItems->total(),
                'page' => $oItems->currentPage(),
                'limit' => ApiController::ARTICLES_LIMIT,
            ];
        }

        return $this->response->array($response);
        //return $this->response->collection($oItems->getCollection(), new ArticleTransformer(), $paginationParameters);
    }

    /**
     * @param Request $request
     * @return bool
     */
    private function hasPaginationPage(Request $request): bool
    {
        if ($request->exists('type')) {
            return false;
        }
        if ($request->exists('type')) {
            return false;
        }
        if ($request->exists('sitemap')) {
            return false;
        }
        return true;
    }

    /**
     * @param $oItems
     * @param $url
     * @return LengthAwarePaginator
     */
    private function pagination($oItems, $url): LengthAwarePaginator
    {
        return $oItems->paginate(ApiController::ARTICLES_LIMIT)->withPath($url);
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function socials(Request $request)
    {
        $oItems = Social::where('status', 1)->orderBy('priority', 'desc')->get();
        return $this->response->collection($oItems, new SocialTransformer());
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function tags(Request $request)
    {
        $oItems = Tag::where('status', 1)->orderBy('title', 'desc')->get();
        return $this->response->collection($oItems, new TagTransformer());
    }

    /**
     * @param Request $request
     * @param $id
     * @param $slug
     * @return \Dingo\Api\Http\Response
     */
    public function article(Request $request, $id, $slug = null)
    {
        $preview = false;
        $hash = null;

        try {
            if (!isset($_SESSION["origURL"]) && array_key_exists('HTTP_REFERER', $_SERVER)) {
                $route = route('index.article.preview', ['id' => $id]).'/';
                if (Str::startsWith($_SERVER['HTTP_REFERER'], $route)) {
                    $hash = str_replace($route, '', $_SERVER['HTTP_REFERER']);
                    $preview = true;
                }
            }
        } catch (\Exception $e) {

        }

        if ($preview) {
            $oUser = User::findByHash($hash);

            if (is_null($oUser)) {
                $this->response->error('Not found.', 404);
            }

            if (!$oUser->isAdmin()) {
                $this->response->error('Not found.', 404);
            }

            $oItem = Article::find($id);

            $aItem = (new ArticleDetailTransformer())->transform($oItem);

            return $this->response->array($aItem);
        } else {
            $request->merge([
                'id' => $id,
            ]);
            return $this->articles($request);
        }
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function promotions(Request $request)
    {
        $oItems = Promotion::where('status', 1)->orderBy('id', 'asc')->get();
        return $this->response->collection($oItems, new PromotionTransformer());
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function subscribe(Request $request)
    {
        if ($request->exists('email')) {
            $validation = Validator::make($request->all(), [
                'email' => 'required|email|max:255|unique:subscriptions',
            ], [
                'email.unique' => 'Этот email уже подписан на рассылку.',
            ]);
            if ($validation->fails()) {
                $this->response->error($validation->getMessageBag()->toArray()['email'][0], 422);
            }
            $email = $request->get('email');
            $oUser = User::where('email', $email)->first();

            Subscription::create([
                'type' => 1,
                'user_id' => !is_null($oUser) ? $oUser->id : null,
                'email' => $email,
            ]);
            event(new ChangeCacheEvent('subscriptions'));
        }
        return $this->response->array([
            'success' => true,
            'message' => 'Спасибо за подписку',
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function comment(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'text' => 'required',
        ]);
        if ($validation->fails()) {
            $this->response->error($validation->getMessageBag()->toArray()['text'][0], 422);
        }
        $oArticle = Article::find($id);

        $parent_id = $request->has('parent_id') ? $request->get('parent_id') : null;
        $reply_id = $request->has('reply_id') ? $request->get('reply_id') : null;

        if (!is_null($reply_id) && is_null($parent_id)) {
            $oReplyComment = Comment::find($reply_id);
            if (!is_null($oReplyComment)) {
                $parent_id = !is_null($oReplyComment->parent_id) ? $oReplyComment->parent_id : $oReplyComment->id;
            }
        }

        $aComment = [
            'comment' => $request->get('text'),
            'rate' => 0,
            'approved' => false,
            'commentable_id' => $oArticle->id,
            'commentable_type' => Article::class,
            'parent_id' => $parent_id,
            'reply_id' => $reply_id,
        ];
        if ($request->has('hash')) {
            $oUser = User::findByHash($request->get('hash'));
            if (!is_null($oUser)) {
                $aComment['commented_id'] = $oUser->id;
                $aComment['commented_type'] = User::class;
            }
        } else {
            $this->response->error('Необходимо авторизоваться.', 422);
        }
        Comment::create($aComment);

        $oArticle->statistic()->update([
            'comments' => $oArticle->approvedComments()->count(),
        ]);

        event(new ChangeCacheEvent('comments-not-approved'));

        return $this->response->array([
            'success' => true,
            'message' => 'Комментарий успешно отправлен и будет отображен после модерации.',
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @param $comment_id
     * @return mixed
     */
    public function commentLike(Request $request, $id, $comment_id)
    {
        $oArticle = Article::find($id);

        if (is_null($oArticle)) {
            $this->response->error('Статья не найдена.', 422);
        }

        $oComment = Comment::find($comment_id);

        if (is_null($oComment)) {
            $this->response->error('Комментарий не найден.', 422);
        }

        if (!$request->has('hash')) {
            $this->response->error('Необходимо авторизоваться.', 422);
        }

        $oUser = User::findByHash($request->get('hash'));

        if (is_null($oUser)) {
            $this->response->error('Пользователь не найден.', 422);
        }

        if ($oUser->hasLoveComment($oComment->id)) {
            $this->response->error('Вы уже оценивали этот комментарий.', 422);
        }

        $oUser->setLoveCommentLike($oComment->id);

        return $this->response->array([
            'success' => true,
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @param $comment_id
     * @return mixed
     */
    public function commentDislike(Request $request, $id, $comment_id)
    {
        $oArticle = Article::find($id);

        if (is_null($oArticle)) {
            $this->response->error('Статья не найдена.', 422);
        }

        $oComment = Comment::find($comment_id);

        if (is_null($oComment)) {
            $this->response->error('Комментарий не найден.', 422);
        }

        if (!$request->has('hash')) {
            $this->response->error('Необходимо авторизоваться.', 422);
        }

        $oUser = User::findByHash($request->get('hash'));

        if (is_null($oUser)) {
            $this->response->error('Пользователь не найден.', 422);
        }

        if ($oUser->hasLoveComment($oComment->id)) {
            $this->response->error('Вы уже оценивали этот комментарий.', 422);
        }

        $oUser->setLoveCommentDislike($oComment->id);

        return $this->response->array([
            'success' => true,
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Dingo\Api\Http\Response
     */
    public function comments(Request $request, $id)
    {
        $oArticle = Article::find($id);
        $oItems = $oArticle->comments()
            ->with('approvedChildren', 'loveUsers')
            ->where('parent_id', null)
            ->where('approved', 1)
            ->get();

        if ($request->has('hash')) {
            $oUser = User::findByHash($request->get('hash'));

            $oUser->load('loveComments');

            return $this->response->collection($oItems, new CommentTransformer([
                'oUser' => $oUser
            ]));
        }
        return $this->response->collection($oItems, new CommentTransformer());
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function userUpdate(Request $request, $id)
    {
        if (!$request->has('hash')) {
            $this->response->error('Необходимо авторизоваться.', 422);
        }

        $oUser = User::findByHash($request->get('hash'));

        if ($oUser->id !== (int)$id) {
            $this->response->error('Вы не можете редактировать этого пользователя.', 422);
        }

        $validation = Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email,'.$oUser->id.'|max:255',
            'name' => 'required|max:255',
            'last_name' => 'required|max:255',
            //'image' => 'max:15000|mimes:jpg,jpeg,gif,png',
        ]);
        if ($validation->fails()) {
            $errors = $validation->getMessageBag()->toArray();
            $error = array_shift($errors);
            $this->response->error($error[0], 422);
        }

        if ($request->has('image') && !Str::startsWith($request->get('image'), 'http')) {
            $image = $request->get('image');
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace('data:image/jpeg;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = str_random(10).'.'.'jpg';
            $folderName = '/images/user/tmp/';
            $destinationPath = public_path() . $folderName;
            \Illuminate\Support\Facades\File::put($destinationPath . $imageName, base64_decode($image));

            $oService = new ImageService();

            try {
                $file = $oService->upload(
                    $request->get('image'),
                    'user',
                    $oUser->id,
                    (new \App\Cmf\Project\User\UserBaseController())->getFilters(),
                    $destinationPath . $imageName,
                    $imageName
                );
                $oImages = Image::where('type', 'user')
                    ->where('imageable_id', $oUser->id)
                    ->where('is_main', 1)
                    ->get();
                foreach ($oImages as $oImage) {
                    $oImage->update([
                        'is_main' => 0,
                    ]);
                }
                Image::create([
                    'type' => 'user',
                    'imageable_id' => $oUser->id,
                    'imageable_type' => User::class,
                    'filename' => $file,
                    'is_main' => 1,
                ]);
            } catch (\Exception $e) {
                $this->response->error('Ошибка, этот файл нельзя загрузить.', 422);
            }
        }

        $oUser->update([
            'email' => $request->get('email'),
            'first_name' => $request->get('name'),
            'last_name' => $request->get('last_name'),
        ]);

        return $this->response->array([
            'success' => true,
            'message' => 'Данные успешно обновлены',
            'hash' => $oUser->hash,
        ]);
    }
}
