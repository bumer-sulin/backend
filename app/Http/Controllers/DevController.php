<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Services\Seo\SeoYandexNewsService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
//use narutimateum\Toastr\Facades\Toastr;
use App\Services\Toastr\Toastr;
use Illuminate\Support\Facades\File;
use Symfony\Component\HttpKernel\Exception\HttpException;

class DevController extends Controller
{
    /**
     * @param Request $request
     * @param $name
     */
    public function php(Request $request, $name)
    {
        $method = 'php'.title_case($name);
        return method_exists($this, $method) ? $this->{$method}() : abort(500, 'Method '.$method.' not found');
    }

    /**
     * @param Request $request
     * @param $name
     * @return \Illuminate\Http\RedirectResponse
     */
    public function command(Request $request, $name)
    {
        $command = $name;

        $this->setCommand($command, $this->commandExists($command));

        return redirect()->back();
    }


    /**
     * @return \Illuminate\Http\Response
     */
    private function phpInfo()
    {
        phpinfo();
        return response()->make('', 200);
    }


    /**
     * @param $name
     * @return bool
     */
    private function commandExists($name)
    {
        return array_has(Artisan::all(), $name);
    }

    /**
     * @param $name
     * @param $exists
     */
    private function setCommand($name, $exists) : void
    {
        if ($exists) {
            Artisan::call($name);
            (new Toastr('Команда php artisan '.$name.' успешно выполнена.'))->success(false);
        } else {
            (new Toastr('Команда php artisan '.$name.' не найдена.'))->error(false);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function email()
    {
        return view('email.article.to_client', [
            'oArticle' => Article::first(),
            'title' => 'title',
            'linkUnsubscribe' => 'linkUnsubscribe',
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pages()
    {
        return view('admin.components.dev.pages');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function yandexNews()
    {
        (new SeoYandexNewsService())->feedArticles();
        return view('admin.components.dev.yandex.news', [
            'data' => '',
        ]);
    }
}
