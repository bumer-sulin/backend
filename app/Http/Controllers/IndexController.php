<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use KekoApp\LaravelMetaTags\Facades\MetaTag;

class IndexController extends Controller
{
    /**
     * @var string
     */
    private $view = 'react';

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view($this->view);
    }

    /**
     * @param Request $request
     * @param $id
     * @param null $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function article(Request $request, $id, $slug = null)
    {
        $oArticle = Article::find($id);

        if (is_null($oArticle)) {
            return redirect()->route('index.welcome');
        }

        MetaTag::set('title', $oArticle->title);
        MetaTag::set('description', $oArticle->description);
        MetaTag::set('keywords', $oArticle->keywords);
        MetaTag::set('image', imagePath()->main('article', 'sm', $oArticle));

        return view($this->view, [
            'metaController' => true,
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @param $hash
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function articlePreview(Request $request, $id, $hash)
    {
        $oUser = User::findByHash($hash);

        if (is_null($oUser)) {
            return redirect()->to('/');
        }

        $oArticle = Article::find($id);

        MetaTag::set('title', $oArticle->title);
        MetaTag::set('description', $oArticle->description);
        MetaTag::set('image', imagePath()->main('article', 'sm', $oArticle));

        $request->session()->flash('preview', $hash);

        return view($this->view, [
            'metaController' => true,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function unsubscribe(Request $request, $hash)
    {
        try {
            $email = Crypt::decrypt($hash);

            $oSubscription = Subscription::where('email', $email)->first();

            if (!is_null($oSubscription)) {
                $oSubscription->update([
                    'status' => 2,
                ]);
            }
            return redirect()->to('/');
        } catch (DecryptException $e) {
            return redirect()->to('/');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        return view($this->view);
    }
}
