<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('app.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dashboard()
    {
        //dd();
        return view('admin.dashboard');
    }

    /**
     * @param $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function unknown($name)
    {
        return view('admin.unknown');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function email()
    {
        $oArticle = Article::first();

        //Mail::to('dposkachei@gmail.com')->send(new \App\Mail\SendArticleToClient($oArticle));

        $linkUnsubscribe = url('/unsubscribe/'.crypt('dposkachei@gmail.com', 'bumer'));

        return view('email.article.to_client', [
            'title' => config('app.name'),
            'oArticle' => $oArticle,
            'linkUnsubscribe' => $linkUnsubscribe,
        ]);
    }
}
