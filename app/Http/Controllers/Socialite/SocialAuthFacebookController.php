<?php

namespace App\Http\Controllers\Socialite;

use App\Http\Controllers\Controller;
use App\Services\Socialite\FacebookAccountService;
use App\Services\Socialite\GoogleAccountService;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthFacebookController extends Controller
{
    /**
     * Create a redirect method to google api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::with('facebook')->redirect();
    }

    /**
     * Return a callback method from google api.
     *
     * @param FacebookAccountService $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function callback(FacebookAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::with('facebook')->user());
        //auth()->login($user);
        return redirect()->to('/?hash='.$user->hash);
    }
}
