<?php

namespace App\Http\Controllers\Socialite;

use App\Http\Controllers\Controller;
use App\Services\Socialite\VkontakteAccountService;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthVkontakteController extends Controller
{
    /**
     * Create a redirect method to google api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::with('vkontakte')->redirect();
    }

    /**
     * Return a callback method from google api.
     *
     * @param Request $request
     * @param VkontakteAccountService $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function callback(Request $request, VkontakteAccountService $service)
    {
        $user = $service->createOrGetUser($request);
        if ($request->has('api')) {
            return responseCommon()->success([
                'hash' => $user->hash,
            ]);
        } else {
            return redirect()->to('/?hash='.$user->hash);
        }
    }
}
