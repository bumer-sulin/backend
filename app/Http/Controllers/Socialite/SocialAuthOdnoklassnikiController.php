<?php

namespace App\Http\Controllers\Socialite;

use App\Http\Controllers\Controller;
use App\Services\Socialite\FacebookAccountService;
use App\Services\Socialite\GoogleAccountService;
use App\Services\Socialite\OdnoklassnikiAccountService;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthOdnoklassnikiController extends Controller
{
    /**
     * Create a redirect method to google api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::with('odnoklassniki')->redirect();
    }

    /**
     * Return a callback method from google api.
     *
     * @param OdnoklassnikiAccountService $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function callback(OdnoklassnikiAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::with('odnoklassniki')->user());
        //auth()->login($user);
        return redirect()->to('/?hash='.$user->hash);
    }
}
