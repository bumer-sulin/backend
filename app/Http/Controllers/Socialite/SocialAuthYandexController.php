<?php

namespace App\Http\Controllers\Socialite;

use App\Http\Controllers\Controller;
use App\Services\Socialite\VkontakteAccountService;
use App\Services\Socialite\YandexAccountService;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthYandexController extends Controller
{
    /**
     * Create a redirect method to google api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::with('yandex')->redirect();
    }

    /**
     * Return a callback method from google api.
     *
     * @param YandexAccountService $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function callback(YandexAccountService $service)
    {
        $user = $service->createOrGetUser(Socialite::with('yandex')->user());
        //auth()->login($user);
        return redirect()->to('/?hash='.$user->hash);
    }
}
