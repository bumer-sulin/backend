<?php


namespace App\Http\Composers;


use App\Models\Setting;
use App\Services\Image\Facades\ImagePath;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

trait CommonComposersTrait
{
    /**
     * @var
     */
    protected $options;

    /**
     * @var
     */
    protected $site;

    /**
     *
     */
    protected function setCommon()
    {
        //dd(__CLASS__);
        $this->options = $this->remember('settings', function () {
            return Setting::with('images')->get();
        });
        $this->site = $this->remember('site', function () {
            return $this->getSiteInfo($this->options);
        });
    }

    /**
     * @param View $view
     */
    protected function setCommonCompose(View $view)
    {
        $view->with('oComposerSite', $this->site);
        $view->with('oComposerOptions', $this->options);
    }

    /**
     * @param $oOptions
     * @return object
     */
    private function getSiteInfo($oOptions)
    {
        //$oContacts = Contact::where('status', 2)->get();
        $aCurrentOptions = [];
        foreach ($oOptions as $oOption) {
            $key = strripos($oOption->key, '.') ? stristr($oOption->key, '.', true) : $oOption->key;
            $value = $oOption->value;
            if ($oOption->isImage) {
                $value['main'] = ImagePath::main('setting', 'original', $oOption);
                $value['others'] = [];
                foreach ($oOption->images->where('is_main', 0) as $image) {
                    $value['others'][] = ImagePath::image('setting', 'original', $image);
                }
                $value['main_object'] = $oOption->images->where('is_main', 1)->first();
            }
            $subKey = substr(stristr($oOption->key, '.'), 1);
            if (isset($aOptions[$key][$subKey])) {
                $aCurrentOptions[$key][$subKey][] = $value;
            } else {
                if (strpos($subKey, '.')) {
                    $array = explode('.', $subKey);
                    foreach ($array as $k => $v) {
                        if ($k !== 0) {
                            $aCurrentOptions[$key][$array[0]][$v] = $value;
                        }
                    }
                } else {
                    $aCurrentOptions[$key][$subKey] = $value;
                }
            }
            if ($oOption->status === 0) {
                $aCurrentOptions[$key][$subKey] = null;
            }
        }
        $defaultOptions = Config::get('site.options');
        foreach ($defaultOptions as $key => $aOptions) {
            if ($key === 'contacts') {
                foreach ($aOptions as $subKey => $aOption) {
                    if (!isset($aCurrentOptions[$key][$subKey])) {
                        $aCurrentOptions[$key][$subKey] = $aOption;
                    }

                    /*
                    $oContactOption = $oContacts->where('key', $subKey)->first();
                    if (!is_null($oContactOption)) {
                        $aCurrentOptions[$key][$subKey] = $oContactOption->value;
                    } else {
                        $aCurrentOptions[$key][$subKey] = null;
                    }
                    */
                }
            } else {
                foreach ($aOptions as $subKey => $aOption) {
                    if (is_array($aOption)) {
                        foreach ($aOption as $k => $v) {
                            if (!isset($aCurrentOptions[$key][$subKey][$k])) {
                                $aCurrentOptions[$key][$subKey][$k] = $v;
                            }
                        }
                    } else {
                        if (!isset($aCurrentOptions[$key][$subKey])) {
                            $aCurrentOptions[$key][$subKey] = $aOption;
                        }
                    }
                }
            }
        }
        $oOptions = [];
        foreach ($aCurrentOptions as $key => $aCurrentOption) {
            $oOptions[$key] = (object)$aCurrentOption;
        }
        return (object)$oOptions;
    }

    public function getFiles($dir, $slug)
    {
        $files = File::allFiles($dir);
        $paths = [];
        $slash = DIRECTORY_SEPARATOR;
        foreach ($files as $file) {
            $paths[] = stristr(stristr(File::dirname((string)$file), $slug), $slash);
        }
        $paths = array_unique($paths);
        $pages = [];
        foreach ($paths as $path) {
            if ($path !== '\components\index' && isset(explode($slash, $path)[1])) {
                $pages[] = explode($slash, $path)[1];
            }
        }
        $pages = array_unique($pages);
        sort($pages);
        return $pages;
    }

    public function getAliases($content)
    {
        $array = [];
        foreach ($content as $key => $value) {
            $tValue = $this->getTitleCase($value);
            $sClass = $tValue . Str::studly('_controller');
            $sClass = 'App\Cmf\Project\\' . $tValue . '\\' . $sClass;
            $sModel = 'App\Models\\' . $tValue;
            if (class_exists($sClass) && class_exists($sModel)) {
                $oController = new $sClass();
                if (isset($oController->menu)) {
                    $array[$value] = $oController->menu;
                }
                if (method_exists($oController, 'getFilters')) {
                    $array[$value]['filters'] = $oController->getFilters();
                }
            }
        }
        return $array;
    }


    public function getModels($content)
    {
        $array = [];
        foreach ($content as $key => $value) {
            $tValue = $this->getTitleCase($value);
            $sClass = 'App\Models\\' . $tValue;
            if (class_exists($sClass)) {
                $oController = new $sClass();
                $array[$value]['fillable'] = $oController->getFillable();
            }
        }
        return $array;
    }

    /**
     * Обычный title_case с возможностью принимать значение вида key_name и транформирую в KeyName
     *
     * @param $value
     * @return mixed
     */
    protected function getTitleCase($value)
    {
        return str_replace(' ', '', Str::title(str_replace('_', ' ', $value)));
    }

    protected function remember($name, $function, $time = 3600)
    {
        return Cache::remember($name, $time, $function);
    }
}
