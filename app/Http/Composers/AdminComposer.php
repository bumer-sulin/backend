<?php

namespace App\Http\Composers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Subscription;
use App\Models\Tag;
use App\Models\Type;
use App\Models\User;
use App\Registries\MemberRegistry;
use Cartalyst\Sentinel\Roles\EloquentRole;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;

class AdminComposer
{
    use CommonComposersTrait;

    private $member = null;
    private $users;
    private $pages;
    private $layout;
    private $content;
    private $theme;
    private $view = null;
    private $categories;
    private $types;
    private $articles;
    private $articlesActive;
    private $tags;
    private $socials;
    private $cmf;
    private $invoices;
    private $roles;
    private $version;
    private $scriptCache;
    private $modelColumns;
    private $subscriptions;
    private $systemUsages;
    private $tablesWithSingleNames;
    private $commentsNotApproved;

    public function __construct(Route $route)
    {
        $this->setCommon();

        $this->member = MemberRegistry::getInstance();
        $this->users = $this->remember('users', function () {
            return User::all();
        });
        $this->pages = $this->remember('pages', function () {
            return $this->getFiles(storage_path('../resources/views/admin/pages'), 'pages');
        });
        $this->content = $this->remember('content', function () {
            return $this->getAliases($this->getFiles(storage_path('../resources/views/admin/content'), 'content'));
        });
        $this->modelColumns = $this->remember('model_columns', function () {
            return $this->getModels($this->getFiles(storage_path('../resources/views/admin/content'), 'content'));
        });
        $this->layout = $this->remember('layout', function () {
            return Config::get('site.layout');
        });
        $this->theme = $this->remember('theme', function () {
            return Config::get('site.theme');
        });
        $this->cmf = $this->remember('cmf', function () {
            return Config::get('site.theme');
        });
        $this->categories = $this->remember('category', function () {
            return Category::with('images', 'activeChildren', 'activeChildren.images', 'parent')
                ->whereIn('status', [1, 2])
                ->orderBy('priority', 'desc')
                ->get()
                ->keyBy('id');
        });
        $this->tags = $this->remember('tags', function () {
            return Tag::where('status', 1)
                ->get()
                ->keyBy('id');
        });
        $this->types = $this->remember('type', function () {
            return Type::with('activeChildren')
                ->where('status', 1)
                ->orderBy('priority', 'desc')
                ->get()
                ->keyBy('id');
        });
        $this->articles = $this->remember('article', function () {
            return Article::all();
        });
        $this->commentsNotApproved = $this->remember('comments-not-approved', function () {
            return Comment::where('approved', 0)->get();
        });
        $this->articlesActive = $this->remember('article-active', function () {
            return Article::whereIn('status', [1, 2])->where('published_at', '<=', now()->startOfDay())->whereNotNull('publication_at')->get();
        });
        $this->subscriptions = $this->remember('subscriptions', function () {
            return Subscription::all();
        });
        $this->roles = $this->remember('roles', function () {
            return EloquentRole::all();
        });
        if (!is_null(Route::current())) {
            $this->view = stristr(Route::current()->getName(), '.', true);
        }
        $this->version = $this->remember('version', function () {
            return config('app.version');
        });
        $this->scriptCache = $this->remember('script_cache', function () {
            return config('site.cache.script');
        });
        $this->tablesWithSingleNames = $this->remember('tables_with_single_names', function () {
            return [
                'categories' => 'category_id',
                'types' => 'type_id',
                'tags' => 'tag_id',
            ];
        });
        $this->systemUsages = $this->remember('system_usages', function () {
            $cpu = systemUsage()->getCpu();
            $memory = systemUsage()->getMemory();
            $hdd = systemUsage()->getSizeDirectory();
            $cpu = is_string($cpu) || is_null($cpu) ? 0 : (int)$cpu;
            $memory = is_string($memory) || is_null($memory) ? 0 : (int)$memory;
            $hdd = is_string($hdd) || is_null($hdd) ? 1 : (int)$hdd;

            return collect([
                'cpu' => $cpu,
                'memory' => $memory,
                'hdd' => $hdd,
            ]);
        });
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $this->setCommonCompose($view);

        $view->with('oComposerMember', $this->member);
        $view->with('oComposerUsers', $this->users);
        $view->with('aComposerPages', $this->pages);
        $view->with('aComposerContent', $this->content);
        $view->with('sComposerLayout', $this->layout);
        $view->with('sComposerTheme', $this->theme);
        $view->with('sComposerRouteView', $this->view);
        $view->with('oComposerCategories', $this->categories);
        $view->with('oComposerTypes', $this->types);
        $view->with('oComposerArticles', $this->articles);
        $view->with('oComposerArticlesActive', $this->articlesActive);
        $view->with('oComposerTags', $this->tags);
        $view->with('oComposerSocials', $this->socials);
        $view->with('oComposerSubscriptions', $this->subscriptions);
        $view->with('oComposerInvoices', $this->invoices);
        $view->with('oComposerRoles', $this->roles);
        $view->with('oComposerModelColumns', $this->modelColumns);
        $view->with('sComposerVersion', $this->version);
        $view->with('sComposerScriptCache', $this->scriptCache);
        $view->with('aComposerTablesWithSingleNames', $this->tablesWithSingleNames);
        $view->with('oComposerSystemUsage', $this->systemUsages);
        $view->with('oComposerCommentsNotApproved', $this->commentsNotApproved);
    }
}
