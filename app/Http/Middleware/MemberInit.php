<?php

namespace App\Http\Middleware;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Closure;
use Illuminate\Support\Facades\Cache;
//use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
//use Illuminate\Contracts\Auth\Factory as Auth;
use Illuminate\Support\Facades\Auth as Auth;

use App\Registries\Member;
use App\Registries\MemberRegistry;


class MemberInit
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Sentinel::guest()) {
            $oMemberData = Member::current()->get();
            MemberRegistry::getInstance()->setMember($oMemberData->toArray());
        }
        return $next($request);
    }
}
