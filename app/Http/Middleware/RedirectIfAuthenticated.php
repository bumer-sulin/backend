<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;

class RedirectIfAuthenticated
{
    private $exceptRoutes = [
        'activate',
        'activate.post',
        'logout.post',
    ];

    /**
     * The guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Sentinel $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $sentinel = $this->auth;
        try {
            return $sentinel::guest() ? $next($request) : redirect()->away('/admin/dashboard');
        } catch (NotActivatedException $e) {
            if (in_array(Route::current()->getName(), $this->exceptRoutes)) {
                return $next($request);
            } else {
                return redirect()->route('activate');
            }
        }
    }
}
