<?php

namespace App\Jobs;

use App\Http\Controllers\ApiController;
use App\Mail\SendArticleToClient;
use App\Models\Article;
use App\Models\Subscription;
use App\Services\Seo\SeoYandexNewsService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Spatie\Sitemap\Sitemap;
use Illuminate\Http\Request;

class UpdateSitemap implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;


    /**
     * UpdateSitemap constructor.
     */
    public function __construct()
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $sitemap = Sitemap::create();

        $aArticles = (new ApiController())->articles((new Request())->merge(['sitemap' => 1]));

        $aArticles = json_decode($aArticles->getContent(), true)['data'];

        $sitemap->add(\Spatie\Sitemap\Tags\Url::create('/')->setLastModificationDate(now()));

        foreach ($aArticles as $aArticle) {
            $sitemap->add(\Spatie\Sitemap\Tags\Url::create('/article/'.$aArticle['id'].'/'.$aArticle['slug'])->setLastModificationDate(now()));
        }

        Log::critical('sitemap created');

        $sitemap->writeToFile(public_path('sitemap.xml'));

        (new SeoYandexNewsService())->feedArticles();
        Log::critical('rss created');
    }

    /**
     * The job failed to process.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        Log::critical($exception->getMessage());
        // Send user notification of failure, etc...
    }
}
