<?php

namespace App\Jobs;

use App\Mail\SendArticleToClient;
use App\Models\Article;
use App\Models\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class SendSubscriptions implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of seconds the job can run before timing out.
     *
     * @var int
     */
    public $timeout = 120;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * @var Article
     */
    private $oItem;
    private $email;

    /**
     * SendLog constructor.
     * @param Article $oItem
     * @param $email
     */
    public function __construct(Article $oItem, $email)
    {
        $this->oItem = $oItem;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->sendEmail();
    }

    /**
     * @param $mail
     * @return bool
     */
    private function sendEmail()
    {
        try {
            Mail::to($this->email)->queue(new SendArticleToClient($this->oItem, $this->email));

            //Mail::send(new \App\Mail\SendArticleToClient($this->oItem, $this->email));
            Log::critical($this->email . ':' . $this->oItem->id);
            return true;
        } catch (\Exception $e) {
            Log::critical($e->getMessage());
            return false;
        }
    }

    /**
     * The job failed to process.
     *
     * @param  \Exception $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        Log::critical($exception->getMessage());
        // Send user notification of failure, etc...
    }
}
