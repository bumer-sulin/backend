<?php

use Illuminate\Http\Request;
use Dingo\Api\Routing\Router;
use Dingo\Api\Auth\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$api = app(Router::class);

$api->version('v1', function ($api) {
    $api->group([
        'middleware' => ['api', 'api.auth'],
    ], function ($api) {
        $api->any('/', ['as' => 'index', 'uses' => \App\Http\Controllers\ApiController::class . '@index']);
        $api->any('/user', ['as' => 'user', 'uses' => \App\Http\Controllers\ApiController::class . '@user']);
        // http://api.laravel.bumer.test/
        $api->any('/settings', ['as' => 'settings', 'uses' => \App\Http\Controllers\ApiController::class . '@settings']);
        $api->any('/categories', ['as' => 'categories', 'uses' => \App\Http\Controllers\ApiController::class . '@categories']);

        $api->any('/articles', ['as' => 'articles', 'uses' => \App\Http\Controllers\ApiController::class . '@articles']);

        $api->any('/socials', ['as' => 'socials', 'uses' => \App\Http\Controllers\ApiController::class . '@socials']);
        $api->any('/tags', ['as' => 'tags', 'uses' => \App\Http\Controllers\ApiController::class . '@tags']);
        $api->any('/sections', ['as' => 'sections', 'uses' => \App\Http\Controllers\ApiController::class . '@sections']);
        $api->any('/promotions', ['as' => 'promotions', 'uses' => \App\Http\Controllers\ApiController::class . '@promotions']);

        $api->post('/article/{id}/comment', ['as' => 'comment', 'uses' => \App\Http\Controllers\ApiController::class . '@comment']);
        $api->post('/article/{id}/comment/{comment_id}/like', ['as' => 'comment.like', 'uses' => \App\Http\Controllers\ApiController::class . '@commentLike']);
        $api->post('/article/{id}/comment/{comment_id}/dislike', ['as' => 'comment.dislike', 'uses' => \App\Http\Controllers\ApiController::class . '@commentDislike']);
        $api->any('/article/{id}/comments', ['as' => 'comments', 'uses' => \App\Http\Controllers\ApiController::class . '@comments']);
        $api->any('/article/{id}/{slug?}', ['as' => 'article', 'uses' => \App\Http\Controllers\ApiController::class . '@article']);
        $api->any('/article/{id}', ['as' => 'article', 'uses' => \App\Http\Controllers\ApiController::class . '@article']);
        $api->post('/subscribe', ['as' => 'subscribe', 'uses' => \App\Http\Controllers\ApiController::class . '@subscribe']);


        $api->post('/login', ['as' => 'login', 'uses' => \App\Http\Controllers\Auth\LoginController::class . '@login']);
        $api->post('/register', ['as' => 'register', 'uses' => \App\Http\Controllers\Auth\RegisterController::class . '@register']);

        $api->post('/password/email', ['as' => 'password.email', 'uses' => \App\Http\Controllers\Auth\ForgotPasswordController::class . '@sendResetLinkEmail']);

        $api->post('/password/reset', ['as' => 'password.reset', 'uses' => \App\Http\Controllers\Auth\ResetPasswordController::class . '@reset']);

        $api->post('/user/{id}/update', ['as' => 'user.update', 'uses' => \App\Http\Controllers\ApiController::class . '@userUpdate']);
    });
});
