<?php

Breadcrumbs::register('dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('dashboard'));
});
Breadcrumbs::register('admin', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('dashboard'));
});
Breadcrumbs::register('product', function ($breadcrumbs, $oItem) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Продукты', route('product.index'));
    if (isset($oItem)) {
        $breadcrumbs->push($oItem->title, route('product.index'));
    }
});
Breadcrumbs::register('article', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Статьи', route('article.index'));
});
Breadcrumbs::register('invoice', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Счета', route('invoice.index'));
});
Breadcrumbs::register('option', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Опции', route('option.index'));
});
Breadcrumbs::register('album', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Альбомы', route('album.index'));
});
Breadcrumbs::register('album.all', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Альбомы', route('album.index'));
    $breadcrumbs->push('Все', route('album.index'));
});
Breadcrumbs::register('user', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Пользователи', route('user.index'));
});
Breadcrumbs::register('category', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Категории', route('category.index'));
});
Breadcrumbs::register('type', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Типы', route('type.index'));
});
Breadcrumbs::register('social', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Соц.сети', route('social.index'));
});
Breadcrumbs::register('brand', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Бренды', route('brand.index'));
});
Breadcrumbs::register('tag', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Тэги', route('tag.index'));
});
Breadcrumbs::register('setting', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Настройки', route('setting.index'));
});
Breadcrumbs::register('info', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Информация', route('info.index'));
});
Breadcrumbs::register('promotion', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Реклама', route('promotion.index'));
});
Breadcrumbs::register('subscription', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Подписки', route('subscription.index'));
});
Breadcrumbs::register('comment', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Комментарии', route('comment.index'));
});

/**
 * --------------------------------------------------
 * ADMIN DEV
 * --------------------------------------------------
 */
Breadcrumbs::register('dev.pages', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', route('admin.dashboard.index'));
    $breadcrumbs->push('Системные страницы', route('dev.pages.index'));
});
Breadcrumbs::register('dev.yandex.news', function ($breadcrumbs) {
    $breadcrumbs->parent('dev.pages');
    $breadcrumbs->push('Яндекс.Новости', route('dev.yandex.news.index'));
});

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});


// Home > About
Breadcrumbs::register('about', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('About', route('about'));
});

// Home > Blog
Breadcrumbs::register('blog', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('Blog', route('blog'));
});


// Home > Blog > [Category] > [Post]
Breadcrumbs::register('post', function ($breadcrumbs, $post) {
    $breadcrumbs->parent('category', $post->category);
    $breadcrumbs->push($post->title, route('post', $post));
});
