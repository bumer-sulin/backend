<?php
/*
|--------------------------------------------------------------------------
| Cmf Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DevController;

Route::group([
    'domain' => env('ADMIN_URL'),
    'middleware' => ['auth', 'member'],
], function () {

    /**
     * @see DevController
     */
    Route::get('/dev/command/{name}', ['uses' => '\\' . DevController::class . '@command', 'as' => 'dev.command.index']);
    Route::get('/dev/php/{name}', ['uses' => '\\' . DevController::class . '@php', 'as' => 'dev.php.index']);
    Route::get('/dev/email', ['uses' => '\\' . DevController::class . '@email', 'as' => 'dev.email.index']);

    Route::get('/dev/pages', ['uses' => '\\' . DevController::class . '@pages', 'as' => 'dev.pages.index']);
    Route::get('/dev/yandex/news', ['uses' => '\\' . DevController::class . '@yandexNews', 'as' => 'dev.yandex.news.index']);
});
