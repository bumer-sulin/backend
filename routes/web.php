<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\IndexController;
use App\Http\Controllers\PackageController;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Socialite\SocialAuthGoogleController;
use App\Http\Controllers\Socialite\SocialAuthVkontakteController;
use App\Http\Controllers\Socialite\SocialAuthYandexController;
use App\Http\Controllers\Socialite\SocialAuthFacebookController;
use App\Http\Controllers\Socialite\SocialAuthOdnoklassnikiController;

Route::group([
    'domain' => env('APP_URL'),
], function () {

    /**
     * @see IndexController::index()
     */
    Route::get('/', ['uses' => '\\' . IndexController::class . '@index', 'as' => 'index.welcome']);

    /**
     * @see IndexController::search()
     */
    Route::get('/search', ['uses' => '\\' . IndexController::class . '@search', 'as' => 'index.search']);

    /**
     * @see IndexController::article()
     */
    Route::get('/article/{id}/{slug?}', ['uses' => '\\' . IndexController::class . '@article', 'as' => 'index.article']);

    /**
     * @see IndexController::article()
     */
    Route::get('/article/{id}/preview/{hash?}', ['uses' => '\\' . IndexController::class . '@articlePreview', 'as' => 'index.article.preview']);

    /**
     * @see IndexController::unsubscribe()
     */
    Route::get('/unsubscribe/{hash}', ['uses' => '\\' . IndexController::class . '@unsubscribe', 'as' => 'index.unsubscribe']);

    /**
     * @see PackageController::sitemap()
     */
    Route::get('/sitemap', ['uses' => '\\' . PackageController::class . '@sitemap', 'as' => 'package.sitemap']);


    /**
     * @see SocialAuthGoogleController
     */
    Route::get('/auth/google/redirect', '\\' . SocialAuthGoogleController::class . '@redirect');
    Route::get('/auth/google/callback', '\\' . SocialAuthGoogleController::class . '@callback');

    Route::get('/auth/vkontakte/redirect', '\\' . SocialAuthVkontakteController::class . '@redirect');
    Route::get('/auth/vkontakte/callback', '\\' . SocialAuthVkontakteController::class . '@callback');

    Route::get('/auth/yandex/redirect', '\\' . SocialAuthYandexController::class . '@redirect');
    Route::get('/auth/yandex/callback', '\\' . SocialAuthYandexController::class . '@callback');

    Route::get('/auth/facebook/redirect', '\\' . SocialAuthFacebookController::class . '@redirect');
    Route::get('/auth/facebook/callback', '\\' . SocialAuthFacebookController::class . '@callback');


    Route::get('/auth/odnoklassniki/redirect', '\\' . SocialAuthOdnoklassnikiController::class . '@redirect');
    Route::get('/auth/odnoklassniki/callback', '\\' . SocialAuthOdnoklassnikiController::class . '@callback');

});
