<?php
/*
|--------------------------------------------------------------------------
| Cmf Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\PackageController;
use App\Cmf\Core\RouteCmf;

Route::group([
    'domain' => env('ADMIN_URL'),
    'prefix' => env('ADMIN_PREFIX', ''),
    'middleware' => ['auth', 'member'],
], function () {

    /**
     * @see HomeController
     */
    Route::get('/', ['uses' => '\\' . HomeController::class . '@dashboard', 'as' => 'admin.dashboard.index']);
    Route::get('/dashboard', ['uses' => '\\' . HomeController::class . '@dashboard', 'as' => 'dashboard']);
    Route::get('/email', ['uses' => '\\' . HomeController::class . '@email', 'as' => 'email']);

    /**
     * @see RouteCmf::resource()
     */
    RouteCmf::resource('category');
    RouteCmf::resource('social');
    RouteCmf::resource('type');
    RouteCmf::resource('product');
    RouteCmf::resource('article');
    RouteCmf::resource('order');
    RouteCmf::resource('file');
    RouteCmf::resource('user');
    RouteCmf::resource('brand');
    RouteCmf::resource('tag');
    RouteCmf::resource('setting');
    RouteCmf::resource('option');
    RouteCmf::resource('invoice');
    RouteCmf::resource('info');
    RouteCmf::resource('promotion');
    RouteCmf::resource('subscription');
    RouteCmf::resource('comment');

    /**
     * @see HomeController
     */
    Route::get('/home', ['uses' => '\\' . HomeController::class . '@index', 'as' => 'home']);
    Route::post('/home', ['uses' => '\\' . HomeController::class . '@index', 'as' => 'home.post']);

    Route::post('/logout', ['uses' => 'Auth\LoginController@logout', 'as' => 'logout.post']);

    Route::get('decompose', ['uses' => '\\' . PackageController::class . '@decompose', 'as' => 'package.decompose']);

    Route::post('/confirm', ['uses' => '\\' . IndexController::class . '@confirm', 'as' => 'confirm']);

    Route::get('/{name}', ['uses' => '\\' . HomeController::class . '@unknown', 'as' => 'unknown']);
});
