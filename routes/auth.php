<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
    'domain' => env('ADMIN_URL'),
    'prefix' => 'auth',
    'middleware' => ['guest'],
], function () {
    //Route::get('/',                         ['uses' => 'IndexController@welcome',   'as' => 'admin.index.welcome']);

    Route::get('/login', ['uses' => 'Auth\LoginController@showLoginForm', 'as' => 'login']);
    Route::post('/login', ['uses' => 'Auth\LoginController@login', 'as' => 'login.post']);

    //Route::get('/register', ['uses' => 'Auth\RegisterController@showRegistrationForm', 'as' => 'register']);
    Route::post('/register', ['uses' => 'Auth\RegisterController@register', 'as' => 'register.post']);

    Route::get('/password/reset', ['uses' => 'Auth\ForgotPasswordController@showLinkRequestForm', 'as' => 'password.request']);
    Route::post('/password/email', ['uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail', 'as' => 'password.email.post']);
    Route::get('/password/reset/{token}', ['uses' => 'Auth\ResetPasswordController@showResetForm', 'as' => 'password.reset']);
    Route::post('/password/reset', ['uses' => 'Auth\ResetPasswordController@reset', 'as' => 'password.reset.post']);

    Route::get('/activate', ['uses' => 'Auth\LoginController@showActivateForm', 'as' => 'activate']);
    Route::post('/activate', ['uses' => 'Auth\LoginController@activate', 'as' => 'activate.post']);

});
