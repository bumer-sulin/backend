const mix = require('laravel-mix');

const webpack = require('webpack');

const ExtractTextPlugin = require("extract-text-webpack-plugin");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 | Env settings 'admin', 'app', 'all'
 |
 */

mix
    .sass('resources/assets/sass/admin/bootstrap/app/app.scss', 'public/css/admin.css')
    .js('resources/assets/js/admin/app.js', 'public/js/admin/app.js');

mix.autoload({
    jquery: ['$', 'window.jQuery', 'jQuery', 'jquery'],
    tether: ['window.Tether', 'Tether'],
    'tether-shepherd': ['Shepherd', 'tether-shepherd'],
    'Popper.js': ['popper.js', 'default'],
    Util: "exports-loader?Util!bootstrap/js/dist/util",
    Dropdown: "exports-loader?Dropdown!bootstrap/js/dist/dropdown",
    toastr: ['window.Toastr', 'toastr'],
    moment: ['window.Moment', 'moment'],
}).webpackConfig({
    resolve: {
        alias: {
            'load-image': 'blueimp-load-image/js/load-image.js',
            'load-image-meta': 'blueimp-load-image/js/load-image-meta.js',
            'load-image-exif': 'blueimp-load-image/js/load-image-exif.js',
            'canvas-to-blob': 'blueimp-canvas-to-blob/js/canvas-to-blob.js',
            'jquery-ui/ui/widget': 'blueimp-file-upload/js/vendor/jquery.ui.widget.js'
        }
    }
}).extract([
    'jquery',
    'toastr',
    'bootstrap',
    'moment',
    'popper.js',
    'tether',
    'tether-shepherd',
    'spin.js',
    'blueimp-file-upload'
]);

if (!mix.inProduction()) {
    mix.options({
        postCss: [
            require('autoprefixer')({
                browsers: [
                    'last 2 versions',
                    'iOS >= 8',
                    'Safari >= 8',
                ],
                cascade: false,
                flexbox: "no-2009"
            }),
            require('postcss-font-magician'),
            require('css-mqpacker'),
            require('postcss-remove-root'),
        ],
        processCssUrls: true,
        //extractVueStyles: 'css/vue.css',
    });
    mix.sourceMaps();
    mix.browserSync(process.env.APP_URL);
    mix.disableNotifications();
}

if (mix.inProduction()) {
    mix.options({
        postCss: [
            require('autoprefixer')({
                browsers: [
                    'last 2 versions',
                    'iOS >= 8',
                    'Safari >= 8',
                ],
                cascade: false,
                flexbox: "no-2009"
            }),
            require('cssnano'),
            require('postcss-font-magician'),
            require('css-mqpacker'),
            require('postcss-remove-root'),
        ],
        processCssUrls: true,
        clearConsole: true,
    });
    mix.version();
}
